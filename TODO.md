#Prioridad (por orden de prioridad)
gb_invoice.py
- [ ] Que si es iva diferente a 21% te avise cuando se exporta a pdf
- [ ] Cuando abres la factura, no borra documento antiguo (en tresoreria-fgb)
gb_factura.py
- [x] Me sería práctico que me genere una factura nueva de cero
	- [x] Al generar la factura, revise las fechas (issueDate y dueDate), el número de factura y compare con el nombre del archivo
	- [x] Revise que la numeración de  esa factura no esté duplicada
gb_registrafactura.py
- [x] Que revise si hay numeración duplicada en los archivos, y en caso que se dupliquen dos facturas, avise
- [ ] Revisar que las facturas en cobraments.yaml estén ordenadas 
- [x] Revisar que la numeración de la factura coincida la numeración del nombre
gb_enviafactura.py
- [ ] Poder añadir a un cliente todas las facturas del año (para modelo 347), y ya si enviara los importes por trimestre aún mejor.
- [ ] Poder añadir todas las facturas de deuda en un solo correo a un cliente o para todos los deudores
- [ ] Poder enviar facturas indicando año y numeracion (ej gb_enviafactura.py --id 1 -i 2023 1-23,34-54)
- [ ] Indicar en cobraments.yaml si las facturas han sido enviadas al cliente
gb_facturamanteniment.py
- [ ] Que no sea necesario indicar los números 
- [ ] Que se pueda generar con un comando más sencillo, por ejemplo (gb_facturamanteniment.py -a) todos los mantenimientos del trimestre que corresponde
- [ ] Estaría bien que ya se registren las facturas y se envíen directamente con el comando anterior.


#Sin prioridad
gb_abonafactura.py
- [ ] El nombre que genera no es apropiado (parece que el nombre lo pilla del subject: del archivo yaml de la factura original)
- [ ] Genera número de 5 dígitos, con 4 ya sería suficiente, no creo que se hagan más de 99 abonos en un año. A mucho 3 dígitos
- [ ] Que no sea necesario indicarle el número que le toca al abono
- [ ] Poder calcular en un mantenimiento, periodo a descontar, si por ejemplo se da de baja a mitad de trimestre.
gb_factura.py
- [x] Sería bueno que me genere una base de datos de clientes a los que les facturamos y, o pille los clientes y en caso que sea nuevo cliente, me lo añada a la base de datos (o en caso contrario, a un listado de .yaml o .csv → se genera proyecto nuevo
gb_balances.py
- [x] Peta
gb_registrafactura.py
- (x) Revisa todas las facturas desde el inicio de los tiempos y cada vez tarda más


#Otros

gb_clients
- Es un script hecho por mi en shell script, no sé si sería bueno pasarlo a python. Pero al estar en .sh yo sé modificarlo y entiendo todo
	Qué hace?
	- Me revisa deudores por dirección, nombre, año o nif (el más efectivo es por nif)
	- Me suma la deuda total
	- Me envía las facturas de deuda de los clientes (mendiante el gb_enviafactura.py), revisando primero qué cliente debe dinero y qué facturas son las que debe
	- Falta revisar los errores, porque el script sigue adelante aunque "petara"


DUDA
- Como apuntar adelantos de presupuestos
- Presupuestos (generación y paso a factura)

Tareas Aitor

- Quitar teléfono fijo de company.yaml
- Revisar si mobil ya lleva acento
