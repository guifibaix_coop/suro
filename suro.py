#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Copyright 2014 Cooperativa GuifiBaix

This file is part of python-wavefile

Suro is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Suro is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

programName = "Gestor d'expedients"
programVersion = '1.0'

doTrace = False
mapEnable = False

"""
TODO's
- ~~Apply changes on md file~~
- ~~Add log entries~~
- ~~Leer y editar coords~~
- ~~Preguntar si grabar cambios al salir~~
- ~~Despues de emplazar, actualizar mapas y streetview~~
- ~~Despues de cambiar estado actualizar iconos en lista y mapa~~
- ~~No actualizar la lista entera si cambia el estado~~
- ~~Reorganizar los iconos de edicion~~
- ~~Avisar graficamente cuando se produzca un error~~
- ~~Cambiar lo de abrir presupuesto/proposta a generar presupuesto y proposta, y quitarlo del crear expediente~~
- ~~Activar y desactivar cuando toca los botones de generar presupuesto y propuesta~~
- ~~No generar presupuestos ni propuestas cuando se crea el expediente~~
- ~~Preguntar directorio de trabajo~~
- ~~Preguntar el nombre de log~~
- ~~Preguntar datos de contacto al crear expediente~~
- ~~Traducciones~~
- ~~Bug: Limpiar busqueda con el boton no actualiza el filtro~~
- ~~Guardar autor tambien cuando creas nuevo~~
- ~~Generar orientaciones a los puntos cercanos~~
- Si no esta geolocalizado marcar geolocalizacion en rojo y no situar en el mapa, para agilizar
- Renombrar expediente
- Gestionar git
- Rellenar presupuesto
- Rellenar propuesta con datos del presupuesto
- Preseleccionar el item que se añade
- Quitar el resaltado del marker en el map cuando cambiamos de expediente
- Bug: Cuando editas el estado a mano en el md el combobox no se actualiza
- Si no guardas los cambios en un cambio de estado hay que revertir los iconos y el mapa
- Binario para windows
- Enriquecer el mapa global
	- Cambiar expediente al clickar (o doble clickar)
	- Enseñar informacion al clickar
- ~~TODO agregado de todos los expedientes~~
- TODO de features para el programa
- Lanzar el qgit
- Dobleclick en fichero no deberia renombar (solo abrirlo)
- Renombrar fichero en un menu contextual
- Borrar expediente
- reloadStreetView: intentar coordenadas primero
"""

newmdcontent = """\
# {idexpediente}

- Estat: {status}
- Finca: {streetaddress}
- Municipi: {postalcode} {city}

## Pendent

## Promotors

- {promoter}
	- Telèfon: {phone}
	- Correu: {email}

## Notes

## Configuració

## Log

- {date} [{logauthor}] Expedient creat

"""
# Aqui los paquetes standard
import sys
import os
import re
import datetime
from qgmap.presets import customPin
from gestor.qtutils import *
from consolemsg import warn
# Non standard packages that might fail if not installed
try :
	from PySide6 import QtCore, QtGui, QtWidgets
	from markdown import markdown
	import qgmap
except ImportError as e :
	reportMissingPackage(e,
		debpackages = dict(
			markdown="python3-markdown",
			),
		pippackages = dict(
			qgmap='qgmap',
			PySide6="pyside6",
			),
		)

def sourceRelative(filename) :
	return os.path.join(os.path.abspath(os.path.dirname(__file__)), filename)

import gestor.resources
from gestor.markdownhighlighter import MarkdownHighlighter
from gestor.expedients import *

@trace
def geolocate(streetAddress) :
	"""
	>>> geolocate("Fructuós Gelabert 1, Sant Joan Despi, 08970, Barcelona, Spain")
	(41.373042, 2.0672909)
	>>> geolocate("Verdaguer 42, Sant Joan Despí, 08970, Barcelona, Spain")
	(41.3685004, 2.057641)
	>>> geolocate("Sant Climent 18, Sant Climent de Llobregat, Barcelona, Spain")
	(41.3382734, 1.9962265)
	>>> geolocate("katakakakatkaka")

	"""
	geocoder = qgmap.GeoCoder(None)
	try : return geocoder.geocode(streetAddress)
	except qgmap.GeoCoder.NotFoundError : return None

@trace
def statusIcon(statusName) :
	iconName = statusIcons[statusName]
	return QtGui.QIcon.fromTheme(iconName, QtGui.QIcon(":/resources/"+iconName+".png"))

class ExpedientCreationDialog(QtWidgets.QDialog) :

	@trace
	def __init__(self, parent = None) :
		super(ExpedientCreationDialog, self).__init__(parent)
		l = QtWidgets.QVBoxLayout()
		self.setLayout(l)

		form = QtWidgets.QFormLayout()
		l.addLayout(form)

		form.addRow(QtWidgets.QLabel("Creant un nou expedient"))

		self.municipiEditor = QtWidgets.QComboBox()
		self.municipiEditor.addItems( [name for short, name in parent.municipis ] )
		form.addRow("&Municipi", self.municipiEditor)

		self.adressEditor = QtWidgets.QLineEdit()
		form.addRow("&Carrer", self.adressEditor)

		self.numEditor = QtWidgets.QLineEdit()
		form.addRow("&Número", self.numEditor)

		self.expedient = QtWidgets.QLineEdit()
		self.expedient.setStyleSheet("font-weight: bold;")
		form.addRow("Id resultant:", self.expedient)

		self.promoterEditor = QtWidgets.QLineEdit()
		form.addRow("&Promotor", self.promoterEditor)

		self.phoneEditor = QtWidgets.QLineEdit()
		form.addRow("&Telephone", self.phoneEditor)

		self.mailEditor = QtWidgets.QLineEdit()
		form.addRow("&E-mail", self.mailEditor)

		authors = expedientAuthors()
		settings = QtCore.QSettings()
		previousAuthor = settings.value('LogAuthor', authors[0])
		self.authorCombo = QtWidgets.QComboBox()
		self.authorCombo.addItems(authors)
		self.authorCombo.setCurrentIndex(authors.index(previousAuthor))
		form.addRow("Autor:", self.authorCombo)

		def recalculaId() :
			municipiCurt, municipi = parent.municipis[self.municipiEditor.currentIndex()]
			address = self.adressEditor.text()
			numero = self.numEditor.text()
			self.expedient.setText(municipiCurt+"-"+expedientify(address)+numero)

		recalculaId()

		self.adressEditor.textEdited.connect(recalculaId)
		self.numEditor.textEdited.connect(recalculaId)
		self.municipiEditor.currentIndexChanged.connect(recalculaId)

		buttons = QtWidgets.QDialogButtonBox(
			QtWidgets.QDialogButtonBox.Ok |
			QtWidgets.QDialogButtonBox.Cancel |
			QtWidgets.QDialogButtonBox.Help,
			QtCore.Qt.Horizontal,
			self)
		l.addWidget(buttons)

		buttons.accepted.connect(self.accept)
		buttons.rejected.connect(self.reject)

	@trace
	def validationError(self) :

		d = self.data()

		if not d.address :
			return "Cal una adreça"

		if not d.numero :
			return "Cal un número de finca"

		if not d.idexpediente :
			return "Cal un identificador d'expedient"

		return None

	@trace
	def data(self) :
		municipiCurt, municipi = self.parent().municipis[self.municipiEditor.currentIndex()]
		return AttributeDict(
			address = self.adressEditor.text(),
			numero = self.numEditor.text(),
			idexpediente = self.expedient.text(),
			postalcode = self.parent().postalCodes[municipiCurt],
			streetaddress = self.adressEditor.text() + ", " + self.numEditor.text(),
			promoter = self.promoterEditor.text(),
			phone = self.phoneEditor.text(),
			city = municipi,
			email = self.mailEditor.text(),
			date = datetime.date.today().isoformat(),
			logauthor = self.authorCombo.currentText(),
			status = 'Pedido',
			)

	@trace
	def runUntilValidatedOrCancelled(self) :
		while True :
			def tryAgain(message) :
				QtWidgets.QMessageBox.critical(self, programName, message)

			if not self.exec() : return False

			error = self.validationError()
			if error :
				tryAgain(error)
				continue

			data = self.data()

			if os.access(data.idexpediente, os.F_OK) :
				tryAgain("Expedient ja existeix")
				continue

			return True

class LocationEditor(QtWidgets.QDialog) :
	def __init__(self) :
		super(LocationEditor, self).__init__() 
		w = QtWidgets.QDialog()
		h = QtWidgets.QVBoxLayout(self)
		l = QtWidgets.QFormLayout()
		h.addLayout(l)

		def addField(labelText, buttonText, callback) :
			lh = QtWidgets.QHBoxLayout()
			edit = QtWidgets.QLineEdit()
			button = QtWidgets.QPushButton(buttonText)
			lh.addWidget(edit)
			lh.addWidget(button)
			lh.setStretchFactor(edit, 4)
			lh.setStretchFactor(button, 1)
			l.addRow(labelText, lh)
			button.clicked.connect(callback)
			return edit

		self.addressEdit = addField('Adreça:', 'Localitza', self.goAddress)
		self.coordsEdit = addField('Coords:', 'Ves', self.goCoords)

		self.gmap = qgmap.QGoogleMap(self)
		self.gmap.markerMoved.connect(self.onMarkerMoved)
		h.addWidget(self.gmap)
		self.gmap.setSizePolicy(
			QtGui.QSizePolicy.MinimumExpanding,
			QtGui.QSizePolicy.MinimumExpanding)

		buttons = QtWidgets.QDialogButtonBox(
			QtWidgets.QDialogButtonBox.Ok |
			QtWidgets.QDialogButtonBox.Cancel |
			QtWidgets.QDialogButtonBox.Help,
			QtCore.Qt.Horizontal,
			self)
		h.addWidget(buttons)
		buttons.rejected.connect(self.reject)
		buttons.accepted.connect(self.accept)

		self.gmap.waitUntilReady()

	def goCoords(self) :
		def resetError() :
			self.coordsEdit.setStyleSheet('')
		try : latitude, longitude = self.coordsEdit.text().split(",")
		except ValueError :
			self.coordsEdit.setStyleSheet("color: red;")
			QtCore.QTimer.singleShot(500, resetError)
		else :
			self.gmap.centerAt(latitude, longitude)
			self.gmap.moveMarker("DraggableMarker", latitude, longitude)

	def goAddress(self) :
		def resetError() :
			self.addressEdit.setStyleSheet('')
		coords = self.gmap.centerAtAddress(self.addressEdit.text())
		if coords is None :
			self.addressEdit.setStyleSheet("color: red;")
			QtCore.QTimer.singleShot(500, resetError)
			return
		self.gmap.moveMarker("DraggableMarker", *coords)
		self.coordsEdit.setText("{}, {}".format(*coords))

	def onMarkerMoved(self, key, latitude, longitude) :
		self.coordsEdit.setText("{}, {}".format(latitude, longitude))

	def setLocation(self, expedientId, streetAddress, coords) :

		if not coords : coords = geolocate(streetAddress)

		self.addressEdit.setText(streetAddress)
		self.coordsEdit.setText("{}, {}".format(*coords))

		self.gmap.addMarker("DraggableMarker", *coords,
			icon=customPin("#FF7777"),
			draggable=1,
			title = expedientId,
		)

		self.gmap.setZoom(17)
		self.gmap.centerAt(*coords)


class MainWindow(QtWidgets.QMainWindow) :
	def __init__(self) :
		super(MainWindow, self).__init__()

		self.editingExpedient = None
		loader = QUiLoader(self)
		uifile = sourceRelative("gestor/mainwindow.ui")
		loader.load(uifile)

		self.mdEditorPanel.hide()
		self.highlighter = MarkdownHighlighter(self.mdEditor.document())
		self.mdEditor.textChanged.connect(self.updatePreview)
		self.actionSave.triggered.connect(self.saveMd)
		self.expedientTree.currentItemChanged.connect(self.changeCurrentExpedient)
		self.actionCreateExpedient.triggered.connect(self.createExpedient)
		self.actionGenerateBudget.triggered.connect(self.generateBudget)
		self.actionGenerateProposal.triggered.connect(self.generateProposal)
		self.actionNewLogEntry.triggered.connect(self.addLog)
		self.locationEditorButton.clicked.connect(self.editLocation)
		self.actionOrientations.triggered.connect(self.showOrientations)

		self.setupFilesWidget()
		self.setupMapTab()
		self.setupTodos()

		self.showMaximized()

		self.setWorkingDir()
		self.loadMunicipis()
		self.setupExpedientFilters()
		self.setupStatusEditor()

		self.updateExpedientList()

	@trace
	def statusMessage(self, message) :
		self.statusBar().showMessage(message)

	@trace
	def setWorkingDir(self) :
		self.settings = QtCore.QSettings()

		self.workingDir = self.settings.value("WorkingDir", None)
		if self.workingDir is None :
			self.workingDir = os.getcwd()

		while True :
			os.chdir(self.workingDir)
			if os.access('master', os.F_OK) :
				if os.access('gestor.municipis', os.F_OK):
					break
			newDir = QtWidgets.QFileDialog.getExistingDirectory(
				self,
				"Selecciona el directory que conté els expedients",
				self.workingDir)
			if not newDir : sys.exit(-1)
			self.workingDir = newDir

		self.settings.setValue("WorkingDir", self.workingDir)

	@trace
	def createExpedient(self) :
		w = ExpedientCreationDialog(self)
		if not w.runUntilValidatedOrCancelled() : return
		data = w.data()
		os.mkdir(data.idexpediente)
		with open(mdFile(data.idexpediente), "w", encoding='utf-8') as f :
			f.write(newmdcontent.format(**data))
		settings = QtCore.QSettings()
		settings.setValue('LogAuthor', data.logauthor)
		QtWidgets.QMessageBox.information(self, programName,
			"Expedient '{}' creat".format(data.idexpediente))
		self.updateExpedientList()


	@trace
	def setupStatusEditor(self) :
		self.statusEditor.clear()
		self.statusEditor.addItems([
			status.capitalize()
			for status
			in statusNames
		])
		for i, name in enumerate(statusNames) :
			self.statusEditor.setItemIcon(i, statusIcon(name))

		self.statusEditor.activated.connect(self.changeStatus)

	@trace
	def updateStatusEditor(self, expedient) :
		status = expedientStatus(expedient)
		if status in statusNames :
			self.statusEditor.setCurrentIndex(statusNames.index(status))
		else :
			self.statusEditor.setEditText(status)

	@trace
	def changeStatus(self) :
		if self.editingExpedient is None : return # No expedient being edited

		newStatus = self.statusEditor.currentText()

		if newStatus.lower() not in statusNames :
			if not ask("L'estat '{}' no es un estat típic, segur que el vols canviar?".format(newStatus)) :
				return

		newMdContent = changeStatusOnMd(self.mdEditor.toPlainText(), newStatus)
		self.setMdContent(newMdContent)

	@trace
	def setupFilesWidget(self) :
		def openFileFromList(modelIndex) :
			startFile(self.filesModel.filePath(modelIndex))

		self.filesModel = model = QtWidgets.QFileSystemModel()
		model.setReadOnly(False) # Disable renaming
		model.setNameFilterDisables(False) # Filter hides instead of disable items

		self.filesWidget = widget = QtWidgets.QTreeView()
		# This should resize the columns as the directory is loaded, but not always works
		widget.doubleClicked.connect(openFileFromList)
		widget.setIconSize(QtCore.QSize(32,32))
		widget.setModel(model)
		model.directoryLoaded.connect(lambda : widget.resizeColumnToContents(0))
		widget.setRootIndex(model.setRootPath(QtCore.QDir.currentPath()))
		model.setFilter(QtCore.QDir.AllEntries | QtCore.QDir.NoDotAndDotDot)
		model.setNameFilters(["un_nombre_raro_para_que_no_salga_ningun_de_entrada"])
		self.rightPanel.addWidget(widget)


	@trace
	def setupMapTab(self) :
		self.mapWidget = widget = qgmap.QGoogleMap(self)
		self.tabs.addTab(widget, "Map")
		self.statusMessage("Loading Google Maps")
		self.mapWidget.waitUntilReady()
		self.statusMessage("")
		self.mapWidget.setZoom(14)
		self.mapWidget.centerAt(41.4,1.95)

	def setupTodos(self) :
		self.todosView = QtWidgets.QTreeWidget()
		self.tabs.addTab(self.todosView, self.tr('Tasques Pendents'))
		self.todosView.setHeaderLabels([self.tr('Expedient/Tasca'), ""])
		self.todosView.setSortingEnabled(True)

	def updateAllTodos(self) :
		self.todosView.clear()
		priorityFg = QtGui.QBrush(QtGui.QColor.fromRgbF(.7,0,0))
		priorityBg = QtGui.QBrush(QtGui.QColor.fromRgbF(1,1,.7))
		expedientBg = QtGui.QBrush(QtGui.QColor.fromRgbF(.8,.8,.9))
		for expedient in self.expedients :
			status = expedientStatus(expedient)
			icon = statusIcon(status)
			expedientItem = QtWidgets.QTreeWidgetItem([expedient])
			expedientItem.setIcon(0, icon)
			self.todosView.addTopLevelItem(expedientItem)
			prioritizedTasks = 0
			tasks = 0
			for _, priority, text in expedientTodos(expedient) :
				tasks +=1
				item = QtWidgets.QTreeWidgetItem(
					[text])
				if priority :
					prioritizedTasks +=1
					item.setForeground(0,priorityFg)
					item.setBackground(0,priorityBg)
					expedientItem.setForeground(0,priorityFg)
				expedientItem.addChild(item)
			expedientItem.setToolTip(1, status.capitalize())
			expedientItem.setText(1,"{} tasques prioritaries, {} normals".format(prioritizedTasks,tasks))
			expedientItem.setBackground(0,expedientBg)
			expedientItem.setBackground(1,expedientBg)

		self.todosView.expandAll()
		self.todosView.resizeColumnToContents(0)

	@trace
	def loadMunicipis(self) :
		self._municipis = Municipis.Load()

		self.fullmunicipis = loadMunicipis()

		self.postalCodes = dict((
			(shortName, postal)
			for postal, shortName, longName
			in self.fullmunicipis))

		self.municipis = [
			(shortName, longName)
			for postal, shortName, longName
			in self.fullmunicipis]

	def setupExpedientFilters(self) :

		self.cityFilter.clear()
		self.cityFilter.addItems(
			[
				"Tots els Municipis",
			]+[
				postal+" "+longName
				for postal, shortName, longName
				in self.fullmunicipis
			]+[
				"Municipis no registrats",
			])

		self.statusFilter.clear()
		self.statusFilter.addItems(
			[
				"Tots els estats",
			]+[
				status.capitalize()
				for status
				in statusNames
			])
		for i, name in enumerate(statusNames) :
			self.statusFilter.setItemIcon(i+1, statusIcon(name))

		self.cityFilter.activated.connect(self.applyExpedientFilters)
		self.statusFilter.activated.connect(self.applyExpedientFilters)
		self.expedientFilter.textEdited.connect(self.applyExpedientFilters)
		self.clearFilterButton.clicked.connect(self.clearExpedientFilter)

	@trace
	def clearExpedientFilter(self) :
		self.expedientFilter.clear()
		self.applyExpedientFilters()

	@trace
	def selectedExpedientInList(self) :
		currentExpedientIndex = self.expedientTree.currentRow()
		currentExpedient = self.expedients[currentExpedientIndex]
		return currentExpedient

	@trace
	def updateExpedientList(self) :
		"""Reloads the expedient tree based on what it finds on the filesystem"""

		self.statusMessage("Loading Expedients...")
		self.setDisabled(True)
		self.expedientTree.clear()
		# my-expedient.md or my-expedient/
		self.expedients = sorted([
			expedient[:-3] if expedient.endswith(".md") else expedient
			for expedient in os.listdir(".")
			if "-" in expedient
			and ( os.path.isdir(expedient) or expedient.endswith(".md"))
			])
		self.expedientTree.addItems( self.expedients )

		for i,expedient in enumerate(self.expedients) :
			status = expedientStatus(expedient)
			item = self.expedientTree.item(i)
			item.setIcon(statusIcon(status))
			item.setToolTip(status.capitalize())
			self.udpateExpedientPositionInMap(expedient)

		self.applyExpedientFilters()
		self.updateAllTodos()
		self.setEnabled(True)
		self.statusMessage("Expedients loaded")

	def updateExpedientListItem(self, expedient) :
		i = self.expedients.index(expedient)
		status = expedientStatus(expedient)
		item = self.expedientTree.item(i)
		item.setIcon(statusIcon(status))
		item.setToolTip(status.capitalize())
		self.udpateExpedientPositionInMap(expedient)

	def udpateExpedientPositionInMap(self, expedient) :
		coords = self.expedientCoords(expedient)
		if not coords : return
		self.mapWidget.addMarker(expedient, *coords, **dict(
			draggable=0,
			title="{0} ({1})".format(
				expedient, expedientStatus(expedient).capitalize()),
			icon=statusMarkers[expedientStatus(expedient)],
			))

	@trace
	def applyExpedientFilters(self) :
		"""Hides the expedients not matching the status, city and expedient id filters"""

		def buildFilter(comboBox, options) :
			selection = comboBox.currentIndex()
			if not selection : return None # Any results
			selection-=1
			if selection == len(options) : return options # Non valid
			return options[selection]

		filterCity = buildFilter(
			self.cityFilter,
			[curt for curt, llarg in self.municipis ])

		filterStatus = buildFilter(
			self.statusFilter, statusNames)

		filterText = self.expedientFilter.text()

		hiddenCounter = 0
		for i, expedient in enumerate(self.expedients) :
			filtered = False
			filtered |= filterText not in expedient
			filtered |= filterCity is not None and expedient.split('-')[0] not in filterCity
			filtered |= filterStatus is not None and expedientStatus(expedient) not in filterStatus
			self.expedientTree.setRowHidden(i, filtered)
			if filtered: hiddenCounter += 1

		self.filterResults.setText(
			'{0} expedients ocults'.format(
				hiddenCounter))
		self.filterResults.setStyleSheet("color: red" if hiddenCounter else "")

	@trace
	def longCityName(self, short) :
		municipis = dict(self.municipis)
		if short not in municipis:
			warn(f"El còdigo de municipio '{short}' no està definido")
			return short
		return dict(municipis)[short]

	@trace
	def updatePreview(self) :
		if not hasattr(self, "expedientContent") : return
		content = self.mdEditor.toPlainText()
		if not content and self.expedientContent is None :
			expedientFormatted = "<span style='color: orange; font-weight: bold;'>{}</span>".format(
				"Encara no té expedient")
		else :
			expedientFormatted = markdown(content)
		self.infoBrowser.setHtml(expedientFormatted)

	@trace
	def updateFilesWidget(self, expedient) :
		if os.path.isdir(expedient) :
			self.filesWidget.setRootIndex(self.filesModel.setRootPath(expedient))
			self.filesModel.setNameFilters(["*"])
		else :
			self.filesWidget.setRootIndex(self.filesModel.setRootPath(QtCore.QDir.currentPath()))
			self.filesModel.setFilter(QtCore.QDir.AllEntries | QtCore.QDir.NoDotAndDotDot)
			self.filesModel.setNameFilters([expedient+".md"])


	@trace
	def manageUnsavedChanges(self) :
		if self.editingExpedient is not None :
			oldExpedient = self.editingExpedient
			text = self.mdEditor.toPlainText()
			try :
				oldText = open(mdFile(oldExpedient), encoding='utf-8').read()
			except IOError :
				oldText = ""
			if oldText != text :
				if ask(self.tr("Vols guardar els canvis a\n{}?").format(mdFile(oldExpedient))) :
					self.saveMd()
		self.editingExpedient = self.selectedExpedientInList()

	@trace
	def setMdContent(self, newMdContent) :
		cursor = self.mdEditor.textCursor()
		cursor.select(QtGui.QTextCursor.Document)
		cursor.insertText(newMdContent)

	@trace
	def saveMd(self) :
		try : editingExpedient = self.editingExpedient
		except AttributeError: return
		content = self.mdEditor.toPlainText()
		open(mdFile(editingExpedient),"w", encoding='utf-8').write(content)
		self.mdEditor.document().setModified(False)
		self.updateExpedientListItem(editingExpedient)
		self.updateStatusEditor(editingExpedient)

	@trace
	def closeEvent(self, event) :
		"""Da la opción de guardar los cambios cuando se cierra el programa."""

		self.manageUnsavedChanges()
		event.accept();

	@trace
	def changeCurrentExpedient(self) :
		"""Updates the interface when user selects a different expedient"""

		self.manageUnsavedChanges()

		expedient = self.selectedExpedientInList()
		expedientFile = mdFile(expedient)
		self.tabs.setTabText(0, expedient)
		try:
			self.expedientContent = open(expedientFile, encoding='utf-8').read()
		except IOError :
			self.expedientContent = None

		self.mdEditor.setPlainText(self.expedientContent or "")

		self.actionSave.setEnabled(False)

		self.updateFilesWidget(expedient)

		self.updateStatusEditor(expedient)

		# Proposal needs a budget, if any is already generated do not give the option
		budgetAvailable = os.access(pressupostFile(expedient),os.F_OK)
		proposalAvailable = os.access(propostaFile(expedient),os.F_OK)
		self.actionGenerateBudget.setEnabled(not budgetAvailable)
		self.actionGenerateProposal.setEnabled(not proposalAvailable and budgetAvailable)

		self.reloadStreetView()

		coords = self.expedientCoords(expedient)

		self.reloadStaticLocationMap()
		self.mapWidget.centerAt(*self.expedientCoords(expedient))
		newIcon = statusMarkers[expedientStatus(expedient)]
		try :
			newIcon.update
		except: pass
		else :
			newIcon.update(scale=1.2, strokeWeight=3)
			self.mapWidget.setMarkerOptions(expedient, icon=newIcon, animation="DROP")

	@trace
	def reloadStreetView(self) :
		# TODO: Coords first
		expedient = self.editingExpedient
		currentAddress = self.expedientAddress(expedient)
		loadRemotePixmap (self.photo,
			"http://maps.googleapis.com/maps/api/streetview?"
				"size=400x200&"
				"location="+currentAddress+"&"
				"sensor=false")

	@trace
	def reloadStaticLocationMap(self) :
		expedient = self.editingExpedient
		def googleStaticMap() :
			return (
				"http://maps.googleapis.com/maps/api/staticmap?"
					"center="+self.expedientAddress(expedient)+"&"
					"size=400x250&"
					"zoom=16&"
					"scale=1&"
					"maptype=satellite&" # roadmap,satellite,terrain,hybrid
					"markers=color:green|"+"|".join((
						self.expedientAddress(exp)
						for exp in self.expedients
						if exp is not expedient
						))+"&"
					"markers=color:red|"+self.expedientAddress(expedient)+"&"
					"sensor=false")
		def openStreetMapStaticMap() :
			return (
				"http://staticmap.openstreetmap.de/staticmap.php?"
					"center="+self.expedientCoordsStr(expedient)+"&"
					"size=400x250&"
					"zoom=16&"
					"markers="+
#						"|".join((
#							self.expedientCoordsStr(exp)+",ol-marker-green"
#							for exp in self.expedients
#							if exp is not expedient
#						))+"|"+
						self.expedientCoordsStr(expedient)+",ol-marker"
					)
		loadRemotePixmap(self.locationMap,
			openStreetMapStaticMap())
#				googleStaticMap())
		


	@trace
	def expedientAddress(self, expedient) :
		"""TODO: De momento, lo deduce muy cutremente del ID"""
		try :
			sections = mdSections(open(mdFile(expedient), encoding='utf-8').read())
		except IOError : pass
		else :
			metadata = parseMdMetadata(sections[0][2])
			if 'finca' in metadata and 'municipi' in metadata :
				return metadata['finca'] + ", " + metadata['municipi'] + ", Barcelona, Spain"

		municipiCurt, streetAddress = expedient.split("-")[:2]
		firstNumber = re.search(r'\d', streetAddress)
		if firstNumber is not None :
			streetAddress = streetAddress[:firstNumber.start()] + " " + streetAddress[firstNumber.start():]
		# TODO: Fix: User may add a bad city prefix
		municipi = self.longCityName(municipiCurt)
		return streetAddress+", "+municipi+", Barcelona, Spain"


	@trace
	def expedientCoords(self, expedient, force=False) :
		"""Given an expedient returns its coords or None if none could get.
		Stored coords are preferred if available, if not,
		it tries to deduce them from the street address using a geocoding service.
		The returned value is a comma separated string of latitude, longitude.
		"""
		if not hasattr(self, 'coordCache') :
			self.coordCache = {}

		if not force and expedient in self.coordCache :
			return self.coordCache[expedient]

		coords = readCoordsFile(expedient)
		if coords : return coords

		streetAddress = self.expedientAddress(expedient)

		location = geolocate(streetAddress)
		if location :
			self.coordCache[expedient] = location
		return location

	@trace
	def setExpedientCoords(self, expedient, latitude, longitude) :
		writeCoordsFile(expedient, latitude, longitude)
		if not hasattr(self, 'coordCache') : return
		if expedient not in self.coordCache : return
		del self.coordCache[expedient]

	@trace
	def expedientCoordsStr(self, expedient) :
		coords = self.expedientCoords(expedient)
		return "{},{}".format(*coords) if coords else None

	def addLog(self) :
		if not hasattr(self, 'editingExpedient') : return # No expedient being edited

		authors = expedientAuthors()
		settings = QtCore.QSettings()
		previousAuthor = settings.value('LogAuthor', authors[0])

		dialog = QtWidgets.QDialog()
		f = QtWidgets.QFormLayout(dialog)
		f.addRow(QtWidgets.QLabel("Afegint una entrada al historic de l'expedient {}".format(self.editingExpedient)))

		authorCombo = QtWidgets.QComboBox()
		authorCombo.addItems(authors)
		authorCombo.setCurrentIndex(authors.index(previousAuthor))
		f.addRow("Autor:", authorCombo)
		datePicker = QtWidgets.QDateEdit(QtCore.QDate.currentDate())
		datePicker.setDisplayFormat('yyyy-MM-dd')
		f.addRow("Data:", datePicker)
		messageEdit = QtWidgets.QLineEdit()
		messageEdit.setFocus()
		f.addRow("Contingut:", messageEdit)
		buttons = QtWidgets.QDialogButtonBox(
			QtWidgets.QDialogButtonBox.Ok |
			QtWidgets.QDialogButtonBox.Cancel)
		buttons.accepted.connect(dialog.accept)
		buttons.rejected.connect(dialog.reject)
		f.addRow(buttons)
		ok = dialog.exec()
		author = authorCombo.currentText()
		settings.setValue('LogAuthor', author)
		date = datePicker.date().toPython()
		message = messageEdit.text()
		if not ok : return
		oldMdContent = self.mdEditor.toPlainText()
		newMdContent = insertLog(oldMdContent, date, author, message)
		self.setMdContent(newMdContent)

	@trace
	def editLocation(self) :
		if not hasattr(self, 'editingExpedient') : return

		# TODO: take them from the coords file if available
		coords = readCoordsFile(self.editingExpedient)

		d = LocationEditor()
		d.show()
		d.setLocation(self.editingExpedient, self.expedientAddress(self.editingExpedient), coords)
		ok = d.exec()
		if not ok : return

		latitude, longitude = tuple((float(val) for val in d.coordsEdit.text().split(',')))
		self.setExpedientCoords(self.editingExpedient, latitude, longitude)
		self.udpateExpedientPositionInMap(self.editingExpedient)
		self.reloadStaticLocationMap()
		self.reloadStreetView()

	@trace
	def generateBudget(self) :
		expedient = self.editingExpedient
		targetFileName = pressupostFile(expedient)
		if os.access(targetFileName, os.F_OK) :
			if not ask("Ja existeix un pressupost al projecte, vol descartar l'antic?") : return
		copyMasterFile(expedient, pressupostFile(expedient))
		# TODO: Fill it automatically
		startFile(targetFileName)

	@trace
	def generateProposal(self) :
		expedient = self.editingExpedient
		targetFileName = propostaFile(expedient)
		if os.access(targetFileName, os.F_OK) :
			if not ask("Ja existeix una proposta al projecte, vol descartar l'antic?") : return
		copyMasterFile(expedient, propostaFile(expedient))
		# TODO: Fill it automatically
		startFile(targetFileName)

	@trace
	def showOrientations(self) :
		if not self.editingExpedient : return

		coords = self.expedientCoords(self.editingExpedient)
		orientations = list(sorted((
			(distance, direction, expedient)
			for expedient, (distance, direction) in ((
				(expedient, distanceAndDirection(coords, self.expedientCoords(expedient)))
				for expedient in self.expedients))
			)))

		def saveOrientationsHtml() :
			page = """\
	<html>
	<head>
	<meta charset="utf-8">
	<style>
	tr {{background-color: #cff; }}
	tr:nth-child(2n) {{background-color: #ddd; }}
	tr:nth-child(2n-1) {{background-color: #eee; }}
	td, th {{ padding-left:15px; padding-right: 15px;}}
	</style>
	</head>
	<body>
	<h1>{description}</h1>
	<table>
	<tr><th>{headers[0]}</th><th>{headers[1]}</th><th>{headers[2]}</th></tr>
	{cells}
	</table>
	</body>
	</html>
	""".format(
				description = description,
				headers = headers,
				cells = "".join((formatOrientationsRow(*orientation) for orientation in orientations[1:]))
			)
			with open(orientationsHtmlFile(self.editingExpedient), 'w', encoding='utf-8') as f :
				f.write(page)

		def saveOrientationsCsv() :
			csv = "\n".join((
				'{:0.3f}\t{:0.2f}\t{}'.format(distance, orientation, expedient)
				for distance, orientation, expedient
				in orientations[1:]))
			with open(orientationsCsvFile(self.editingExpedient), 'w', encoding='utf-8') as f :
				f.write(csv)


		class NumericTreeItem( QtWidgets.QTreeWidgetItem ):
			def __init__(self, *args):
				super(NumericTreeItem, self).__init__(*args)

			def __lt__(self, otherItem):
				column = self.treeWidget().sortColumn()
				try:
					return float( self.text(column) ) > float( otherItem.text(column) )
				except ValueError:
					return self.text(column) > otherItem.text(column)

		headers = ("Distància (km)", "Orientació (graus)", "Node")
		description = "Orientacions des de {}".format(self.editingExpedient)


		d = QtWidgets.QDialog(self)
		l = QtWidgets.QVBoxLayout()
		d.setLayout(l)
		label = QtWidgets.QLabel(description)
		l.addWidget(label)
		table = QtWidgets.QTreeWidget()
		table.setSortingEnabled(True)
		table.setHeaderLabels(headers)
		table.addTopLevelItems([
			NumericTreeItem(['{:0.3f}'.format(distance), '{:0.2f}'.format(orientation), expedient])
			for distance, orientation, expedient in orientations[1:]])
		l.addWidget(table)
		buttons = QtWidgets.QDialogButtonBox(
			QtWidgets.QDialogButtonBox.Ok |
			QtWidgets.QDialogButtonBox.Cancel |
			QtWidgets.QDialogButtonBox.Help,
			QtCore.Qt.Horizontal,
			self)
		saveCsv = buttons.addButton("Save as CSV", QtWidgets.QDialogButtonBox.ActionRole)
		saveHtml = buttons.addButton("Save as Html", QtWidgets.QDialogButtonBox.ActionRole)

		buttons.accepted.connect(d.accept)
		buttons.rejected.connect(d.reject)
		saveHtml.clicked.connect(saveOrientationsHtml)
		saveCsv.clicked.connect(saveOrientationsCsv)
		l.addWidget(buttons)
		d.exec()





def main() :

	app = createApp()

	if "--trace" in sys.argv :
		doTrace = True
		gestor.qtutils.doTrace = True

	if "--test" in sys.argv :
		doTrace = False
		import doctest
		print(doctest.testmod())
		sys.exit()

	app.setApplicationName(programName)
	app.setApplicationVersion(programVersion)
	app.setOrganizationName('Guifibaix SCCL')
	app.setOrganizationDomain('guifibaix.coop')

	translatorQt = QtCore.QTranslator()
	translationPath = QtCore.QLibraryInfo.path(QtCore.QLibraryInfo.TranslationsPath)
	translatorQt.load(QtCore.QLocale.system(), "qt", "_", translationPath, ".qm")
	app.installTranslator(translatorQt)

	translatorApp = QtCore.QTranslator()
	translatorApp.load(QtCore.QLocale.system(), "gestor", "_", '://i18n/', ".qm")
	app.installTranslator(translatorApp)

	mainWindow = MainWindow()

	app.exec()

if __name__ == '__main__' :
	main()



