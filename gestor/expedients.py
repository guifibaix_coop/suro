from .qtutils import trace
import collections
import datetime
import os
from qgmap.presets import customPin

class AttributeDict(dict) :
	"""A Dictionary whose keys act also as object attributes"""
	def __init__(self, *args, **kw) :
		super(AttributeDict, self).__init__(*args, **kw)
		self.__dict__ = self

@trace
def mdSections(mdtext) :
	"""
	>>> mdSections("") # empty
	[(0, None, '')]
	>>> mdSections("# Section 1\\n# Section 2") # stripping same level empty sections
	[(1, 'Section 1', ''), (1, 'Section 2', '')]
	>>> mdSections("# Section 1\\n## Section 1.1") # different levels
	[(1, 'Section 1', ''), (2, 'Section 1.1', '')]
	>>> mdSections("# Section1\\nhola\\ntu\\n") # section with content
	[(1, 'Section1', 'hola\\ntu\\n')]
	>>> mdSections("hola\\ntu\\n") # content without section
	[(0, None, 'hola\\ntu\\n')]
	"""
	lines = mdtext.split("\n")
	sections = []
	for line in lines :
		if line and line[0] == '#' :
			sectionName = line.lstrip('#')
			sectionLevel = len(line) - len(sectionName)
			sections.append( (sectionLevel, sectionName.strip(), []) )
			continue
		if not sections :
			sections.append( (0, None, []) )
		sections[-1][2].append(line)
	return [ (level, name, "\n".join(lines)) for level, name, lines in  sections ]

@trace
def parseMdMetadata(text) :
	"""
	>>> parseMdMetadata('')
	OrderedDict()
	>>> parseMdMetadata('not a pair')
	OrderedDict()
	>>> parseMdMetadata('lowerkey: A Value')
	OrderedDict([('lowerkey', 'A Value')])
	>>> parseMdMetadata('UpperKey: A Value') # uppercase keys get lowered
	OrderedDict([('upperkey', 'A Value')])
	>>> parseMdMetadata('- key: A Value') # itemized
	OrderedDict([('key', 'A Value')])
	>>> parseMdMetadata('\\t- key: A Value') # indented item
	OrderedDict([('key', 'A Value')])
	>>> parseMdMetadata('key: A Value\\nkey2: Another value') # multi valued
	OrderedDict([('key', 'A Value'), ('key2', 'Another value')])
	>>> parseMdMetadata('key: A Value\\n\\nkey2: Another value') # interrupted
	OrderedDict([('key', 'A Value'), ('key2', 'Another value')])
	"""
	result = []
	for line in text.split('\n') :
		colonpos = line.find(':')
		if colonpos < 0 : continue # not a map
		key = line[:colonpos].strip().lstrip('-').strip().lower()
		value = line[colonpos+1:].strip()
		result.append((key,value))
	return collections.OrderedDict(result)

@trace
def dumpMdMetadata(metadata) :
	"""
	>>> dumpMdMetadata(collections.OrderedDict([('clau', 'valor')]))
	'\\n- Clau: valor\\n'
	>>> dumpMdMetadata(collections.OrderedDict([('clau', 'valor'),('clau2', 'valor2')]))
	'\\n- Clau: valor\\n- Clau2: valor2\\n'
	"""
	return '\n' + "".join((
		"- {}: {}\n".format(clau.capitalize(), valor)
		for clau, valor in metadata.items()
		))
@trace
def dumpMdSections(sections) :
	return "".join((
		"{} {}\n{}\n".format('#'*level, title, content)
		for level, title, content in sections ))

@trace
def changeStatusOnMd(oldContent, newStatus) :
	"""
	Changes the status of an expedient
	"""
	sections = mdSections(oldContent)
	for i, (level, title, sectionContent) in enumerate(sections) :
		metadata = collections.OrderedDict(parseMdMetadata(sectionContent))
		if "estat" not in metadata : continue
		metadata["estat"] = newStatus
		sections[i] = sections[i][0], sections[i][1], dumpMdMetadata(metadata)
		break
	# TODO: Y si no hay estado, que?
	return dumpMdSections(sections)

@trace
def insertLog(oldContent, date, author, line) :
	"""
	Inserts a log line into the expedient Log

	>>> insertLog("# Logless", datetime.date(2010,10,2), 'me', 'my entry')
	'# Logless\\n\\n## Log\\n\\n- 2010-10-02 [me] my entry\\n\\n'
	>>> insertLog("# Logless\\nHello", datetime.date(2010,10,2), 'me', 'my entry')
	'# Logless\\nHello\\n## Log\\n\\n- 2010-10-02 [me] my entry\\n\\n'
	>>> insertLog("# Empty log\\n## Log", datetime.date(2010,10,2), 'me', 'my entry')
	'# Empty log\\n\\n## Log\\n\\n- 2010-10-02 [me] my entry\\n'
	>>> insertLog("# Full log\\n## Log\\n\\n- 2003-03-23 [she] old entry\\n", datetime.date(2010,10,2), 'me', 'my entry')
	'# Full log\\n\\n## Log\\n\\n- 2010-10-02 [me] my entry\\n- 2003-03-23 [she] old entry\\n\\n'
	>>> insertLog("# Close get separated\\n## Log\\n- 2003-03-23 [she] old entry\\n", datetime.date(2010,10,2), 'me', 'my entry')
	'# Close get separated\\n\\n## Log\\n\\n- 2010-10-02 [me] my entry\\n- 2003-03-23 [she] old entry\\n\\n'
	>>> insertLog("# Separated get close\\n## Log\\n\\n\\n\\n- 2003-03-23 [she] old entry\\n", datetime.date(2010,10,2), 'me', 'my entry')
	'# Separated get close\\n\\n## Log\\n\\n- 2010-10-02 [me] my entry\\n- 2003-03-23 [she] old entry\\n\\n'
	>>> insertLog("# Kept level\\n### Log", datetime.date(2010,10,2), 'me', 'my entry')
	'# Kept level\\n\\n### Log\\n\\n- 2010-10-02 [me] my entry\\n'
	"""
	newLogLine = "- {} [{}] {}".format(
				date.isoformat(), author, line)
	sections = mdSections(oldContent)
	logAdded = False
	for i, (level, title, sectionContent) in enumerate(sections) :
		if title.strip().lower() != "log" : continue
		newSectionContent = [""]
		for line in sectionContent.split("\n") :
			if not logAdded and line.strip() :
				newSectionContent.append(newLogLine)
				logAdded = True
			if logAdded :
				newSectionContent.append(line)
		if not logAdded :
			newSectionContent.append(newLogLine)

		sections[i] = sections[i][0], sections[i][1], "\n".join(newSectionContent)
		logAdded = True

	if not logAdded :
		sections.append( (2, "Log", "\n"+newLogLine+'\n' ))

	return dumpMdSections(sections)

@trace
def readCoordsFile(expedient) :
	try :
		with open(coordsFile(expedient), encoding='utf-8') as f :
			return [float(c) for c in f.read().strip().split(",")]
	except (IOError,) :
		return None
	except ValueError as e :
		print ("Llegint {}: {}".format(coordsFile(expedient), str(e)), file=sys.stderr)
		return None

@trace
def writeCoordsFile(expedient, latitude, longitude) :
	with open(coordsFile(expedient),'w', encoding='utf-8') as f :
		f.write('{},{}'.format(latitude,longitude))

@trace
def distanceAndDirection( origin, destination) :
	"""
	- http://www.movable-type.co.uk/scripts/latlong.html
	- Orientacion: θ = atan2( sin(Δλ).cos(φ2), cos(φ1).sin(φ2) − sin(φ1).cos(φ2).cos(Δλ) ) (Angulos en radianes)
	- Distancia: d = acos( sin(φ1).sin(φ2) + cos(φ1).cos(φ2).cos(Δλ) ).R  (simplificada, tambien esta la haversinus)

	>>> distanceAndDirection( (0., 0.), (4., 0) ) # north
	(444.77970657823954, 0.0)
	>>> distanceAndDirection( (0., 0.), (-4., 0) ) # south
	(444.77970657823954, 180.0)
	>>> distanceAndDirection( (0., 0.), (0, 4) ) # west
	(444.77970657823954, 90.0)
	>>> distanceAndDirection( (0., 0.), (0, -4) ) # east
	(444.77970657823954, -90.0)
	"""
	from math import sin, cos, atan2, radians, acos, degrees

	latitude1, longitude1 = [radians(x) for x in origin]
	latitude2, longitude2 = [radians(x) for x in destination ]
	deltalongitude = longitude2 - longitude1
	R = 6371 # Km
	c1 = cos(latitude1)
	c2 = cos(latitude2)
	s1 = sin(latitude1)
	s2 = sin(latitude2)
	cd = cos(deltalongitude)
	sd = sin(deltalongitude)
	orientation = atan2(sd*c2, c1*s2 - s1*c1*cd)
	distance = acos( s1*s2 + c1*c2*cd) * R
	return distance, degrees(orientation)

def formatOrientationsRow(distance, orientation, expedient) :
	"""\
	>>> formatOrientationsRow(300.123456789123456, 30.123456789123456789, "my-expedient")
	'<tr><td>300.123</td><td>30.12</td><td>my-expedient</td></tr>\\n'
	"""
	return '<tr><td>{:0.3f}</td><td>{:0.2f}</td><td>{}</td></tr>\n'.format(distance, orientation, expedient)

@trace
def expedientify(cadena) :
	"""
	>>> expedientify("cadenabuena")
	'cadenabuena'
	>>> expedientify("cadena con espacios")
	'cadenaconespacios'
	>>> expedientify("cadena con a'postrofes")
	'cadenaconapostrofes'
	>>> expedientify("cadena con under_lines")
	'cadenaconunderlines'
	>>> expedientify("cadena con àçèntós ñç")
	'cadenaconacentosnc'
	>>> expedientify("cadena con MAyÚSCULAS")
	'cadenaconmayusculas'
	"""
	source = "áéíóúàèìòùäëïöüâêîôûçÇñÑ"
	target = "aeiouaeiouaeiouaeioucCnN"
	result = cadena.lower()
	result = result.translate(result.maketrans(source, target))
	result = "".join([c for c in result if c.isalnum()])
	return result

"""
Some online marker icon sources:

https://sites.google.com/site/gmapicons/
https://sites.google.com/site/gmapsdevelopment/

http://labs.google.com/ridefinder/images/mm_20_{color}.png
	color: purple, white, black, yellow, blue, green, red, orange, grey, brown, shadow

http://www.google.com/mapfiles/marker{letter}.png
	letter: A..Z, "" for a dot

http://chart.apis.google.com/chart?chst={shape}&chld={text}|{color}|000000
	shape can be 'd_map_pin_letter'
	color: rrggbb

http://thydzik.com/thydzikGoogleMap/markerlink.php?text={text}&color={color}
	color: rrggbb code
	text: ascii text
"""

def pinIcon(color='fffaaaa', shape='d_map_pin_letter', text='%E2%80%A2') :
	return 'http://thydzik.com/thydzikGoogleMap/markerlink.php?text={text}&color={color}'.format(
		shape=shape,
		text=text,
		color=color)

def pinSymbol(color='ffaaaa', **keys) :
	return customPin(color)

def flagSymbol(color='ffaaaa') :
	path='M 0,0 -1,-2 V -43 H 1 V -2 z M 1,-40 H 30 V -20 H 1 z',
	return customPin(color, path=path)

# name, icon, marker
statusTable = [
	('desconocido',   'unknown',       'http://google.com/mapfiles/ms/micons/question.png'),
	('pedido',        'folder-yellow', pinSymbol('#ffff00')),
	('contactado',    'folder-blue',   pinSymbol('#6699ff')),
	('medido',        'folder-brown',  pinSymbol('#aa6655')),
	('presupuestado', 'folder-orange', pinSymbol('#ff7700')),
	('provisional'  , 'folder-violet', pinSymbol('#ff77ff')),
	('aceptado',      'folder-red',    pinSymbol('#ff3333')),
	('instalado',     'folder-green',  pinSymbol('#99dd77')),
	('incidencia',    'flag-red',      flagSymbol('#ee1111')),
	('descartado',    'folder-grey',   pinSymbol('#aaaaaa')),
	('postergado',    'folder-black',  pinSymbol('#222222')),
	]

statusNames = [name for name, icon, marker in statusTable]
statusIcons = dict(((name, icon) for name, icon, marker in statusTable))
statusMarkers = dict(((name, marker) for name, icon, marker in statusTable))

@trace
def expedientFile(expedient, suffix) :
	"""
	>>> expedientFile('expedientid', 'suffix')
	'expedientid/expedientidsuffix'
	"""
	return os.path.join(expedient,expedient+suffix)

@trace
def mdFile(expedientId) :
	if os.path.isdir(expedientId) :
		return expedientFile(expedientId,'.md')
	return expedientId+".md"

@trace
def coordsFile(expedient) :
	return expedientFile(expedient,".coords")

@trace
def propostaFile(expedient) :
	return expedientFile(expedient,"-proposta.odt")

@trace
def pressupostFile(expedient) :
	return expedientFile(expedient,"-pressupost.ods")

@trace
def orientationsHtmlFile(expedient) :
	return expedientFile(expedient,"-orientations.html")

@trace
def orientationsCsvFile(expedient) :
	return expedientFile(expedient,"-orientations.csv")

@trace
def expedientStatus(expedient) :
	"""TODO: De momento se lo inventa"""
	try :
		sections = mdSections(open(mdFile(expedient), encoding='utf-8').read())
	except IOError : pass
	else :
		metadata= parseMdMetadata(sections[0][2])
		if 'estat' in metadata  :
			estat = metadata['estat'].lower()
			if estat in statusNames :
				return estat
	return 'desconocido'

@trace
def parseTodo(line) :
	line = line.strip()
	line = line.lstrip('-')
	line = line.strip()
	result = line.lstrip('*')
	priority = len(line)-len(result)
	result = result.rstrip('*')
	return (priority, result)

@trace
def expedientTodos(expedient) :
	try :
		sections = mdSections(open(mdFile(expedient), encoding='utf-8').read())
	except FileNotFoundError : return []
	for level, name, content in sections :
		if not name : continue
		if "Pendent" not in name : continue
		return [
			(expedient, priority, text) 
			for priority, text in (
				parseTodo(line)
				for line in content.split('\n')
				if line.strip()
				)
		]
	return []

@trace
def copyMasterFile(expedient, targetFileName) :
	import shutil
	"""Given a target expedient file, tries to generate it from the analog file at master project"""
	masterFileName = targetFileName.replace(expedient, 'master')
	shutil.copy2(masterFileName, targetFileName)

def expedientAuthors() :
	return ["aitor", "david", "raulb", 'rauls', 'victor']


def loadMunicipis() :
	return [
		[field.strip() for field in line.split("\t")]
		for line in open("gestor.municipis", encoding='utf-8')
		if line.strip() ]


class Municipis() :
	class FormatError(Exception) : pass
	def __init__(self) :
		self.municipis = collections.OrderedDict()
	def add(self, postalCode, short, fullName) :
		self.municipis[short] = AttributeDict(
			short = short,
			fullName = fullName,
			postalCode = postalCode,
			)
	def postalCode(self, short) :
		return self.municipis[short].postalCode
	def fullName(self, short) :
		return self.municipis[short].fullName
	def __iter__(self) :
		for item in self.municipis.values() :
			yield item
	def dumpStr(self) :
		return "\n".join([
			"\t".join((item.postalCode, item.short, item.fullName))
			for item in self])
	def load(self, filename) :
		oldValue = self.municipis
		self.municipis = collections.OrderedDict()
		try :
			with open(filename, encoding='utf-8') as f :
				for line in f :
					line = line.strip()
					if not line : continue
					if line[0] == '#' : continue
					fields = [field.strip() for field in line.split("\t")]
					if len(fields)!=3 : raise Municipis.FormatError()
					self.add(*fields)
		except :
			self.municipis = oldValue
			raise

	@classmethod
	def Load(self) :
		m = Municipis()
		m.load('gestor.municipis')
		return m


