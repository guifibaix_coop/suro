<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="en">
<context>
    <name>GestorProyectes</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Gestor de Projectes</source>
        <translation>Project manager</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="33"/>
        <source>Cap expedient seleccionat</source>
        <translation>No expedient selected</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <source>Edita</source>
        <translation>Edit</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="119"/>
        <source>Ajusta l&apos;emplaçament</source>
        <translation>Set the location</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="138"/>
        <source>Mapa d&apos;emplaçament</source>
        <translation>Location map</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <source>Vista de carrer</source>
        <translation>Street view</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="186"/>
        <source>&amp;Arxiu</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="193"/>
        <source>&amp;Edita</source>
        <translation>&amp;Edita</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="200"/>
        <source>E&amp;xpedient</source>
        <translation>E&amp;xpedient</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="244"/>
        <source>Expedients</source>
        <translation>Expedients</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="315"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <source>Tots els municipis</source>
        <translation>All municipalities</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>Tots els estats</source>
        <translation>All the status</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="343"/>
        <source>Estats extranys</source>
        <translation>Other states</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="360"/>
        <source>Crea e&amp;xpedient</source>
        <translation>Creat e&amp;xpedient</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="363"/>
        <source>Crea un nou expedient</source>
        <translation>Create a new expedient</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="366"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="378"/>
        <source>Desa l&apos;expedient</source>
        <translation>Save the expedient</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="381"/>
        <source>Desa l&apos;expedient actual</source>
        <translation>Save the current expedient</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="393"/>
        <source>Cerca</source>
        <translation>Search</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="396"/>
        <source>Cerca ràpida per nom d&apos;expedient</source>
        <translation>Quick search by expedient id</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="399"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="412"/>
        <source>Desfés</source>
        <translation>Undo</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="415"/>
        <source>Desfés la darrera modificació</source>
        <translation>Undo the latest modification</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="431"/>
        <source>Refés</source>
        <translation>Redo</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="434"/>
        <source>Refés el darrera acció desfeta</source>
        <translation>Redo the last undone action</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="437"/>
        <source>Ctrl+Shift+Z</source>
        <translation>Ctrl+Shift+Z</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="445"/>
        <source>Genera pressupost</source>
        <translation>Generate budget</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="448"/>
        <source>Genera pressupost a partir del pressupost mestre</source>
        <translation>Generate budget from the master budget</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="456"/>
        <source>Genera proposta</source>
        <translation>Generate proposal</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="459"/>
        <source>Genera una proposta de pressupost a partir de la proposta mestra</source>
        <translation>Generate a budget proposal from the master proposal</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="467"/>
        <source>Nova entrada de històric</source>
        <translation>New log entry</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="470"/>
        <source>Afegeix una nova entrada a l&apos;històric</source>
        <translation>Adds a new log entry</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="223"/>
        <source>Barra d&apos;eines</source>
        <translation>Toolbar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="278"/>
        <source>S&apos;esta filtrant...</source>
        <translation>Filtering....</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <source>Cerca...</source>
        <translation>Search...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="285"/>
        <source>Detalls</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="475"/>
        <source>Orientacions</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="478"/>
        <source>Genera orientacións</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="481"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="486"/>
        <source>Aplicar els canvis dels companys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="491"/>
        <source>Pujar els meus canvis al servidor</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="208"/>
        <source>Col·laboració</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="496"/>
        <source>En quin estat estic?</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="501"/>
        <source>Història</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="suro.py" line="497"/>
        <source>Tasques Pendents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="suro.py" line="498"/>
        <source>Expedient/Tasca</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="suro.py" line="707"/>
        <source>Vols guardar els canvis a
{}?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
