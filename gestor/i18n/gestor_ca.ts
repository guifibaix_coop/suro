<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="ca">
<context>
    <name>GestorProyectes</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Gestor de Projectes</source>
        <translation>Gestor de Projectes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="33"/>
        <source>Cap expedient seleccionat</source>
        <translation>Cap expedient seleccionat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <source>Edita</source>
        <translation>Edita</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="119"/>
        <source>Ajusta l&apos;emplaçament</source>
        <translation>Ajusta l&apos;emplaçament</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="138"/>
        <source>Mapa d&apos;emplaçament</source>
        <translation>Mapa d&apos;emplaçament</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <source>Vista de carrer</source>
        <translation>Vista de carrer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="186"/>
        <source>&amp;Arxiu</source>
        <translation>&amp;Arxiu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="193"/>
        <source>&amp;Edita</source>
        <translation>&amp;Edita</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="200"/>
        <source>E&amp;xpedient</source>
        <translation>E&amp;xpedient</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="244"/>
        <source>Expedients</source>
        <translation>Expedients</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="315"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <source>Tots els municipis</source>
        <translation>Tots els municipis</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>Tots els estats</source>
        <translation>Tots els estats</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="343"/>
        <source>Estats extranys</source>
        <translation>Estats extranys</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="360"/>
        <source>Crea e&amp;xpedient</source>
        <translation>Crea e&amp;xpedient</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="363"/>
        <source>Crea un nou expedient</source>
        <translation>Crea un nou expedient</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="366"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="378"/>
        <source>Desa l&apos;expedient</source>
        <translation>Desa l&apos;expedient</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="381"/>
        <source>Desa l&apos;expedient actual</source>
        <translation>Desa l&apos;expedient actual</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="393"/>
        <source>Cerca</source>
        <translation>Cerca</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="396"/>
        <source>Cerca ràpida per nom d&apos;expedient</source>
        <translation>Cerca ràpida per nom d&apos;expedient</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="399"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="412"/>
        <source>Desfés</source>
        <translation>Desfés</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="415"/>
        <source>Desfés la darrera modificació</source>
        <translation>Desfés la darrera modificació</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="431"/>
        <source>Refés</source>
        <translation>Refés</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="434"/>
        <source>Refés el darrera acció desfeta</source>
        <translation>Refés el darrera acció desfeta</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="437"/>
        <source>Ctrl+Shift+Z</source>
        <translation>Ctrl+Shift+Z</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="445"/>
        <source>Genera pressupost</source>
        <translation>Genera pressupost</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="448"/>
        <source>Genera pressupost a partir del pressupost mestre</source>
        <translation>Genera pressupost a partir del pressupost mestre</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="456"/>
        <source>Genera proposta</source>
        <translation>Genera proposta</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="459"/>
        <source>Genera una proposta de pressupost a partir de la proposta mestra</source>
        <translation>Genera una proposta de pressupost a partir de la proposta mestra</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="467"/>
        <source>Nova entrada de històric</source>
        <translation>Nova entrada de històric</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="470"/>
        <source>Afegeix una nova entrada a l&apos;històric</source>
        <translation>Afegeix una nova entrada a l&apos;històric</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="223"/>
        <source>Barra d&apos;eines</source>
        <translation>Barra d&apos;eines</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="278"/>
        <source>S&apos;esta filtrant...</source>
        <translation>S&apos;esta filtrant...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <source>Cerca...</source>
        <translation>Cerca...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="285"/>
        <source>Detalls</source>
        <translation>Detalls</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="475"/>
        <source>Orientacions</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="478"/>
        <source>Genera orientacións</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="481"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="486"/>
        <source>Aplicar els canvis dels companys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="491"/>
        <source>Pujar els meus canvis al servidor</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="208"/>
        <source>Col·laboració</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="496"/>
        <source>En quin estat estic?</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="501"/>
        <source>Història</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="suro.py" line="497"/>
        <source>Tasques Pendents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="suro.py" line="498"/>
        <source>Expedient/Tasca</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="suro.py" line="707"/>
        <source>Vols guardar els canvis a
{}?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
