<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="an">
<context>
    <name>GestorProyectes</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Gestor de Projectes</source>
        <translation type="unfinished">Chestor de proyeutos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="33"/>
        <source>Cap expedient seleccionat</source>
        <translation type="unfinished">Dengún espedién trigau</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <source>Edita</source>
        <translation type="unfinished">Edita</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="119"/>
        <source>Ajusta l&apos;emplaçament</source>
        <translation type="unfinished">Achusta o plazamiento</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="138"/>
        <source>Mapa d&apos;emplaçament</source>
        <translation type="unfinished">Mapa de plazamiento</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <source>Vista de carrer</source>
        <translation type="unfinished">Bista de carrera</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="186"/>
        <source>&amp;Arxiu</source>
        <translation type="unfinished">&amp;Archiu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="193"/>
        <source>&amp;Edita</source>
        <translation type="unfinished">&amp;Edita</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="200"/>
        <source>E&amp;xpedient</source>
        <translation type="unfinished">E&amp;spedién</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="244"/>
        <source>Expedients</source>
        <translation type="unfinished">Espediens</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="315"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <source>Tots els municipis</source>
        <translation type="unfinished">Toz os munezipios</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>Tots els estats</source>
        <translation type="unfinished">Toz os estaus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="343"/>
        <source>Estats extranys</source>
        <translation type="unfinished">Estaus rarizos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="360"/>
        <source>Crea e&amp;xpedient</source>
        <translation type="unfinished">Cre&amp;ya l&apos;espedién</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="363"/>
        <source>Crea un nou expedient</source>
        <translation type="unfinished">Creya un nuebo espedién</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="366"/>
        <source>Ctrl+N</source>
        <translation type="unfinished">Ctrl+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="378"/>
        <source>Desa l&apos;expedient</source>
        <translation type="unfinished">Alza l&apos;espedién</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="381"/>
        <source>Desa l&apos;expedient actual</source>
        <translation type="unfinished">Alza l&apos;espedién autual</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <source>Ctrl+S</source>
        <translation type="unfinished">Ctrl+S</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="393"/>
        <source>Cerca</source>
        <translation type="unfinished">Busca</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="396"/>
        <source>Cerca ràpida per nom d&apos;expedient</source>
        <translation type="unfinished">Busca rapeda per nombre d&apos;espedién</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="399"/>
        <source>Ctrl+F</source>
        <translation type="unfinished">Ctrl+F</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="412"/>
        <source>Desfés</source>
        <translation type="unfinished">Desfer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="415"/>
        <source>Desfés la darrera modificació</source>
        <translation type="unfinished">Desfer la zaguera modificazión</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished">Ctrl+Z</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="431"/>
        <source>Refés</source>
        <translation type="unfinished">Refer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="434"/>
        <source>Refés el darrera acció desfeta</source>
        <translation type="unfinished">Refer la zaguera aizión desfecha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="437"/>
        <source>Ctrl+Shift+Z</source>
        <translation type="unfinished">Ctrl+Shift+Z</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="445"/>
        <source>Genera pressupost</source>
        <translation type="unfinished">Chenera presupuesto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="448"/>
        <source>Genera pressupost a partir del pressupost mestre</source>
        <translation type="unfinished">Chenera presupuesto a radiz d&apos;o presupuesto mayestro</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="456"/>
        <source>Genera proposta</source>
        <translation type="unfinished">Chenera propuesta</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="459"/>
        <source>Genera una proposta de pressupost a partir de la proposta mestra</source>
        <translation type="unfinished">Chenera una propuesta de presupuesto a radiz d&apos;ha propuesta mayestra</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="467"/>
        <source>Nova entrada de històric</source>
        <translation type="unfinished">Nueba dentrada d&apos;estorico</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="470"/>
        <source>Afegeix una nova entrada a l&apos;històric</source>
        <translation type="unfinished">Adibir una nueba dentrada a l&apos;estorico</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="223"/>
        <source>Barra d&apos;eines</source>
        <translation type="unfinished">Barra d&apos;einas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="278"/>
        <source>S&apos;esta filtrant...</source>
        <translation type="unfinished">Se ye tresminán...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <source>Cerca...</source>
        <translation type="unfinished">Busca...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="285"/>
        <source>Detalls</source>
        <translation type="unfinished">Detalle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="475"/>
        <source>Orientacions</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="478"/>
        <source>Genera orientacións</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="481"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="486"/>
        <source>Aplicar els canvis dels companys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="491"/>
        <source>Pujar els meus canvis al servidor</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="208"/>
        <source>Col·laboració</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="496"/>
        <source>En quin estat estic?</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="501"/>
        <source>Història</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="suro.py" line="497"/>
        <source>Tasques Pendents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="suro.py" line="498"/>
        <source>Expedient/Tasca</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="suro.py" line="707"/>
        <source>Vols guardar els canvis a
{}?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
