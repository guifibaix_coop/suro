<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="es">
<context>
    <name>GestorProyectes</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Gestor de Projectes</source>
        <translation>Gestor de Projectos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="33"/>
        <source>Cap expedient seleccionat</source>
        <translation>Ningun expediente seleccionado</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <source>Edita</source>
        <translation>Edita</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="119"/>
        <source>Ajusta l&apos;emplaçament</source>
        <translation>Ajusta el emplazamiento</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="138"/>
        <source>Mapa d&apos;emplaçament</source>
        <translation>Mapa de emplazamiento</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <source>Vista de carrer</source>
        <translation>Vista de calle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="186"/>
        <source>&amp;Arxiu</source>
        <translation>&amp;Fichero</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="193"/>
        <source>&amp;Edita</source>
        <translation>&amp;Edita</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="200"/>
        <source>E&amp;xpedient</source>
        <translation>E&amp;xpediente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="223"/>
        <source>Barra d&apos;eines</source>
        <translation>Barra de herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="244"/>
        <source>Expedients</source>
        <translation>Expedientes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="278"/>
        <source>S&apos;esta filtrant...</source>
        <translation>Filtrando...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <source>Cerca...</source>
        <translation>Buscar...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="315"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <source>Tots els municipis</source>
        <translation>Todos los municipios</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>Tots els estats</source>
        <translation>Todos los estados</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="343"/>
        <source>Estats extranys</source>
        <translation>Estados extraños</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="360"/>
        <source>Crea e&amp;xpedient</source>
        <translation>Crea e&amp;xpediente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="363"/>
        <source>Crea un nou expedient</source>
        <translation>Crea un nuevo expediente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="366"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="378"/>
        <source>Desa l&apos;expedient</source>
        <translation>Guardar expediente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="381"/>
        <source>Desa l&apos;expedient actual</source>
        <translation>Guarda el expediente actual</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="393"/>
        <source>Cerca</source>
        <translation>Busca</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="396"/>
        <source>Cerca ràpida per nom d&apos;expedient</source>
        <translation>Busquedar rápida por nombre de expediente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="399"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="412"/>
        <source>Desfés</source>
        <translation>Deshacer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="415"/>
        <source>Desfés la darrera modificació</source>
        <translation>Deshacer la última modificación</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="418"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="431"/>
        <source>Refés</source>
        <translation>Rehacer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="434"/>
        <source>Refés el darrera acció desfeta</source>
        <translation>Rehacer la ultima accion deshecha</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="437"/>
        <source>Ctrl+Shift+Z</source>
        <translation>Ctrl+Shift+Z</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="445"/>
        <source>Genera pressupost</source>
        <translation>Generar presupuesto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="448"/>
        <source>Genera pressupost a partir del pressupost mestre</source>
        <translation>Generar presupuesto a partir del maestro</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="456"/>
        <source>Genera proposta</source>
        <translation>Generar propuesta</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="459"/>
        <source>Genera una proposta de pressupost a partir de la proposta mestra</source>
        <translation>Genera una propuesta a partir del presupuesto y la propuesta maestra</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="467"/>
        <source>Nova entrada de històric</source>
        <translation>Nueva entrada de historial</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="470"/>
        <source>Afegeix una nova entrada a l&apos;històric</source>
        <translation>Añade una nueva entrada al historial</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="285"/>
        <source>Detalls</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="475"/>
        <source>Orientacions</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="478"/>
        <source>Genera orientacións</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="481"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="486"/>
        <source>Aplicar els canvis dels companys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="491"/>
        <source>Pujar els meus canvis al servidor</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="208"/>
        <source>Col·laboració</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="496"/>
        <source>En quin estat estic?</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="mainwindow.ui" line="501"/>
        <source>Història</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="suro.py" line="497"/>
        <source>Tasques Pendents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="suro.py" line="498"/>
        <source>Expedient/Tasca</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="suro.py" line="707"/>
        <source>Vols guardar els canvis a
{}?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
