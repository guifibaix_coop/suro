#!/bin/bash

pyside-lupdate -noobsolete ../suro.py *.py *.ui -ts i18n/*ts
rm -f *.qm
lrelease-qt4 i18n/*ts
pyside-rcc -py3 resources.qrc  > resources.py

