#!/usr/bin/env python3

from PySide6 import QtCore, QtGui, QtWidgets
from PySide6 import QtUiTools, QtNetwork

def createApp(args=[]) :
	"""Ensures the Qt app is created, avoiding creating it twice"""
	if "QtCore" not in globals() : return False
	app = QtCore.QCoreApplication.instance()
	if app : return app
	return QtWidgets.QApplication(args)


def anyTopLevel() :
	return (createApp().topLevelWidgets()[1:] or [None])[0]

def appName() :
	return createApp().applicationName()

def appVersion() :
	return createApp().applicationVersion()

def die(message) :
	print(message, file=sys.stderr)
	if createApp() :
		QtWidgets.QMessageBox.critical(anyTopLevel(), appName(), message)
	sys.exit(-1)

def log(message) :
	print(message, file=sys.stderr)
	if createApp() :
		QtWidgets.QMessageBox.information(anyTopLevel(), appName(), message)

doTrace = False

import sys
import os
import subprocess

def reportMissingPackage(e, debpackages={}, pippackages={}) :
	if not hasattr(e, 'name') or e.name is None :
		if 'PySide' not in e.args[0] :
			print ("Bump")
			raise e # Esta el paquete pero no contiene lo experado
		package = 'PySide6'
	else:
		package = e.name
	commonMessage = (
		"No s'ha pogut trobar el mòdul '{}' per a Python3.\n\n"
		"Ho podeu solucionar executant a les següents instruccions a un terminal:\n\n""{}\n"
	)
	if package in debpackages :
		die(commonMessage.format(package, 
			"$ sudo apt-get install {}".format(debpackages[package])))
	if package in pippackages :
		die(commonMessage.format(package, 
			"  $ sudo apt-get install python3-pip\n"
			"  $ sudo pip3 install {}".format(pippackages[package])))
	raise


def registerWindowedExceptionHandling() :
	"""Change the unhandled exceptions handler to show a Qt message box"""

	def excepthook(excType, excValue, tracebackobj):
		"""
		Global function to catch unhandled exceptions.
		@param excType exception type
		@param excValue exception value
		@param tracebackobj traceback object
		"""
		app = createApp()

		import io
		import traceback
		notice = (
			"<p>L'applicació <em>{0}</em> versió {1} no ha previst una condició excepcional.</p>\n"
			"<p>Si us plau, per ajudar a corregir el problema, informeu-ne als desenvolupadors, "
			"tot copiant complert aquest missatge d'error i explicant el que estaveu fent.</p>\n"
			"<p><b>{2}</b></p>"
			"<p style='white-space: pre-wrap'>{3}</p>"
			)

		tbinfofile = io.StringIO()
		traceback.print_tb(tracebackobj, None, tbinfofile)
		tbinfo = tbinfofile.getvalue()
		errmsg = '%s: \n%s' % (excType.__name__, str(excValue))
		errorbox = QtWidgets.QMessageBox.critical(
			anyTopLevel(), appName(),
			notice.format(
				appName(), appVersion(), errmsg, tbinfo))
		oldExeptHook(excType, excValue, tracebackobj)

	oldExeptHook = sys.excepthook
	sys.excepthook = excepthook

registerWindowedExceptionHandling()

try :
	from PySide6 import QtCore, QtGui, QtWidgets
	from PySide6 import QtUiTools, QtNetwork
	import decorator
except ImportError as e :
	print("Windowed error handling",e)
	reportMissingPackage(e,
		pippackages = dict(
			PySide6="pyside6",
			decorator="decorator",
			),
		)

@decorator.decorator
def trace(function, *args, **kwds) :
	"""Decorates a function by tracing the begining and
	end of the function execution, if doTrace global is True"""

	if doTrace : print ("> "+function.__name__, args, kwds)
	result = function(*args, **kwds)
	if doTrace : print ("< "+function.__name__, args, kwds, "->", result)
	return result

@trace
def ask(question, options=QtWidgets.QMessageBox.Yes|QtWidgets.QMessageBox.No) :
	"""Asks a yes/no question to the user"""
	return QtWidgets.QMessageBox.question(anyTopLevel(), appName(), question, options) == QtWidgets.QMessageBox.Yes


# Taken from https://github.com/lunaryorn/snippets/blob/master/qt4/designer/pyside_dynamic.py
class QUiLoader(QtUiTools.QUiLoader):
	"""Like QtUiTools.QUiLoader but it may take a base
	widget to fill instead of always create a new one.
	"""

	#@trace
	def __init__(self, baseinstance):
		super(QUiLoader, self).__init__(baseinstance)
		self.baseinstance = baseinstance

	@trace
	def createWidget(self, class_name, parent=None, name=''):

		if parent is None and self.baseinstance:
			# Just return the provided top level
			return self.baseinstance

		# create a new widget for child widgets as usual
		widget = super(QUiLoader, self).createWidget(class_name, parent, name)
		# Mimic PyQt4.uic.loadUi behaviour, by setting the baseinstance attribute
		if self.baseinstance:
			setattr(self.baseinstance, name, widget)
		return widget

@trace
def startFile(filepath) :
	"Obre el fitxer amb la applicació adient al seu tipus"
	if sys.platform.startswith('darwin'):
		subprocess.call(('open', filepath))
	elif os.name == 'nt':
		os.startfile(filepath)
	elif os.name == 'posix':
		subprocess.call(('xdg-open', filepath))

@trace
def loadRemotePixmap(label, url) :
	"""Loads the pixmap for a label from remote url"""

	@trace
	def applyPixmap(reply) :
		"""Called on complete or failed loading"""
		try:
			if reply.error() is not QtNetwork.QNetworkReply.NoError :
				label.setText("Error retrieving image")
#				log("Error loading remote image: "+ str(reply.errorString()))
				return;

			data = reply.readAll()
			if not data.size() : return

			pixmap = QtGui.QPixmap()
			pixmap.loadFromData(data)
			label.setPixmap(pixmap)
		finally:
			if reply in label.requests :
				label.requests.remove(reply)

	# Not a loader yet, so set it up
	if not hasattr(label, "loader") :
		label.loader = QtNetwork.QNetworkAccessManager(label)
		label.loader.finished.connect( applyPixmap )

	movie=QtGui.QMovie(":/resources/loading.gif")
	movie.start()
	label.setMovie(movie)

	if not hasattr(label, "requests") :
		label.requests = []

	if label.requests :
		label.requests[-1].abort()
		label.requests.pop()

	label.requests.append(label.loader.get(QtNetwork.QNetworkRequest(QtCore.QUrl(url))))




