import gestor.expedients
from gestor.expedients import Municipis


import unittest
import doctest
import os

class MunicipiTests(unittest.TestCase) :
	def setUp(self) :
		self._toRemove = []
	def tearDown(self) :
		for f in self._toRemove :
			try :
				os.remove(f)
			except:
				raise

	def toRemove(self, name) :
		self._toRemove.append(name)

	def createFile(self, name, content, *args, **kw) :
		self.toRemove(name)
		with open(name, 'w', *args, **kw) as f :
			f.write(content)

	def test_add(self) :
		m = Municipis()
		m.add('08970','sjd','Sant Joan Despí')
		self.assertEqual('08970', m.postalCode('sjd'))
		self.assertEqual('Sant Joan Despí', m.fullName('sjd'))

	def test_missing(self) :
		m = Municipis()
		m.add('08970','sjd','Sant Joan Despí')
		try :
			m.postalCode('bad')
		except KeyError as e :
			self.assertEqual(e.args[0], 'bad')
		try :
			m.fullName('bad')
		except KeyError as e :
			self.assertEqual(e.args[0], 'bad')

	def test_iter_whenOne(self) :
		m = Municipis()
		m.add('08970','sjd','Sant Joan Despí')
		result = [(item.postalCode, item.short, item.fullName) for item in m]
		self.assertEqual(result,[
			('08970', 'sjd', 'Sant Joan Despí'),
			])

	def test_iter_whenMany_keepsOrder(self) :
		m = Municipis()
		m.add('08970','sjd','Sant Joan Despí')
		m.add('08830','sboill','Sant Boi de Llobregat')
		result = [(item.postalCode, item.short, item.fullName) for item in m]
		self.assertEqual(result,[
			('08970', 'sjd', 'Sant Joan Despí'),
			('08830', 'sboill', 'Sant Boi de Llobregat'),
		])

	def test_dumpStr(self) :
		m = Municipis()
		m.add('08970','sjd','Sant Joan Despí')
		m.add('08830','sboill','Sant Boi de Llobregat')
		self.assertEqual(m.dumpStr(),
			'08970\tsjd\tSant Joan Despí\n'
			'08830\tsboill\tSant Boi de Llobregat'
		)

	def test_load(self) :
		self.createFile("municipis.data",
			'08970\tsjd\tSant Joan Despí\n'
			'08830\tsboill\tSant Boi de Llobregat'
			)
		m = Municipis()
		m.load('municipis.data')
		result = [(item.postalCode, item.short, item.fullName) for item in m]
		self.assertEqual(result,[
			('08970', 'sjd', 'Sant Joan Despí'),
			('08830', 'sboill', 'Sant Boi de Llobregat'),
		])
	def test_load_withEmptyLines(self) :
		self.createFile("municipis.data",
			'08970\tsjd\tSant Joan Despí\n'
			'\n'
			'08830\tsboill\tSant Boi de Llobregat\n'
			)
		m = Municipis()
		m.load('municipis.data')
		result = [(item.postalCode, item.short, item.fullName) for item in m]
		self.assertEqual(result,[
			('08970', 'sjd', 'Sant Joan Despí'),
			('08830', 'sboill', 'Sant Boi de Llobregat'),
		])

	def test_load_withBadLines_failsAndRestoreValue(self) :
		self.createFile("municipis.data",
			'08970\tsjd\tSant Joan Despí\n'
			'uno\tdos\n'
			'08830\tsboill\tSant Boi de Llobregat\n'
			)
		m = Municipis()
		try :
			m.load('municipis.data')
			self.fail("Exception expected")
		except Municipis.FormatError as e :
			pass
		result = [(item.postalCode, item.short, item.fullName) for item in m]
		self.assertEqual(result,[
		])

	def test_load_withCommentedLines(self) :
		self.createFile("municipis.data",
			'08970\tsjd\tSant Joan Despí\n'
			' # a comment\n'
			'08830\tsboill\tSant Boi de Llobregat\n'
			)
		m = Municipis()
		m.load('municipis.data')
		result = [(item.postalCode, item.short, item.fullName) for item in m]
		self.assertEqual(result,[
			('08970', 'sjd', 'Sant Joan Despí'),
			('08830', 'sboill', 'Sant Boi de Llobregat'),
		])





def load_tests(loader, tests, ignore):
    tests.addTests(doctest.DocTestSuite(gestor.expedients))
    return tests


if __name__ == '__main__' :
	import sys
	sys.exit(unittest.main())


