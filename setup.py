#!/usr/bin/env python3
from setuptools import setup
import glob
import os

readme = open("README.md").read()

setup(
	name = "suro",
	version = "0.2",
	description = "GuifiBaix lightweight project manager",
	author = "David Garcia Garzon",
	author_email = "voki@canvoki.net",
	url = 'https://github.com/GuifiBaix/suro',
	long_description = readme,
	license = 'GNU General Public License v3 or later (GPLv3+)',
	packages=[
		'gestor',
		'fact',
		],
	entry_points=dict(
	    console_scripts=[
		'{0} = fact.{0}:main'.format(script)
		for script in (
		    os.path.splitext(os.path.basename(script_file))[0]
		    for script_file in glob.glob('fact/gb_*.py')
		)
	    ],
	),
	scripts=[
		'suro.py',
		'fact/movements.py',
		'fact/iban.py',
		] + glob.glob('fact/gb_*.py'), # Not yet runnable as installed scripts
	install_requires=[
		'markdown',
		'decorator',
		'qgmap',
		'yamlns',
		'reportlab',
		'python-stdnum',
		'b2btest',
		'consolemsg', # from b2btest
		'wavefile', # from b2btest
		'lxml', # from b2btest
		'csb43',
		'selenium',
		'GitPython',
#		'PySide',
		'emili',
		'click',
		'pyside6',
		'ldap3',
		'pydantic',
	],
	package_data = {
		'gestor': [
			'i18n/*.ts',
			'i18n/*.qm',
			'*.ui',
		],
    },
	test_suite = 'gestor',
	classifiers = [
		'Programming Language :: Python',
		'Programming Language :: Python :: 3',
		'Topic :: Multimedia',
		'Topic :: Scientific/Engineering',
		'Topic :: Software Development :: Libraries :: Python Modules',
		'Intended Audience :: Developers',
		'Intended Audience :: Science/Research',
		'Development Status :: 5 - Production/Stable',
		'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
		'Operating System :: OS Independent',
	],
)

