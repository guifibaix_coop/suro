#!/usr/bin/python3
import sys,os
from gb_ldap import *

def main(args=sys.argv) :
	import argparse

if __name__ == '__main__' :

	l = gb_ldap(host='ldap.guifibaix.net')

	l.debug = 0
	
	'''
	# Pruebas simples, listados y busquedas
	# Es suficiente con conexión LDAP Anonima
	
	res=l.connectLDAP() # Conexión anónima
	print(res)
	
	#print(l.listUsers())
	print(l.listUsers('YAML'))
	#print(l.listGroups())
	print(l.listGroups('YAML'))
	#print(l.searchUser('test'))
	print(l.searchUser('test','YAML'))
	
	res=l.disconnectLDAP()
	print(res)
	'''
	
	
	
	'''
	# Pruebas complejas, altas, modificaciones y bajas
	# Requiere conexión con LDAP usando credenciales
	
	# CREACION DE USUARIO	
	Parametros para la creación de un usuario:
		OU: Cornella | El-Prat | Esplugues | Sant-Boi | Sant-Coloma-de-Cervello | Sant-Joan-Despi
		Username: Nombre de usuario
		Password: Contraseña a asignar al usuario. Si se deja en blanco asigna 'Guifi01'
		Nombre: Nombre del usuario
		Apellidos: Apellidos del usuario
		Email: Email del usuario
		Telefono: Telefono de contacto del usuario
		NombreNodo: Nombre del nodo Guifi asignado
		Direccion: Dirección del usuario
		CodPostal: Código Postal
		Poblacion: Población actual
		Provincia: Provicia
		Idioma: Català | Castellano (Otros idiomas podrian añadirse, pero no estan contemplados aun... TODO)
		RutaFoto: Ruta completa del fichero de imagen JPEG a ajuntar al perfil del usuario
	

	# MODIFICACION DE USUARIO
	Parametros para la modificación de un usuario (es posible escoger que parametros modificar):
		Username: Nombre de usuario
		Password: Contraseña a asignar al usuario. Si se deja en blanco asigna 'Guifi01'
		Nombre: Nombre del usuario
		Apellidos: Apellidos del usuario
		Email: Email del usuario
		Telefono: Telefono de contacto del usuario
		NombreNodo: Nombre del nodo Guifi asignado
		Direccion: Dirección del usuario
		CodPostal: Código Postal
		Poblacion: Población actual
		Provincia: Provicia
		Idioma: Català | Castellano (Otros idiomas podrian añadirse, pero no estan contemplados aun... TODO)
		RutaFoto: Ruta completa del fichero de imagen JPEG a ajuntar al perfil del usuario

	# ELIMINACION DE USUARIO	
	Parametros para la eliminación de un usuario:
		Username: Nombre de usuario		
		
	'''	
	
	res=l.connectLDAP('admin','Guifi@2013') # Conexión autentificada
	print(res)
	
	l.addUser('Cornella','prova','','Prova','Testing','prova@test.com','000000000','SJD-NODO','Direccion 1, 1-1','08970','Sant Joan Despi','Barcelona','Català','guifibaix-logo.jpg')		
	
	print(l.searchUser('prova'))
	
	l.editUser('prova','','Prova','Testing','prova@prova.com','111111111','SJD-KKKKKK','Direccion 2, 1-1','08970','Sant Joan Despi','Barcelona','Català','test.jpg')
	
	l.editUser(Username='prova',Email='prova@prova.com',Telefono='222222222',Direccion='Adreça 2, 1-1',Idioma='Castellano')
	
	l.editUser(Username='prova',RutaFoto='guifibaix-logo.jpg')
	
	l.changeUserPassword('prova','PROVA')
	
	print(l.searchUser('prova','YAML'))

	
	res=l.deleteUser('prova')
	print(res)

	res=l.disconnectLDAP()
	print(res)
	
	
	
