#!/usr/bin/python3
import sys,os
from consolemsg import success,warn, step, fail
import contextlib
import ldap3
from ldap3.utils.hashed import hashed
import json
import yaml


class gb_ldap(object):

	def getdebug(self):
		return self.__debug

	def setdebug(self, debug):
		self.__debug = debug

	debug = property(getdebug, setdebug)


	# -----------------------------------------------------------------------------------
	def __init__ (self,baseDN="DC=guifibaix,DC=net",host="10.1.40.6"):
		self.baseDN = baseDN
		self.host = host
		self.connection = ""


	# -----------------------------------------------------------------------------------
	def connectLDAP(self,ldapUsername="",ldapPassword="") :
		try:
			if self.debug :
				step("USER: " + ldapUsername)
				#step("PASSWD: " + ldapPassword)
				
			if ldapUsername != "" :
				ldapUsername = "cn=" + ldapUsername + "," + self.baseDN
			
			s = ldap3.Server(self.host, use_ssl=False, get_info=ldap3.ALL)
			# Solo necesitamos pasar credenciales en el caso de necesitar realizar alguna modificacion,
			# si no, no es necesario, acceso de lectura tendremos siempre.
			connection = ldap3.Connection(s,user = ldapUsername, password = ldapPassword, version=3, read_only=False)
			#connection = ldap3.Connection(s, version=3, read_only=False)

			# Realizamos el bind, para poder abrir el socket contra el servidor LDAP
			if not connection.bind():
				fail('\r\n-------------------------------\r\ERROR: Implosible realizar el BIND: ' + connection.result)
				return connection.result

			# Si todo va bien almacenamos la conexión para utilizarla posteriormente
			self.connection = connection
			
			return connection.result
			
		except AttributeError:
			fail('\r\n-------------------------------\r\ERROR:')
		except	ldap3.core.exceptions.LDAPSocketOpenError:
			fail('\r\n-------------------------------\r\ERROR: No es posible conectar a ' + self.host)
		except ldap3.LDAPExceptionError:
			fail('\r\n-------------------------------\r\ERROR:', self.connection.result)


	# -----------------------------------------------------------------------------------
	def disconnectLDAP(self) :
		try:
		
			return self.connection.unbind()
		
		except ldap3.LDAPExceptionError:
		   print('\r\n-------------------------------\r\nEXCEPTION ERROR:', self.connection.result)	


	# -----------------------------------------------------------------------------------
	def listUsers(self,resformat="JSON") :
		try:
			if self.debug :
				success("\r\n\r\nLista de usuarios\r\n")
				success("--------------------------------------------------------\r\n")
			
			self.connection.search(search_base = self.baseDN,
					search_filter = "(objectClass=inetOrgPerson)",
					search_scope = ldap3.SUBTREE,
					attributes = ["cn", "uid", "mail","telephoneNumber","postalAddress","postalCode","l","preferredLanguage","uidNumber","gidNumber","employeeNumber"])	
				
			'''
			total_entries=0					
			total_entries += len(self.connection.response)
			for entry in self.connection.response:
				#print(entry['dn'], entry['attributes'])
			
			'''
			
			jsonres=self.connection.response_to_json()
			
			if self.debug :
				print(jsonres)
			
			if resformat == "YAML" :
				l=yaml.load(json.dumps(json.loads(jsonres)))
				yamlres=yaml.dump(l,allow_unicode=True,default_flow_style=False)

				return yamlres
			
			
			return ldap3.utils.conv.to_unicode(jsonres)
			
		except ldap3.LDAPExceptionError:
		   fail("\r\n-------------------------------\r\nEXCEPTION ERROR:", self.connection.result)


	# -----------------------------------------------------------------------------------
	def listGroups(self,resformat="JSON") :	
		try:
			
			if self.debug :
				success("\r\n\r\nLista de grupos y sus miembros\r\n")
				success("--------------------------------------------------------\r\n")
			
			self.connection.search(search_base = self.baseDN,
					search_filter = '(objectClass=groupOfNames)',
					search_scope = ldap3.SUBTREE,
					attributes = ['cn','member'])	
			
			'''
			total_entries = 0
			total_entries += len(self.connection.response)
			for entry in self.connection.response:
				print(entry['dn'],entry['attributes'])	
			'''
			
			jsonres=self.connection.response_to_json()
			
			if self.debug :
				print(jsonres)
			
			if resformat == "YAML" :
				l=yaml.load(json.dumps(json.loads(jsonres)))
				yamlres=yaml.dump(l,allow_unicode=True,default_flow_style=False)

				return yamlres
			
			
			return ldap3.utils.conv.to_unicode(jsonres)
			
			
		except ldap3.LDAPExceptionError:
		   fail('\r\n-------------------------------\r\nEXCEPTION ERROR:', self.connection.result)


	# -----------------------------------------------------------------------------------
	def searchUser(self,Username,resformat="JSON") :
		try:
			if self.debug :
				success("\r\n\r\nBuscar Usuario " + Username + "\r\n")
				success("--------------------------------------------------------\r\n")
			
			self.connection.search(search_base = self.baseDN,
					search_filter = '(&(uid='+Username+')(objectClass=inetOrgPerson))',
					search_scope = ldap3.SUBTREE,
					attributes = ["cn", "uid", "mail","telephoneNumber","postalAddress","postalCode","l","preferredLanguage","uidNumber","gidNumber","employeeNumber"])	
				

			jsonres=self.connection.response_to_json()
			
			if self.debug :
				print(resformat)
			
			if resformat == "YAML" :
				l=yaml.load(json.dumps(json.loads(jsonres)))
				yamlres=yaml.dump(l,allow_unicode=True,default_flow_style=False)

				return yamlres
			
			
			return ldap3.utils.conv.to_unicode(jsonres)
			
		except ldap3.LDAPExceptionError:
		   fail("\r\n-------------------------------\r\nEXCEPTION ERROR:", self.connection.result)


	# -----------------------------------------------------------------------------------
	def addUser(self,OU,Username,Password,Nombre,Apellidos,Email,Telefono,NombreNodo,Direccion,CodPostal,Poblacion,Provincia,Idioma,RutaFoto) :
		try:
			if self.debug :
				print("-- AÑADIENDO USUARIO: " + Username)
			# Buscamos en todos los usuarios para calcular el UIDNUMBER siguiente y el EMPLOYEEID siguiente
			total_entries=0
			
			self.connection.search(search_base = self.baseDN,
					search_filter = '(objectClass=inetOrgPerson)',
					search_scope = ldap3.SUBTREE,
					attributes = ['uidNumber','employeeNumber'])	
			
			total_entries += len(self.connection.response)
			max_uidNumber = 0
			max_employeeNumber = 0
			
			for entry in self.connection.response:
				if entry['attributes']['uidNumber'] > max_uidNumber:
					max_uidNumber = entry['attributes']['uidNumber']
				if int(entry['attributes']['employeeNumber']) > max_employeeNumber:
					max_employeeNumber = int(entry['attributes']['employeeNumber'])

			next_uidNumber = max_uidNumber + 1
			next_employeeNumber = max_employeeNumber + 1		
			#gidNumber = 5000 # 5000 para usuario estandar, 500 para usuario cooperativa
			
			# Una vez tenemos los IDs, podemos crear al nuevo usuario
			
			if Password == "" : Password = "Guifi01"
			# Codificamos el password por defecto ---> TODO: Testear con SHA2 por temas de seguridad
			PWD_hash = ldap3.utils.hashed.hashed(ldap3.HASHED_MD5, Password)
			
			DN_Base_Usuarios = 'ou=Usuarios,' + self.baseDN
			OU_Grupo_Usuarios = 'ou='+OU+',' + DN_Base_Usuarios
			CN_USER = 'cn=' + Nombre + ' ' + Apellidos + ',' + OU_Grupo_Usuarios
			
			# Leemos el fichero JPG con la imagen para insertar el binario en el atributo de LDAP
			# TODO: Comprombar que el fichero exista
			if RutaFoto == "" :
				RutaFoto = "guifibaix-logo.jpg"
				
			jpgBin = open(RutaFoto,"rb").read()
			
			if self.debug :
				print("\r\n\r\n--------------------------------------------------------------------------")
				print(CN_USER)
			
			self.connection.add(CN_USER,
				['inetOrgPerson', 'posixAccount', 'top'], 
				{
				'uid': Username,
				'userPassword': PWD_hash,
				'cn': Nombre + ' ' + Apellidos,
				'givenName': Nombre,
				'displayName': Nombre,
				'sn': Apellidos,
				'mail': Email,
				'telephoneNumber': Telefono,
				'destinationIndicator': NombreNodo,
				'postalAddress': Direccion,
				'postalCode': CodPostal,
				'l': Poblacion,
				'st': Provincia,
				'preferredLanguage': Idioma,
				'uidNumber': next_uidNumber,
				'gidNumber': 5000,
				'employeeType': 2,
				'employeeNumber': next_employeeNumber,
				'loginShell': '/bin/sh',
				'homeDirectory': '/home/users/' + Username,
				'jpegPhoto': jpgBin
				})
			
			#content = dir(ldap3)
			#print (content)
			
			resuser=self.connection.result
			
			if self.debug :
				print("--------------------------------------------------------")
				print(self.connection.result)
				print("--------------------------------------------------------\r\n")
				
			DN_Base_Grupos = 'ou=Grupos,' + self.baseDN
			CN_GRUPO_GUIFIBAIX = 'cn=Usuarios-GuifiBaix,' + DN_Base_Grupos
			CN_GRUPO_MEDIATECA = 'cn=Acceso-MEDIATECA,' + DN_Base_Grupos
			CN_GRUPO_OWNCLOUD = 'cn=Acceso-OWNCLOUD,' + DN_Base_Grupos

			# Segun la OU seleccionada, se asigna al usuario a un grupo de Población específico
			if OU == 'Cornella': CN_GRUPO_POBLACION = 'cn=Usuarios-Cornella,' + DN_Base_Grupos
			elif OU == 'El-Prat': CN_GRUPO_POBLACION = 'cn=Usuarios-ElPrat,' + DN_Base_Grupos
			elif OU == 'Esplugues': CN_GRUPO_POBLACION = 'cn=Usuarios-Esplugues,' + DN_Base_Grupos
			elif OU == 'Sant-Boi': CN_GRUPO_POBLACION = 'cn=Usuarios-SantBoi,' + DN_Base_Grupos
			elif OU == 'Sant-Coloma-de-Cervello': CN_GRUPO_POBLACION = 'cn=Usuarios-SantColomaCervello,' + DN_Base_Grupos
			elif OU == 'Sant-Joan-Despi': CN_GRUPO_POBLACION = 'cn=Usuarios-SantJoanDespi,' + DN_Base_Grupos
			
			if self.debug :
				print("\r\n\r\n--------------------------------------------------------------------------\r\n")
				print(CN_GRUPO_GUIFIBAIX)
				print(CN_GRUPO_MEDIATECA)
				print(CN_GRUPO_OWNCLOUD)
				print(CN_GRUPO_POBLACION)

			# Modificamos el array member de cada grupo para añadir al nuevo usuario
			self.connection.modify(CN_GRUPO_GUIFIBAIX, {'member': [(ldap3.MODIFY_ADD, CN_USER )]})
			self.connection.modify(CN_GRUPO_MEDIATECA, {'member': [(ldap3.MODIFY_ADD, CN_USER )]})
			self.connection.modify(CN_GRUPO_OWNCLOUD, {'member': [(ldap3.MODIFY_ADD, CN_USER )]})
			self.connection.modify(CN_GRUPO_POBLACION, {'member': [(ldap3.MODIFY_ADD, CN_USER )]})
			
			if self.debug :
				print("--------------------------------------------------------")
				print(self.connection.result)
				print("--------------------------------------------------------\r\n")
			
			return resuser
			
		except ldap3.LDAPExceptionError:
		   fail('\r\n-------------------------------\r\nEXCEPTION ERROR:', self.connection.result)


	# -----------------------------------------------------------------------------------
	def editUser(self,Username,Password="",Nombre="",Apellidos="",Email="",Telefono="",NombreNodo="",Direccion="",CodPostal="",Poblacion="",Provincia="",Idioma="",RutaFoto="") :
		try:	
			if self.debug :
				print("-- MODIFICANDO USUARIO: " + Username)

				
			self.connection.search(search_base = self.baseDN,
					search_filter = '(&(uid='+Username+')(objectClass=inetOrgPerson))',
					search_scope = ldap3.SUBTREE,
					attributes = ['cn'])
			
			CN_USER=self.connection.response[0]['dn']

			if self.debug :
				print("\r\n\r\n--------------------------------------------------------------------------")
				print(CN_USER)
				
			# Control de variables asignadas
			
			if Password != "" : 
				# Codificamos el password por defecto ---> TODO: Testear con SHA2 por temas de seguridad
				PWD_hash = ldap3.utils.hashed.hashed(ldap3.HASHED_MD5, Password)
				self.connection.modify(CN_USER,{'userPassword': [(ldap3.MODIFY_REPLACE, [PWD_hash])]})
			
			if Nombre != "" :
				self.connection.modify(CN_USER,{'givenName': [(ldap3.MODIFY_REPLACE, [Nombre])],'displayName': [(ldap3.MODIFY_REPLACE, [Nombre])]})
				
			if Apellidos != "" :
				self.connection.modify(CN_USER,{'sn': [(ldap3.MODIFY_REPLACE, [Apellidos])]})
			
			if Nombre != "" and Apellidos != "":
				self.connection.modify(CN_USER,{'cn': [(ldap3.MODIFY_REPLACE, [Nombre + ' ' + Apellidos])]})
			
			if Email != "":
				self.connection.modify(CN_USER,{'mail': [(ldap3.MODIFY_REPLACE, [Email])]})
			
			if Telefono != "":
				self.connection.modify(CN_USER,{'telephoneNumber': [(ldap3.MODIFY_REPLACE, [Telefono])]})
			
			if NombreNodo != "":
				self.connection.modify(CN_USER,{'destinationIndicator': [(ldap3.MODIFY_REPLACE, [NombreNodo])]})
			
			if Direccion != "":
				self.connection.modify(CN_USER,{'postalAddress': [(ldap3.MODIFY_REPLACE, [Direccion])]})
		
			if CodPostal != "":
				self.connection.modify(CN_USER,{'postalCode': [(ldap3.MODIFY_REPLACE, [CodPostal])]})
			
			if Poblacion != "":
				self.connection.modify(CN_USER,{'l': [(ldap3.MODIFY_REPLACE, [Poblacion])]})
			
			if Provincia != "":
				self.connection.modify(CN_USER,{'st': [(ldap3.MODIFY_REPLACE, [Provincia])]})
				
			if Idioma != "":
				self.connection.modify(CN_USER,{'preferredLanguage': [(ldap3.MODIFY_REPLACE, [Idioma])]})
			
			if RutaFoto != "" :
				# Leemos el fichero JPG con la imagen para insertar el binario en el atributo de LDAP
				# TODO: Comprombar que el fichero exista
				jpgBin = open(RutaFoto,"rb").read()
				self.connection.modify(CN_USER,{'jpegPhoto': [(ldap3.MODIFY_REPLACE, [jpgBin])]})

				
			if self.debug :
				print("--------------------------------------------------------")
				print(self.connection.result)
				print("--------------------------------------------------------\r\n")

			return self.connection.result			
			
		except ldap3.LDAPExceptionError:
		   fail('\r\n-------------------------------\r\nEXCEPTION ERROR:', self.connection.result)

		   
	# -----------------------------------------------------------------------------------
	def deleteUser(self,Username) :
		try:	
			if self.debug :
				print("-- ELIMINANDO USUARIO: " + Username)

			self.connection.search(search_base = self.baseDN,
					search_filter = '(&(uid='+Username+')(objectClass=inetOrgPerson))',
					search_scope = ldap3.SUBTREE,
					attributes = ['cn'])	
					
			
			CN_USER=self.connection.response[0]['dn']
			
			if self.debug :
				print("\r\n\r\n--------------------------------------------------------------------------")
				print(CN_USER)
			
			self.connection.delete(CN_USER)
			
			if self.debug :
				print("--------------------------------------------------------")
				print(self.connection.result)
				print("--------------------------------------------------------\r\n")
				
			return self.connection.result
			
		except ldap3.LDAPExceptionError:
		   fail('\r\n-------------------------------\r\nEXCEPTION ERROR:', self.connection.result)


	# -----------------------------------------------------------------------------------
	def changeUserPassword(self,Username,NewPassword) :	
		try:
			if self.debug :
				print("-- CAMBIO PASSWORD USUARIO: " + Username)
				
			return self.editUser(Username=Username,Password=NewPassword)
			
		except ldap3.LDAPExceptionError:
		   fail('\r\n-------------------------------\r\nEXCEPTION ERROR:', self.connection.result)



