#!/usr/bin/env python3

"""
- Timer: Which indicators should be enabled
- Options:
	- Auto-fetch: Fetch if remote commits are available
	- Auto-merge: Merge if remote commits available, unmerged and no conflict is created
		- Warn on auto-merge
	- Auto-push: Push if local commits and network is available
	- On open/close:
		- Unconfirmed changes
		- Unpushed commits
		- Unfetched commits
		- Unmerged commits

- Display: Network status: Online/Offline
- Display: Uncommited changes
	- Action: Commit changes
		- Dialog alowing:
			- Show tracked/untracked/ignored
			- Select changes (files) to commit
			- Select all changes to be commited
			- Inspect the changed lines of a file
			- Edit summary: 'Resumeix aquests canvis amb una frase'
			- Edit description: 'Si calgués, escriu aclaracions addicionals sobre els canvis'
			- Confirm/Cancel
				- If online, propose to upload them (Upload/Defer)
				- If offline, warn the user
			- Revert changes
			- Ignore further changes
- Display: Commits pending to upload
	- Action: Upload commits
		- Progress
		- If upstream has changes go to unapplied remote changes
- Display: Unapplied remote changes
	- Action: Apply remote commits
		- Show:
			- Remote commits to apply
			- Local commits to rebase
			- Local changes to temporary stash
			- Posible conflicts
		- Confirm
	- Action: Inspect remote commits
- Display: Conflicting incomming changes
	- Action: Inspect conflicts
	- Action: Resolve conflicts, How??
	- Action: Ignore my changes

isFetchRequired = fetchedRemoteHead() is not remoteHead()
rebaseRequired = commonAncestor() is not remoteHead()
pushRequired = commonAncestor() is not localHead()




- Si no existe el repositorio clonarlo
- Bajar los cambios remotos
- Aplicar los cambios remotos

- Does the repository exists?
- Clone the remote repository
- Has pending changes to commit
- See pending changes
- Commit changes
- Has pending commits to push
- See commits
- Push commits
- Has remote commits to pull
- See remote commits
- Pull commits
"""

import os
import git
from consolemsg import fail, step

def repositoryBase() :
	import appdirs
	return os.path.join(
		appdirs.user_data_dir(
			appauthor='guifibaix',
			appname='suro',
			),
			'repos',
		)
def repositoryPath(name):
	return os.path.join(repositoryBase(), name)

class Repository(object):

	def __init__(self, path):
		self.path = path
		self.repo = None
		try:
			self.repo = git.Repo(path)
		except git.NoSuchPathError:
			pass

	def exists(self) :
		# TODO: turn it into classmethod
		return os.access(os.path.join(self.path, '.git'), os.R_OK)

	@classmethod
	def fullDownload(self, path, remote):
		return git.Repo.clone_from(
			url=remote,
			to_path=path,
			)

	def lastFetchableCommit(self):
		# TODO: When offline
		# TODO: Deduce 'master' bit from followed branch
		remoteUrl = self.repo.remote().url
		try:
			return self.repo.git.ls_remote(remoteUrl, 'master').split()[0]
		except git.GitCommandError as e:
			return None

	@property
	def remoteHead(self):
		# TODO: Cache the value if it gets too slow
		return 'remotes/{}/HEAD'.format(self.repo.remote().name)

	def isFetchRequired(self):
		lastFetched = self.repo.commit(self.remoteHead).hexsha
		lastFetchable = self.lastFetchableCommit()
		if lastFetchable is None: return None
		return lastFetchable != lastFetched

	def fetch(self):
		self.repo.git.fetch()

	def isRemoteAhead(self):
		remoteRevision = self.repo.commit(self.remoteHead).hexsha
		return remoteRevision not in (
			c.hexsha for c in self.repo.iter_commits() )

	def isDirty(self) :
		return self.repo.is_dirty()

	def isLocalAhead(self):
		head = self.repo.commit('HEAD')
		commonAncestor = self.repo.merge_base('HEAD', self.remoteHead)[0]
		return head.hexsha != commonAncestor.hexsha

	def uncommited(self) :
		return [
			(line[:2], line[3:].strip())
			for line in self.repo.git.status('--porcelain').splitlines()
		]

	def hasUntracked(self):
		return not not self.repo.untracked_files


	def changed(self):
		"""Lists how tracked files in working dir has changed
		in reference to HEAD"""
		return [
			('R', d.rename_from, d.rename_to)
				if d.renamed else
			('A', d.b_blob.path)
				if d.new_file else
			('D', d.a_blob.path)
				if d.deleted_file else
			('M', d.a_blob.path)
			for d in self.repo.head.commit.diff(None)
			] + [
			('?', filename) 
			for filename in self.repo.untracked_files
			]

	def undo(self, filename):
		"""Undoes changes in working dir and index"""
		if filename in (
				f.path for f in self.repo.head.commit.tree.traverse()) :
			output = self.repo.git.rm(filename,'--cached')
#			self.repo.git.checkout('HEAD',filename)
			return
		if os.access(os.path.join(self.repo.working_dir, filename), os.F_OK):
			self.repo.git.rm('--cached', filename)
			return
		raise git.GitCommandError()
		print("THECASE")

		output = self.repo.git.checkout('HEAD',filename)



import unittest
import shutil
class RepositoryPath_Test(unittest.TestCase):

	def test_repositoryBase_linux(self) :
		self.assertEqual(
			os.path.join(
				os.environ['HOME'],
				'.local',
				'share',
				'suro',
				'repos',
			),
			repositoryBase())

	def test_repositoryPath(self) :
		self.assertEqual(
			os.path.join(repositoryBase(), 'privat'),
			repositoryPath('privat'))

class Repository_Test(unittest.TestCase):

	def removable(self, filename) :
		self.toDelete.append(filename)
		return filename

	def edit(self, repo, filename, content, message=None):
		repofilename = os.path.join(repo.working_dir, filename)
		with open(repofilename, 'a') as edited:
			edited.write(content)
		if message is None: return
		repo.git.add(filename)
		if message is True: return
		repo.git.commit(filename,m=message)

	def localEdit(self, filename, content, message=None):
		repo = git.Repo(self.path)
		self.edit(self.local, filename, content, message)

	def remoteEdit(self, filename, content, message=None):
		self.edit(self.remote, filename, content, message)

	def setUp(self):
		self.path='gitrepo'
		self.remotePath='gitreporemote'
		self.toDelete = [
			self.path,
			self.remotePath,
		]
		for toDelete in self.toDelete:
			if os.path.isdir(toDelete):
				shutil.rmtree(toDelete)
		repo = git.Repo.init(self.remotePath)
		self.edit(repo, 'existing','line1','first commit')
		self.edit(repo, 'existing','line2','second commit')
		git.Repo.clone_from(url=self.remotePath, to_path=self.path)
		self.local = git.Repo(self.path)
		self.remote = git.Repo(self.remotePath)

	def tearDown(self):
		for toDelete in self.toDelete:
			if os.path.isdir(toDelete):
				shutil.rmtree(toDelete)

	def test_exists_whenItDoes(self):
		r = Repository(self.path)
		self.assertTrue(r.exists())

	def test_exists_whenNot(self):
		shutil.rmtree(self.path)
		r = Repository(self.path)
		self.assertFalse(r.exists())

	def test_exists_whenNotARepo(self):
		r = Repository(self.path)
		shutil.rmtree(os.path.join(self.path,'.git'))
		self.assertFalse(r.exists())

	def test_clone(self):
		shutil.rmtree(self.path)
		Repository.fullDownload(self.path, self.remotePath)
		r = Repository(self.path)
		self.assertTrue(r.exists())
		self.assertEqual(
			r.repo.remote('origin').url,
			os.path.abspath(self.remotePath))

	def test_lastFetchableCommit(self) :
		self.localEdit('myfile', 'line1')
		self.remoteEdit('otherfile', 'line1')
		lastFetch = self.remote.commit('HEAD').hexsha
		r = Repository(self.path)
		self.assertEqual(lastFetch, r.lastFetchableCommit())

	def test_isFetchRequired_whenInSync(self):
		r = Repository(self.path)
		self.assertFalse(r.isFetchRequired())

	def test_isFetchRequired_whenRemoteAhead(self):
		self.remoteEdit('otherfile', 'line1', 'remote change')
		self.localEdit('localfile', 'line1', 'local change')
		r = Repository(self.path)
		self.assertTrue(r.isFetchRequired())

	def test_isFetchRequired_afterFetch(self):
		self.remoteEdit('otherfile', 'line1', 'remote change')
		self.localEdit('localfile', 'line1', 'local change')
		r = Repository(self.path)
		r.fetch()
		self.assertFalse(r.isFetchRequired())

	@unittest.skip("not yet implemented")
	def test_isFetchRequired_ifOffline(self):
		pass

	def test_remoteAhead_whenInSynch(self):
		r = Repository(self.path)
		self.assertFalse(r.isRemoteAhead())

	def test_remoteAhead_whenRemoteAhead(self):
		self.remoteEdit('myfile', 'line1', 'myfile added')
		r = Repository(self.path)
		r.fetch()
		self.assertTrue(r.isRemoteAhead())

	def test_remoteAhead_whenRemoteAheadUnfetched(self):
		self.remoteEdit('myfile', 'line1', 'myfile added')
		r = Repository(self.path)
		self.assertFalse(r.isRemoteAhead())

	def test_remoteAhead_whenLocalAhead(self):
		self.localEdit('myfile', 'line1', 'myfile added')
		r = Repository(self.path)
		r.fetch()
		self.assertFalse(r.isRemoteAhead())

	def test_remoteAhead_whenBothAhead(self):
		self.localEdit('myfile', 'line1', 'myfile added')
		self.remoteEdit('otherfile', 'line1', 'otherfile added')
		r = Repository(self.path)
		r.fetch()
		self.assertTrue(r.isRemoteAhead())

	def test_localAhead_whenInSynch(self):
		r = Repository(self.path)
		r.fetch()
		self.assertFalse(r.isLocalAhead())

	def test_localAhead_whenRemoteAhead(self):
		self.remoteEdit('othersfile', 'line1', 'othersfile added')
		r = Repository(self.path)
		r.fetch()
		self.assertFalse(r.isLocalAhead())

	def test_localAhead_whenLocalAhead(self):
		self.localEdit('myfile', 'line1', 'myfile added')
		r = Repository(self.path)
		r.fetch()
		self.assertTrue(r.isLocalAhead())

	def test_localAhead_whenBothAhead(self):
		self.localEdit('myfile', 'line1', 'myfile added')
		self.remoteEdit('otherfile', 'line1', 'otherfile added')
		r = Repository(self.path)
		r.fetch()
		self.assertTrue(r.isLocalAhead())

	def test_localAhead_whenLocalChanges(self):
		self.localEdit('myfile', 'line1')
		r = Repository(self.path)
		r.fetch()
		self.assertFalse(r.isLocalAhead())

	def test_changed_whenNoChange(self):
		r = Repository(self.path)
		self.assertEqual(r.changed(), [
			])

	def test_changed_whenExistingChanged(self):
		self.localEdit('existing', 'New line')
		r = Repository(self.path)
		self.assertEqual(r.changed(), [
			('M','existing'),
			])

	def test_changed_whenExistingChangedAndIndexed(self):
		self.localEdit('existing', 'New line', True)
		r = Repository(self.path)
		self.assertEqual(r.changed(), [
			('M','existing'),
			])

	def test_changed_whenUntracked(self):
		self.localEdit('myfile', 'line1')
		r = Repository(self.path)
		self.assertEqual(r.changed(), [
			('?','myfile'),
			])

	def test_changed_whenAdded(self):
		self.localEdit('myfile', 'line1', True)
		r = Repository(self.path)
		self.assertEqual(r.changed(), [
			('A','myfile'),
			])

	def test_changed_whenRemovedAndUntracked(self):
		repo = git.Repo(self.path)
		repo.git.rm('existing')
		r = Repository(self.path)
		self.assertEqual(r.changed(), [
			('D','existing'),
			])

	def test_changed_whenRemovedButStillTracked(self):
		repo = git.Repo(self.path)
		os.remove(os.path.join(repo.working_dir, 'existing'))
		r = Repository(self.path)
		self.assertEqual(r.changed(), [
			('D','existing'),
			])

	def test_changed_whenMoved(self):
		repo = git.Repo(self.path)
		repo.git.mv(
			os.path.join(repo.working_dir, 'existing'),
			os.path.join(repo.working_dir, 'newname'))

		r = Repository(self.path)
		self.assertEqual(r.changed(), [
			('R','existing', 'newname'),
			])

	def test_undo_whenExistingChanged(self):
		self.localEdit('existing', 'New line')
		r = Repository(self.path)
		r.undo('existing')
		self.assertEqual(r.changed(), [
			])

	@unittest.skip("Not implemented yet")
	def test_commit_whenExistingChanged(self):
		self.localEdit('existing', 'New line')
		r = Repository(self.path)
		r.commit("Commit message", 'existing')
		self.assertEqual(r.changed(), [
			])

	def test_undo_otherFilesNotUndone(self):
		self.localEdit('another', 'New line', True)
		self.localEdit('existing', 'New line')
		r = Repository(self.path)
		r.undo('existing')
		self.assertEqual(r.changed(), [
			('A', 'another'),
			])

	def test_undo_whenExistingChangedAndIndexed(self):
		self.localEdit('existing', 'New line', True)
		r = Repository(self.path)
		r.undo('existing')
		self.assertEqual(r.changed(), [
			])

	def test_undo_whenUntracked(self):
		self.localEdit('myfile', 'line1')
		r = Repository(self.path)
		with self.assertRaises(git.GitCommandError) as assertion:
			r.undo('myfile')
		self.assertEqual(assertion.exception.stderr,
			b'fatal: pathspec \'myfile\' did not match any files')
		self.assertEqual(r.changed(), [
			('?','myfile'),
			])

	def test_undo_whenAddedIndexed(self):
		self.localEdit('myfile', 'line1', True)
		r = Repository(self.path)
		r.undo('myfile')
		self.assertEqual(r.changed(), [
			('?','myfile'),
			])

	def test_undo_whenRemovedAndUntracked(self):
		repo = git.Repo(self.path)
		repo.git.rm('existing')
		r = Repository(self.path)
		r.undo('existing')
		self.assertEqual(r.changed(), [
			])

	def test_undo_whenRemovedButStillTracked(self):
		repo = git.Repo(self.path)
		os.remove(os.path.join(repo.working_dir, 'existing'))
		r = Repository(self.path)
		r.undo('existing')
		self.assertEqual(r.changed(), [
			])

	def test_undo_whenMoved(self):
		# TODO: review undo move behaviour
		repo = git.Repo(self.path)
		repo.git.mv(
			os.path.join(repo.working_dir, 'existing'),
			os.path.join(repo.working_dir, 'newname'))

		r = Repository(self.path)
		r.undo('existing')
		r.undo('newname')
		self.assertEqual(r.changed(), [
			('?', 'newname')
			])

	def test_undo_whenNothingToUndo(self):
		repo = git.Repo(self.path)

		r = Repository(self.path)
		r.undo('existing')
		self.assertEqual(r.changed(), [
			])

	def test_undo_badFile(self):
		repo = git.Repo(self.path)

		r = Repository(self.path)
		with self.assertRaises(git.GitCommandError) as assertion:
			r.undo('badfile')
		self.assertEqual(
			assertion.exception.stderr,
			b'fatal: pathspec \'badfile\' did not match any files')


	def test_uncommitted_whenNone(self):
		r = Repository(self.path)
		self.assertEqual(r.uncommited(), [
			])

	def test_uncommitted_whenUntracked(self):
		self.localEdit('myfile', 'line1')
		r = Repository(self.path)
		self.assertEqual(r.uncommited(), [
			('??', 'myfile')
			])

	def test_uncommitted_whenModified(self):
		self.localEdit('existing', 'line1')
		r = Repository(self.path)
		self.assertEqual(r.uncommited(), [
			(' M', 'existing')
			])

	def test_uncommitted_whenModicationIndexed(self):
		self.localEdit('existing', 'line1', True)
		r = Repository(self.path)
		self.assertEqual(r.uncommited(), [
			('M ', 'existing')
			])

	def test_uncommitted_whenNewFileAdded(self):
		self.localEdit('myfile', 'aline', True)
		r = Repository(self.path)
		self.assertEqual(r.uncommited(), [
			('A ', 'myfile')
			])

	def test_uncommitted_whenIgnored(self):
		self.localEdit('.gitignore', 'myfile', 'ignoring myfile')
		self.localEdit('myfile', 'line1')
		r = Repository(self.path)
		self.assertEqual(r.uncommited(), [
			])


"""
TODO:
+ untracked
+ changed when removed
+ changed when removed and still tracked
+ changed when renamed
- remoteFetch
+ undo changes
+ undo bad file
- commit
- rebase
- stash
- conflict resolution
- show diffs
"""

from PySide import QtGui, QtCore

class RepositoryToolBar(QtGui.QToolBar):
	def __init__(self, engine=None):
		super(RepositoryToolBar, self).__init__()

		self._watcher = QtCore.QFileSystemWatcher(self)
		step("Connecting signal")

		def trackedDirs(repository):
			def recursive(tree):
				return sum((recursive(st) for st in tree.trees), [tree.abspath])
			return recursive(repository.tree())
		
		step('\n'.join(trackedDirs(engine.repo)))
		self._watcher.directoryChanged.connect(self.updateStatus)
		repoPaths = trackedDirs(engine.repo)
		step("Adding Path {}".format(repoPaths))
		self._watcher.addPaths(repoPaths)
		step("Done")

		def addButton(text, icon=None, slot=None):
			a = QtGui.QAction(
				QtGui.QIcon.fromTheme(icon),
				text,
				self,
			)
			a.activated.connect(slot)
			self.addAction(a)
			return a

		def addStatusLabel(arg):
			w = QtGui.QLabel(arg)
			self._layout.addWidget(w)

		self._fetch = addButton(
			text = "Canvis dels companys\npendents de baixar",
			icon = "internet-disconnect",
			slot = self.onFetchButton,
			)

		self._fetch = addButton(
			text = "Canvis dels companys\npendents de baixar",
			icon = "go-down",
			slot = self.onFetchButton,
			)

		self._fetch = addButton(
			text = "Canvis dels companys\npendents de baixar",
			icon = "go-down",
			slot = self.onFetchButton,
			)
		self._merge = addButton(
			text = "Canvis dels companys no incorporats",
			icon = 'merge',
			slot = self.onMergeButton,
			)
		self._commit = addButton(
			text = "Canvis teus no confirmats",
			icon = 'checkbox',
			slot = self.onCommitButton,
			)
		self._push = addButton(
			text = "Canvis teus confirmats no pujats",
			icon = 'go-up',
			slot = self.onPushButton,
			)

		self._trackNew = addButton(
			text = "Afegeix control a un fitxer nou",
			icon = 'list-add',
			slot = self.onCommitButton,
			)

		self.setEngine(engine)

	def setEngine(self, engine):
		self._engine = engine
		self.updateStatus()

	def updateStatus(self, *args):
		print("Change detected! {}".format(args))
		isFetchRequired = self._engine.isFetchRequired()
		isOffline = isFetchRequired is None
		isRemoteAhead = self._engine.isRemoteAhead()
		isDirty = self._engine.isDirty()
		isLocalAhead = self._engine.isLocalAhead()
		hasUntracked = self._engine.hasUntracked()

		self._fetch.setVisible(isFetchRequired!=False)
		self._fetch.setIcon(QtGui.QIcon.fromTheme(
			'user-offline' if isFetchRequired is None else 'go-down'))
		self._fetch.setText(
			"No s'ha pogut connectar al servidor\nper consultar els canvis dels companys"
			if isFetchRequired is None else
			"Canvis dels companys\npendents de baixar"
			)
		self._merge.setVisible(isRemoteAhead)
		self._commit.setVisible(isDirty)
		self._push.setVisible(isLocalAhead)
		self._trackNew.setVisible(hasUntracked)

		if isFetchRequired or True:
			answer = QtGui.QMessageBox.question(
				self,
				"Sincronizando con el servidor",
				"Els teus companys han pujat canvis al servidor.\n"
				"Vols baixar-los ara?",
				QtGui.QMessageBox.No | QtGui.QMessageBox.Yes
				)
			if answer is QtGui.QMessageBox.Yes:
				self.onFetchButton()

	def onFetchButton(self):
		print("Fetching...")
		# TODO: Implicit fetching??
		# TODO: Check if on-line
		if self._engine.isFetchRequired():
			self._engine.fetch()
		# TODO: Call onMergeButton

	def onMergeButton(self):
		print("Merging...")
		# TODO: Inspect revisions to fetch
		# TODO: Check for possible conflicts
		# TODO: Stash uncommited
		# TODO: Do the actual merge

	def onCommitButton(self):
		print("Commiting...")
		d = UncommitedDialog(self, self._engine)
		d.show()
		# TODO: Show the modified files
		# TODO: Option to revert changes
		# TODO: Option to select files to commit
		# TODO: Option to add to control
		# TODO: Option to ignore or not files

	def onPushButton(self):
		print("Pushing...")
		# TODO: If fetch or merge is available then do it
		# TODO: Show the commits to upload

class UncommitedDialog(QtGui.QDialog):

	def __init__(self, parent, engine):
		super(UncommitedDialog, self).__init__(parent)
		self.setWindowTitle("Canvis no confirmats")
		self._engine = engine

		layout = QtGui.QVBoxLayout(self)


		layout.addWidget(QtGui.QLabel(
			"Els següents canvis locals no han estat confirmats:"))
		self.changeList = QtGui.QListWidget()
		self.fillChanges()
		layout.addWidget(self.changeList)

		commitLayout = QtGui.QHBoxLayout()
		layout.addLayout(commitLayout)
		self.commitMessage = QtGui.QLineEdit()
		self.commitMessage.setPlaceholderText(
			"Descripció dels canvis")
		commitLayout.addWidget(self.commitMessage)
		self.commitButton = QtGui.QPushButton(
			QtGui.QIcon.fromTheme('checkbox'),
			"Confirma els canvis",
			)
		self.commitButton.clicked.connect(self.doCommit)
		commitLayout.addWidget(self.commitButton)

		layout.addWidget(QtGui.QWidget())
		buttons = QtGui.QDialogButtonBox(
			QtGui.QDialogButtonBox.Close,
			)
		layout.addWidget(buttons)

	def doCommit(self):
		def error(message):
			QtGui.QMessageBox.critical(
				self,
				"No s'ha pogut confirmar els canvis",
				message,
				)
		if not self.commitMessage.text():
			return error(
				"Per a confirmar els canvis has de posar una descripció")
		for item in self.changeList.items(0):
			print (item.text())
		print("TODO: Commit not implemented yet")

	def fillChanges(self):
		statusTooltip = dict(
			R = 'Renombrat',
			D = 'Esborrat',
			A = 'Afegit',
			M = 'Modificat',
			)
		statusTooltip['?']='Sense vigilància'
		statusIcon = dict(
			R = 'edit-rename',
			D = 'list-remove',
			A = 'list-add',
			M = 'document-edit',
			)
		statusIcon['?']='unknown'
		self.changeList.setSelectionMode(
			QtGui.QListWidget.SelectionMode.SingleSelection)
		self.changeList.setAlternatingRowColors(True)
		for change in self._engine.changed():
			status = change[0]
			itemText = ' -> '.join(change[1:])
			item = QtGui.QListWidgetItem(
				QtGui.QIcon.fromTheme(statusIcon.get(status,"checkbox")),
				status+" "+itemText,
				)
			item.setToolTip(statusTooltip.get(status, "Indeterminat"))
			item.setCheckState(
				QtCore.Qt.CheckState.Checked
					if status in "MAD" else
				QtCore.Qt.CheckState.Unchecked)
			self.changeList.addItem(item)


def main():
	repositoriDir = sys.argv[1] if len(sys.argv) > 1 else os.getcwd()
	app = QtGui.QApplication([])
	w = QtGui.QDialog()
	l = QtGui.QHBoxLayout(w)
	rp = RepositoryToolBar(Repository(repositoriDir))
	l.addWidget(rp)
#	w = UncommitedDialog(None, Repository(repositoriDir))
	w.show()
	app.exec_()
	return


if __name__ == '__main__' :

	import sys
	if '--test' in sys.argv:
		unittest.TestCase.__str__ = unittest.TestCase.id
		sys.argv.remove('--test')
		sys.exit(unittest.main())

	sys.exit(main())




