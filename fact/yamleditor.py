#!/usr/bin/env python3

from PySide import QtCore, QtGui
from yamlns import namespace as ns
from yamlns.dateutils import Date
import datetime
from decimal import Decimal

class YamlFieldUnsupported(QtGui.QLineEdit):
	def __init__ (self, data, label):
		super(YamlFieldUnsupported, self).__init__(str(data[label]))
		self._data = data
		self._label = label
		self.textEdited.connect(self.updateData)

	def updateData(self):
		self._data[self._label] = ns.loads((self.text()))

class YamlFieldText(QtGui.QLineEdit):
	def __init__ (self, data, label):
		super(YamlFieldText, self).__init__(str(data[label]))
		self._data = data
		self._label = label
		self.textEdited.connect(self.updateData)

	def updateData(self):
		self._data[self._label] = str(self.text())

class YamlFieldInteger(QtGui.QLineEdit):
	def __init__ (self, data, label):
		super(YamlFieldInteger, self).__init__()
		self.setValidator(QtGui.QIntValidator())
		self.setText(str(data[label]))
		self._data = data
		self._label = label
		self.textEdited.connect(self.updateData)

	def updateData(self):
		self._data[self._label] = int(self.text())

class YamlFieldFloat(QtGui.QLineEdit):
	def __init__ (self, data, label):
		super(YamlFieldFloat, self).__init__()
		self.setValidator(QtGui.QDoubleValidator())
		self.setText(str(data[label]))
		self._data = data
		self._label = label
		self.textEdited.connect(self.updateData)

	def updateData(self):
		self._data[self._label] = Decimal(self.text())

class YamlFieldBool(QtGui.QPushButton):
	def __init__ (self, data, label):
		super(YamlFieldBool, self).__init__("Choose")
		self.setCheckable(True)
		self.setChecked(data[label])
		self._data = data
		self._label = label
		self.updateData()
		self.clicked.connect(self.updateData)

	def updateData(self):
		value = self.isChecked()
		self._data[self._label] = value
		self.setText("Yes" if value else "No")
		self.setIcon(QtGui.QIcon.fromTheme('dialog-ok-apply' if value else 'edit-delete'))
#		self.setIcon(QtGui.QIcon('squared-led-on' if value else 'squared-led-off'))
#		self.setStyleSheet("background-color: {}".format('greenyellow' if value else 'ligthred'))

class YamlFieldDate(QtGui.QDateEdit):
	def __init__ (self, data, label):
		date = QtCore.QDate(*data[label].timetuple()[:3])
		super(YamlFieldDate, self).__init__(date)
		self.setCalendarPopup(True)
		self.setDisplayFormat("yyyy-MM-dd")
		self._data = data
		self._label = label
		self.dateChanged.connect(self.updateData)

	def updateData(self):
		self._data[self._label] = Date(self.text())

class YamlFieldSub(QtGui.QWidget):
	def __init__ (self, data, label):
		super(YamlFieldSub, self).__init__()
		self._data = data
		self._label = label
		l = QtGui.QHBoxLayout()
		self.setLayout(l)
		self.readonly = QtGui.QLabel()
		l.addWidget(self.readonly)
		self.updateReadOnly()
		button = QtGui.QPushButton("Edit...")
		l.addWidget(button)
		button.clicked.connect(self.showSub)

	def updateReadOnly(self):
		self.readonly.setText(ns(self._data[self._label]).dump())
	def showSub(self):
		sub = YamlEditor(ns(self._data[self._label]))
		if not sub.exec_() : return
		self._data[self._label] = sub.data
		self.updateReadOnly()

class YamlFieldList(QtGui.QWidget):
	def __init__ (self, data, label):
		super(YamlFieldList, self).__init__()
		self._data = data
		self._label = label
		l = QtGui.QVBoxLayout()
		self.setLayout(l)
		self.datalayout = QtGui.QVBoxLayout()
		l.addLayout(self.datalayout)
		self.refreshItems()

		addButton = QtGui.QPushButton(QtGui.QIcon.fromTheme("list-add"), "Add")
		addButton.clicked.connect(self.add)
		l.addWidget(addButton)

	def refreshItems(self):

		def clearLayout(l):
			while True:
				item = l.takeAt(0)
				if item is None: break
				if item.widget():
					item.widget().deleteLater()
				if item.layout():
					clearLayout(item.layout())
					item.layout().deleteLater()

		clearLayout(self.datalayout)
		items = self._data[self._label]
		for i,item in enumerate(items):
			self.addItem(items, i)

	def addItem(self, data, key):
		subl = QtGui.QHBoxLayout()
		self.datalayout.addLayout(subl)
		editor = editorFor(data,key)
		subl.addWidget(editor)
		removeButton = QtGui.QPushButton(QtGui.QIcon.fromTheme("list-remove"), "")
		subl.addWidget(removeButton)
		removeButton.item = key
		removeButton.clicked.connect(self.remove)

	def remove(self):
		key = self.sender().item
		del self._data[self._label][key]
		self.refreshItems()

	def add(self):
		l = self._data[self._label]
		types=list(set(type(d) for d in l))
		if len(types)==1 and len(l)>len(types):
			l.append(types[0]())
			self.addItem( l, len(l)-1)
			return
		if len(types)==len(l): types = editorFor.types.keys()
		typename, ok = QtGui.QInputDialog.getItem(self,
			"Adding new Item",
			"Choose the type of the new item",
			[t.__name__ for t in types],
			editable = False,
			)
		if not ok: return
		choosentype = [ t for t in types if t.__name__ == typename ][0]
		l.append(choosentype())
		self.addItem(l, len(l)-1)


def editorFor(data,key):
	editorFor.types = {
		str: YamlFieldText,
		Decimal: YamlFieldFloat,
		bool: YamlFieldBool,
		list: YamlFieldList,
		int: YamlFieldInteger,
		datetime.date: YamlFieldDate,
		Date: YamlFieldDate,
		ns: YamlFieldSub,
		}
	editorClass = editorFor.types.get(
		type(data[key]), YamlFieldUnsupported)
	return editorClass(data, key)


class YamlEditor (QtGui.QDialog):
	def __init__ (self, data):
		super(YamlEditor, self).__init__()
		self.data = data
		l = QtGui.QVBoxLayout()
		self.setLayout(l)
		fl = QtGui.QFormLayout()
		l.addLayout(fl)

		for key, value in data.items():
			editor = editorFor(data, key)
			fl.addRow(key, editor)

		bb = QtGui.QDialogButtonBox()
		bb.setStandardButtons(
			QtGui.QDialogButtonBox.Ok | 
			QtGui.QDialogButtonBox.Cancel |
			0)
		bb.accepted.connect(self.accept)
		bb.rejected.connect(self.reject)
		l.addWidget(bb)



sample="""\
text: textValue
date: 2015-06-10
datetime: 2015-02-20 15:45:20
number: 342
yesbool: True
nobool: False
number: 34.32
list:
- 4
- 3
- canela
emptylist: []
homegeneusList:
- 4
- 5
sub:
  text: value2
  subsub:
    text: value1
unknown: ~
"""


if __name__ == '__main__':
	app = QtGui.QApplication([])
	import sys
	from consolemsg import fail
	data = ns.load(sys.argv[1]) if len (sys.argv)>1 else ns.loads(sample)
	w = YamlEditor(data)
	w.exec_() or fail("Dialogo cancelado")
	if len(sys.argv)>1:
		w.data.dump(sys.argv[1])
	else:
		print(w.data.dump())


