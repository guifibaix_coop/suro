import unittest
from pathlib import Path
from .invoice import Invoice, Client
from pydantic import BaseModel, ValidationError
from ..testutils import sandbox_dir

class MyTestCase(unittest.TestCase):
    from yamlns.testutils import assertNsContains, assertNsEqual
    maxDiff=None

    def assertValidationError(self, ctx, field, msg):
        self.assertEqual(ctx.exception.error_count(), 1, ctx.exception.errors())
        error = ctx.exception.errors()[0]
        self.assertNsContains(error, dict(msg=msg, loc=[field]))

    def assertMissingFields(self, ctx, fields):
        self.assertNsContains(
            dict(errors=ctx.exception.errors()),
            dict(errors=[
                dict(
                    msg="Field required",
                    loc=[field],
                    url="https://errors.pydantic.dev/2.10/v/missing",
                    type="missing",
                    input={},
                )
                for field in fields
            ])
        )

class Client_Test(MyTestCase):

    @classmethod
    def base_case(self, **overrides):
        data = dict(
            nif="12345678Z",
            expedient="sjd-verdaguer42",
            name="Mengano Zutano",
            address="Verdaguer, 42",
            city="Sant Joan Despí",
            postalcode="08970",
            state="Barcelona",
            iban="ES7714001234661234567890",
            bic="BICMYBANK",
        )
        return Client(**dict(data,**overrides))

    def test_required_fields(self):
        with self.assertRaises(ValidationError) as ctx:
            c = Client()
        self.assertMissingFields(ctx, [
                "nif",
                "expedient",
                "name",
                "address",
                "city",
                "postalcode",
                "state",
                "iban",
                "bic",
        ])

    def test_basecase(self):
        c = self.base_case()
        self.assertNsEqual(c.model_ns(), """\
            address: Verdaguer, 42
            bic: BICMYBANK
            city: Sant Joan Despí
            expedient: sjd-verdaguer42
            iban: ES7714001234661234567890
            name: Mengano Zutano
            nif: 12345678Z
            postalcode: 08970
            state: Barcelona
        """)


class Invoice_Test(MyTestCase):

    def base_case(self, **overrides):
        data = dict(
            year = '2000',
            number = 34,
            subject = 'ventas',
            issueDate = '2000-01-02',
            dueDate = '2000-03-03',
            client = Client_Test.base_case(),
            concepts = [],
        )
        return Invoice(**dict(data, **overrides))

    def test_required_fields(self):
        with self.assertRaises(ValidationError) as ctx:
            i = Invoice()
        self.assertMissingFields(ctx, [
            'year',
            'number',
            'subject',
            'issueDate',
            'dueDate',
            'client',
            'concepts',
        ])

    def test_basecase(self):
        i = self.base_case()
        self.assertNsEqual(i.model_ns(), """\
            client:
              address: Verdaguer, 42
              bic: BICMYBANK
              city: Sant Joan Despí
              expedient: sjd-verdaguer42
              iban: ES7714001234661234567890
              name: Mengano Zutano
              nif: 12345678Z
              postalcode: 08970
              state: Barcelona
            concepts: []
            dueDate: 2000-03-03
            issueDate: 2000-01-02
            number: 34
            subject: ventas
            year: 2000
        """)

    def test_invoiceId(self):
        i = self.base_case()
        self.assertEqual(i.invoiceId, "2000-0034")

    def test_invoiceIdDisplay(self):
        i = self.base_case()
        self.assertEqual(i.invoiceIdDisplay, "0034/2000")


    def test_totalBeforeTaxes(self):
        i = self.base_case(
            concepts=[
                "Ignored"
                "concept 1\t30.10",
                "concept 2\t69.90",
            ],
        )

        self.assertEqual(i.totalBeforeTaxes, 100.0)

    def test_total__defaultVat(self):
        i = self.base_case(
            concepts=[
                "Ignored"
                "concept 1\t30.10",
                "concept 2\t69.90",
            ],
        )

        self.assertEqual(i.total, 121.0)

    def test_total__specialVat(self):
        i = self.base_case(
            vatRate=0.1,
            concepts=[
                "Ignored"
                "concept 1\t30.10",
                "concept 2\t69.90",
            ],
        )

        self.assertEqual(i.total, 110.0)


    def test_filePatternById(self):
        self.assertEqual(
            Invoice.filePaternById("2000-0034"),
            'factures/factura-2000-0034-*.yaml'
        )

    def test_filePatternById_withSpecificKind(self):
        self.assertEqual(
            Invoice.filePaternById("2000-0034", kind="abonament"),
            'factures/abonament-2000-0034-*.yaml'
        )

    from contextlib import contextmanager
    @contextmanager
    def invoice_dir(self):
        with sandbox_dir() as temp:
            invoice_directory = Path("factures")
            invoice_directory.mkdir()
            yield invoice_directory

    def test_loadById(self):
        with self.invoice_dir() as invoice_dir:
            i = self.base_case()
            target = invoice_dir/f'factura-2000-0034-myexpedient.yaml'
            i.model_ns().dump(target)
            print(list(invoice_dir.glob('*')))

            loaded = Invoice.loadById(i.invoiceId)

            self.assertIsNotNone(loaded)
            self.assertNsEqual(
                i and i.model_ns(),
                loaded and loaded.model_ns(),
            )



# vim: ts=4 sw=4 et
