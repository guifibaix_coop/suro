from pydantic import BaseModel, AfterValidator
from typing import Annotated
import decimal
from .basics import Nif, Iban, PostalCode
from ..tresoreria import (
	invoiceNumberPadding,
)
import datetime
from yamlns import ns

class GBModel(BaseModel):
	def model_ns(self):
		return ns(**self.model_dump(
			exclude_defaults=True,
		))

class Person(GBModel):
	nif: Nif

class Client(Person):
	expedient: str
	name: str
	address: str
	city: str
	postalcode: PostalCode
	state: str
	iban: str # TODO validate iban
	bic: str

class Provider(Person):
	pass

class Invoice(GBModel):
	year: int
	number: int
	subject: str
	issueDate: datetime.date
	dueDate: datetime.date
	vatRate: decimal.Decimal = decimal.Decimal("0.21")
	client: Client
	provider: Provider | None = None
	concepts: list[str]
	draft: bool = False
	amends: str | None = None

	@property
	def invoiceId(self):
		return (
			'{year}-{number:0{numberPadding}}'
			.format(
				numberPadding=invoiceNumberPadding,
				**self.model_dump()))

	@property
	def invoiceIdDisplay(self):
		return (
			'{number:0{numberPadding}}/{year}'
			.format(
				numberPadding=invoiceNumberPadding,
				**self.model_dump()))

	@property
	def totalBeforeTaxes(self) -> decimal.Decimal:
		total = decimal.Decimal('0.00')
		for concept in self.concepts :
			total += decimal.Decimal(concept.split('\t')[1])
		return total

	@property
	def total(self) -> decimal.Decimal:
		return (
			self.totalBeforeTaxes * (decimal.Decimal(1) + self.vatRate)
			).quantize(decimal.Decimal('0.01'), rounding=decimal.ROUND_HALF_EVEN)

	@classmethod
	def filePaternById(cls, id: str, kind: str='factura') -> str:
		return 'factures/{}-{}-*.yaml'.format(kind, id)

	@classmethod
	def loadById(cls, id: str, kind: str='factura'): # -> Invoice
		pattern = cls.filePaternById(id, kind)
		from pathlib import Path
		results = list(Path().glob(pattern))
		if not results:
			return None
		return Invoice(**ns.load(results[0]))

# vim: ts=4 sw=4 et
