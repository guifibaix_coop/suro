from pydantic import AfterValidator
from typing import Annotated
import stdnum.es
import stdnum.iban

@AfterValidator
def _nif_is_valid(nif: str) -> str:
	assert stdnum.es.nif.is_valid(nif), f"Invalid NIF '{nif}'"
	return nif.upper() # TODO: clean it

@AfterValidator
def _postalcode_is_valid(postal_code: str) -> str:
	assert len(postal_code) == 5, \
		f"Invalid Postal Code '{postal_code}', length should be 5"

	assert all(x.isdigit() for x in postal_code), \
		f"Invalid Postal Code '{postal_code}', contains non-numbers"
	return postal_code

@AfterValidator
def _iban_is_valid(iban: str) -> str:
	assert stdnum.iban.is_valid(iban), f"Invalid IBAN '{iban}'"
	return ''.join([x for x in iban.upper().split()]) # TODO: clean it


Nif = Annotated[str, _nif_is_valid]

PostalCode = Annotated[str, _postalcode_is_valid]

Iban = Annotated[str, _iban_is_valid]


