import unittest
from .basics import Nif, Iban, PostalCode
from pydantic import BaseModel, ValidationError

class FieldTestCase(unittest.TestCase):
    from yamlns.testutils import assertNsContains, assertNsEqual

    def assertValidationError(self, ctx, field, msg):
        self.assertEqual(ctx.exception.error_count(), 1, ctx.exception.errors())
        error = ctx.exception.errors()[0]
        self.assertNsContains(error, dict(msg=msg, loc=[field]))


class PostalCode_Test(FieldTestCase):
    class Model(BaseModel):
        postalcode: PostalCode

    def test_postalcode__valid(self):
        o = self.Model(postalcode='12345')
        self.assertEqual(PostalCode("00000"), '00000')

    def test_postalcode__not_numbers__invalid(self):
        with self.assertRaises(ValidationError) as ctx:
            o = self.Model(postalcode='1234A')

        self.assertValidationError(ctx,
            field="postalcode",
            msg="Assertion failed, Invalid Postal Code '1234A', contains non-numbers"
        )

    def test_postalcode__short__invalid(self):
        with self.assertRaises(ValidationError) as ctx:
            o = self.Model(postalcode='1234')

        self.assertValidationError(ctx,
            field="postalcode",
            msg="Assertion failed, Invalid Postal Code '1234', length should be 5"
        )

class Nif_Test(FieldTestCase):
    from yamlns.testutils import assertNsContains, assertNsEqual

    class Model(BaseModel):
        nif: Nif

    def test_nif__valid(self):
        o = self.Model(nif='12345678Z')
        self.assertEqual(o.nif, '12345678Z')

    def test_nif__invalid(self):
        with self.assertRaises(ValidationError) as ctx:
            o = self.Model(nif='12345678W')

        self.assertValidationError(ctx,
            field="nif",
            msg="Assertion failed, Invalid NIF '12345678W'"
        )

    def test_nif__normalizes(self):
        # Notice the lower case 'z'
        o = self.Model(nif='12345678z')
        self.assertEqual(o.nif, '12345678Z')

class Iban_Test(FieldTestCase):
    from yamlns.testutils import assertNsContains, assertNsEqual

    class Model(BaseModel):
        iban: Iban

    def test_nif__valid(self):
        o = self.Model(iban='ES7712341234161234567890')
        self.assertEqual(o.iban, 'ES7712341234161234567890')

    def test_nif__invalid(self):
        with self.assertRaises(ValidationError) as ctx:
            o = self.Model(iban='12345678W')

        self.assertValidationError(ctx,
            field="iban",
            msg="Assertion failed, Invalid IBAN '12345678W'"
        )

    def test_nif__normalizes(self):
        # Notice the lower case 'es' and spaces
        o = self.Model(iban='es77 1234 1234 16 1234567890')
        self.assertEqual(o.iban, 'ES7712341234161234567890')

    

