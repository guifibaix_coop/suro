import unittest
from fact import movements
from yamlns import namespace as ns
import os

class Movements_Test(unittest.TestCase):
	__str__ = unittest.TestCase.id

	def assertNsEqual(self, data, expectation):
		expectation = expectation.expandtabs(2)
		self.maxDiff=None
		self.assertMultiLineEqual(
			ns(sorted(data.items())).dump(),
			ns(sorted(ns.loads(expectation).items())).dump()
			)

	def test_parseHeader_notEnoughData(self):
		with self.assertRaises(Exception) as ctx:
			movements.parseEntryHeader(4, ['2014-01-01'])
		self.assertEqual(str(ctx.exception),
			"Error a la linia 4: "
			"Els inicis de seient, contenen dos cel·les: data i descripció, s'han trobat 1 cel·les"
			)

	def test_parseHeader_tooMuchData(self):
		with self.assertRaises(Exception) as ctx:
			movements.parseEntryHeader(4, ['2014-01-01', 'description', 'garbage'])
		self.assertEqual(str(ctx.exception),
			"Error a la linia 4: "
			"Els inicis de seient, contenen dos cel·les: data i descripció, s'han trobat 3 cel·les"
			)

	def test_parseHeader_notADate(self):
		with self.assertRaises(Exception) as ctx:
			movements.parseEntryHeader(4, ['notadate', 'description'])
		self.assertEqual(str(ctx.exception),
			"Error a la linia 4: "
			"S'esperava la data en format iso d'un seient, pero s'ha llegit 'notadate'"
			)

	def test_parseHeader_properly(self):
		data = movements.parseEntryHeader(4, ['2010-01-02', 'a description'])
		self.assertNsEqual(data, """\
			date: 2010-01-02
			description: a description
			movements: []
			""")


	def test_parseMovement_wrongNumberOfCells(self):
		with self.assertRaises(Exception) as ctx:
			movements.parseMovement(10, 2*['cell'])
		self.assertEqual(str(ctx.exception),
			"Error a la linia 10: "
			"Els moviments de seient, contenen "
			"3 cel·les despres de la primera cel·la buida: "
			"quantitat, sentit i compte, "
			"pero s'han trobat 2 cel·les: ['cell', 'cell']."
			)

	def test_parseMovement_commasRejected(self):
		with self.assertRaises(Exception) as ctx:
			movements.parseMovement(10, ['34,23', '->', 'Account'])
		self.assertEqual(str(ctx.exception),
			"Error a la linia 10: "
			"La quantitat no esta en format numèric decimal anglosaxò. "
			"S'ha rebut '34,23'."
			)

	def test_parseMovement_badDirection(self):
		with self.assertRaises(Exception) as ctx:
			movements.parseMovement(10, ['34.23', '>', 'Account'])
		self.assertEqual(str(ctx.exception),
			"Error a la linia 10: "
			"El camp direcció d'un seient ha d'indicar-se "
			"amb una fletxa '->' o '<-'. "
			"S'ha rebut '>'."
			)

	def test_parseMovement_inMovement(self):
		data = movements.parseMovement(10, ['34.23', '->', 'Account'])
		self.assertNsEqual(data, """\
			amount: 34.23
			account: Account
			""")

	def test_parseMovement_strips(self):
		data = movements.parseMovement(10, ['   34.23   ', ' -> ', '   Account   '])
		self.assertNsEqual(data, """\
			amount: 34.23
			account: Account
			""")

	def test_parseMovement_outMovement(self):
		data = movements.parseMovement(10, ['34.23', '<-', 'Account'])
		self.assertNsEqual(data, """\
			amount: -34.23
			account: Account
			""")

	def test_parseMovement_withReference(self):
		data = movements.parseMovement(10, ['34.23', '<-', 'Account', 'areference'])
		self.assertNsEqual(data, """\
			amount: -34.23
			account: Account
			reference: areference
			""")

	def parseFile(self, content):
		# TODO: use a temporary
		with open('test.csv','w') as f:
			f.write(content)
		try:
			return movements.parseMovements('test.csv')
		finally:
			os.remove('test.csv')

	def test_parseMovements_emptyFile(self):
		data = self.parseFile("""\
			""")

		self.assertNsEqual(data, """\
			book: []
			""")

	# TODO: Not requiring closing empty line for the last entry
	
	def test_parseMovements_emptyEntry(self):
		data = self.parseFile(
			"2010-01-02\tAn entry\n"
			)

		self.assertNsEqual(data, """\
			book:
			- 
				date: 2010-01-02
				description: An entry
				movements: []
			""")

	def test_parseMovements_forwardedEntryHeaderErrors(self):
		with self.assertRaises(Exception) as ctx:
			self.parseFile(
				"notadate\tAn entry\n"
				)
		self.assertEqual(str(ctx.exception),
			"Error a la linia 1: "
			"S'esperava la data en format iso d'un seient, pero s'ha llegit 'notadate'"
			)

	def test_parseMovements_misingSeparationLine(self):
		with self.assertRaises(Exception) as ctx:
			self.parseFile(
				"2010-01-02\tAn entry\n"
				"2010-01-03\tAnother entry\n"
				)
		self.assertEqual(str(ctx.exception),
			"Error a la linia 2: "
			"Has de finalitzar el seient anterior amb una linia buida"
			)

	def test_parseMovements_toMovements(self):
		data = self.parseFile(
			"2010-01-02\tAn entry\n"
			"\n"
			"2010-01-03\tAnother entry\n"
			)

		self.assertNsEqual(data, """\
			book:
			- 
				date: 2010-01-02
				description: An entry
				movements: []
			- 
				date: 2010-01-03
				description: Another entry
				movements: []
			""")

	def test_parseMovements_errorsInDifferentLines(self):
		with self.assertRaises(Exception) as ctx:
			self.parseFile(
				"notadate\tAn entry\n"
				"\n"
				"notadateeither\tAnother entry\n"
				)
		self.assertEqual(str(ctx.exception),
			"Error a la linia 1: "
			"S'esperava la data en format iso d'un seient, pero s'ha llegit 'notadate'"
			"\n"
			"Error a la linia 3: "
			"S'esperava la data en format iso d'un seient, pero s'ha llegit 'notadateeither'"
			)

	def test_parseMovements_havingEntries(self):
		data = self.parseFile(
			"2010-01-02\tAn entry\n"
			"\t200.00\t<-\tFrom account\n"
			"\t200.00\t->\tInto account\n"
			)

		self.assertNsEqual(data, """\
			book:
			- 
				date: 2010-01-02
				description: An entry
				movements:
				- account: From account
				  amount: -200.00
				- account: Into account
				  amount: 200.00
			""")

	def test_parseMovements_umbalancedEntries(self):
		with self.assertRaises(Exception) as ctx:
			self.parseFile(
				"2010-01-02\tAn entry\n"
				"\t200.00\t<-\tFrom account\n"
				"\t100.00\t->\tInto account\n"
				)

		self.assertEqual(str(ctx.exception),
			"Error a la linia 3: "
			"El moviment no esta compensat, el balanç és -100.00"
			)

	def test_parseMovements_separatedEntryFails(self):
		with self.assertRaises(Exception) as ctx:
			self.parseFile(
				"2010-01-02\tAn entry\n"
				"\t200.00\t<-\tFrom account\n"
				"\t200.00\t->\tInto account\n"
				"\n"
				"\t300.00\t->\tInto account\n"
				)

		self.assertEqual(str(ctx.exception),
			"Error a la linia 5: "
			"S'esperava una capcelera de seient "
			"amb la data i la descripció com a primera i segona columna"
			)


	def test_parseMovements_ignoreEmptyCellsOnTheRight(self):
		data = self.parseFile(
			"2010-01-02\tAn entry \t\t\t  \t \n"
			"\t200.00\t<-\tFrom account\t\t  \t \n"
			"\t200.00\t->\tInto account\n"
			)

		self.assertNsEqual(data, """\
			book:
			- 
				date: 2010-01-02
				description: An entry
				movements:
				- account: From account
				  amount: -200.00
				- account: Into account
				  amount: 200.00
			""")

	def test_parseMovements_comments(self):
		data = self.parseFile(
			"2010-01-02\tAn entry # This is not part of the content\n"
			"\t200.00\t<-\tFrom account # Neither that\n"
			"\t200.00\t->\tInto account\n"
			)

		self.assertNsEqual(data, """\
			book:
			- 
				date: 2010-01-02
				description: An entry
				movements:
				- account: From account
				  amount: -200.00
				- account: Into account
				  amount: 200.00
			""")

	def test_parseMovements_ignoreCommentsAndEmptyCells(self):
		data = self.parseFile(
			"2010-01-02\tAn entry \t\t#Ignored content\n"
			"\t200.00\t<-\tFrom account\t\t#This is ignored too\t \n"
			"\t200.00\t->\tInto account\n"
			)

		self.assertNsEqual(data, """\
			book:
			- 
				date: 2010-01-02
				description: An entry
				movements:
				- account: From account
				  amount: -200.00
				- account: Into account
				  amount: 200.00
			""")


	def test_parseMovements_ignoreCommentsAndEmptyCells(self):
		data = self.parseFile(
			"2010-01-02\tAn entry\n"
			"\t200.00\t<-\tFrom account\n"
			"\t# Comment lines do not close entries\n"
			"\t200.00\t->\tInto account\n"
			)

		self.assertNsEqual(data, """\
			book:
			- 
				date: 2010-01-02
				description: An entry
				movements:
				- account: From account
				  amount: -200.00
				- account: Into account
				  amount: 200.00
			""")



	def test_balance_singleEntry(self):
		data = self.parseFile(
			"2010-01-02\tAn entry\n"
			"\t200.00\t<-\tFrom account\n"
			"\t200.00\t->\tInto account\n"
			)
		self.assertNsEqual(movements.balance(data), """\
			From account: -200.00
			Into account: 200.00
			""")
		
	def test_balance_manyEntries(self):
		data = self.parseFile(
			"2010-01-02\tAn entry\n"
			"\t200.00\t<-\tFrom account\n"
			"\t200.00\t->\tInto account\n"
			"\n"
			"2010-01-03\tAnother entry\n"
			"\t300.00\t<-\tFrom account\n"
			"\t300.00\t->\tInto other account\n"
			)
		self.assertNsEqual(movements.balance(data), """\
			From account: -500.00
			Into account: 200.00
			Into other account: 300.00
			""")


	def test_accountReferences_filtersUnreferenced(self):
		data = self.parseFile(
			"2010-01-02\tAn entry\n"
			"\t200.00\t<-\tAn account\tareference\n"
			"\t200.00\t->\tAn account\n"
			)
		references = movements.accountReferences(data,"An account")
		self.assertNsEqual(ns(references=references), """\
			references:
				- reference: areference
				  amount: -200.00
			""")

	def test_accountReferences_filtersOtherAccounts(self):
		data = self.parseFile(
			"2010-01-02\tAn entry\n"
			"\t200.00\t<-\tAn account\tareference\n"
			"\t200.00\t->\tOther account\tanotherreference\n"
			)
		references = movements.accountReferences(data,"An account")
		self.assertNsEqual(ns(references=references), """\
			references:
				- reference: areference
				  amount: -200.00
			""")

	def test_accountReferences_listMany(self):
		data = self.parseFile(
			"2010-01-02\tAn entry\n"
			"\t200.00\t<-\tAn account\tareference\n"
			"\t200.00\t->\tAn account\tanotherreference\n"
			)
		references = movements.accountReferences(data,"An account")
		self.assertNsEqual(ns(references=references), """\
			references:
				- reference: areference
				  amount: -200.00
				- reference: anotherreference
				  amount: 200.00
			""")







