import decimal
from fact.tresoreria import (
	appData,
	invoiceNumberPadding,
)
from consolemsg import warn, step, fail

def printInvoice(invoice, outputFilename, draft=None, debuglayout=None) :
	"""
	Receives a namespace representing an invoice and renders
	the printed invoice as pdf file.
	"""

	provider = invoice.provider
	client = invoice.client
	concepts = invoice.concepts

	if 'draft' not in invoice :
		invoice.draft = False

	if draft is not None:
		invoice.draft = draft

	if invoice.draft:
		warn("Afegint marca d'aigua d'esborrany")


	from reportlab.lib import colors
	from reportlab.lib.enums import TA_JUSTIFY, TA_RIGHT, TA_CENTER, TA_LEFT
	from reportlab.lib.pagesizes import A4
	from reportlab.platypus import SimpleDocTemplate, Paragraph, Image, Table
	from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
	from reportlab.lib.units import mm
	from reportlab.pdfbase import pdfmetrics
	from reportlab.pdfbase.ttfonts import TTFont, TTFError

	styles=getSampleStyleSheet()
	try :
		pdfmetrics.registerFont(TTFont('Ubuntu', 'Ubuntu-R.ttf'))
		pdfmetrics.registerFont(TTFont('Ubuntu-I', 'Ubuntu-RI.ttf'))
		pdfmetrics.registerFont(TTFont('Ubuntu-B', 'Ubuntu-B.ttf'))
		pdfmetrics.registerFont(TTFont('Ubuntu-BI', 'Ubuntu-BI.ttf'))
		pdfmetrics.registerFontFamily('Ubuntu', normal='Ubuntu',italic='Ubuntu-I',bold='Ubuntu-B',boldItalic='Ubuntu-BI')

		ParagraphStyle.defaults['fontName'] = 'Ubuntu'
		styles['Normal'].fontName='Ubuntu'
	except TTFError :
		warn("Using default font instead coorporative one, 'Ubuntu', consider installing it.")

	styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
	styles.add(ParagraphStyle(name='Right', alignment=TA_RIGHT))
	styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))

	invoice.invoiceIdDisplay = (
		'{number:0{numberPadding}}/{year}'
		.format(
			numberPadding=invoiceNumberPadding,
			**invoice))

	def p(text, style='Normal') :
		return Paragraph(text, style=styles[style])

	def bold(string) :
		return '<b>{}</b>'.format(string)

	def color(color, string) :
		return '<font color={}>{}</font>'.format(color, string)

	import contextlib
	@contextlib.contextmanager
	def catalan():
		import locale
		oldlocale = locale.getlocale(locale.LC_NUMERIC)
		locale.setlocale(locale.LC_NUMERIC, ("ca","utf8"))
		yield
		locale.setlocale(locale.LC_NUMERIC, oldlocale)
		
		
	def number(quantity):
		with catalan():
			return p("{:n}".format(quantity), "Right")

	def percent(quantity) :
		return p("{:.0f} %".format(100*quantity))

	_bold = bold
	def euros(quantity, bold=False) :
		"""Returns a money quantity in euros properly rounded
		and represented in catalan locale"""
		import locale
		with catalan():
			result = locale.format_string("%.2f", round(quantity,2), grouping=True)+" €"
		if bold :
			result = _bold(result)
		return p(result, 'Right')

	def image(path, width=None, height=None) :
		"""Returns an image to be inserted into a pdf,
		maybe constrained by width and/or height"""
		im = Image(path)
		if width is None and height :
			im.drawWidth = height*im.drawWidth / im.drawHeight
			im.drawHeight = height
		if height is None and width :
			im.drawHeight = width*im.drawHeight / im.drawWidth
			im.drawWidth = width
		if height and width :
			im.drawWidth = width
			im.drawHeight = height
		return im

	def titleBox(data, style):
		title = 'FACTURA'
		if 'amends' in invoice:
			title+=" RECTIFICADORA D'ABONAMENT"

		data[0][0] = p(bold(title), style='Center')
		style += [
			# Title
			('BACKGROUND', (0,0), (0,0), colors.lightgrey),
		]

	def logoBox(data, style):
		offset = 0
		logopath = appData("logo-guifibaix.png")
		data[offset][-2]= image(logopath, height=23*mm)
		style += [
			# Logo
			('SPAN',   (-2,offset), (-1,offset+3), ),
			('VALIGN', (-2,offset), (-1,offset), 'MIDDLE'),
			('ALIGN',  (-2,offset), (-1,offset), 'CENTER'),
		]

	def providerBox(data, style):
		offset = 2
		providerLines = [
			color('darkred', provider.name),
			'&nbsp;'*6 + provider.address,
			'&nbsp;'*6 + provider.postalcode+" "+provider.city,
			'&nbsp;'*6 + provider.state,
			"NIF: "+provider.nif,
			]

		for i,text in enumerate(providerLines) :
			data[offset+i][0] = p(bold(text))

	def invoiceInfoBox(data, style):
		row = 4
		data[row+0][-2] = p(bold('Número:'))
		data[row+0][-1] = p(bold(invoice.invoiceIdDisplay))
		data[row+1][-2] = p(bold('Data:'))
		data[row+1][-1] = invoice.issueDate.slashDate
		if 'amends' in invoice:
			data[row+2][-2] = p(bold('Rectifica:'))
			data[row+2][-1] = invoice.amends
		else:
			data[row+2][-2] = p(bold('Venciment:'))
			data[row+2][-1] = invoice.dueDate.slashDate
		style+=[
			# Numero factura
			('BOX',        (-2,row), (-1,row+2), 1, colors.black),
			('BOX',        (-2,row), (-1,row+1), 1, colors.black),
			('BACKGROUND', (-2,row), (-1,row+1), colors.lightgrey),
		]

	def footer(data, style):
		offset=-3
		data[offset+0][0]=p(bold("IBAN: ")+ invoice.provider.iban + " - " + bold("BIC: ")+ invoice.provider.bic, style='Center')
		data[offset+1][0]=p(
			bold(" Suport tècnic: ")+
			" suport@guifibaix.coop - " +
			bold(" Telèfon: ")+
			" 649.49.59.72"
			, style="Center")
		data[offset+2][0]=p(
			bold(" Facturació: ")+
			" facturacio@guifibaix.coop"
			, style='Center')

		style+=[
			# Footer
			('SPAN', (0,offset+0), (-1,offset+0), ),
			('SPAN', (0,offset+1), (-1,offset+1), ),
			('SPAN', (0,offset+2), (-1,offset+2), ),
			('LINEABOVE', (0,offset), (-1,offset), 1, colors.black),
			('LINEBELOW', (0,offset), (-1,offset+2), 1, colors.black),
		]

	def clientBox(data, style):
		row=8
		data[row][0] = p(bold('Client'))
		clientLines = [
			client.name,
			'&nbsp;'*6 + client.address,
			'&nbsp;'*6 + client.postalcode + " " + client.city,
			'&nbsp;'*6 + client.state,
			"NIF: "+client.nif,
			]

		for i,text in enumerate(clientLines) :
			data[row+i+1][0] = p(bold(text))
		style+=[
			# Client box
			('BOX', (0,row), (-1,row+len(clientLines)), 1, colors.black),
			('SPAN', (0,row+0), (-1,row+0), ),
			('SPAN', (0,row+1), (-1,row+1), ),
			('SPAN', (0,row+2), (-1,row+2), ),
			('SPAN', (0,row+3), (-1,row+3), ),
			('SPAN', (0,row+4), (-1,row+4), ),
			('SPAN', (0,row+5), (-1,row+5), ),
			# Client Title
			('BOX',        (0,row), (-1,row), 1, colors.black),
			('BACKGROUND', (0,row), (-1,row), colors.lightgrey),
		]

	def itemsBox(data, style, baseImponible, concepts):
		offset=14
		lastrow = -6

		data[offset][0]=p(bold('Concepte'), style='Center')
		if showUnitPrice:
			data[offset][1]=p(bold('Num'), style='Center')
			data[offset][2]=p(bold('Preu Unitari'), style='Center')
		data[offset][-1]=p(bold('Import'), style='Center')

		for i, concept in enumerate(concepts) :

			parts = concept.split('\t')
			nparts = len(parts)

			if nparts > 3:
				fail("Més de dos tabuladors a la línia de concepte {}".format(i+1))

			if nparts == 1:
				data[offset+1+i][0] = p(bold(concept))
				style += [
					('SPAN', (0, offset+1+i), (-2, offset+1+i)),
				]
				continue

			if nparts == 2:
				item, quantity = parts
				try:
					quantity=decimal.Decimal(quantity)
				except:
					fail("A la línia de concepte {} s'esperava un import "
						"en format anglosaxó (punt pels decimals) i sense moneda".format(i+1))

			if nparts == 3:
				item, units, unitprice = parts
				try:
					unitprice=decimal.Decimal(unitprice)
				except:
					fail("A la línia de concepte {} s'esperava un preu unitari "
						"en format anglosaxó (punt pels decimals) i sense moneda "
						"pero ha rebut '{}'"
						.format(i+1, unitprice))
				try:
					units=decimal.Decimal(units)
				except:
					fail("A la línia de concepte {} s'esperava un import "
						"en format anglosaxó (punt pels decimals), pero ha rebut '{}'".format(i+1, units))

				quantity = units*unitprice
				data[offset+1+i][1] = number(units)
				data[offset+1+i][2] = euros(unitprice)
			if not showUnitPrice:
				style.append(('SPAN', (0,offset+1+i), (-2,offset+1+i),  ))
			data[offset+1+i][0] = p("&nbsp;"*4+item)
			data[offset+1+i][-1] = euros(quantity)
			baseImponible+=quantity
			continue

		style += [
			# Items box
			('BOX', (0,offset), (-1,lastrow), 1, colors.black),
			# Items header
			('BACKGROUND', (0,offset), (-1,offset), colors.lightgrey),
			('BOX', (0,offset), (-1,offset), 1, colors.black),
			# Numeros
			('BOX', (-1,offset+1), (-1,lastrow), 1, colors.black),
		]
		return baseImponible


	def totalBox(data, style, baseImponible):
		offset = -8

		iva = baseImponible * decimal.Decimal(invoice.vatRate)
		total = baseImponible + iva

		data[offset+0][0]="Total base imposable"
		data[offset+0][-1]=euros(baseImponible)
		data[offset+1][0]="IVA"
		data[offset+1][1]=percent(invoice.vatRate)
		data[offset+1][-1]=euros(iva)
		data[offset+2][0]=p(bold('Total Factura'))
		data[offset+2][-1]=euros(total, bold=True)
		style += [
			('BOX', (0,offset+2), (-1,offset+2), 1, colors.black),
		]

	def pagination(data, style, page, pages):
		if pages<=1: return
		offset = 8
		data[offset][-1] = "Pàgina {}/{}".format(page+1, pages)
		style.append(('ALIGN',  (-1,offset), (-1,offset), 'RIGHT'))
		if page+1 != pages:
			offset = -7
			data[offset][0] = p('<i>'+color('grey',"(Continua a la pàgina següent)")+'</i>','Right')
			style.append(('SPAN',  (0,offset), (-2,offset)))


	doc = SimpleDocTemplate(
		outputFilename,
		pagesize=A4,
		rightMargin=22*mm,
		leftMargin=22*mm,
		topMargin=22*mm,
		bottomMargin=22*mm,
		title="Factura {invoiceIdDisplay} - {subject}"
			.format(**invoice),
		author='GuifiBaix',
		showBoundary=1 if debuglayout else 0,
		)

	rows=39
	cols=4
	conceptsPerPage = 14

	pages = (len(concepts)+conceptsPerPage-1)//conceptsPerPage
	# When any concept has two tabs, we are using unit price
	showUnitPrice = any((x.count('\t')==2 for x in concepts))

	baseImponible=decimal.Decimal('0')
	elements=[]
	for page in range(pages):
		style = []
		data = [[None for _ in range(cols)] for _ in range(rows)]
		if debuglayout:
			style += [
				('GRID', (0,0),(-1,-1),1, colors.lightgrey),
				]

		titleBox(data, style)
		logoBox(data, style)
		providerBox(data, style)
		invoiceInfoBox(data, style)
		clientBox(data, style)
		pagination(data, style, page, pages)
		baseImponible = itemsBox(data,style,baseImponible,concepts[page*conceptsPerPage:(page+1)*conceptsPerPage])
		if page == pages-1:
			totalBox(data, style, baseImponible)
		footer(data, style)

		t=Table(
			data,
			style=style,
			colWidths=[
				.55*doc.width,
				.10*doc.width,
				.15*doc.width,
				.20*doc.width,
				],
			)

		elements.append(t)

	def waterMarkedPage(canvas, doc):
		if not doc.draft: return

		canvas.saveState()

		canvas.setFont("Helvetica", 100)
		canvas.setStrokeGray(0.90)
		canvas.setFillGray(0.90)
		canvas.setFillColor((0.8,0.8,1.0))
		canvas.rotate(45)
		canvas.drawCentredString(170 * mm, 10 * mm, 'ESBORRANY')

		canvas.restoreState()

	doc.draft = invoice.draft

	doc.build(elements, onFirstPage=waterMarkedPage, onLaterPages=waterMarkedPage)


