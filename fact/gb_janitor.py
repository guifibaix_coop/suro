#!/usr/bin/env python3

"""
This script checks several potential problems with the expedients
"""

from consolemsg import *
import os
import re
import glob
from yamlns import namespace as ns
from fact import tresoreria

def yamlKeys(yaml):
	"""
	>>> yamlKeys(ns.loads("a: 1"))
	['a']
	>>> yamlKeys(ns.loads("a: 1\\nb: 3"))
	['a', 'b']
	>>> yamlKeys(ns.loads("1"))
	[]
	>>> yamlKeys(ns.loads("a:\\n  b: 1"))
	['a', 'a.b']
	>>> yamlKeys(ns.loads("a:\\n  b: \\n    c: 2"))
	['a', 'a.b', 'a.b.c']
	>>> yamlKeys(ns.loads("a:\\n- 1\\n- 2"))
	['a']
	"""
	try:
		keys = list(yaml.keys())
		for key in yaml.keys():
			keys += [
				key + '.' + subkey
				for subkey in yamlKeys(yaml[key])
				]
		return keys
	except AttributeError:
		pass
	return []

def getDotted(data, key):
		keys = key.split('.')
		info = data
		for k in keys:
			if k not in info:
				return False
			info = info[k]
		return info

def checkYaml(filename, mandatory=[], nullable=[], optional=[], open=False):

	def reportIfAny(keys, message):
		if not keys: return
		error("{}: {}: {}".format(
			filename, message, ', '.join(sorted(keys))))

	try:
		data = ns.load(filename)
	except Exception as e:
		error("Failed to load {}: {}"
			.format(filename, e))
		return None
	dataKeys = yamlKeys(data)

	missingKeys = {
		key for key in mandatory
		if key not in dataKeys
		}
	reportIfAny(missingKeys, "Falten camps")

	if not open: 
		extraKeys = {
			key for key in dataKeys
			if key not in optional
			if key not in mandatory
			}

		reportIfAny(extraKeys, "Claus no esperades")

	emptyKeys = {
		key for key in dataKeys
		if key not in nullable
		if getDotted(data, key) is None
		}
	reportIfAny(emptyKeys, "Claus buides")

	return data

def checkDadesFacturacio(expedient):
	filename=expedient + '-dadesfacturacio.yaml'
	data = checkYaml(
		filename,
		mandatory= [
			'expedient',
			'name',
			'nif',
			'address',
			'city',
			'genre',
			'postalcode',
			'state',
			'iban',
			'bic',
			'contact',
			'contact.email',
			'contact.phone',
			],
			nullable = """
				genre
				""".split(),
		)

	dataKeys = yamlKeys(data)

	if 'expedient' in dataKeys:
		if data.expedient != expedient.split('/')[-1]:
			error("{}: El nom del fitxer no concorda amb l'expedient que val '{}'"
				.format(expedient, data.expedient))

	if 'contact.phone' in dataKeys:
		if type(data.contact.phone) != list :
			error("{}: contact.phone ha de ser una llista amb minim un element pero es '{}'"
				.format(expedient, data.contact.phone))
		for phone in data.contact.phone:
			if not tresoreria.cleanPhone(phone):
				error("{}: Telefon incorrecte: '{}'"
					.format(expedient, phone))

	if 'contact.email' in dataKeys:
		if type(data.contact.email) != list :
			error("{}: contact.email ha de ser una llista amb minim un element pero es {}"
				.format(expedient, data.contact.email))
		for email in data.contact.email:
			if not tresoreria.cleanEmail(email):
				error("{}: E-mail incorrecte: '{}'"
					.format(expedient, email))

	if 'genre' in dataKeys:
		if data.genre not in (None, 'male', 'female', 'legal'):
			error("{}: 'genre' ha de ser buit, 'male' o 'female' o 'legal', es {}"
				.format(expedient, data.genre))

	if 'nif' in dataKeys:
		if not tresoreria.isNifValid(data.nif):
			error("{}: invalid field 'nif': {}"
				.format(expedient, data.nif))

	if 'postalcode' in dataKeys:
		if data.postalcode[:2] != '08':
			error("{}: invalid field 'postalcode': {}"
				.format(expedient, data.postalcode))

	return data


def checkDadesContracte(expedient):
	filename=expedient + '-contractemanteniment.yaml'
	data = checkYaml(filename,
		mandatory="""
			dataInstallacio
			inici
			periodesExempts
			puntsDeServei
		""".split(),
		optional = """
			quotaMensualSenseIva
			users
			user
			afadrinats
			aportadors
			fadri
		""".split()
		)
	if not data: return None

	dataKeys = yamlKeys(data)

	afadrinats = data.get('afadrinats', 0)
	if afadrinats:
		if 'fadri' not in data:
			error("{}: afadrinats requereixen un 'fadri'"
				.format(expedient+"-contractemanteniment.yaml"))
		# TODO: check that facri exists as expedient has has aportadors

	return data

def main():
	allMantainments = glob.glob('*/*-contractemanteniment.yaml')
	allInvoicingData = glob.glob('*/*-dadesfacturacio.yaml')
	if not allInvoicingData and not allInvoicingData:
		fail(
			"No s'han trobat fitxers als subdirectoris acabats "
			"en '-contractemanteniment.yaml' o '-dadesfacturacio.yaml'.\n"
			"Possiblement no estàs executant aquest script des de "
			"'privat/projectes'"
			)

	expedientsWithMaintainment = set((
		expedient[:-len('-contractemanteniment.yaml')]
		for expedient in allMantainments
		))
	expedientsWithInvoicing = set((
		expedient[:-len('-dadesfacturacio.yaml')]
		for expedient in allInvoicingData
		))
	expedients = (
		expedientsWithInvoicing |
		expedientsWithMaintainment
		)

	for expedient in sorted(expedients):
		step("Comprovant {}...".format(expedient))
		if expedient not in expedientsWithInvoicing:
			error("Expedient sense dades de facturacio: {}"
				.format(expedient))

		if expedient not in expedientsWithMaintainment:
			error("Expedients sense manteniment: {}"
				.format(expedient))
		
		dadesFacturacio = checkDadesFacturacio(expedient)
		dadesContracte = checkDadesContracte(expedient)

def load_tests(loader, tests, ignore):
	import doctest
	tests.addTests(doctest.DocTestSuite())
	return tests


import sys

if __name__ == '__main__':
	if '--test' not in sys.argv:
		main()

