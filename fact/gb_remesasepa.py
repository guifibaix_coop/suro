#!/usr/bin/env python3


from itertools import zip_longest, chain
from consolemsg import *
from yamlns import dateutils
from fact import tresoreria
from fact.sequence import sequence
from fact.iban import iban

def parseCommandLine() :
	"""
	Parses the command line options of the module when used as script.
	"""
	import argparse

	parser = argparse.ArgumentParser(
		description=
			"""
			Genera una remesa SEPA, és a dir, un fitxer que s'envia al banc
			per cobrar als clients (carregar un dèbit).
			Parteix de la informacio a les factures en yaml i la informació
			de pagaments a 'cobraments.yaml'.
			Els  es poden filtrar per any i nombre de factura, per seqüència de venciment
			si fos a plaços o per data de venciment.
			"""
			,
		epilog = ""
	)
	parser.add_argument(
		'--test',
		action='store_true',
		help="run unittests",
		)
	parser.add_argument(
		'years',
		type=sequence,
		nargs='?',
		metavar="ANY",
		help="Anys d'emissió de les factures a incloure a la remesa "
			" separats per comes, i guions per indicar intervals (ie 2011,2014-2016 seria 2011,2014,2015,2016)",
		)
	parser.add_argument(
		'numbers',
		type=sequence,
		nargs='?',
		metavar="NUMEROS_FACTURA",
		help="Conjunt de números de factura a incloure a la remesa"
			" separats per comes, i guions per indicar intervals (ie 3,6-9 seria 3,6,7,8,9)",
		)
	parser.add_argument(
		'debits',
		type=sequence,
		nargs='?',
		metavar="NUMEROS_VENCIMIENT",
		help="Conjunt de números d'orcre dels dèbit de la factures a incloure a la remesa"
			" separats per comes, i guions per indicar intervals (ie 3,6-9 seria 3,6,7,8,9)",
		)
	parser.add_argument(
		'--slow',
		action='store_true',
		help="Escull el procediment 19.14, lent però més acceptat que el 19.15 (procediment express)",
		)
	parser.add_argument(
		'--paid',
		action='store_true',
		help="No ignoris els venciments pagats (normalment per fer proves)",
		)
	parser.add_argument(
		'--due',
		dest='dueDate',
		type=dateutils.date,
		metavar='DUE_DATE',
		help="Força la data de venciment al dia indicat (YYYY-MM-DD)",
		)
	parser.add_argument(
		'--from',
		dest='fromDate',
		type=dateutils.date,
		metavar='FROM_DATE',
		help="Data del venciment més antic que s'inclourà a la remesa (YYYY-MM-DD)",
		)
	parser.add_argument(
		'--to',
		dest='toDate',
		type=dateutils.date,
		metavar='TO_DATE',
		help="Data del venciment més recent que s'inclourà a la remesa (YYYY-MM-DD)",
		)
	parser.add_argument(
		'--today',
		type=dateutils.date,
		metavar='SIMULATED_TODAY',
		help="Simula que avui es el SIMULATED_TODAY en format YYYY-MM-DD",
		)
	parser.add_argument(
		'--now',
		metavar='SIMULATED_NOW',
		type=dateutils.isoToTime,
		help="Simula que l'hora del dia es el SIMULATED_NOW en format HH[:MM[:SS[:uuuuuu]]]",
		)
	parser.add_argument(
		'--past-ok',
		action='store_true',
		help="No força els venciments a dates al futur",
		)
	parser.add_argument(
		'--wait',
		metavar='DAYS',
		type=int,
		default=4,
		help="Dies mínims que han de passar per l'abonament, per defecte 4",
		)
	parser.add_argument(
		'--reference',
		default='BORRADOR',
		help="Una referencia de màxim 13 caracters que s'introduira al final de l'identificador de fitxer, després de la marca de temps",
		)
	parser.add_argument(
		'--issuer',
		metavar='company.yaml',
		help="fichero yaml con los datos de un emisor de la remesa alternativo",
		)
	"""
	parser.add_argument(
		'--exclude',
		type=str,
		nargs=
		help="Llista separada per comes de factures a excloure de la remesa",
		)
	"""
	return parser

import glob
import unittest
from itertools import zip_longest, chain
from yamlns import namespace as ns
from consolemsg import *
import datetime
from fact.sequence import sequence
from fact.iban import iban
from fact.sepa import SepaN1914


def main(args) :
	def filter(message) :
		import sys
		print(color('35', "Filtrant: "+message), file=sys.stderr)

	issuer = ns.load(args.issuer) if args.issuer else tresoreria.currentCompany()
	today = args.today or dateutils.Date.today()
	currentTime = datetime.datetime.now().time() if args.now is None else args.now
	now = datetime.datetime.combine(today, currentTime)
	earlierPaymentDate = dateutils.date(today + datetime.timedelta(days=args.wait))
	fileReference = args.reference

	baseFile = now.strftime("PRE-%Y%m%d-%H%M%S-%f")[:-1]+'-'+fileReference
	sepaFile = baseFile+'.c19'
	csvFile = baseFile+'.csv'

	csv = ['\t'.join([
		'Factura',
		'Sèrie',
		'Referència',
		'DataEmisió',
		'DataVenciment',
		'ReferenciaMandat',
		'DataSignaturaMandat',
		'NifDeutor',
		'IBANDeutor',
		'Import',
		'Concepte',
		'NomDeutor',
		])]

	sepa = SepaN1914(
		reference = fileReference,
		creation = now,
		presenter = issuer,
		fastProcedure = not args.slow,
		)

	payments = tresoreria.cobraments()
	tresoreria.issuedInvoices_computePaidAndPending(payments)

	referenceBase = now.strftime('%Y%m%d%H%M%S')
	debitSerie=1
	for issuedInvoice in payments:
		yamlfile = 'factures/factura-{}-{}.yaml'.format(
			issuedInvoice.factura,
			issuedInvoice.concepte,
			)
		try :
			yamlfile = glob.glob('factures/factura-{}-*.yaml'
				.format(issuedInvoice.factura))[0]
		except IndexError:
			filter("Factura a cobraments.yaml sense arxiu yaml "+yamlfile)
			continue

		step("Processant {}...".format(yamlfile))

		invoice = ns.load(yamlfile)
		payer = invoice.get('payer', invoice.client)

		pagaments = issuedInvoice.get('pagaments', [])
		pagaments = [
			v for v in issuedInvoice.get('venciments',[])
			if v.get('pagat', None)
			]
		nPagaments = len(pagaments)

		if issuedInvoice.pagat >= issuedInvoice.valor and not args.paid:
			filter("Factura pagada")
			continue

		if not issuedInvoice.get('pendent',0) and not args.paid:
			filter("Factura sense import pendent".format(issuedInvoice))
			continue

		if args.years and invoice.year not in args.years :
			filter("Factura filtrada per any d'emisió {}".format(invoice.year))
			continue

		if args.numbers and invoice.number not in args.numbers :
			filter("Factura filtrada per número {}".format(invoice.number))
			continue

		# TODO: KLUDGES
		payer.postalCode=payer.postalcode
		payer.iban = iban(payer)

		if not payer.iban :
			filter("Factura sense IBAN: {}".format(yamlfile))
			continue

		if 'bic' not in payer or not payer.bic :
			filter("Factura sense BIC: {}".format(yamlfile))
			continue

		if 'venciments' not in issuedInvoice:
			error("La factura no te venciments, construint-los per defecte")
			
			try:
				dueDate = dateutils.Date(invoice.dueDate)
			except ValueError:
				error("La data de venciment tampoc és entendible: '{}' prenent com a bona {}".format(invoice.dueDate, earlierPaymentDate))
				dueDate = earlierPaymentDate
			issuedInvoice.venciments = [
				ns(
					data = dueDate,
					valor = issuedInvoice.valor,
					)]

		dues = issuedInvoice.venciments

		debtor = invoice.get('payer', invoice.client)
		debtor.nif = ''.join(debtor.nif.split("-"))
		if not 'mandate' in debtor:
			debtor.mandate = ns(
				reference='SEPA-{}-{}-{:08}'.format(debtor.nif,today,1),
				date=today,
				sequence='RCUR',
				)
			warn("No hi ha un mandat actiu, fent servir: {} {} {}".format(
				debtor.mandate.reference,
				debtor.mandate.date,
				debtor.mandate.sequence,
				))

		nVenciments = len(dues)
		for debitSequence, debit in enumerate(dues, 1):

			reference = referenceBase + "%05d"%(debitSerie)

			step("\tDebit {}".format(reference))

			if not args.paid and debit.get('pagat', False) :
				filter("Debit {} ja pagat".format(debitSequence))
				continue

			if not args.paid and debit.get('emes', False) :
				filter("Debit {} ja emés".format(debitSequence))
				continue

			if debit.get('aturat', False) :
				filter("Dèbit marcat com a postergat")
				continue

			if args.debits and debitSequence not in args.debits:
				filter("Dèbit filtrat per ordre de venciment ({}) a la factura".format(debitSequence))
				continue

			if args.fromDate and args.fromDate>debit.data:
				filter("Dèbit amb data de venciment {} anterior a la data d'inici indicada".format(debit.data))
				continue

			if args.toDate and args.toDate<debit.data:
				filter("Dèbit amb data de venciment {} posterior a la data de final indicada".format(debit.data))
				continue

			dueDate = debit.data
			if args.dueDate :
				dueDate = args.dueDate
			elif not args.past_ok and debit.data < earlierPaymentDate:
				warn("Adjustant la data de {} a una data futura {}".format(debit.data, earlierPaymentDate))
				dueDate = earlierPaymentDate
			

			serie = " {}/{}".format(
				debitSequence,
				nVenciments,
				) if nVenciments>1 else ""
			concept = "Guifibaix F{}{} {}".format(
				issuedInvoice.factura,
				serie,
				issuedInvoice.concepte,
				)
			success("\t\tAfegint debit {} {}{} {} {}".format(
				reference,
				issuedInvoice.factura,
				serie,
				debit.valor,
				issuedInvoice.concepte,
				))
			csv.append('\t'.join([
				issuedInvoice.factura,
				serie or ' 1/1',
				reference,
				invoice.issueDate.slashDate,
				dueDate.slashDate,
				debtor.mandate.reference,
				dateutils.slashDate(debtor.mandate.date),
				debtor.nif,
				iban(debtor),
				str(debit.valor),
				concept,
				debtor.name,
			]))
			sepa.addDebit(
				reference = reference,
				dueDate = dueDate,
				concept = concept,
				amount = debit.valor,
				debtor = debtor,
				mandate = debtor.mandate,
				creditor = issuer,
				)

			debitSerie+=1

	step("Generant {}".format(csvFile))
	with open(csvFile,'w') as f:
		f.write('\n'.join(csv))

	step("Generant {}".format(sepaFile))
	with open(sepaFile,'w') as f:
		f.write(sepa.generate())


if __name__ == '__main__' :
	import sys
	if '--test' in sys.argv :
		sys.argv.remove('--test')
		sys.exit(unittest.main())

	argsparser = parseCommandLine()
	args = argsparser.parse_args()
	sys.exit(main(args))




