#!/usr/bin/env python3



def getFdf(filename) :
	import subprocess
	command = [
		'pdftk',
		filename,
		'generate_fdf',
		'output',
		'-',
	]
	proc = subprocess.Popen(command, stdout=subprocess.PIPE)
	(out, err) = proc.communicate()
	return out

def setFdf(inputpdf, fdf, outputpdf, flatten=False) :
	import subprocess
	command = [
		'pdftk',
		inputpdf,
		'fill_form',
		'-',
		'output',
		outputpdf,
	]
	if flatten : command.append('flatten')
	proc = subprocess.Popen(command, stdin=subprocess.PIPE)
	(out, err) = proc.communicate(fdf)
	return out

def fdfTemplate(**kwd) :
	def pdfEncode(text) :
		import codecs
		try:
			return text.encode("ascii")
		except ValueError:
			try :
				# BUG: PDF encoding is not fully latin1
				return text.encode("latin1")
			except ValueError :
				return b'\xff\xfe' + text.encode('utf-16-le')

	content = b' '.join((
		b'\n'
		b'<<\n'
		b'/V (' + pdfEncode(value) + b')\n'
		b'/T (' + pdfEncode(key) + b')\n'
		b'>>'
		for key, value in sorted(kwd.items())))

	return (
		b'%FDF-1.2\n'
		b'%\xe2\xe3\xcf\xd3\n'
		b'1 0 obj \n'
		b'<<\n'
		b'/FDF \n'
		b'<<\n'
		b'/Fields ['+content+b']\n'
		b'>>\n'
		b'>>\n'
		b'endobj \n'
		b'trailer\n'
		b'\n'
		b'<<\n'
		b'/Root 1 0 R\n'
		b'>>\n'
		b'%%EOF\n'
		)

import unittest
import os

class Fdf_Test(unittest.TestCase) :

	def setUp(self) :
		self._toRemove = []

	def toRemove(self, f) :
		self._toRemove.append(f)

	def tearDown(self) :
		for f in self._toRemove :
			os.remove(f)

	def generatePdf(self, filename, fields=None):
		self.toRemove(filename)
		from reportlab.pdfgen.canvas import Canvas
		from reportlab.pdfbase.pdfform import textFieldRelative
		c = Canvas(filename)
		if fields :
			for i, (param, value) in enumerate(fields.items()) :
				textFieldRelative(c, param, 30, i*30, 100, 30, value)
		c.showPage()
		c.save()

	def test_fdfExtract(self) :
		self.generatePdf("boo.pdf")
		fdf = getFdf("boo.pdf")
		self.assertEqual(fdf,
			fdfTemplate()
			)

	def test_fdfExtract_withAField(self) :
		self.generatePdf("boo.pdf", {'param1':'val1'})
		fdf = getFdf("boo.pdf")
		self.assertEqual(fdf,
			fdfTemplate(param1='val1')
				)

	def test_fdfExtract_withTwoFields(self) :
		self.generatePdf("boo.pdf", dict(
			param1='val1',
			param2='val2',
			))
		fdf = getFdf("boo.pdf")
		self.assertEqual(fdf,
			fdfTemplate(
				param1='val1',
				param2='val2',
			)
		)

	def test_setFdf(self) :
		self.generatePdf("boo.pdf", dict(
			param1='val1',
			param2='val2',
			))

		self.toRemove('output.pdf')
		setFdf('boo.pdf',
			fdfTemplate(
				param1='newval1',
				param2='newval2',
			), 'output.pdf')

		fdf = getFdf("output.pdf")
		self.assertEqual(fdf,
			fdfTemplate(
				param1='newval1',
				param2='newval2',
			)
		)

	def test_fdfExtract_withUnicodeValue(self) :
		self.generatePdf("foo.pdf", dict(
			paramUnicòde='valç',
			paramUnicodé='valñ',
			))
#		import shutil
#		shutil.copy('foo.pdf', 'mirame.pdf')
		fdf = getFdf("foo.pdf")
		self.assertEqual(fdf,
			fdfTemplate(
				paramUnicòde='valç',
				paramUnicodé='valñ',
			))

import sys

def main(args=sys.argv) :
	import argparse

	parser = argparse.ArgumentParser(
		description="Extracts, applies and generates fdf files.",
		)
	parser.add_argument(
		'--test',
		action='store_true',
		help="Run unittests",
		)
	subparsers= parser.add_subparsers(
		dest='command',
		)

	subargs = subparsers.add_parser(
		'extract',
		help='Extracts form data from a PDF file into a FDF file',
		description='Extracts form data from a PDF file into a FDF file',
		)
	subargs.add_argument(
		metavar='input.pdf',
		dest='input_pdf',
		)
	subargs.add_argument(
		metavar='output.fdf',
		dest='output_fdf',
		)

	subargs = subparsers.add_parser(
		'fill',
		help='Fills a PDF form with data from a FDF file.',
		description='Fills a PDF form with data from a FDF file.',
		)
	subargs.add_argument(
		metavar='input.pdf',
		dest='input_pdf',
		)
	subargs.add_argument(
		metavar='input.fdf',
		dest='input_fdf',
		)
	subargs.add_argument(
		metavar='output.pdf',
		dest='output_pdf',
		)
	subargs.add_argument(
		'--flatten',
		dest='flatten',
		action='store_true',
		help='Makes the form fields non-editable',
		)

	args = parser.parse_args()

	if args.command == 'extract' :
		fdf = getFdf(args.input_pdf)
		with open(args.output_fdf, 'wb') as f :
			f.write(fdf)
		return 0

	if args.command == 'fill' :
		with open(args.input_fdf, 'rb') as f :
			fdf = f.read()
		setFdf(args.input_pdf, fdf, args.output_pdf, flatten = args.flatten)
		return 0

	parser.print_help()
	return -1


if __name__  == '__main__' :
	if '--test' in sys.argv :
		sys.argv.remove('--test')
		sys.exit(unittest.main())

	sys.exit(main())





