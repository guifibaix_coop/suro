# Instrucciones basicas para generar facturas, contratos y demas

Espero simplificar los procesos pero de momento funcionan asi.

## Instalar las dependencias

Primero hay que instalar algunos paquetes:

```bash
$ sudo apt-get install scons libpoppler-cpp-dev libyaml-cpp-dev python3-yaml python3-reportlab libboost-dev pkg-config 
$ git clone git@github.com:vokimon/pdf-form-burner.git
$ cd pdf-form-burner
$ scons
$ sudo scons install
$ sudo pip install gitpython
```

## Hacer accesibles los scripts

Para poder ejecutar los scripts sin tener que poner siempre
todo el camino, hay que añadir en el PATH el directorio donde están.
Si el clone del suro lo teneis en `~/Documents/guifibaix/`,
entonces hay que añadir la siguiente linea al final del `~/.bashrc`

```bash
export PATH=$PATH:~/Documents/guifibaix/suro/fact
```

## Facturas (no mantenimiento)

1. Mirar si hay numeros disponibles en `privat/tresoreria/factures/numeros-reservats`.
   Si no hay pedirlos a AT2 y añadirlos al fichero.

2. Cuando los haya, asignar el numero como se explica en la cabecera de ese fichero.
   Basicamente hay que poner el '#' delante para que deje de estar disponible a los scripts automaticos,
   y detrás una breve descripción de para que se ha asignado.
   Digamos que asignamos el 44/2014, la linea quedaría:

	```
		# 44/2014 Instalacion Rue del Percebe 13
	```

3. Commit y push de `numeros-reservats` para que los otros no te puedan quitar tu asignación.

4. Generar una factura con datos de ejemplo (en `privat/tresoreria/factures/`):

	```bash
	$ gb_factura.py --dump factura-2013-44-sjd-percebe13-1o1a-PericoPalotes-install.yaml
	```

5. Editar los datos del fichero para nuestra factura

6. Eliminar la linea el numero reservado de `numeros-reservats` y comitear y pushear ese fichero y el yaml.
   Aitor sugeria de dejar la linea pero si hay información duplicada hay posibilidad de inconsistencia y dolores de cabeza.
   Para evitarlo, o hay YAML o hay linea en `numeros-reservados`.

	```bash
	$ kate privat/tresoreria/factures/numeros-reservats  # Eliminamos la linea
	$ git add privat/tresoreria/factures/factura-2013-44*.yaml
	$ git commit privat/tresoreria/factures/{numeros-reservats,factura-2013-44*.yaml}
	```

7. Generar el PDF con el mismo comando que para crear el ejemplo pero sin el `--dump`.
   Crea un fichero con el mismo nombre y extension `.pdf`:

	```bash
	$ gb_factura.py factura-2013-44-sjd-percebe13-1o1a-PericoPalotes-install.yaml
	```

8. El PDF solo lo commiteamos cuando hayamos enviado el PDF por correo.
   Digamos que si hay PDF, quiere decir que ese fichero es **EL ENVIADO**.

9. Registrar la factura y editar los vencimientos. Desde 'tresoreria':

	$ gb_registrafactura.py 2014 44

10. Si requiere domiciliación, se puede generar un fichero de remesa SEPA.

	$ gb_remesasepa.py 

### TODO's

- Usar la informacion de facturación que ya usamos en las facturas de mantenimiento (2 horas)
- Generar el nombre de la factura a partir de informacion ofrecida por linea de comandos (2 horas)
- Automatizar la reserva de numero de factura sin Git (3 horas)
- Automatizar la reserva de numero de factura con Git (10 horas)
- Notificar los ficheros que se van generando


## Facturas de mantenimiento

Estan algo mas automatizadas aunque se puede automatizar más aún seguro.

1. La primera vez que facturamos mantenimiento a un usuario, necesitamos crear dos ficheros de datos.
	- Para el ejemplo estos ficheros serian:
		- `privat/projectes/sjd-percebe13/sjd-percebe13-1o1a-dadesfacturacio.yaml`
		- `privat/projectes/sjd-percebe13/sjd-percebe13-1o1a-contractemanteniment.yaml`
	- Y teneis ejemplos del contenido en:
		- `suro/fact/data/exemple-dadesfacturacio.yaml`
		- `suro/fact/data/exemple-contractemanteniment.yaml`

2. Asignamos un numero y commiteamos `numeros-reservats` como en el procedimiento de facturas normales.

3. Generamos la factura por pantalla para comprobar que todo esta bien.
   Supongamos que queremos hacer el mantenimiento del 4o trimestre de 2014 y nos tocó el número 45/2014.

	```bash
	$ gb_facturamanteniment.py 4 2014 45 sjd-percebe13-1o1a-contractemanteniment.yaml --show
	```

4. Si tiene buena pinta, lanzar el mismo comando sin la opción `--show` y generará un yaml con un nombre normalizado.
   del estilo: `factura-2014-45-sjd-percebe13-1o-1a-PericoPalotes-quota3T.yaml`

	```bash
	$ gb_facturamanteniment.py 4 2014 45 sjd-percebe13-1o1a-contractemanteniment.yaml
	Generando 'factura-2014-45-sjd-percebe13-1o1a-PericoPalotes-quota4T.yaml'...
	Done.
	```

5. Si además quiere generar el PDF añada la opción `--pdf`

	```bash
	$ gb_facturamanteniment.py 4 2014 45 sjd-percebe13-1o1a-contractemanteniment.yaml --pdf
	Generando 'factura-2014-45-sjd-percebe13-1o1a-PericoPalotes-quota4T.yaml'...
	Generando 'factura-2014-45-sjd-percebe13-1o1a-PericoPalotes-quota4T.pdf'...
	Done.
	```

6. Seguir el mismo protocolo de commits y reserva de numeros que para las otras facturas.

7. Normalmente queremos generar todos los contratos existentes.
   Si tuvieramos disponibles los numeros de factura de la 30 a la 40 con la excepcion de la 31,
   desde el directorio `projectes` podemos ejecutar:

	```bash
	$ gb_facturamanteniment.py 4 2014 30,32-40 */*contractemanteniment.yaml --pdf
	```

### Periodos exentos

Los periodos exentos se especifican en el `contratomantenimiento.yaml`.

### Facturas con importe cero (o negativo)

Si la factura sale a 0 o negativa (el trimestre es anterior a contrato...) retorna un mensaje de error
y no se genera nada.

Para ver porque, hay que usar la opcion `--show` para ver el YAML dado que no se genera el fichero físico.

### Fechas de vencimientos y emisión atípicas

Por defecto, la emision es el 1 del primer mes del trimestre y vence el 10.
Si eso no es correcto, usa las opciones `--issue` (emision) y `--due` (vencimiento)
con la fecha correspondiente detrás 


### TODO's

- Generación en lotes (3 horas)
- Que el contrato pueda referenciar el yaml con los datos de facturación a usar (??)
- ~~Evitar la generación de facturas con importe cero~~
- ~~Notificar los ficheros que se van generando~~
- ~~Neutralizar, en el nombre del fichero generado, caracteres no ingleses del nombre cliente~~


## Contratos de Voip

1. Generar un ejemplo de yaml con los datos del contrato

	```bash
	$ gb_altasnubip.py --dump sjd-percebe13-1o1a-contracteveuip.yaml
	```

2. Editar los datos

3. Generar los contratos:

	```bash
	$ gb_altasnubip.py sjd-percebe13-1o1a-contracteveuip.yaml
	```
	Genera dos archivos:

	- `sjd-percebe13-1o1a-contractevoip-ContratoPortabilidadNubip.pdf`
	- `sjd-percebe13-1o1a-contractevoip-SolicitudYContratoServicios.pdf`

4. Revisar los datos, commitear y enviarlos al cliente para que los firme.
   Recordar tambien enviar la autorización bancaria que no genermos pero que está en:
   `suro/fact/templates/



### TODO's

- Reusar datos de facturación del cliente
- Añadir la autorización bancària al pack
- Notificar los ficheros que se van generando










