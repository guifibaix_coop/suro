# GuifiBaix assisted workflows

## `gb_factura`: Generate invoices

1. Dump a template for the invoice data

	:::bash
	$ gb_factura.py --dump factura-2013-44-sjd-percebe13-1o1a-PericoPalotes-install.yaml

2. Edit `myinvoice.yaml` with a text editor (kate, notepad).

3. Generate `myinvoice.pdf`:

	:::bash
	$ gb_factura.py myinvoice.yaml


## `gb_facturamanteniment`: Generate trimestral maintainment invoices

- Each invoice receiver has a yaml file with fiscal data like the one in `data/client.yaml`
- Each invoice receiver has a yaml file with maintainment contract data like the one in `data/contract.yaml`
- Generate the invoice (both yaml and pdf) by using:

	:::bash
	$ ./gb_facturamanteniment.py TRIMESTER YEAR INVOICE_NUMBER CLIENT.yaml CONTRACT.yaml --pdf

- For example

	:::bash
	$ gb_facturamanteniment.py 3 2013 32 data/example-client.yaml data/example-contract.yaml

- This will generate `factura-2013-32-sjd-percebe13-PericoPalotes-quota3T.yaml` and a similarly named PDF
- Move them to `~/Documents/guifibaix/privat/tresoreria/factures` and commit the pdf only if it has been sent.
- Use the `--issue` and `--due` options if the default dates are not appropiate

```bash
	$ gb_facturamanteniment.py 3 2013 32 data/example-client.yaml data/example-contract.yaml --issue 2013-02-10 --due 2013-02-25
```


## `gb_altanubip`: Generate contracts and fill forms

1. Dump dummy data:

```bash
	$ gb_altanubip.py --dump sjd-percebe13-1o1a-contractevoip.yaml
```

2. Modify the yaml file with you own data

3. Generate the contracts pdfs

```bash
	$ gb_altanubip.py sjd-percebe13-1o1a-contractevoip.yaml sjd-percebe13-1o1a-contractevoip
```

It will generate:

	sjd-percebe13-1o1a-contractevoip-ContratoPortabilidadNubip.pdf
	sjd-percebe13-1o1a-contractevoip-SolicitudYContratoServicios.pdf


-------------------------

The chapters bellow are not final user utilities.
You will need them if you have to add new generated documents
or the providers change the PDF forms.


## `nstemplate`: Filling text templates with YAML files

Whenever you have to generate a text file with changing parts,
you can templatize it using regular Python templates
and fill it with YAML files.

	:::bash
	$ nstemplate.py apply data.yaml template.txt output.txt

Templates can access YAML data hierarchy by using either subscript or attributes.

	Hello, my name is {client.name}
	Hola, el meu nom és {client['name']}

In order to know the required fiels to fill a template you can use the `extract` subcommand:

	:::bash nstemplate.py extract template.txt emptydata.yaml


## `pdfform`: Extract and fill PDF form data

If a provider requires a new PDF form to be filled,
`pdfform` can be used to prepare the templates to automate the process.

`pdfform` extracts the PDF form fields as yaml key-value map:

	:::bash
	$ pdfform.py extract form.pdf output.form

We can edit the generated yaml (.form extension, but still yaml).

	:::bash
	$ pdfform.py apply input.pdf input.form output.pdf

But PDF form fields usally have quite meaninless names such as `textfield1_112_21`.
and directly editing the .form file is not practical.
Thus we propose to templatize the .form file with nicely named vars,
and to fill it with a nicely named yaml which the user will understand.

	contract.yaml + contract-template.form -> contract.form

	contract-empty.pdf + contract.form -> contract.pdf

The problem here is that whenever we get a new pdf form from the provider,
we need to generate a new template to fill.
The process is described below.

### Form preparation workflow

- Say the provider issues a new form `newform.pdf`.
- We generate a `newform-template.form`

Keep the original pdf clean as reference:

	:::bash
	mv newform.pdf templates/newform-original.pdf

And take a copy to play with:

	:::bash
	cp templates/newform-original.pdf templates/newform-template.pdf

Let's extract all the fields of the form as a yaml file (extension .form, to differentiate it from the 

	:::bash
	pdfform.py extract templates/newform-template.pdf templates/newform-template.form

Open the template.pdf with a PDF viewer (acrobat, okular...) and fill the form fields 
with content that would allow us to identify each one.
Save the filled form and extract again the form data

	:::bash
	pdfform.py extract templates/newform-template.pdf templates/newform-template.form

Edit templates/newform-template.yaml to set the values to something like `{var}` to be filled from YAML vars

If unsure you can apply the changes to the pdf and then edit them from the pdf viewer:

	:::bash
	pdfform.py apply templates/newform-original.pdf templates/newform-template.form templates/newform-template.pdf

Once you have templatized the yaml file, collect the vars you used on the templates in a template yaml file.

	:::bash
	nstemplate.py extract templates/newform-template.form templates/newform-template.yaml









