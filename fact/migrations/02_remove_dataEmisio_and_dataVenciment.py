#!/usr/bin/env python3


import sys
from yamlns import namespace as ns
from consolemsg import step, warn, error, success

for yaml in sys.argv[1:]:
	step(yaml)
	invoice = ns.load(yaml)
	if 'dataVenciment' not in invoice and 'dataEmisio' not in invoice:
		warn("Fields already were removed")
		continue
	if 'issueDate' not in invoice:
		error("cannot remove 'dataEmisio' if 'issueDate' is not already added (apply previous migration script)")
		continue
	if 'dueDate' not in invoice:
		error("cannot remove 'dataVenciment' if 'dueDate' is not already added (apply previous migration script)")
		continue
	del invoice.dataEmisio
	del invoice.dataVenciment
	success("Removed 'dataEmisio' and 'dataVenciment'")
	invoice.dump(yaml)



