#!/usr/bin/env python3


import sys
from yamlns import namespace as ns
from consolemsg import step, warn, success
from yamlns.dateutils import date

def insertafter(odict, key, item):
	keys = list(odict.keys())
	i = keys.index(key)
	values = list(odict.items())
	values.insert(i+1, item)
	return 	ns(values)

def replace(invoice, old, new):
	if old not in invoice:
		warn("Missing attribute '{}' to be migrated, ignoring".format(old))
		return invoice
	if new in invoice:
		warn("Attribute '{}' already existed, overwriting".format(new))
	try:
		newValue = date(invoice[old])
	except:
		error("Value '{}' for attribute '{}' is not a date".format(newValue, old))
		return invoice
	success("Adding attribute '{}' with value '{}'".format(new, newValue))
	return insertafter(invoice, old, (new, newValue))

for yaml in sys.argv[1:]:
	step(yaml)
	invoice = ns.load(yaml)
	invoice = replace(invoice, 'dataEmisio', 'issueDate')
	invoice = replace(invoice, 'dataVenciment', 'dueDate')
	invoice.dump(yaml)



