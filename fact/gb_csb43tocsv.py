#!/usr/bin/env python3
from fact.gb_descarregaextractes import csb43_to_csv
import sys
import csb43.csb43
from yamlns import namespace as ns

def main():
	csb43_to_csv(sys.argv[1], sys.argv[2], sys.argv[3])

if __name__ == '__main__':
	sys.exit(main())



