#!/usr/bin/env python3

import glob
import os
import re

from fact import tresoreria
from yamlns import namespace as ns
from yamlns.dateutils import Date
from consolemsg import warn, error, step, success
import sys

def providerNumberCleanUp(number):
	number = number.replace('/',' ')
	number = number.replace('-',' ')
	return '_'.join(number.split())

def providerNameCleanUp(name):
	"""
	>>> providerNameCleanUp("unnom")
	'Unnom'
	>>> providerNameCleanUp("un nom")
	'UnNom'
	>>> providerNameCleanUp("Un-nom S.L.U")
	'UnNomSLU'
	>>> providerNameCleanUp("Pc Componentes Y Multimedia SL") # In list
	'PcComponentes'
	"""
	separators='./:-,'
	for sep in separators:
		name = name.replace(sep, ' ')
	name = ''.join(name.title().split())

	providers = dict([
		('CablemàticSlu', 'Cablematic'),
		('NubiptelSLU', 'Nubip'),
		('EticomSccl', 'Eticom'),
		('LaPlataformaDeLaConstruccióSl', 'Plataforma'),
		('LandatelComunicacionesSL', 'Landatel'),
		('AubertSa', 'Aubert'),
		('PcComponentesYMultimediaSl','PcComponentes'),
		('BucofiGrupSl', 'Bucofi'),
		('SubministresIndustrialsDagsaSl','Dagsa'),
		('ReymanSl','Reyman'),
		('FerreteriaXaloc', 'Xaloc'),
		('DiotronicSa', 'Diotronic'),
		('Vil\'Hbo', 'VILHBO'),
		('SuministrosIndustrialRoviSl', 'Rovi'),
		('AdamEcotechSociedadAnónimaEs', 'AdamVoip'),
		('ElectronicaReymanSl', 'Reyman'),
		('ComercialEléctricaDelLlobregatSau', 'Cell'),
		('TeclisaTècniquesDeClimaISanitari', 'Teclisa'),
		('FerreteriaPujolSl', 'FerreteriaPujol'),
		('HostgatorCom', 'HostGatorCom'),
		('SuepratSL', 'Sueprat'),
		('ElPratBricoYSuministrosSl','PratBrico'),
		('MªSagrarioAlvarezJimenezNotario','Notaria'),
		('OficinaDeGestióEmpresarial','OGE'),
		('FerreteriasJaimeVallsCB', 'JaimeValls'),
		('FerreteriasGammaMiquelObradorSolà', 'GammaSJD'),
		('FerreteriasGammaMaterialesDeConstuccionYDecoracionAlonsoSL', 'GammaPrat'),
		('EstebanGòmezPinel', 'ImpactoVisual'),
		('TelefonicaDeEspañaSAU','Movistar'),
		('EuropeDomainsCoop', 'DomainsCoop'),
		('KamilosetasMuskariaSL', 'Kamilosetas'),
		('RaúlBeasHernández', 'RaulBeas'),
		('GrupoOdinSolucionesInformáticas','Slimbook'),
		('SomGestióSccl', 'SomGestio'),
		('PlanaFàbregaBarcelonaSl', 'PlanaFabrega'),
		('SomConnexióSccl', 'SomConnexio'),
		('MercadeEspaiDeBanySl', 'MercadeSL'),
		('BarcelonaLedIluminaciónSL', 'Bled'),
		('TamoilEspañaSA','Tamoil'),
		('OvhHispano', 'OVH'),
		])

	if name in providers:
		name = providers[name]
	return name

def purchase_invoiceFile(purchase, infix=''):
	"""
	Returns the filename of the purchase invoice (or ticket).
	"""
	year = purchase.number.split("-")[0]
	matchingFiles = glob.glob('compres{}/{}/{}*'.format(infix, year, purchase.number))
	if not matchingFiles: return None
	variations = set(
		os.path.splitext(f)[0] for f in matchingFiles)
	if len(variations) != 1:
		warn("Files for purchase {} have different basenames:\n{}".format(
			purchase.number,
			"\n".join(
				"\t- {}".format(f)
				for f in matchingFiles
				),
			))

	return matchingFiles[0]

def purchase_suggestedFileName(purchase, infix=''):
	"""
	Given a purchase returns the suggested filename base (no extension) for its invoice.
	"""
	year = purchase.number.split("-")[0]
	return "compres{}/{}/{}-{}-{}-{}".format(
		infix,
		year,
		purchase.number,
		purchase.date,
		providerNameCleanUp(purchase.providerName),
		providerNumberCleanUp(purchase.providerNumber),
		)


def csvFieldsToYaml(purchase):
	"""
	Takes a purchase in old recibidas.csv format and generates
	a regular purchase.
	"""
	p2=ns()
	p2.number = purchase.Num
	p2.date = Date(purchase.Data)
	p2.providerName = purchase.Proveidor
	p2.providerVat = purchase['NIF-Proveidor']
	p2.providerNumber = purchase['Numeracion Proveedor']
	p2.customer = purchase.Nif
	p2.payedBy = purchase.Pagador
	p2.description = purchase.Notas

	p2.taxableAmount = purchase['Base imponible']
	p2.vatAmount = purchase.IVA
	p2.totalAmount = purchase.total

	p2.bankReference = None
	if 'Referencia' in purchase:
		p2.bankReference = purchase.Referencia

	return p2

def purchases_loadCompresCsv(infix=''):
	import csv
	pagamentsFile = 'recibidas{}.csv'.format(infix)
	try: 
		with open(pagamentsFile) as csvfile:
			csvfile.readline()
			reader = csv.DictReader(csvfile, delimiter='\t')
			rebudes = [
				ns((k,v and v.strip()) for k,v in row.items())
				for row in reader if row['Num'][0]!='#']
	except FileNotFoundError as e:
		tresoreria._failNotInTresoreria(e)

	for factura in rebudes:
		try:
			factura['Base imponible'] = tresoreria.spfloat(factura['Base imponible'])
			factura.IVA = tresoreria.spfloat(factura.IVA)
			factura.total = factura['Base imponible'] + factura.IVA

			# Busca [whatever] en la columna de Notas
			matches = re.search(r'\[([^]]+)\]', factura.Notas)
			if matches:
				factura.Referencia = matches.group(1)
		except:
			error("Dades de factura rebuda incorrectes dins '{}':\n{}".format(
				pagamentsFile,
				factura.dump(),
			))
			raise
	return rebudes

def purchases_loadFromOld(infix=''):
	purchases = purchases_loadCompresCsv(infix)
	# turn into modern format
	return [
		csvFieldsToYaml(purchase)
		for purchase in purchases
		]

def purchases_summary(purchases, company):

	s = ns()

	s.totalPayments = sum(( f.totalAmount for f in purchases))

	payers = list(sorted(set( f.payedBy for f in purchases )))
	nifs = list(sorted(set( f.customer for f in purchases )))
	proveidors = list(sorted(set( f.providerName for f in purchases )))

	# purchases paid by means of company's bank
	s.sumProperlyPaid = sum(
		f.totalAmount for f in purchases
		if f.payedBy == company
		and f.customer == company
		)

	# purchases paid in cash by partners
	s.sumPaidByPartners = sum(
		f.totalAmount for f in purchases
		if f.payedBy != company
		and f.customer == company
		)

	s.paidByPartners = ns()
	for pagador in payers:
		if pagador == company : continue
		s.paidByPartners[pagador] = sum(
			f.totalAmount for f in purchases
			if f.payedBy == pagador
			and f.customer == company
			)

	# purchases by partners contributed to the company in kind
	s.sumContributionsInKind = sum(
		f.totalAmount for f in purchases
		if f.customer != company
		)

	s.contributionsInKind = ns()
	for pagador in payers:
		if pagador == company: continue
		s.contributionsInKind[pagador] = sum(
			f.totalAmount for f in purchases
			if f.payedBy == pagador
			and f.customer != company
			)

	# Classify purchases

	s.byVat = ns()
	for nifpagador in nifs:
		s.byVat[nifpagador] = sum(
			f.totalAmount for f in purchases
			if f.customer==nifpagador
			)
	s.byPayer = ns()
	for payer in payers:
		s.byPayer[payer] = sum(
			f.totalAmount for f in purchases
			if f.payedBy==payer
			)

	s.byProvider = ns()
	for provider in proveidors:
		s.byProvider[provider] = sum(
			f.totalAmount for f in purchases
			if f.providerName==provider
			)

	return s


def providers_fromPurchases(purchases):
	providers = ns()
	for vat, short, name in sorted(set(
			(p.providerVat, providerNameCleanUp(p.providerName), p.providerName)
			for p in purchases
		)):
		if short in providers:
			error("El proveïdor {} té diferents versions:\n- {}: {}\n- {}: {}\n"
				.format(short,
					vat, name,
					providers[short].nif, providers[short].nom,
					))
		item = ns()
		item.nif = vat
		item.nom = name
		providers[short or 'Unknown'] = item
	return providers

def providers_fromCsv():
	items = ns()
	with open('proveidors.csv') as proveidorsCsv:
		fields = [h.strip() for h in proveidorsCsv.readline().split('\t')]
		for nline, line in enumerate(proveidorsCsv,2):
			try:
				values = [v.strip() for v in line.split('\t')]
				if not values: continue
				if values[0][0] == '#': continue
				vat, short, name = values
				item = ns()
				item.nif = vat # TODO: not having vat
				item.nom = name
				items[short or 'Unknown'] = item

			except Exception as e:
				error("Error llegint 'proveidors.csv' a la linia {}: {}"
					.format(nline, e))
	return items

def providers_toCsv(providers):
	with open('proveidors.csv','w') as proveidorsCsv:
		fields = "vat short name".split()
		proveidorsCsv.write('\t'.join(fields)+'\n')
		proveidorsCsv.write(
			'\n'.join('\t'.join(
				[proveidor.nif, short, proveidor.nom])
			for short, proveidor in providers.items()))

def providers_fromYaml():
	return ns.load('proveidors.yaml')

def providers_toYaml(providers):
	with open('proveidors.yaml','w') as proveidorsYaml:
		providers.dump(proveidorsYaml)


def load_tests(loader, tests, ignore):
	import doctest
	tests.addTests(doctest.DocTestSuite())
	return tests


def main():
	company = tresoreria.currentCompany()
	companies = [ 
		(companyName, infix if companyName != company.short else '')
		for companyName, infix
		in [
			('At2', '-at2'),
			('GB', '-gb'),
			('FGB', '-fgb'),
		]]

	allpurchases = []
	summary = ns()
	for companyName, infix in companies:
		step("Compres de {}".format(companyName))
		purchases = purchases_loadFromOld(infix)
		purchasesByNumber = dict((p.number, p) for p in purchases)

		for invoice in sorted(glob.glob('compres{}/????/*'.format(infix))):
			invoiceNum = os.path.basename(invoice)[:len('2000-0000')]
			if invoiceNum not in purchasesByNumber:
				error("Factura no registrada {} {}".format(invoiceNum, invoice))

		for purchase in purchases:
#			step('\tChecking '+purchase.number)

			fromMetadata = purchase_suggestedFileName(purchase, infix)
			componentsFromMeta = fromMetadata.split('-')[:7]
			invoice = purchase_invoiceFile(purchase, infix)
			if not invoice:
				error("No s'ha trobat la versió electronica de la factura "
					"{number} {providerName} {providerNumber} {description}"
					.format(**purchase))
				continue

			invoiceBase = os.path.splitext(invoice)[0]
			componentsFromInvoice=invoiceBase.split('-')[:7]
			if componentsFromInvoice!=componentsFromMeta:
				error("No coincideixen els prefixos per a la factura {}\n"
					"- Esperat:  {}\n"
					"- Obtingut: {}\n"
					.format(
						purchase.number,
						invoiceBase,
						fromMetadata,
					))
	
			if False:
				step('\tGenerating '+purchase.number)
				p2 = ns()
				p2.number = purchase.number
				p2.date = purchase.date
				p2.provider = providerNameCleanUp(purchase.providerName)
				p2.providerNumber = purchase.providerNumber
				p2.customer = purchase.customer
				p2.payer = purchase.payedBy
				p2.description = purchase.description

				p2.taxableAmount = purchase.taxableAmount
				p2.vatAmount = purchase.vatAmount
				p2.totalAmount = purchase.totalAmount
				p2.bankReference = purchase.bankReference
				p2.dump(invoiceBase+'.yaml')

		summary[companyName]=purchases_summary(purchases, companyName)
		allpurchases += purchases

	providers = providers_fromPurchases(allpurchases)
	providers_toCsv(providers)
	providers_toYaml(providers)
	providers2 = providers_fromCsv()
	providers3 = providers_fromYaml()
	assert providers.dump() == providers2.dump()
	assert providers.dump() == providers3.dump()

	print(summary.dump())

if __name__ =='__main__':
	sys.exit(main())

