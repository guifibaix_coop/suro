#!/usr/bin/env python3

def sequence(string) :
	"""
	Interprets strings like the ones in the standard Print Dialog
	uses to specify pages to be printed.
	ie. "2,4,6-9,13" means "2, 4, from 6 to 9 and 13"

	>>> sequence("2")
	[2]
	>>> sequence("2,9")
	[2, 9]
	>>> sequence("2,9,4")
	[2, 4, 9]
	>>> sequence("2-4")
	[2, 3, 4]
	>>> sequence("2-4,9")
	[2, 3, 4, 9]
	>>> sequence("2-4,3-5")
	[2, 3, 4, 5]
	>>> sequence("2-4,b")
	Traceback (most recent call last):
	...
	ValueError: invalid literal for int() with base 10: 'b'
	>>> sequence("2-b,7")
	Traceback (most recent call last):
	...
	ValueError: invalid literal for int() with base 10: 'b'
	>>> sequence("2-4-5,7")
	Traceback (most recent call last):
	...
	ValueError: too many values to unpack (expected 2)
	"""
	def processItem(item):
		if '-' not in item: return [int(item)]
		a, b = item.split("-")
		return list(range(int(a),int(b)+1))

	return sorted(set(sum(
		( processItem(a) for a in string.split(',') )
		,[])))

def load_tests(loader, tests, ignore):
	import doctest
	tests.addTests(doctest.DocTestSuite())
	return tests

if __name__ == '__main__' :
	import unittest
	import sys
	sys.exit(unittest.main())




