#!/usr/bin/env python3

from yamlns import namespace
import sys
import os

def dummyAltaNubip() :
	return namespace(
		bc = '0123456789',
		ccc = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
		cliente = namespace(
			cargo = '',
			correo = '',
			cp = '',
			direccion = '',
			nif = '',
			nombre = '',
			poblacion = '',
			provincia = '',
			telefono = '',
			),
		servicio = namespace(
			cargo = '',
			correo = '',
			cp = '',
			direccion = '',
			nombre = '',
			poblacion = '',
			provincia = '',
			telefono = '',
			),
		lineaPortada = '',
		comentarios = '',
		orden = namespace(
			duracionEnMeses = '',
			fecha = '',
			),
		precios = namespace(
			comunicacion = namespace(
				instalacion = '',
				quotamensual = '',
				),
			mobil = namespace(
				instalacion = '',
				quotamensual = '',
				),
			otros = namespace(
				instalacion = '',
				quotamensual = '',
				),
			total = namespace(
				instalacion  = '',
				quotamensual = '',
				),
			),
		configuracion = ['']*9,
		equipamiento = ['']*9,
		modalidadservicio = ['']*8,
		servicios =  ['']*10,
		)


masters = [
	'ContratoPortabilidadNubip',
	'SolicitudYContratoServicios',
]

def generate_contractesNubip(data, outputPrefix, masters=masters) :
	import pdfform
	for master in masters :
		masterBase = os.path.join(os.path.dirname(__file__), 'templates', master)
		originalPdf = masterBase+'-original.pdf'
		formTemplate = masterBase+'-template.form'
		destination = outputPrefix+'-'+master+'.pdf'
		with open(formTemplate) as f :
			template = f.read()
		formData = template.format(**data)
		pdfform.fill(originalPdf, formData, destination)


def main(args=sys.argv) :
	import argparse

	parser = argparse.ArgumentParser(
		description="Genera contractes d'alta de Nubip donat un arxiu YAML.",
		)
	parser.add_argument(
		'--test',
		action='store_true',
		help="Run unittests",
		)
	parser.add_argument(
		'input',
#		type=argparse.FileType('r'),
		nargs='?',
		metavar="input.yaml",
		help="YAML file containing the new contract data",
		)
	parser.add_argument(
		'output',
#		type=argparse.FileType('w'),
		nargs='?',
		metavar="outputPrefix",
		help="prefix for the output pdf files",
		)
	parser.add_argument(
		"--dump",
		dest='dump',
		metavar = "dumped.yaml",
		help="Dumps a dummy YAML invoice file",
		)

	args = parser.parse_args()

	if args.input :
		data = namespace.load(args.input)
	else :
		data = dummyAltaNubip()

	if args.dump :
		data.dump(args.dump)
		return 0

	import os
	outputPrefix = args.output if args.output else (
		os.path.splitext(args.input)[0] if args.input else "output")
	generate_contractesNubip(data, outputPrefix)

	return 0

if __name__ == '__main__' :
	if '--test' in sys.argv :
		sys.argv.remove('--test')
		sys.argv[0]+=" --test"
		import unittest
		sys.exit(unittest.main())

	sys.exit(main())

