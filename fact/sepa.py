#!/usr/bin/env python3
"""
This module provides functions to generate the 19-14 or 19-15 files
to initiate a SEPA Direct Debit - Core Scheme (SDD Core).
That in human words, means that it generates the files
you must send the bank to perform a debit to your clients.
"""

from yamlns.dateutils import compactDate

"""
TODO's
- Mandate Reference: Unique within creditor? Universal?
- Debit Reference: Unique within creditor? Universal?
- Creditor ID clearing: Remove ASCII non-alphanumeric
- Creditor ID clearing: force uppercase
- Creditor ID check: Bad comercial code if they are not 3 digits when space trimmed
- Creditor ID check: Longer than 28
- Creditor ID check: Country code format (two alpha)
- Creditor ID check: Country cody exists
- Should semicolon be kept in formatField?
- Check mandate reference (AT-01) has no left spaces
- Debtor country as data, defaulting ES
"""

# Field formating functions

def zeroPad(numberOrString, length) :
	try :
		intValue = int(numberOrString)
	except ValueError :
		raise ValueError("Not an integer '{}'".format(numberOrString))
	strValue = '{{:0{}d}}'.format(length).format(intValue)
	if len(strValue) != length :
		raise ValueError("Expected a number with {} digits, got {}".format(length, numberOrString))
	return strValue

def formatField(text, length) :
	orig = 'áéíóúàèìòùäëïöüâêîôûñçªº'
	dest = 'aeiouaeiouaeiouaeiouncao'
	text = text.translate(dict(((ord(o),ord(d)) for o,d in zip(orig, dest))))
	text = ''.join(( c for c in text if c.isalnum() or c in " /-?:().,'+;"))
	text = text.upper()
	text = text.ljust(length)
	text = text[:length]
	return text

def formatMoney(amount, positions=17) :
	try:
		value = float(amount)
	except ValueError:
		raise ValueError("Not a monetary amount '{}'".format(amount))
	return zeroPad(int(round(value*100)), positions)

def formatDate(date) :
	return compactDate(date)

import unittest
class FormatUtils_Test(unittest.TestCase) :
	def test_zeroPad_withNiceString(self) :
		self.assertEqual(zeroPad("002",3), '002')
	def test_zeroPad_withNiceString(self) :
		self.assertEqual(zeroPad("002",4), '0002')
	def test_zeroPad_withNumber(self) :
		self.assertEqual(zeroPad(2,3), '002')
	def test_zeroPad_withBigNumber(self) :
		with self.assertRaises(ValueError) as e :
			zeroPad(2000,3)
		self.assertEqual(str(e.exception),
			"Expected a number with 3 digits, got 2000")
	def test_zeroPad_withNotNumber(self) :
		with self.assertRaises(ValueError) as e :
			zeroPad("a",3)
		self.assertEqual(str(e.exception),
			"Not an integer 'a'")

	def test_formatField_addsPadding(self) :
		self.assertEqual(formatField("HELLO", 8), "HELLO   ")
	def test_formatField_turnsUpper(self) :
		self.assertEqual(formatField("hello", 8), "HELLO   ")
	def test_formatField_cutsExcess(self) :
		self.assertEqual(formatField("HELLO", 4), "HELL")
	def test_formatField_cleansWeirdChars(self) :
		self.assertEqual(formatField("HE[[O", 8), "HEO     ")
	def test_formatField_keepsAllowedPunctuation(self) :
		self.assertEqual(formatField("HELLO /-?:().,'+", 18), "HELLO /-?:().,'+  ")
	def test_formatField_translateAccents(self) :
		self.assertEqual(formatField("áéíóú àèìòù äëïöü âêîôû ñç",26),
			"AEIOU AEIOU AEIOU AEIOU NC")
	def test_formatField_translateOrdinals(self) :
		self.assertEqual(formatField("ºª",26),
			"OA"+24*" ")

	def test_formatMoney_withoutDecimals(self) :
		self.assertEqual(formatMoney(200),
			'00000000000020000')
	def test_formatMoney_withDecimals(self) :
		self.assertEqual(formatMoney(200.23),
			'00000000000020023')
	def test_formatMoney_withString(self) :
		self.assertEqual(formatMoney('200.23'),
			'00000000000020023')
	def test_formatMoney_withBadString(self) :
		with self.assertRaises(ValueError) as e:
			formatMoney('caca'),
		self.assertEqual(e.exception.args[0],
			"Not a monetary amount 'caca'")
	def test_formatMoney_withDifferent(self) :
		self.assertEqual(formatMoney(200.23, 11),
			'00000020023')
	def test_formatMoney_rounding(self) :
		self.assertEqual(formatMoney(200.239, 11),
			'00000020024')

	def test_formatDate(self) :
		self.assertEqual(formatDate(datetime.date(2014,1,5)),
			'20140105')
	def test_formatDate_withDateTime(self) :
		self.assertEqual(formatDate(datetime.datetime(2014,1,5)),
			'20140105')
	def test_formatDate_withTuple(self) :
		self.assertEqual(formatDate((2014,1,5)),
			'20140105')
	def test_formatDate_withIsoString(self) :
		self.assertEqual(formatDate("2014-01-05"),
			'20140105')
	def test_formatDate_withCompressedString(self) :
		self.assertEqual(formatDate("20140105"),
			'20140105')


# Creditor identifier functions

def at02_toDigits(text) :
	_alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	return ''.join((str(_alphabet.index(c)) for c in text))

def at02_controlDigits(text) :
	return '{:02d}'.format(98-int(text)%97)


def creditorIdentifier(nationalId, countryCode='ES', suffix='ZZZ') :
	# http://inza.wordpress.com/2013/10/25/como-preparar-los-mandatos-sepa-identificador-del-acreedor/
	try :
		suffix = '{:03d}'.format(suffix)
	except ValueError :
		pass

	controlDigits = at02_controlDigits(at02_toDigits(nationalId + countryCode + '00'))
	return countryCode + controlDigits + suffix + nationalId

import unittest
class Sepa_CreditorIdentifier_Test(unittest.TestCase) :
	def test_toDigits_withJustNumbers(self) :
		self.assertEqual(
			at02_toDigits(
				"B85626240ES00"),
				"1185626240142800")

	def test_creditorIdentifier(self) :
		self.assertEqual(
			"ES77ZZZB85626240",
			creditorIdentifier(
				nationalId='B85626240',
				))

	def test_creditorIdentifier_withOtherCountry(self) :
		self.assertEqual(
			"AT13ZZZB85626240",
			creditorIdentifier(
				nationalId='B85626240',
				countryCode='AT',
				))

	def test_creditorIdentifier_withSuffixSameControl(self) :
		self.assertEqual(
			"ES77037B85626240",
			creditorIdentifier(
				nationalId='B85626240',
				suffix=37,
				))

	def test_creditorIdentifier_withSuffixAsText(self) :
		self.assertEqual(
			"ES77KKKB85626240",
			creditorIdentifier(
				nationalId='B85626240',
				suffix='KKK',
				))

	@unittest.skip('stdnum.eu.at_02 not available yet')
	def test_validateCreditorIdentifier(self) :
		import stdnum.eu.at_02
		self.assertTrue(
			stdnum.eu.at_02.validate(creditorIdentifier('36517097C')))


# SEPA lines


def presenterLine(
		presenterId,
		presenterName,
		creationDate,
		creationTime,
		recipientEntity,
		recipientOffice,
		reference,
		fastPayment=False,
		) :
	# TODO: Check wrong date (size and content)
	# TODO: Check wrong time (size and content)
	return ''.join((
		'01', # Register code
		'19154' if fastPayment else '19143', # Version 19-14 DC:3, alternativa 19154 (version rapida)
		'001', # Data number
		formatField(presenterId, 35), # Presenter ID (35)
		formatField(presenterName, 70), # Presenter Name (70)
		creationDate, # File creation date
		# Message identification
			'PRE', # Debit presentation
			creationDate,
			creationTime, # HHMMSSmmmmm
			formatField(reference,13), # Presenter assigned id
		zeroPad(recipientEntity, 4),
		zeroPad(recipientOffice, 4),
		(' '*434) # padding
	))
	
def creditorLine(
		creditorId,
		creditorName,
		creditorAddress,
		creditorAccount,
		dueDate,
		creditorPostalCode='',
		creditorCity='',
		creditorState='',
		fastPayment=False,
		) :
	creditorCountry = creditorId[:2]
	return ''.join((
		'02', # Register code
		'19154' if fastPayment else '19143', # Version 19-14 DC:3, alternativa 19154 (version rapida)
		'002', # Data number
		formatField(creditorId, 35),
		formatDate(dueDate),
		formatField(creditorName, 70),
		formatField(creditorAddress, 50),
		formatField(creditorPostalCode, 6),
		formatField(creditorCity, 44),
		formatField(creditorState, 40),
		formatField(creditorCountry, 2),
		formatField(creditorAccount, 34),
		' '*301
		))

def mandatoryIndividualDebit(
		debitReference,
		mandateReference,
		mandateSequence,
			# FRST (first), RCUR (not first nor last),
			# FNAL (last), OOFF (no recurrent)
		mandateDate,
		debtorAccount,
		concept,
		amount,
		debtorBic,
		debtorName,
		debtorAddress,
		debtorCity,
		debtorPostalCode,
		debtorState,
#		isPhysicalPerson,
		debtorId='',
		debtorCountry='ES',
		fastPayment=False,
		) :

#	debtorCountry = debtorId[:2]
	if mandateSequence not in ('FRST', 'RCUR', 'FNAL', 'OOFF') :
		pass # TODO: raise exception

	fullDebtorId = 'J' + debtorId if debtorId else ''

	return ''.join((
		'03', # Register code
		'19154' if fastPayment else '19143', # Version 19-14 DC:3, alternativa 19154 (version rapida)
		'003', # Data number
		formatField(debitReference, 35), # debitReference
		formatField(mandateReference, 35),
		formatField(mandateSequence, 4),
		'    ', # categoria de proposito
		formatMoney(amount, 11),
		formatDate(mandateDate),
		formatField(debtorBic,11), # Entidad del deudor BIC
		formatField(debtorName, 70),
		formatField(debtorAddress, 50),
		formatField(debtorPostalCode+' '+debtorCity, 50),
		formatField(debtorState, 40),
		formatField(debtorCountry, 2),
		'2' if debtorId else ' ', # debtorId type, 1: organization 2: physical person
		formatField(fullDebtorId, 36), # debtorId
		formatField('NIF' if debtorId else '', 35), # kind of debtorId if not BIC (a banc)
		'A', # Means account is codified as IBAN
		formatField(debtorAccount,34),
		formatField('',4), # Proposito del adeudo ??
		formatField(concept, 140),
		' '*19
		))


def fileTotalLine(
		totalAmount, # amount sharing date and creditor
		nDebits, # how many 03 registers included
		nLines, # how many registers included
		) :
	return ''.join((
		'99', # Register code (Total for date and creditor)
		formatMoney(totalAmount),
		zeroPad(nDebits, 8),
		zeroPad(nLines, 10),
		' '*563,
		))


def creditorTotalLine(
		creditorId,
		totalAmount, # amount sharing date and creditor
		nDebits, # how many 03 registers included
		nLines, # how many registers included
		) :
	return ''.join((
		'05', # Register code (Total for date and creditor)
		formatField(creditorId, 35),
		formatMoney(totalAmount),
		zeroPad(nDebits, 8),
		zeroPad(nLines, 10),
		' '*528,
		))


def dateCreditorTotalLine(
		creditorId,
		dueDate,
		totalAmount, # amount sharing date and creditor
		nDebits, # how many 03 registers included
		nLines, # hom many registers included
		) :
	return ''.join((
		'04', # Register code (Total for date and creditor)
		formatField(creditorId, 35),
		formatDate(dueDate),
		formatMoney(totalAmount),
		zeroPad(nDebits, 8),
		zeroPad(nLines, 10),
		' '*520,
		))

class SepaN1914Lines_Test(unittest.TestCase) :

	def test_presenterLine(self) :
		self.maxDiff = None
		self.assertEqual(
			presenterLine(
				presenterId=creditorIdentifier('B4523678', suffix=0),
				presenterName="Academia de clases particulares",
				creationDate='20141024',
				creationTime='10352112345',
				recipientEntity=75,
				recipientOffice=1,
				reference='UNAREFERENCIA',
				),
				'0119143001'
				'ES04000B4523678                    '
				'ACADEMIA DE CLASES PARTICULARES                                       '
				'20141024'
				'PRE2014102410352112345UNAREFERENCIA'
				'00750001'
				.ljust(600))

	def test_creditorLine(self) :
		self.maxDiff=None
		self.assertEqual(
			creditorLine(
				creditorId=creditorIdentifier('B4523678', suffix=0),
				creditorName='Empresa de ejemplo',
				creditorAddress="C/. Mariano Granados, N- 15",
				creditorPostalCode="08820",
				creditorCity="El Prat de Llobregat",
				creditorState="Barcelona",
				creditorAccount='ES1300750001840000099999',
				dueDate=datetime.date(2014,10,24),
				),

				'0219143002'
				'ES04000B4523678                    '
				'20141024'
				'EMPRESA DE EJEMPLO                                                    '
				'C/. MARIANO GRANADOS, N- 15                       '
				'08820 EL PRAT DE LLOBREGAT                        '
				'BARCELONA                               '
				'ES'
				'ES1300750001840000099999          '
				.ljust(600))


	def test_dateCreditorTotalLine(self) :
		self.maxDiff = None
		self.assertEqual(
			dateCreditorTotalLine(
				creditorId=creditorIdentifier('B4523678', suffix=0),
				dueDate=datetime.date(2014,10,24),
				totalAmount=500,
				nDebits=1,
				nLines=2,
				),
				'04'
				'ES04000B4523678                    '
				'20141024'
				'00000000000050000'
				'00000001'
				'0000000002'
				.ljust(600)
				)


	def test_creditorTotalLine(self) :
		self.maxDiff = None
		self.assertEqual(
			creditorTotalLine(
				creditorId=creditorIdentifier('B4523678', suffix=0),
				totalAmount=555,
				nDebits=1,
				nLines=3,
				),
				'05'
				'ES04000B4523678                    '
				'00000000000055500'
				'00000001'
				'0000000003'
				.ljust(600)
				)

	def test_fileTotalLine(self) :
		self.maxDiff = None
		self.assertEqual(
			fileTotalLine(
				totalAmount=555.99,
				nDebits=1,
				nLines=3,
				),
				'99'
				'00000000000055599'
				'00000001'
				'0000000003'
				.ljust(600)
				)


	def test_mandatoryIndividualDebit_withNIF(self) :
		self.maxDiff = None
		self.assertEqual(
			mandatoryIndividualDebit(
				mandateDate = datetime.date(2014,1,10),
				mandateReference = 'REF0001',
				mandateSequence = 'RCUR',
				debitReference = '2014102414563300001',
				debtorName = 'Electro Shack Fernández',
				debtorId = '12345678C',
				debtorAddress = 'Carrer Doctor Octopus 8 8o 8a',
				debtorCity = 'Sant Joan Despí',
				debtorPostalCode = '08970',
				debtorState = 'Barcelona',
				debtorAccount = 'ES7712341234161234567890',
				debtorBic = 'PRBAESM1XXX',
				concept = 'Quota 4 trimestre 2014',
				amount = 440.44,
				),
				'0319143003'
				'2014102414563300001                '
				'REF0001                            '
				'RCUR'
				'    '
				'00000044044'
				'20140110'
				'PRBAESM1XXX'
				'ELECTRO SHACK FERNANDEZ                                               '
				'CARRER DOCTOR OCTOPUS 8 8O 8A                     '
				'08970 SANT JOAN DESPI                             '
				'BARCELONA                               '
				'ES2J12345678C                          '
				'NIF                                '
				'AES7712341234161234567890              '
				'QUOTA 4 TRIMESTRE 2014'
				.ljust(600)
			)


	def test_mandatoryIndividualDebit_different(self) :
		self.maxDiff = None
		self.assertEqual(
			mandatoryIndividualDebit(
				mandateDate = datetime.date(2009,10,31),
				mandateReference = '000A82602871-3-0-B-4523678900000000',
				mandateSequence = 'RCUR',
				debitReference='000000000003',
				debtorName = 'Colegio Residencia Madrid',
				debtorId = '',
				debtorAddress = 'Proción, 1',
				debtorCity = 'Madrid',
				debtorPostalCode = '28019',
				debtorState = '',
				debtorAccount = 'ES1100091111650009999333',
				debtorBic = '',
				concept = 'Clases de Filosofía.: 250.00;',
				amount = 250.04,
				),
				'0319143003'
				'000000000003                       '
				'000A82602871-3-0-B-4523678900000000'
				'RCUR'
				'    '
				'00000025004'
				'20091031'
				'           '
				'COLEGIO RESIDENCIA MADRID                                             '
				'PROCION, 1                                        '
				'28019 MADRID                                                                              '
				'ES'
				'                                                                        '
				'AES1100091111650009999333              '
				'CLASES DE FILOSOFIA.: 250.00;'
				.ljust(600)
		)

def at02CreditorIdentifier(creditor) :
	return creditorIdentifier(
		creditor.nif,
		suffix=creditor.get("suffix", 0),
		countryCode=creditor.get("country", 'ES'),
		)


from yamlns import namespace as ns
from iban import ibanEntity, ibanOffice, compact

class SepaN1914(ns) :
	def __init__(self,
			creation,
			presenter,
			reference,
			debits=None,
			fastProcedure=False,
			) :
		super(SepaN1914, self).__init__(
			reference=reference,
			creation=creation,
			presenter=presenter,
			debits=[] if debits is None else debits,
			fastProcedure=fastProcedure,
			)

	def creditorLine(self, creditor, dueDate) :
		return creditorLine(
			creditorId=at02CreditorIdentifier(creditor),
			creditorName=creditor.name,
			creditorAddress=creditor.address,
			creditorAccount=compact(creditor.iban),
			creditorPostalCode=creditor.postalcode,
			creditorCity=creditor.city,
			creditorState=creditor.state,
			dueDate=dueDate,
			fastPayment=self.fastProcedure,
			)
	def mandatoryIndividualDebit(self, debit) :
		return mandatoryIndividualDebit(
			mandateDate = debit.mandate.date,
			mandateReference = debit.mandate.reference,
			mandateSequence = debit.mandate.sequence,
			debitReference = debit.reference,
			debtorName = debit.debtor.name,
			debtorId = debit.debtor.nif,
			debtorAddress = debit.debtor.address,
			debtorCity = debit.debtor.city,
			debtorPostalCode = debit.debtor.postalcode,
			debtorState = debit.debtor.state,
			debtorAccount = compact(debit.debtor.iban),
			debtorBic = debit.debtor.bic,
			concept = debit.concept,
			amount = debit.amount,
			fastPayment=self.fastProcedure,
			)
		

	def generateCreditorLines(self) :
		if not self.debits : return []
		lines = []
		dates = sorted(set(((
			debit.creditor.nif if debit.creditor else self.presenter.nif,
			debit.dueDate,
			)
			for debit in self.debits
			)))
		for creditorNif in sorted(set((nif for nif,date in dates))) :
			creditorTotal = 0
			creditorDebits = 0
			creditorLines = []
			for dueDate in (date for c,date in dates if c== creditorNif) :
				dateCreditorDebits = 0
				dateCreditorTotal = 0
				dateCreditorLines = []
				for debit in self.debits :
					if debit.dueDate != dueDate: continue
					creditor = debit.creditor or self.presenter
					creditorId = at02CreditorIdentifier(creditor)
					if creditor.nif != creditorNif: continue
					if not dateCreditorLines :
						dateCreditorLines += [ self.creditorLine(creditor, dueDate), ]
					dateCreditorDebits += 1
					dateCreditorTotal += debit.amount
					dateCreditorLines += [ self.mandatoryIndividualDebit(debit) ]
				dateCreditorLines += [ 
					dateCreditorTotalLine(
						creditorId=creditorId,
						dueDate=dueDate,
						totalAmount=dateCreditorTotal,
						nDebits=dateCreditorDebits,
						nLines=len(dateCreditorLines)+1,
						),
					]
				creditorLines += dateCreditorLines
				creditorTotal += dateCreditorTotal
				creditorDebits += dateCreditorDebits
			creditorLines += [ 
				creditorTotalLine(
					creditorId=creditorId,
					totalAmount=creditorTotal,
					nDebits=creditorDebits,
					nLines=len(creditorLines)+1,
					)
				]
			lines+=creditorLines
		return lines

	def generate(self) :
		creditorLines = self.generateCreditorLines()
		return '\r\n'.join([
			presenterLine(
				presenterId=at02CreditorIdentifier(self.presenter), 
				presenterName=self.presenter.name,
				creationDate=formatDate(self.creation),
				creationTime=self.creation.strftime('%H%M%S%f')[:-1],
				# TODO Specification is shit: "5 positions of milliseconds"
				# two upper digits should be always zero,
				# but provided example has nonzero digits.
				# Interpretation: 5 decimal fractions of second
				recipientEntity=ibanEntity(self.presenter.iban),
				recipientOffice=ibanOffice(self.presenter.iban),
				fastPayment=self.fastProcedure,
				reference=self.reference,
				),
			] + creditorLines + [
			fileTotalLine(
				totalAmount=sum((debit.amount for debit in self.debits)),
				nDebits=len(self.debits),
				nLines=len(creditorLines)+2,
				),
			])

	def addDebit(self,
			reference,
			dueDate,
			concept,
			amount,
			debtor,
			mandate,
			creditor=None,
			):
		self.debits.append(ns(
			reference = reference,
			dueDate = dueDate,
			concept = concept,
			amount = amount,
			debtor = debtor,
			mandate = mandate,
			creditor = creditor,
			))

import datetime
class DummyTestData(object) :
	debtor1 = ns(
		name = 'Electro Shack Fernández',
		nif = '12345678C',
		address = 'Carrer Doctor Octopus 8 8o 8a',
		city = 'Sant Joan Despí',
		postalcode = '08970',
		state = 'Barcelona',
		iban = 'ES7712341234161234567890',
		bic = 'PRBAESM1XXX',
		)
	debtor2 = ns(
		name = 'Colegio Residencia Madrid',
		nif = '',
		address = 'Proción, 1',
		city = 'Madrid',
		postalcode = '28019',
		state = '',
		iban = 'ES1100091111650009999333',
		bic = '',
		)
	mandate = ns(
		date = datetime.date(2014,1,10),
		reference = 'REF0001',
		sequence = 'RCUR',
		)
	mandate2 = ns(
		date = datetime.date(2014,4,4),
		reference = 'REF0002',
		sequence = 'FRST',
		)
	presenter = ns(
		nif='B7123456',
		name='Gestoría Factúrez',
		iban='ES1300750001840000099999',
		)
	creditor = ns(
		nif='B4523678',
		name='Academia de clases particulares',
		address="C/. Mariano Granados, N- 15",
		city='El Prat de Llobregat',
		postalcode='08820',
		state='Barcelona',
		iban='ES1300750001840000099999',
		)
	creditor2 = ns(
		nif='36517097C',
		name='David García Garzón',
		address="C/. El Lokal 69",
		postalcode='08970',
		suffix=3,
		city='Sanllu',
		state='Makropolia',
		iban='ES1300750001840000077777',
		)


import unittest
class SepaN1914_Full_Test(unittest.TestCase, DummyTestData) :

	def test_sepa_withNoDebits(self) :
		self.maxDiff = None

		sepa = SepaN1914(
			reference='REFERENCIAXXX',
			creation = datetime.datetime(2014,10,24,10,35,21,123456),
			presenter = self.creditor,
			)
		self.assertEqual(sepa.generate(),
			'\r\n'.join(register.ljust(600) for register in [
			'0119143001'
			'ES04000B4523678                    '
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'20141024'
			'PRE2014102410352112345REFERENCIAXXX'
			'0075'
			'0001'
			,
			'99'
			'00000000000000000'
			'00000000'
			'0000000002'
		]))


	def test_sepa_withSingleDebit(self) :
		self.maxDiff = None

		sepa = SepaN1914(
			reference='REFERENCIAXXX',
			creation = datetime.datetime(2014,10,24,10,35,21,123456),
			presenter = self.creditor,
			)
		sepa.addDebit(
			reference = '2014102414563300001',
			dueDate = datetime.date(2014,11,24),
			concept = "Quota 4 Trimestre 2014",
			amount = 200.31,
			debtor = self.debtor1,
			mandate = self.mandate,
			)
		self.assertMultiLineEqual(sepa.generate(),
			'\r\n'.join(register.ljust(600) for register in [
			'01'
			'19143001'
			'ES04000B4523678                    '
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'20141024'
			'PRE2014102410352112345REFERENCIAXXX'
			'0075'
			'0001'
			,
			'02'
			'19143002'
			'ES04000B4523678                    '
			'20141124'
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'C/. MARIANO GRANADOS, N- 15                       '
			'08820 EL PRAT DE LLOBREGAT                        '
			'BARCELONA                               '
			'ES'
			'ES1300750001840000099999          '
			,
			'03'
			'19143003'
			'2014102414563300001                '
			'REF0001                            '
			'RCUR'
			'    '
			'00000020031'
			'20140110'
			'PRBAESM1XXX'
			'ELECTRO SHACK FERNANDEZ                                               '
			'CARRER DOCTOR OCTOPUS 8 8O 8A                     '
			'08970 SANT JOAN DESPI                             '
			'BARCELONA                               '
			'ES2J12345678C                          '
			'NIF                                '
			'AES7712341234161234567890              '
			'QUOTA 4 TRIMESTRE 2014'
			,
			'04'
			'ES04000B4523678                    '
			'20141124'
			'00000000000020031'
			'00000001'
			'0000000003'
			,
			'05'
			'ES04000B4523678                    '
			'00000000000020031'
			'00000001'
			'0000000004'
			,
			'99'
			'00000000000020031'
			'00000001'
			'0000000006'
		]))
	def test_sepa_singleCredit_fastProcedure(self) :
		self.maxDiff = None

		sepa = SepaN1914(
			reference='REFERENCIAXXX',
			creation = datetime.datetime(2014,10,24,10,35,21,123456),
			presenter = self.creditor,
			fastProcedure=True,
			)
		sepa.addDebit(
			reference = '2014102414563300001',
			dueDate = datetime.date(2014,11,24),
			concept = "Quota 4 Trimestre 2014",
			amount = 200.31,
			debtor = self.debtor1,
			mandate = self.mandate,
			)
		self.assertMultiLineEqual(sepa.generate(),
			'\r\n'.join(register.ljust(600) for register in [
			'01'
			'19154001'
			'ES04000B4523678                    '
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'20141024'
			'PRE2014102410352112345REFERENCIAXXX'
			'0075'
			'0001'
			,
			'02'
			'19154002'
			'ES04000B4523678                    '
			'20141124'
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'C/. MARIANO GRANADOS, N- 15                       '
			'08820 EL PRAT DE LLOBREGAT                        '
			'BARCELONA                               '
			'ES'
			'ES1300750001840000099999          '
			,
			'03'
			'19154003'
			'2014102414563300001                '
			'REF0001                            '
			'RCUR'
			'    '
			'00000020031'
			'20140110'
			'PRBAESM1XXX'
			'ELECTRO SHACK FERNANDEZ                                               '
			'CARRER DOCTOR OCTOPUS 8 8O 8A                     '
			'08970 SANT JOAN DESPI                             '
			'BARCELONA                               '
			'ES2J12345678C                          '
			'NIF                                '
			'AES7712341234161234567890              '
			'QUOTA 4 TRIMESTRE 2014'
			,
			'04'
			'ES04000B4523678                    '
			'20141124'
			'00000000000020031'
			'00000001'
			'0000000003'
			,
			'05'
			'ES04000B4523678                    '
			'00000000000020031'
			'00000001'
			'0000000004'
			,
			'99'
			'00000000000020031'
			'00000001'
			'0000000006'
		]))

	def test_sepa_withDistinctCreditorAndPresenter(self) :
		self.maxDiff = None

		sepa = SepaN1914(
			reference='REFERENCIAXXX',
			creation = datetime.datetime(2014,10,24,10,35,21,123456),
			presenter = self.presenter,
			)
		sepa.addDebit(
			reference = '2014102414563300001',
			dueDate = datetime.date(2014,11,24),
			concept = "Quota 4 Trimestre 2014",
			amount = 200.31,
			debtor = self.debtor1,
			creditor = self.creditor,
			mandate = self.mandate,
			)
		self.assertMultiLineEqual(sepa.generate(),
			'\r\n'.join(register.ljust(600) for register in [
			'01'
			'19143001'
			'ES48000B7123456                    '
			'GESTORIA FACTUREZ                                                     '
			'20141024'
			'PRE2014102410352112345REFERENCIAXXX'
			'0075'
			'0001'
			,
			'02'
			'19143002'
			'ES04000B4523678                    '
			'20141124'
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'C/. MARIANO GRANADOS, N- 15                       '
			'08820 EL PRAT DE LLOBREGAT                        '
			'BARCELONA                               '
			'ES'
			'ES1300750001840000099999          '
			,
			'03'
			'19143003'
			'2014102414563300001                '
			'REF0001                            '
			'RCUR'
			'    '
			'00000020031'
			'20140110'
			'PRBAESM1XXX'
			'ELECTRO SHACK FERNANDEZ                                               '
			'CARRER DOCTOR OCTOPUS 8 8O 8A                     '
			'08970 SANT JOAN DESPI                             '
			'BARCELONA                               '
			'ES2J12345678C                          '
			'NIF                                '
			'AES7712341234161234567890              '
			'QUOTA 4 TRIMESTRE 2014'
			,
			'04'
			'ES04000B4523678                    '
			'20141124'
			'00000000000020031'
			'00000001'
			'0000000003'
			,
			'05'
			'ES04000B4523678                    '
			'00000000000020031'
			'00000001'
			'0000000004'
			,
			'99'
			'00000000000020031'
			'00000001'
			'0000000006'
		]))

	def test_sepa_withManyDebitsSingleDate(self) :
		self.maxDiff = None

		sepa = SepaN1914(
			reference='REFERENCIAXXX',
			creation = datetime.datetime(2014,10,24,10,35,21,123456),
			presenter = self.creditor,
			)
		sepa.addDebit(
			reference = '2014102414563300001',
			dueDate = datetime.date(2014,11,24),
			concept = "Quota 4 Trimestre 2014",
			amount = 200.31,
			debtor = self.debtor1,
			mandate = self.mandate,
			)
		sepa.addDebit(
			reference = '2014102414563300002',
			dueDate = datetime.date(2014,11,24),
			concept = "Quota 4 Trimestre 2014",
			amount = 400.52,
			debtor = self.debtor1,
			mandate = self.mandate2,
			)
		self.assertMultiLineEqual(sepa.generate(),
			'\r\n'.join(register.ljust(600) for register in [
			'01'
			'19143001'
			'ES04000B4523678                    '
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'20141024'
			'PRE2014102410352112345REFERENCIAXXX'
			'0075'
			'0001'
			,
			'02'
			'19143002'
			'ES04000B4523678                    '
			'20141124'
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'C/. MARIANO GRANADOS, N- 15                       '
			'08820 EL PRAT DE LLOBREGAT                        '
			'BARCELONA                               '
			'ES'
			'ES1300750001840000099999          '
			,
			'03'
			'19143003'
			'2014102414563300001                '
			'REF0001                            '
			'RCUR'
			'    '
			'00000020031'
			'20140110'
			'PRBAESM1XXX'
			'ELECTRO SHACK FERNANDEZ                                               '
			'CARRER DOCTOR OCTOPUS 8 8O 8A                     '
			'08970 SANT JOAN DESPI                             '
			'BARCELONA                               '
			'ES2J12345678C                          '
			'NIF                                '
			'AES7712341234161234567890              '
			'QUOTA 4 TRIMESTRE 2014'
			,
			'03'
			'19143003'
			'2014102414563300002                '
			'REF0002                            '
			'FRST'
			'    '
			'00000040052'
			'20140404'
			'PRBAESM1XXX'
			'ELECTRO SHACK FERNANDEZ                                               '
			'CARRER DOCTOR OCTOPUS 8 8O 8A                     '
			'08970 SANT JOAN DESPI                             '
			'BARCELONA                               '
			'ES2J12345678C                          '
			'NIF                                '
			'AES7712341234161234567890              '
			'QUOTA 4 TRIMESTRE 2014'
			,
			'04'
			'ES04000B4523678                    '
			'20141124'
			'00000000000060083'
			'00000002'
			'0000000004'
			,
			'05'
			'ES04000B4523678                    '
			'00000000000060083'
			'00000002'
			'0000000005'
			,
			'99'
			'00000000000060083'
			'00000002'
			'0000000007'
		]))

	def test_sepa_withManyDebitsManyDates(self) :
		self.maxDiff = None

		sepa = SepaN1914(
			reference='REFERENCIAXXX',
			creation = datetime.datetime(2014,10,24,10,35,21,123456),
			presenter = self.creditor,
			)
		sepa.addDebit(
			reference = '2014102414563300001',
			dueDate = datetime.date(2014,11,24),
			concept = "Quota 4 Trimestre 2014",
			amount = 200.31,
			debtor = self.debtor1,
			mandate = self.mandate,
			)
		sepa.addDebit(
			reference = '2014102414563300002',
			dueDate = datetime.date(2014,11,25),
			concept = "Quota 4 Trimestre 2014",
			amount = 400.52,
			debtor = self.debtor1,
			mandate = self.mandate2,
			)
		self.assertMultiLineEqual(sepa.generate(),
			'\r\n'.join(register.ljust(600) for register in [
			'01'
			'19143001'
			'ES04000B4523678                    '
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'20141024'
			'PRE2014102410352112345REFERENCIAXXX'
			'0075'
			'0001'
			,
			'02'
			'19143002'
			'ES04000B4523678                    '
			'20141124'
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'C/. MARIANO GRANADOS, N- 15                       '
			'08820 EL PRAT DE LLOBREGAT                        '
			'BARCELONA                               '
			'ES'
			'ES1300750001840000099999          '
			,
			'03'
			'19143003'
			'2014102414563300001                '
			'REF0001                            '
			'RCUR'
			'    '
			'00000020031'
			'20140110'
			'PRBAESM1XXX'
			'ELECTRO SHACK FERNANDEZ                                               '
			'CARRER DOCTOR OCTOPUS 8 8O 8A                     '
			'08970 SANT JOAN DESPI                             '
			'BARCELONA                               '
			'ES2J12345678C                          '
			'NIF                                '
			'AES7712341234161234567890              '
			'QUOTA 4 TRIMESTRE 2014'
			,
			'04'
			'ES04000B4523678                    '
			'20141124'
			'00000000000020031'
			'00000001'
			'0000000003'
			,
			'02'
			'19143002'
			'ES04000B4523678                    '
			'20141125'
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'C/. MARIANO GRANADOS, N- 15                       '
			'08820 EL PRAT DE LLOBREGAT                        '
			'BARCELONA                               '
			'ES'
			'ES1300750001840000099999          '
			,
			'03'
			'19143003'
			'2014102414563300002                '
			'REF0002                            '
			'FRST'
			'    '
			'00000040052'
			'20140404'
			'PRBAESM1XXX'
			'ELECTRO SHACK FERNANDEZ                                               '
			'CARRER DOCTOR OCTOPUS 8 8O 8A                     '
			'08970 SANT JOAN DESPI                             '
			'BARCELONA                               '
			'ES2J12345678C                          '
			'NIF                                '
			'AES7712341234161234567890              '
			'QUOTA 4 TRIMESTRE 2014'
			,
			'04'
			'ES04000B4523678                    '
			'20141125'
			'00000000000040052'
			'00000001'
			'0000000003'
			,
			'05'
			'ES04000B4523678                    '
			'00000000000060083'
			'00000002'
			'0000000007'
			,
			'99'
			'00000000000060083'
			'00000002'
			'0000000009'
		]))

	def test_sepa_withManyCreditors(self) :
		self.maxDiff = None

		sepa = SepaN1914(
			reference='REFERENCIAXXX',
			creation = datetime.datetime(2014,10,24,10,35,21,123456),
			presenter = self.creditor,
			)
		sepa.addDebit(
			reference = '2014102414563300001',
			dueDate = datetime.date(2014,11,24),
			concept = "Quota 4 Trimestre 2014",
			amount = 200.31,
			debtor = self.debtor1,
			mandate = self.mandate,
			creditor = self.creditor2,
			)
		sepa.addDebit(
			reference = '2014102414563300002',
			dueDate = datetime.date(2014,11,25),
			concept = "Quota 4 Trimestre 2014",
			amount = 400.52,
			debtor = self.debtor1,
			mandate = self.mandate,
			creditor = self.creditor,
			)
		self.assertMultiLineEqual(sepa.generate(),
			'\r\n'.join(register.ljust(600) for register in [
			'01'
			'19143001'
			'ES04000B4523678                    '
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'20141024'
			'PRE2014102410352112345REFERENCIAXXX'
			'0075'
			'0001'
			,
			'02'
			'19143002'
			'ES1800336517097C                   '
			'20141124'
			'DAVID GARCIA GARZON                                                   '
			'C/. EL LOKAL 69                                   '
			'08970 SANLLU                                      '
			'MAKROPOLIA                              '
			'ES'
			'ES1300750001840000077777          '
			,
			'03'
			'19143003'
			'2014102414563300001                '
			'REF0001                            '
			'RCUR'
			'    '
			'00000020031'
			'20140110'
			'PRBAESM1XXX'
			'ELECTRO SHACK FERNANDEZ                                               '
			'CARRER DOCTOR OCTOPUS 8 8O 8A                     '
			'08970 SANT JOAN DESPI                             '
			'BARCELONA                               '
			'ES2J12345678C                          '
			'NIF                                '
			'AES7712341234161234567890              '
			'QUOTA 4 TRIMESTRE 2014'
			,
			'04'
			'ES1800336517097C                   '
			'20141124'
			'00000000000020031'
			'00000001'
			'0000000003'
			,
			'05'
			'ES1800336517097C                   '
			'00000000000020031'
			'00000001'
			'0000000004'
			,
			'02'
			'19143002'
			'ES04000B4523678                    '
			'20141125'
			'ACADEMIA DE CLASES PARTICULARES                                       '
			'C/. MARIANO GRANADOS, N- 15                       '
			'08820 EL PRAT DE LLOBREGAT                        '
			'BARCELONA                               '
			'ES'
			'ES1300750001840000099999          '
			,
			'03'
			'19143003'
			'2014102414563300002                '
			'REF0001                            '
			'RCUR'
			'    '
			'00000040052'
			'20140110'
			'PRBAESM1XXX'
			'ELECTRO SHACK FERNANDEZ                                               '
			'CARRER DOCTOR OCTOPUS 8 8O 8A                     '
			'08970 SANT JOAN DESPI                             '
			'BARCELONA                               '
			'ES2J12345678C                          '
			'NIF                                '
			'AES7712341234161234567890              '
			'QUOTA 4 TRIMESTRE 2014'
			,
			'04'
			'ES04000B4523678                    '
			'20141125'
			'00000000000040052'
			'00000001'
			'0000000003'
			,
			'05'
			'ES04000B4523678                    '
			'00000000000040052'
			'00000001'
			'0000000004'
			,
			'99'
			'00000000000060083'
			'00000002'
			'0000000010'
		]))

if __name__ == '__main__' :
	import sys
	if '--test' in sys.argv :
		sys.argv.remove('--test')
		sys.exit(unittest.main())

	



