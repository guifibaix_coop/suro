#!/usr/bin/env python3

from fact.tresoreria import appData, cleanEmail
import glob
import os

templates = {
	os.path.basename(templateName): open(templateName).read()
	for templateName in glob.glob(appData('enviafactura/*'))
	}

from consolemsg import *

def parseCommandLine() :
	"""
	Parses the command line options of the module when used as script.
	"""
	import argparse

	parser = argparse.ArgumentParser(
		description=
			"Obre una finestra del Thunderbird de redacció de missatge per enviar una factura.\n\n"
			,
		epilog =
			"Per trobar l'identificador del compte de Thunderbird, "
			"heu d'anar a Edita/Preferencies/Avançat/General/Editor de Configuracio "
			"i filtrar per 'useremail', "
			"surtiran els correus de les diferents comptes que tens configurades "
			"que tenen la forma 'idN', pe. id1, id4 o id6.",
	)
	parser.add_argument(
		'--test',
		action='store_true',
		help="run unittests",
		)

	parser.add_argument(
		'yamlinvoices',
		metavar="FACTURA.yaml",
		nargs='+',
		help="fitxers yaml amb la informació de les factures a enviar",
		)
	parser.add_argument(
		'-t',
		'--template',
		required=True,
		action='store',
		dest='template',
		metavar='TEMPLATE',
		choices=templates.keys(),
		help="escull un text com a template per el correu",
		)
	parser.add_argument(
		'--id',
		dest='id',
		metavar='THUNDERBIRD_ID',
		help="identificador del compte del thunderbird amb el que es vol enviar els correus.",
		)
	parser.add_argument(
		'--emili',
		dest='emiliconfig',
		metavar='CONFIG.py',
		help="indica el fitxer de configuracio de l'Emili si es vol fer servir en comptes del Thunderbird.",
		)
	parser.add_argument(
		'--dump',
		action='store_true',
		help="instead of sending, dump a file",
		)

	return parser


import os
import sys
import unittest
from yamlns import namespace as ns

def main() :

	argsparser = parseCommandLine()
	args = argsparser.parse_args()

	def personAddress(person):
		email = cleanEmail(person.contact.email[0])
		if email is None:
			error("email incorrecto: {}"
				.format(person.contact.email[0]))
		return "{name} <{email}>".format(
			name=person.name,
			email=email,
			)
	for invoiceyaml in args.yamlinvoices :
		invoicepdf = invoiceyaml.replace(".yaml",".pdf")
		invoiceeml = invoiceyaml.replace(".yaml",".eml")
		invoice = ns.load(invoiceyaml)
		template = templates[args.template]
		content = template.format(invoice=invoice)
		step("Llençant redactor de correu per a '{}'".format(invoiceyaml))
		if not 'contact' in invoice.client:
			error("Saltant, sense informació de contacte: '{}'".format(invoiceyaml))
			continue
		if cleanEmail(invoice.client.contact.email[0]) is None:
			error("Saltant, no hi ha email: '{}'".format(invoiceyaml))
			continue
		if args.emiliconfig or args.dump:
			import emili
			emili.sendMail(
				sender = 'GuifiBaix Facturació <facturacio@guifibaix.coop>',
				to = [
#					'David García Garzón <david.garcia@somenergia.coop>',
					personAddress(invoice.client),
					],
				cc = [
					'Guifibaix Facturació <facturacio@guifibaix.coop>',
					],
				subject="[Factura {}-{:04}] {}".format(
					invoice.year,
					invoice.number,
					invoice.subject,
					),
				attachments=[os.path.abspath(invoicepdf)],
				md=content,
				config=args.emiliconfig,
				dump=invoiceeml if args.dump else None,
				)
		else:
			os.system(
				"""thunderbird -compose "to='{clientAddress}',cc='{providerAddress}',subject='{subject}',reply-to='{providerAddress}',attachment='{attachment}',body='{content}'{extra}" """
				.format(
				# providerAddress = personAddress(invoice.provider),
				providerAddress = 'GuifiBaix SCCL <contacte@guifibaix.coop>',
				clientAddress = personAddress(invoice.client),
				subject="[Factura {}-{:04}] {}".format(
					invoice.year,
					invoice.number,
					invoice.subject,
					),
				attachment=os.path.abspath(invoicepdf),
				content=content,
				extra=',preselectid={}'.format(args.id) if args.id else '',
				))

if __name__ == '__main__' :
	if '--test' in sys.argv :
		sys.argv.remove('--test')
		sys.exit(unittest.main())

	sys.exit(main())




