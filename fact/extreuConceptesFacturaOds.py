#!/usr/bin/env python3

import gb_factura
import glob
from consolemsg import step, warn, success
from yamlns import namespace as ns
from tresoreria import appData, currentCompany
import os
import sys

projectes = os.path.expanduser('~/Documents/guifibaix/privat/projectes')

clientDataGlob = os.path.join(
	projectes,'*','*dadesfacturacio.yaml')

clientlist = [
	ns.load(clientDataFile)
	for clientDataFile in glob.glob(clientDataGlob)
	]

clients = dict([
	(client.nif, client)
	for client in clientlist
	])



def invoiceLine(line):
	if not line[2] :
		return line[0]
	price = round(float(line[2].replace(',', '.')), 2)
	return '{} \t {}'.format(line[0], price)


for filename in glob.glob("factura-*.csv"):
	step(filename)

	with open(filename) as csv:
		table = [
			line[:-1].split('\t')
			for line in csv
			]

	invoice = gb_factura.dummyInvoice()
	nif = table[15][0].split()[1].replace('-', '')
	if nif not in clients:
		warn("no tenim dades de facturacio per {}".format(filename))
		invoice.client = ns()
	else:
		invoice.client = clients[nif]
	invoiceId = table[4][2].split('/')
	invoice.number = int(invoiceId[0])
	invoice.year = int(invoiceId[1])
	issueDate = table[5][2]
	dueDate = table[7][2]
	if 'omptat' in dueDate :
		dueDate = issueDate
	invoice.issueDate = dateutils.date(issueDate)
	invoice.dueDate = dateutils.date(dueDate)
	invoice.subject = filename[len('factura-XXXX-XX-'):-4]
	invoice.concepts = [
		invoiceLine(line)
		for line in table[18:36]
		if line[0]
		]
	invoice.provider = currentCompany()

	outputfilename = filename[:-4]+'.yaml'
	success("writting {}".format(outputfilename))
	with open(outputfilename, 'w') as outputfile:
		outputfile.write(invoice.dump())





