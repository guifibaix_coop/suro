#!/usr/bin/env python3

from yamlns import namespace as ns
from yamlns import dateutils
import sys
import os
from consolemsg import warn, step, fail
from fact.tresoreria import (
	currentCompany,
	payingDaysInterval,
)
from fact.invoiceprint import printInvoice
from fact.invoice import dummyInvoice

def main(args=sys.argv) :
	import argparse

	parser = argparse.ArgumentParser(
		description="Genera factures en PDF donat un arxiu YAML amb les dades.",
		)
	parser.add_argument(
		'input',
#		type=argparse.FileType('r'),
		nargs='?',
		metavar="INPUT.yaml",
		help="fitxer YAML amb les dades de la factura. Si no s'especifica, es fan servir dades de joguina.",
		)
	parser.add_argument(
		'-o','--output',
		dest='output',
#		type=argparse.FileType('w'),
		metavar="OUTPUT.pdf",
		help="especifica el nom del fitxer PDF generat amb la factura. Per defecte es fa servir el de l'entrada canviant l'extensió.",
		)
	parser.add_argument(
		"--dump",
		dest='dump',
		metavar = "EXEMPLE.yaml",
		help="genera un YAML d'exemple que pot modificar i fer servir com a entrada",
		)
	parser.add_argument(
		'-p','--provider',
		dest='provider',
		metavar="PROVIDER.yaml",
		help="fitxer YAML amb la informació de proveidor que substituirà la del proveidor especificat a la factura o al proveidor per defecte en cas del dump",
		)
	parser.add_argument(
		'-c','--client',
		dest='client',
		metavar="CLIENT.yaml",
		help="fitxer YAML amb la informació del client que substituirà la del client especificat a la factura o al client tonto per defecte en cas del dump",
		)
	parser.add_argument(
		'-n',
		dest='number',
		type=int,
		metavar="NUMBER",
		help="Número de facture",
		)
	parser.add_argument(
		'--payer',
		dest='payer',
		metavar="PAYER.yaml",
		help="afegeix un pagador diferent del client, especificant un fitxer YAML.",
		)
	parser.add_argument(
		'--subject -s',
		dest='subject',
		metavar="SUBJECT",
		help="concepte general de la factura",
		)
	parser.add_argument(
		'--issue',
		dest='issueDate',
		type=dateutils.date,
		metavar="YYY-MM-DD",
		help="força la data d'emisió",
		)
	parser.add_argument(
		'--due',
		dest='dueDate',
		type=dateutils.date,
		metavar="YYY-MM-DD",
		help="força la data de venciment",
		)
	parser.add_argument(
		'--draft',
		action='store_true',
		default=None,
		dest='draft',
		help="afegeix una marca d'aigua al pdf, per indicar que es un esborrany"
	)
	parser.add_argument(
		'--final',
		action='store_false',
		default=None,
		dest='draft',
		help="treu la marca d'aigua d'esborrany al pdf, tot i que ho digui el yaml"
	)
	parser.add_argument(
		'--debuglayout',
		action='store_true',
		default=None,
		help="mostra la graella de maquetació",
		)

	args = parser.parse_args()

	if args.input :
		invoice = ns.load(args.input)
#		validateInvoice(invoice)
		outputpdf = os.path.splitext(args.input)[0]+'.pdf'
	elif args.dump :
		invoice = dummyInvoice()
		outputpdf = None
	else:
		parser.print_help()
		return -1

	if not invoice.get('provider') or args.provider :
		invoice.provider = ns.load(args.provider) if args.provider else currentCompany()

	if args.client :
		invoice.client = ns.load(args.client)

	if args.payer :
		invoice.payer = ns.load(args.payer)

	if args.number is not None:
		invoice.number = args.number

	import datetime

	if args.issueDate:
		invoice.issueDate = args.issueDate
	elif args.dump:
		invoice.issueDate = dateutils.Date.today()

	if args.dueDate:
		invoice.dueDate = args.dueDate
	elif args.dump:
		invoice.dueDate = dateutils.date(
			invoice.issueDate+datetime.timedelta(
				days=payingDaysInterval
		))

	if args.subject:
		invoice.subject = args.subject

	invoice.year = invoice.issueDate.year

	if args.dump :
		step("Generating '{}'...".format(args.dump))
		invoice.dump(args.dump)

	outputpdf = args.output or outputpdf

	if outputpdf :
		step("Generating '{}'...".format(outputpdf))
		printInvoice(invoice, outputpdf, draft=args.draft, debuglayout=args.debuglayout )

if __name__ == '__main__' :
	sys.exit(main())

