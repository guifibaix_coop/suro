Si llega un nuevo formulario:

- Copiarlo como XXXXXXX-original.pdf
- Rellenar los campos con codigos para identificarlos (llevan nombres tontos)
- Guardar el pdf como XXXXXXX-template.pdf
- Extraer el formulario con los codigos
	pdfform.py extract XXXXXXX-template.pdf XXXXXXX-template.form
- Editar el form para que tenga las variables que nos molan en {format}
	- Podemos poner {nombresSencillos}, {jerarquias.de.nombres} y {indices[3]}
- Para comprobar que las variables estan bien podemos aplicar el form
	pdfform.py apply XXXXXXX-template.pdf XXXXXXX-template.form XXXXXXX-template.pdf
- Extraer las variables en un yaml
	nstemplate.py extract XXXXXXX-template.form XXXXXXX-data.yaml
- Nos guardamos:
	- XXXXXXX-original.pdf
	- XXXXXXX-template.form
	- XXXXXXX-data.yaml
- Descartable:
	- XXXXXXX-template.pdf


Para rellenar un formulario:
- Copiamos el yaml final
	cp XXXXXXX-data.yaml XXXXXXX-MiCliente.yaml
- Editamos XXXXXXX-MiCliente.yaml
- Aplicamos los datos para generar el formulario
	nstemplate.py apply XXXXXXX-template.form XXXXXXX-MiCliente.yaml XXXXXXX-MiCliente.form
- Aplicamos el formulario al pdf
	pdfform.py apply XXXXXXX-original.pdf XXXXXXX-MiCliente.form XXXXXXX-MiCliente.pdf
- Nos guardamos:
	- XXXXXXX-MiCliente.pdf
	- XXXXXXX-MiCliente.yaml






