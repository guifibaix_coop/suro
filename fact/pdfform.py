#!/usr/bin/env python3

import yamlns
from collections import OrderedDict

def extract(filename) :
	import subprocess
	command = [
		'pdfformburner',
		filename,
	]
	proc = subprocess.Popen(command, stdout=subprocess.PIPE)
	(out, err) = proc.communicate()
	return out.decode('utf8')+'\n' if out else '{}\n'

def fill(inputpdf, inputyaml, outputpdf, flatten=False) :
	import subprocess
	command = [
		'pdfformburner',
		inputpdf,
		'-',
		outputpdf,
	]
	if flatten : command.append('flatten')
	proc = subprocess.Popen(command, stdin=subprocess.PIPE)
	(out, err) = proc.communicate(inputyaml.encode('utf-8'))
	return out


def yamlTemplate(**kwd) :
	return yamlns.namespace(OrderedDict(sorted(kwd.items()))).dump()

import unittest
import reportlab
import sys
import os

class PdfForm_Test(unittest.TestCase) :

	def setUp(self) :
		self._toRemove = []

	def toRemove(self, f) :
		self._toRemove.append(f)

	def tearDown(self) :
		for f in self._toRemove :
			os.remove(f)

	def writeFile(self, filename, content) :
		self.toRemove(filename)
		with open(filename, 'w') as f :
			f.write(content)

	def generatePdf(self, filename, fields=None): 
		self.toRemove(filename)
		from reportlab.pdfgen.canvas import Canvas
		from reportlab.pdfbase.pdfform import textFieldRelative
		c = Canvas(filename)
		if fields :
			for i, (param, value) in enumerate(fields.items()) :
				textFieldRelative(c, param, 30, 400-i*30, 100, 30, value)
		c.showPage()
		c.save()

	def assertEqualYaml(self, expected, result) :
		import yaml
		self.assertEqual(yaml.load(expected), yaml.load(result))


	def test_pdfFormVars(self) :
		self.generatePdf("boo.pdf")
		yaml = extract("boo.pdf")
		self.assertEqual(yaml,
			yamlTemplate()
			)

	def test_pdfFormVars_withAField(self) :
		self.generatePdf("boo.pdf", {'param1':'val1'})
		yaml = extract("boo.pdf")
		self.assertEqual(yaml,
			yamlTemplate(param1='val1')
			)

	def test_pdfFormVars_withTwoFields(self) :
		self.generatePdf("boo.pdf", OrderedDict(
			param1='val1',
			param2='val2',
			))
		yaml = extract("boo.pdf")
		self.assertEqualYaml(yaml,
			"param1: val1\n"
			"param2: val2\n"
		)

	def test_setFormVars(self) :
		self.generatePdf('boo.pdf', OrderedDict(
			param1='val1',
			param2='val2',
			))

		self.toRemove('output.pdf')
		fill('boo.pdf',
			"param1: newval1\n"
			"param2: newval2\n"
			,
			'output.pdf')
#		import shutil
#		shutil.copy("output.pdf", 'mirame.pdf')

		yaml = extract("output.pdf")
		self.assertEqualYaml(yaml,
			"param1: newval1\n"
			"param2: newval2\n"
		)

	def test_pdfFormVars_withUnicodeField(self) :
		self.generatePdf("boo.pdf", OrderedDict(
			paramUnicòde='valç',
			paramUnicodé='valñ',
			))
		yaml = extract("boo.pdf")
		self.assertEqualYaml(yaml,
			"paramUnicòde: valç\n"
			"paramUnicodé: valñ\n"
		)

def main(args=sys.argv) :
	import argparse

	parser = argparse.ArgumentParser(
		description="Extracts, applies and generates yaml files.",
		)
	parser.add_argument(
		'--test',
		action='store_true',
		help="Run unittests",
		)
	subparsers= parser.add_subparsers(
		dest='command',
		)

	subargs = subparsers.add_parser(
		'extract',
		help='Extracts form data from a PDF file into a YAML file',
		description='Extracts form data from a PDF file into a YAML file',
		)
	subargs.add_argument(
		metavar='input.pdf',
		dest='input_pdf',
		)
	subargs.add_argument(
		metavar='output.yaml',
		dest='ouput_yaml',
		)

	subargs = subparsers.add_parser(
		'fill',
		help='Fills a PDF form with data from a YAML file.',
		description='Fills a PDF form with data from a YAML file.',
		)
	subargs.add_argument(
		metavar='input.pdf',
		dest='input_pdf',
		)
	subargs.add_argument(
		metavar='input.yaml',
		dest='input_yaml',
		)
	subargs.add_argument(
		metavar='output.pdf',
		dest='output_pdf',
		)
	subargs.add_argument(
		'--flatten',
		dest='flatten',
		action='store_true',
		help='Makes the form fields non-editable',
		)

	args = parser.parse_args()

	if args.command == 'extract' :
		yaml = extract(args.input_pdf)
		with open(args.ouput_yaml, 'w') as f :
			f.write(yaml)
		return 0

	if args.command == 'fill' :
		with open(args.input_yaml, 'r') as f :
			yaml = f.read()
		fill(args.input_pdf, yaml, args.output_pdf, flatten = args.flatten)
		return 0

	parser.print_help()
	return -1


if __name__  == '__main__' :
	if '--test' in sys.argv :
		sys.argv.remove('--test')
		sys.exit(unittest.main())

	sys.exit(main())





