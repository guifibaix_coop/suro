#!/usr/bin/env python3
"""
TODO:
- Check CCC on IBAN check
- Provide the BIC given the account
"""


def iban(invoicingData) :
	account = invoicingData.iban if 'iban' in invoicingData else invoicingData.ccc if 'ccc' in invoicingData else None
	if not account: return
	return cleanAccount(account)

def checksum_iban(iban):
	"""Calculate 2-digit checksum of an IBAN."""
	"""Taken from <http://iban.es/iban.py>."""
	code     = iban[:2]
	checksum = iban[2:4]
	bban     = iban[4:]

	# Assemble digit string
	digits = ""
	for ch in bban.upper():
		if ch.isdigit():
			digits += ch
		else:
			digits += str(ord(ch) - ord("A") + 10)
	for ch in code:
		digits += str(ord(ch) - ord("A") + 10)
	digits += checksum

	# Calculate checksum
	checksum = 98 - int(digits)%97
	return '{:02d}'.format(checksum)

class BadAccountError(Exception) : pass

def compact(account):
	account = ''.join(account.split())
	account = ''.join(account.split('-'))
	return account.upper()

def prettyIban(account):
	"""
	>>> prettyIban("ES1212345678001234567890")
	ES12 1234 5678 00 1234567890
	>>> prettyIban("ES-12123456780 01234567890")
	ES12 1234 5678 00 1234567890
	"""
	account = compact(account)
	account = ' '.join((
		account[:-20],
		account[-20:-16],
		account[-16:-12],
		account[-12:-10],
		account[-10:],
		))
	return account

def ibanOffice(iban) :
	"""Returns the office number in IBAN"""
	return int(compact(iban)[-16:-12])

def ibanEntity(iban) :
	"""Returns the entity number in IBAN"""
	return int(compact(iban)[-20:-16])

def cleanAccount(account) :
	"""
	Turns an account field from a person yaml and turns it into a valid IBAN compliant
	"""
	# trim annotations in parenthesis
	if '(' in account :
		account = account[:account.index('(')]

	account = compact(account)

	if len(account) == 20 :
		# CCC, not IBAN, construct it
		checksum = checksum_iban('ES' + '00' + account[-20:])
		account = 'ES'+ checksum + account

	if len(account) != 24 :
		raise BadAccountError('Wrong number of digits. Expected 24 (or 20 for a CCC), got {}'.format(len(account)))

	# TODO: Just if account[:-22]=='ES', other country may require a different check
	if not CodigoCuentaCorriente.is_valid(account[-20:]):
		raise BadAccountError('Account does not validate the Spanish CCC check code')

	checksum = checksum_iban( account[:-22]  + '00' + account[-20:])

	if checksum != account[2:4] :
		raise BadAccountError('IBAN Checksum failed')

	return prettyIban(account)


import unittest

class IbanAccount_Test(unittest.TestCase) :

	def test_compactIban_withHyphensAndSpaces(self):
		self.assertEqual(
			compact("ES-12-1234 56780 01234567890"),
			"ES1212345678001234567890")

	def test_compactIban_withLowerCaseCountry(self):
		self.assertEqual(
			compact("es1212345678001234567890"),
			"ES1212345678001234567890")

	def test_prettyIban_withCompactInput(self):
		self.assertEqual(
			prettyIban("ES1212345678001234567890"),
			"ES12 1234 5678 00 1234567890")

	def test_prettyIban_withHyphensAndSpaces(self):
		self.assertEqual(
			prettyIban("ES-12123456780 01234567890"),
			"ES12 1234 5678 00 1234567890")

	def test_ibanEntity_withCompactIban(self):
		self.assertEqual(
			ibanEntity("ES12 5678 1234 00 1234567890"),
			5678)

	def test_ibanEntity_withPrettyIban(self):
		self.assertEqual(
			ibanEntity("ES12 5678 1234 00 1234567890"),
			5678)

	def test_ibanEntity_withSmalNumber(self):
		self.assertEqual(
			ibanEntity("ES12 0078 1234 00 1234567890"),
			78)

	def test_ibanOffice_withCompactIban(self):
		self.assertEqual(
			ibanOffice("ES12 1234 5678 00 1234567890"),
			5678)

	def test_ibanOffice_withPrettyIban(self):
		self.assertEqual(
			ibanOffice("ES12 1234 5678 00 1234567890"),
			5678)

	def test_ibanOffice_withSmalNumber(self):
		self.assertEqual(
			ibanOffice("ES12 1234 0078 00 1234567890"),
			78)

	def test_cleanAccount_whenWellFormatted(self) :
		self.assertEqual(
			cleanAccount('ES77 1234 1234 16 1234567890'),
			'ES77 1234 1234 16 1234567890')

	def test_cleanAccount_whenCompact(self) :
		self.assertEqual(
			cleanAccount('ES7712341234161234567890'),
			'ES77 1234 1234 16 1234567890')

	def test_cleanAccount_withHyphens(self) :
		self.assertEqual(
			cleanAccount('ES77-1234-1234-16-1234567890'),
			'ES77 1234 1234 16 1234567890')

	def test_cleanAccount_whenComments(self) :
		self.assertEqual(
			cleanAccount('ES77 1234 1234 16 1234567890 (Triodos)'),
			'ES77 1234 1234 16 1234567890')

	def test_cleanAccount_whenCCC(self) :
		self.assertEqual(
			cleanAccount('1234 1234 16 1234567890'),
			'ES77 1234 1234 16 1234567890')

	def test_cleanAccount_withBadIbanChecksum(self) :
		with self.assertRaises(BadAccountError) as e :
			cleanAccount('ES69 1234 1234 16 1234567890')
		self.assertEqual( 
			"IBAN Checksum failed",
			e.exception.args[0])

	def test_cleanAccount_withMissingDigit(self) :
		with self.assertRaises(BadAccountError) as e :
			cleanAccount('ES77 1234 123 16 1234567890')
		self.assertEqual( 
			"Wrong number of digits. Expected 24 (or 20 for a CCC), got 23",
			e.exception.args[0])

	def test_cleanAccount_withTooManyDigits(self) :
		with self.assertRaises(BadAccountError) as e :
			cleanAccount('ES77 A234 12345 16 1234567890')
		self.assertEqual( 
			"Wrong number of digits. Expected 24 (or 20 for a CCC), got 25",
			e.exception.args[0])

	def test_cleanAccount_withBadSpanishCCC(self) :
		with self.assertRaises(BadAccountError) as e :
			cleanAccount('ES77 1234 1234 15 1234567890')
		self.assertEqual( 
			"Account does not validate the Spanish CCC check code",
			e.exception.args[0])

class CodigoCuentaCorriente :
	class InvalidCCC(Exception):pass
	@classmethod
	def compact(cls, code) :
		code = ''.join(code.split())
		code = ''.join(code.split('-'))
		return code

	@classmethod
	def validate(cls, code) :
		code = cls.compact(code)
		def DC(digitos):
			dc = 11 - sum( int(d) * 2**i for i,d in enumerate(digitos)) % 11
			return str(dc if dc < 10 else 11 - dc)
		if not code.isdigit() :
			raise cls.InvalidCCC("Non digit '{}' found in CCC code".format(''.join(c for c in code if not c.isdigit())))
		if len(code) != 20 :
			raise cls.InvalidCCC("Expected 20 digits")
		entidad, oficina, dc, account = code[:4], code[4:8], code[8:10], code[10:]
		expectedDc = (DC('00'+entidad+oficina) + DC(account))
		if dc != expectedDc :
			raise cls.InvalidCCC("Wrong control digit, expected {} got {}".
				format(expectedDc,dc))
		return code

	@classmethod
	def is_valid(cls, code) :
		try:
			cls.validate(code)
		except cls.InvalidCCC:
			return False
		else:
			return True

class CodigoCuentaCorriente_Test(unittest.TestCase) :

	def test_codigoCuentaCorriente_isValid_withValid(self) :
		ccc='12341234161234567890'
		self.assertTrue(
			CodigoCuentaCorriente.is_valid(ccc)
			)
		self.assertEqual(
			'12341234161234567890',
			CodigoCuentaCorriente.validate(ccc))

	def test_codigoCuentaCorriente_isValid_withWrongDc(self) :
		ccc='12341234161234567891'
		self.assertFalse(
			CodigoCuentaCorriente.is_valid(ccc)
			)
		with self.assertRaises(CodigoCuentaCorriente.InvalidCCC) as e :
			CodigoCuentaCorriente.validate(ccc)
		self.assertEqual(e.exception.args[0],
			"Wrong control digit, expected 10 got 16")

	def test_codigoCuentaCorriente_isValid_withWrongLength(self) :
		ccc = '123412341612345678901'
		self.assertFalse(
			CodigoCuentaCorriente.is_valid(ccc)
			)
		with self.assertRaises(CodigoCuentaCorriente.InvalidCCC) as e :
			CodigoCuentaCorriente.validate(ccc)
		self.assertEqual(e.exception.args[0],
			"Expected 20 digits")

	def test_codigoCuentaCorriente_isValid_withWeirdChars(self) :
		ccc = '1234a1234 16 1234567890'
		with self.assertRaises(CodigoCuentaCorriente.InvalidCCC) as e :
			CodigoCuentaCorriente.validate(ccc)
		self.assertEqual(e.exception.args[0],
			"Non digit 'a' found in CCC code")

	def test_codigoCuentaCorriente_isValid_withSpaces(self) :
		ccc = '1234 1234 16 1234567890'
		self.assertTrue(
			CodigoCuentaCorriente.is_valid(ccc)
			)
		self.assertEqual(
			'12341234161234567890',
			CodigoCuentaCorriente.validate(ccc))

	def test_codigoCuentaCorriente_isValid_withHyphens(self) :
		ccc = '1234 1234-16-1234567890'
		self.assertTrue(
			CodigoCuentaCorriente.is_valid(ccc)
			)
		self.assertEqual(
			'12341234161234567890',
			CodigoCuentaCorriente.validate(ccc))

from fact import tresoreria

_bicTable = None
def readBicTable() :
	global _bicTable
	if _bicTable is not None: return _bicTable
	with open(tresoreria.appData('biccodes.csv')) as f:
		_bicTable = [
			(int(bankCode), name.strip(), bic.strip())
			for bankCode, name, bic in [
				line.split('\t') for line in f
				]
			]
	return _bicTable

_bancCodeLookUpTable = None
def bicFromSpanishBancCode(aBankCode):
	global _bancCodeLookUpTable
	_bancCodeLookUpTable = _bancCodeLookUpTable if _bancCodeLookUpTable is not None else {
		bankCode: bic
		for bankCode, name, bic in readBicTable()
		}
	try:
		return _bancCodeLookUpTable[aBankCode]
	except KeyError: pass
	raise BadAccountError("Bank Code {} not found".format(aBankCode))

_bicLookUpTable = None
def nameFromBic(aBic):
	global _bicLookUpTable
	table = _bicLookUpTable if _bicLookUpTable is not None else {
		bic: name
		for bankCode, name, bic in readBicTable()
		}
	return table[aBic]

_nameLookUpTable = None
def nameFromSpanishBankCode(aBankCode):
	global _nameLookUpTable
	table = _nameLookUpTable if _nameLookUpTable is not None else {
		bankCode: name
		for bankCode, name, bic in readBicTable()
		}
	return table[aBankCode]

def bankFromCCC(account):
	assert(len(account) in (20, 24)) # 20 CCC, 24 IBAN
	return int(account[-20:-16])

def bicFromAccount(account) :
	account = compact(account)
	entity = bankFromCCC(account)
	return bicFromSpanishBancCode(entity)


class BankIdentifierCode_Test(unittest.TestCase):

	def test_readBicTable(self):
		table = readBicTable()
		self.assertEqual(
			table[-1],
			(9000, 'BANCO DE ESPAÑA', 'ESPBESMMXXX'))
		self.assertEqual(230, len(table))

	def test_bicFromCode(self):
		self.assertEqual(
			'BCOEESMM140',
			bicFromSpanishBancCode(3140))

	def test_bicFromCode_noSuchBank(self):
		with self.assertRaises(BadAccountError) as e:
			bicFromSpanishBancCode(9999)
		self.assertEqual(
			e.exception.args[0],
			"Bank Code 9999 not found")

	def test_bancFromCCC(self):
		self.assertEqual(
			3100,
			bankFromCCC('31001234691234567890'))

	def test_bicFromAccount(self):
		self.assertEqual(
			'BCOEESMM140',
			bicFromAccount('31401234691234567890'))

	def test_bicFromAccount_whenIBAN(self):
		self.assertEqual(
			'BCOEESMM140',
			bicFromAccount('ES7731401234691234567890'))

	def test_nameFromBic(self):
		self.assertEqual(
			'CAJA RURAL DE GUISSONA',
			nameFromBic('BCOEESMM140'))

	def test_nameFromSpanishBankCode(self):
		self.assertEqual(
			'CATALUNYA BANC',
			nameFromSpanishBankCode(2013))


if __name__ == '__main__' :

	import sys


	def usage():
		print("""\
Usage: {program} <banc account>

Validates the banc account, and computes the IBAN (Internation Banc Account Number) and the BIC (Banc International Code).

Examples:
$ {program} 1234 1234 16 1234567890
IBAN: ES77 1234 1234 16 1234567890
BIC: PRBA

""".format(program=sys.argv[0]))
		return -1

	if '--test' in sys.argv :
		sys.argv.remove('--test')
		sys.exit(unittest.main())

	if '--help' in sys.argv or '-h' in sys.argv:
		sys.exit(usage())

	from consolemsg import error, fail

	account = " ".join(sys.argv[1:])
	if not account.strip() :
		sys.exit(usage())

	try:
		account = cleanAccount(account)
		print("IBAN: {}".format(compact(account)))
		bic = bicFromAccount(compact(account))
		bancName = nameFromSpanishBankCode(bankFromCCC(compact(account)))
		print("BIC: {} ({})".format(bic, bancName))
	except BadAccountError as e:
		fail(e.args[0])


