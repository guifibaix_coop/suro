#!/bin/bash

color()
{
	echo -en "\033[${1}m"
	echo -n "${*:2}"
	echo -e "\033[0m"
}

usage() {
	color "34;1" "\
Ús:
	$0 <directoriDelExpedient> <nouNomExpedient> [-f]
Exemple:
	$0 privat/projectes/sjd-verdaguer42  sjd-verdaguer40
"
}

help() {
	color "34;1" "\
Aquest script canvia el nom d'un expedient. Afecta al nom del
directori, al nom dels fitxers que conté i al seu contingut.
On troba el nom antic el canvia pel nou.

Nota: És sensible a majúscules, i, per exemple, no canviarà
els noms de les antenes que no estan en minúscules, o si hi
ha cap.

   -f  Aplica els canvis. Sense l'opció només mostra les
       comandes que aplicara.
"
}

step() {
	color 32 ":: $*" >&2
}
error() {
	color '31;1' "Error: $*" >&2
}
warn() {
	color 33 "Avís: $*" >&2
}

fatal() {
	{
	usage
	help
	error "$*"
	echo
	} >&2
	exit -1
}


isgittracked() {
	git ls-files --error-unmatch "$1" > /dev/null 2>&1
}

target() {
	echo "${1//$oldname/$newname}"
}

guarda=""
if [ "$1" = "-f" ]
then
	shift
elif [ "$3" = "-f" ]
then
	true
else
	guarda=echo
fi

olddir="$(dirname "$1")/$(basename "$1")"
newname="$2"

[ "$olddir" = '/' ] && fatal "Especifica el directori original de l'expedient"
[ -z "$newname" ] && fatal "Especifica el nou nom de l'expedient"
[ -e "$olddir" ] || fatal "El directori de l'expedient no existeix: '$olddir'"
[ -d "$olddir" ] || fatal "No és un directori: '$olddir'"
[ "$(basename "$newname")" = "$newname" ] || fatal "Especifica el nou nom de l'expedient, no pas un encaminament. Has posat '$newname'."

oldname="$(basename "$olddir")"

recursivemove()
{
	dir="$1"
	newdir="$(target "$dir")"
	[ -e "$newdir" ] && fatal "El directori pel nou nom d'expedient ja existeix: '$newdir'"

	$guarda mkdir "$newdir" || {
		error "Errors movent el directori '$dir' a '$newdir'"
		return
	}
	for a in "$dir"/*
	do
		[ -d "$a" ] && {
			step "Entrant al directory '$a'"
			recursivemove "$a"
			continue
		}
		[ "$(basename "$(target "$a")")" = "$(basename "$a")" ] &&
			warn "El fitxer '$(target "$a") no conté '$oldname', potser estava mal escrit?"
		step "$(basename "$a") -> $(basename "$(target "$a")")"
		if isgittracked "$a"
		then
			$guarda git mv "$a" "$(target "$a")" ||
				error "No ha estat possible moure '$a'"
		else
			warn "Es mou sense git: $(basename "$a")"
			$guarda cp "$a" "$(target "$a")" ||
				error "No ha estat possible moure '$a'"
		fi
		$guarda sed -i "s/$oldname/$newname/" "$(target "$a")"
		if [ ! -z "$guarda" ]
		then
			grep -I --color "$oldname" "$a"
		fi
	done || error "Errors movent el directori '$dir' a '$newdir'"
}

recursivemove "$olddir"

if [ "$guarda" != "" ]
then
	warn "Execució sense efecte, si vols aplicar-ho afegeix l'opció -f"
fi




