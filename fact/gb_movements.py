#!/usr/bin/env python3

from yamlns.dateutils import Date
from yamlns import namespace as ns
from decimal import Decimal
from consolemsg import fail
import sys

import movements

def main():
	movementsFileName = sys.argv[1]
	try:
		movs = movements.parseMovements(movementsFileName)
	except Exception as e:
		fail("No s'han pogut llegir els moviments:\n{}".format(e))

	print(movs.dump())
	print(movements.balance(movs).dump())


if __name__ == '__main__':
	sys.exit(main())



