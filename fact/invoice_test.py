import unittest
from yamlns import namespace as ns
from yamlns import dateutils
from yamlns.dateutils import Date
import datetime
from fact.tresoreria import appData
from fact.invoice import (
	InvalidContract,
	generateInvoice,
	defaultDueDate,
	trimesterPayDay,
	defaultIssueDueDates,
	invoiceErrors,
	invoiceConcepts,
	invoiceSubject,
	invoiceFileName,
	invoicePeriod,
	periodPrice,
	jointName,
	)

unittest.TestCase.__str__ = unittest.TestCase.id

class MaintainmentInvoice_Test(unittest.TestCase) :

	from yamlns.testutils import assertNsEqual

	# Test fixtures (input data for tests)

	contractWithExemption = ns(
		quotaMensualSenseIva=12.50,
		inici='2014-05-06',
		periodesExempts=[
			ns(
				inici='2014-05-06',
				final='2014-08-05',
			),
		],
	)
	contract = ns(
		quotaMensualSenseIva=12.50,
		inici='2014-05-06',
		periodesExempts=[
		],
	)

	contract_newFormat = ns(
		inici='2014-05-06',
		puntsDeServei=1,
		periodesExempts=[
		],
	)

	client = ns()
	client.expedient = 'sjd-percebe13'
	client.name = "Perico Palotes"
	client.nif = "12345678Z"
	client.address = "Rue del Percebe, 13"
	client.city = "Sant Joan Despí"
	client.postalcode = '08970'
	client.state = 'Barcelona'
	client.ccc = '1234 1234 16 1234567890'

	provider = ns()
	provider.infix = 'gb'
	provider.short = 'GB'
	provider.name = 'Guifibaix, SCCL'
	provider.nif = "F66576380"
	provider.suffix = '001'
	provider.address = "Carrer Major 22, 3a"
	provider.city = "Sant Joan Despí"
	provider.postalcode = "08970"
	provider.state = "Barcelona"
	provider.iban = 'ES42 3140 0001 9200 1215 8700'
	provider.bic = 'BCOEESMM140'
	provider.proxy = ns()
	provider.proxy.name = "David García Garzón"
	provider.proxy.nif = "36517097C"
	provider.contact = ns()
	provider.contact.phone = [
		'694-495-972',

		]
	provider.contact.email = [
		'contacte@guifibaix.coop (generic)',
		'suport@guifibaix.coop (suport)',
		]

	invoice = ns()
	invoice.vatRate = 0.21
	invoice.number = 30
	invoice.year = 2014
	invoice.subject = "Manteniment GuifiBaix 3er Trimestre 2014 - sjd-percebe13"
	invoice.issueDate = dateutils.date('2014-07-01')
	invoice.dueDate = dateutils.date('2014-07-16')
	invoice.client = client
	invoice.provider = provider
	invoice.maintenance = ns(
		trimester = 3,
		year = 2014,
		firstDate = dateutils.date('2014-07-10'),
		lastDate = dateutils.date('2014-10-09'),
	)
	invoice.concepts = [
		"Manteniment trimestral Guifibaix sjd-percebe13",
		"Període 10 de juliol a 9 d'octubre \t 37.50",
		"Descompte 10 de juliol a 5 d'agost \t -11.01",
	]

	def setUp(self):
		self.maxDiff = None

	# Test cases

	def test_invoicePeriod_t1(self) :
		self.assertEqual(
			invoicePeriod(2015, 1, self.contract).t,
			('2015-01-10', '2015-04-09'))

	def test_invoicePeriod_firstPeriod(self) :
		self.assertEqual(
			invoicePeriod(2014, 2, self.contract).t,
			('2014-05-06', '2014-07-09'))

	def test_periodPrice_initialMonth(self) :
		self.assertEqual(
			periodPrice(2014, 2, self.contract, 12.50),
			26.785714285714285)

	def test_periodPrice_fullMonth(self) :
		self.assertEqual(
			periodPrice(2015, 1, self.contract, 12.50),
			12.50*3)

	def test_invoiceConcepts(self) :
		self.assertEqual(
			invoiceConcepts(2015, 3, self.contract), [
				"Període 10 de juliol a 9 d'octubre \t 37.50",
			])

	def test_invoiceConcepts_firstInvoice(self) :
		self.assertEqual(
			invoiceConcepts(2014, 2, self.contract), [
				"Període 6 de maig a 9 de juliol \t 26.79",
			])

	def test_invoiceConcepts_discounted(self) :
		self.assertEqual(
			invoiceConcepts(2014, 3, self.contractWithExemption), [
				"Període 10 de juliol a 9 d'octubre \t 37.50",
				"Descompte 10 de juliol a 5 d'agost \t -11.01",
			])

	def test_invoiceConcepts_discountNotApplied(self) :
		self.assertEqual(
			invoiceConcepts(2015, 3, self.contractWithExemption), [
				"Període 10 de juliol a 9 d'octubre \t 37.50",
			])

	def test_invoiceConcepts_withSingleServicePoint(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 1
		self.assertEqual(
			invoiceConcepts(2015, 3, contract), [
				"Període 10 de juliol a 9 d'octubre \t 37.50",
			])

	def test_invoiceConcepts_withTwoServicePoints(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 2
		self.assertEqual(
			invoiceConcepts(2015, 3, contract), [
				"Període 10 de juliol a 9 d'octubre (x2) \t 75.00",
			])

	def test_invoiceConcepts_transitional_withTwoServicePoints_matching(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 2
		contract.quotaMensualSenseIva = 25.00
		self.assertEqual(
			invoiceConcepts(2015, 3, contract), [
				"Període 10 de juliol a 9 d'octubre (x2) \t 75.00",
			])

	def test_invoiceConcepts_transitional_withTwoServicePoints_notMatching(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 2
		contract.quotaMensualSenseIva=12.50
		with self.assertRaises(InvalidContract) as e :
			invoiceConcepts(2015, 3, contract)
		self.assertEqual(e.exception.args[0],
			"Transition missmatch: Different prices from attributes. "
			"Annotated: 12.50€ Computed: 25.00€"
			)

	def test_invoiceConcepts_withAfadrinats(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 1
		contract.afadrinats = 1
		self.assertEqual(
			invoiceConcepts(2015, 3, contract), [
				"Període 10 de juliol a 9 d'octubre \t 37.50",
				"Descompte 'Connectivitat' \t -15.00",
			])

	def test_invoiceConcepts_withManyAfadrinats(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 2
		contract.afadrinats = 2
		self.assertEqual(
			invoiceConcepts(2015, 3, contract), [
				"Període 10 de juliol a 9 d'octubre (x2) \t 75.00",
				"Descompte 'Connectivitat' (x2) \t -30.00",
			])

	def test_invoiceConcepts_transitional_withAfadrinats_matching(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 2
		contract.afadrinats = 2
		contract.quotaMensualSenseIva=15.00
		self.assertEqual(
			invoiceConcepts(2015, 3, contract), [
				"Període 10 de juliol a 9 d'octubre (x2) \t 75.00",
				"Descompte 'Connectivitat' (x2) \t -30.00",
			])
	def test_invoiceConcepts_transitional_withAfadrinats_notMatching(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.quotaMensualSenseIva=12.50
		contract.puntsDeServei = 1
		contract.afadrinats = 1
		with self.assertRaises(InvalidContract) as e :
			invoiceConcepts(2015, 3, contract)
		self.assertEqual(e.exception.args[0],
			"Transition missmatch: Different prices from attributes. "
			'Annotated: 12.50€ Computed: 7.50€'
			)

	def test_invoiceConcepts_withAportadors(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 1
		contract.aportadors = 1
		self.assertEqual(
			invoiceConcepts(2015, 3, contract), [
				"Període 10 de juliol a 9 d'octubre \t 37.50",
				"Descompte 'Aportador' \t -37.50",
			])

	def test_invoiceConcepts_withManyAportadors(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 3
		contract.aportadors = 2
		self.assertEqual(
			invoiceConcepts(2015, 3, contract), [
				"Període 10 de juliol a 9 d'octubre (x3) \t 112.50",
				"Descompte 'Aportador' (x2) \t -75.00",
			])

	def test_invoiceConcepts_withAfadrinatsAndAportadors(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 3
		contract.afadrinats = 2
		contract.aportadors = 1
		self.assertEqual(
			invoiceConcepts(2015, 3, contract), [
				"Període 10 de juliol a 9 d'octubre (x3) \t 112.50",
				"Descompte 'Aportador' \t -37.50",
				"Descompte 'Connectivitat' (x2) \t -30.00",
			])

	def test_invoiceConcepts_transitional_withAportadors_notMatching(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.quotaMensualSenseIva=25.00
		contract.puntsDeServei = 2
		contract.aportadors = 1
		with self.assertRaises(InvalidContract) as e :
			invoiceConcepts(2015, 3, contract)
		self.assertEqual(e.exception.args[0],
			"Transition missmatch: Different prices from attributes. "
			'Annotated: 25.00€ Computed: 12.50€'
			)

	def test_invoiceConcepts_withTooManyAportadors(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 1
		contract.aportadors = 2
		with self.assertRaises(InvalidContract) as e :
			invoiceConcepts(2015, 3, contract)
		self.assertEqual(e.exception.args[0],
			"Discounts outnumber access points in the contract")

	def test_invoiceConcepts_withTooManyAfadrinats(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 1
		contract.afadrinats = 2
		with self.assertRaises(InvalidContract) as e :
			invoiceConcepts(2015, 3, contract)
		self.assertEqual(e.exception.args[0],
			"Discounts outnumber access points in the contract")

	def test_invoiceConcepts_withTooManyAfadrinatsAndAportadorsTogether(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.afadrinats = 2
		contract.aportadors = 2
		contract.puntsDeServei = 2 # just the sum
		with self.assertRaises(InvalidContract) as e :
			invoiceConcepts(2015, 3, contract)
		self.assertEqual(e.exception.args[0],
			"Discounts outnumber access points in the contract")

	def test_invoiceConcepts_withExemptionsAndDiscounts(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 3
		contract.afadrinats = 2
		contract.aportadors = 1
		contract.periodesExempts = [
			ns(
				inici='2014-11-01',
				final='2015-02-13',
			)
		]
		self.assertEqual(
			invoiceConcepts(2015, 1, contract), [
				"Període 10 de gener a 9 d'abril (x3) \t 112.50",
				"Descompte 'Aportador' \t -37.50",
				"Descompte 'Connectivitat' (x2) \t -30.00",
				"Descompte 10 de gener a 13 de febrer \t -17.50",
			])

	def test_invoiceConcepts_withExemptionsWithMotivation(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 3
		contract.afadrinats = 2
		contract.aportadors = 1
		contract.periodesExempts = [
			ns(
				motiu="Descompte 'Pioners'",
				inici='2014-11-01',
				final='2015-02-13',
			)
		]
		self.assertEqual(
			invoiceConcepts(2015, 1, contract), [
				"Període 10 de gener a 9 d'abril (x3) \t 112.50",
				"Descompte 'Aportador' \t -37.50",
				"Descompte 'Connectivitat' (x2) \t -30.00",
				"Descompte 'Pioners' 10 de gener a 13 de febrer \t -17.50",
			])

	def test_invoiceConcepts_firstInvoice_withAportadors(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 1
		contract.aportadors = 1
		self.assertEqual(
			invoiceConcepts(2014, 2, contract), [
				"Període 6 de maig a 9 de juliol \t 26.79",
				"Descompte 'Aportador' \t -26.79",
			])

	def test_invoiceConcepts_firstInvoice_withAfadrinats(self) :
		contract = self.contract_newFormat.deepcopy()
		contract.puntsDeServei = 1
		contract.afadrinats = 1
		self.assertEqual(
			invoiceConcepts(2014, 2, contract), [
				"Període 6 de maig a 9 de juliol \t 26.79",
				"Descompte 'Connectivitat' \t -10.71",
			])

	def test_invoiceSubject_t1(self) :
		self.assertEqual(
			invoiceSubject(2014, 1, 'sjd-percebe13'),
			"Manteniment GuifiBaix 1er Trimestre 2014 - sjd-percebe13")

	def test_invoiceSubject_t2(self) :
		self.assertEqual(
			invoiceSubject(2014, 2, 'sjd-percebe13'),
			"Manteniment GuifiBaix 2on Trimestre 2014 - sjd-percebe13")

	def test_invoiceSubject_t3(self) :
		self.assertEqual(
			invoiceSubject(2014, 3, 'sjd-percebe13'),
			"Manteniment GuifiBaix 3er Trimestre 2014 - sjd-percebe13")

	def test_invoiceSubject_t4(self) :
		self.assertEqual(
			invoiceSubject(2014, 4, 'sjd-percebe13'),
			"Manteniment GuifiBaix 4rt Trimestre 2014 - sjd-percebe13")

	def test_trimesterPayDay_t1(self) :
		self.assertEqual(
			trimesterPayDay(2014,1).isoDate, '2014-01-01')

	def test_trimesterPayDay_t2(self) :
		self.assertEqual(
			trimesterPayDay(2014,2).isoDate, '2014-04-01')

	def test_trimesterPayDay_t3(self) :
		self.assertEqual(
			trimesterPayDay(2014,3).isoDate, '2014-07-01')

	def test_trimesterPayDay_t4(self) :
		self.assertEqual(
			trimesterPayDay(2014,4).isoDate, '2014-10-01')

	def test_defaultDueDate_tenDaysAfter(self) :
		self.assertEqual(
			defaultDueDate('2014-01-01').isoDate, '2014-01-16')

	def test_generateInvoice(self) :
		payer = self.client
		issuer = self.provider
		contract = self.contractWithExemption
		factura = generateInvoice(
			year=2014,
			trimester=3,
			number=30,
			issuer=issuer,
			payer=payer,
			contract=contract,
			)

		self.assertNsEqual(factura, self.invoice)

	def test_generateInvoice_draft(self) :
		payer = self.client
		issuer = self.provider
		contract = self.contractWithExemption
		factura = generateInvoice(
			year=2014,
			trimester=3,
			number=30,
			issuer=issuer,
			payer=payer,
			contract=contract,
			draft=True,
			)

		expected = ns(self.invoice)
		expected.draft=True
		self.assertNsEqual(factura, expected)

	def test_generateInvoice_fileBased(self) :
		payer = ns.load(appData('example-client.yaml'))
		issuer = ns.load(appData('provider-gb.yaml'))
		contract = ns.load(appData('example-contract.yaml'))
		factura = generateInvoice(
			year=2014,
			trimester=3,
			number=30,
			issuer=issuer,
			payer=payer,
			contract=contract,
			)

		self.assertNsEqual(factura, self.invoice)

	def test_generateInvoice_install(self) :
		# TODO
		expected = ns(self.invoice)
		expected.subject = "Instal·lació GuifiBaix - sjd-percebe13"
		expected.concepts[:0] = [
			'Instal·lació Guifibaix sjd-percebe13',
			'Antena Ubiquity Nanostation M5 (MAC DESCONEGUDA) \t 91.00',
			'TP-Link TL-WR841N (NS DESCONEGUT) \t 17.39',
			"Màstil, suports i elements de fixació \t 35.00",
			'Cablejat i elements de fixació \t 38.00',
			"Ma d'obra i transport \t 110.00",
		]
		payer = self.client
		issuer = self.provider
		contract = self.contractWithExemption
		factura = generateInvoice(
			year=2014,
			trimester=3,
			number=30,
			install=True,
			issuer=issuer,
			payer=payer,
			contract=contract,
			)

		self.assertMultiLineEqual(
			factura.dump(),
			expected.dump(),
		)

	def test_jointName_withNormalName(self) :
		self.assertEqual(jointName(
			"Perico Palotes"),
			"PericoPalotes")

	def test_jointName_withNonAsciiCharsName(self) :
		self.assertEqual( jointName(
			'ÑñÇçàèìòùáéíóúâêîôûäëïöüÁÉÍÒÙÁÉÍÓÚÂÊÎÔÛÄËÏÖÜ'),
			'Nnccaeiouaeiouaeiouaeiouaeiouaeiouaeiouaeiou')

	def test_jointName_titleCase(self) :
		self.assertEqual( jointName(
			'Robert de Niro'),
			'RobertDeNiro')

	def test_jointName_nonLetersSeparateParts(self) :
		self.assertEqual( jointName(
			"Abe-Al'dat2"),
			'AbeAlDat2')

	def test_invoiceFileName(self) :
		self.assertEqual(
			invoiceFileName(
				year=2014,
				trimester=3,
				number=1234,
				payer=self.client,
			),
			'factura-2014-1234-sjd-percebe13-PericoPalotes-quota3T.yaml')

	def test_invoiceFileName_withLessThan1000Number(self) :
		self.assertEqual(
			invoiceFileName(
				year=2014,
				trimester=3,
				number=3,
				payer=self.client,
			),
			'factura-2014-0003-sjd-percebe13-PericoPalotes-quota3T.yaml')

	def test_invoiceFileName_install(self) :
		self.assertEqual(
			invoiceFileName(
				year=2014,
				trimester=3,
				number=3,
				payer=self.client,
				install=True,
			),
			'factura-2014-0003-sjd-percebe13-PericoPalotes-instal.yaml')


	def test_validNIF_whenValid(self) :
		import stdnum.es
		self.assertEqual(True, stdnum.es.nif.is_valid("12345678Z"))

	def test_validNIF_whenFormatted(self) :
		import stdnum.es
		self.assertEqual(True, stdnum.es.nif.is_valid("12 345-678-Z"))

	def test_validNIF_withInvalidCheckLetter(self) :
		import stdnum.es
		self.assertEqual(False, stdnum.es.nif.is_valid("12345678X"))

	def test_validateInvoice_whenValid(self) :
		invoice = ns(self.invoice)
		self.assertEqual(invoiceErrors(invoice),[
			])

	def test_validateInvoice_withBadIssuerNif(self) :
		invoice = self.invoice.deepcopy()
		invoice.provider.nif="caca"
		self.assertEqual(invoiceErrors(invoice),[
			"Invalid provider.nif 'caca'"
			])

	def test_validateInvoice_withBadPayerNif(self) :
		invoice = self.invoice.deepcopy()
		invoice.client.nif="caca"
		self.assertEqual(invoiceErrors(invoice),[
			"Invalid client.nif 'caca'"
			])

	def test_validateInvoice_withInvalidIBAN(self) :
		invoice = self.invoice.deepcopy()
		invoice.client.iban="BAD1601101050000010547023795"
		self.assertEqual(invoiceErrors(invoice),[
			"Invalid client.iban 'BAD1601101050000010547023795'"
			])

	def test_validateInvoice_withNoAccountWhatSoEver(self) :
		invoice = self.invoice.deepcopy()
		del invoice.client.ccc
		self.assertEqual(invoiceErrors(invoice),[
			"Neither client.iban nor client.ccc could be found",
			])

	def test_validateInvoice_withInvalidCCC(self) :
		invoice = self.invoice.deepcopy()
		invoice.client.ccc = "12341234061234567890"
		self.assertEqual(invoiceErrors(invoice),[
			"Invalid client.ccc '12341234061234567890'",
			])

	def test_defaultIssueDueDates_provided(self):
		self.assertEqual(
			defaultIssueDueDates(
				Date('2060-02-02'), Date('2060-02-09'),
			), (
				Date('2060-02-02'), Date("2060-02-09"),
			),
		)

	def test_defaultIssueDueDates_lesserDueDateUp15Days(self):
		self.assertEqual(
			defaultIssueDueDates(
				Date('2060-02-02'), Date('2060-02-01'),
			), (
				Date('2060-02-02'), Date("2060-02-17"),
			),
		)

	def test_defaultIssueDueDates_missingDueDate(self):
		self.assertEqual(
			defaultIssueDueDates(
				Date('2060-02-02'), None
			), (
				Date('2060-02-02'), Date("2060-02-17"),
			),
		)

	def test_defaultIssueDueDates_missingIssueDate(self):
		self.assertEqual(
			defaultIssueDueDates(
				None, None
			), (
				Date.today(), Date.today() + datetime.timedelta(days=15),
			),
		)

