#!/usr/bin/env python3

from yamlns import namespace
import sys
import os
from consolemsg import warn, step, fail
from yamlns import dateutils
from fact.tresoreria import appData

def main(args=sys.argv) :
	import argparse

	parser = argparse.ArgumentParser(
		description="Genera una factura rectificativa d'abonament d'una factura emesa.",
		)
	parser.add_argument(
		'factura',
		metavar="ORIGINAL.yaml",
		help="fitxer YAML de la factura que cal abonar.",
		)
	parser.add_argument(
		dest='number',
		type=int,
		metavar="ABONAMENTNUMBER",
		help="Número de factura (serie d'abonament)",
		)
	parser.add_argument(
		'--issue',
		dest='issueDate',
		type=dateutils.date,
		metavar="YYY-MM-DD",
		help="força la data d'emisió",
		)
	parser.add_argument(
		'--draft',
		action='store_true',
		default=None,
		dest='draft',
		help="afegeix una marca d'aigua al pdf, per indicar que es un esborrany"
	)
	parser.add_argument(
		'--final',
		action='store_false',
		default=None,
		dest='draft',
		help="treu la marca d'aigua d'esborrany al pdf, tot i que ho digui el yaml"
	)

	args = parser.parse_args()

	invoice = namespace.load(args.factura)
#	validateInvoice(invoice)

	invoice.amends = '{}-{:05d}'.format(
		invoice.year,invoice.number)

	if args.number is not None:
		invoice.number = args.number

	import datetime

	if args.issueDate:
		invoice.issueDate = args.issueDate
	else:
		invoice.issueDate = dateutils.Date.today()

	invoice.year = invoice.issueDate.year

	invoice.year = invoice.issueDate.year

	outputfile = 'abonament-{year}-{number:05d}-{subject}.yaml'.format(
		**invoice
		)

	step("Generating '{}'...".format(outputfile))
	invoice.dump(outputfile)


if __name__ == '__main__' :
	if '--test' in sys.argv :
		sys.argv.remove('--test')
		sys.argv[0]+=" --test"
		import unittest
		sys.exit(unittest.main())

	sys.exit(main())

