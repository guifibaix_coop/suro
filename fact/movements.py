#!/usr/bin/env python3
"""
movements: parse and analyze account movements (caixa.csv)

The file is based on TSV Tab Separated Values format.
Each journal entry can be represented in several rows like this:

```
isodate	Movement description
	amount1	<-	Banco B	reference
	amount2	->	account2
	amount3	->	Banco A	reference
```

- The First line (not starting by a tab) has the date in iso format and the description.
- Next lines are debit and credit entries having first column empty and then 3 or 4 fields
	- First the absolute amount in 0.0 format
	- Then the direction of the entry.
		- "<-" if is a credit (account less)
		- "->" if it is a debit (account adds)
	- Then the account. We use mnemonic names.
		- Caja <Parner> (La caja responsabilidad de Partner)
		- Banco <Entity> (Los bancos llevan la referencia del movimiento para cuadrar
		- Facturas (Ventas, campo adicional id de facturas)
		- Nómina <Partner>
		- Seguridad Social
		- Capital Social <Partner>

"""


from yamlns.dateutils import Date
from yamlns import namespace as ns
from decimal import Decimal

def parseEntryHeader(nline, cells):
	if len(cells)!=2:
		raise Exception("Error a la linia {line}: "
			"Els inicis de seient, contenen dos cel·les: "
			"data i descripció, s'han trobat {ncells} cel·les"
			.format(line=nline, ncells=len(cells)))

	entry = ns()
	try:
		entry.date = Date(cells[0].strip())
	except:
		raise Exception("Error a la linia {line}: "
			"S'esperava la data en format iso d'un seient, "
			"pero s'ha llegit '{value}'"
			.format(value=cells[0].strip(), line=nline))

	entry.description = cells[1].strip()
	entry.movements = []

	return entry

def parseMovement(nline, cells):
	try:
		amount, direction, account, *extras = (
			cell.strip() for cell in cells) # TODO: test strip
	except:
		raise Exception("Error a la linia {line}: "
			"Els moviments de seient, contenen "
			"3 cel·les despres de la primera cel·la buida: "
			"quantitat, sentit i compte, "
			"pero s'han trobat {cells} cel·les: {values}."
			.format(line=nline, cells=len(cells), values=cells))
	try:
		amount=Decimal(amount) # TODO: test decimal
	except:
		raise Exception("Error a la linia {line}: "
			"La quantitat no esta en format numèric decimal anglosaxò. "
			"S'ha rebut '{value}'."
			.format(line=nline, value=amount))

	if direction not in ('->', '<-'):
		raise Exception("Error a la linia {line}: "
			"El camp direcció d'un seient ha d'indicar-se "
			"amb una fletxa '->' o '<-'. "
			"S'ha rebut '{value}'."
			.format(line=nline, value=direction))

	movement = ns()
	movement.account = account
	movement.amount = amount if direction == '->' else -amount
	if extras:
		movement.reference = extras[0]
	return movement

def parseMovements(filename):
	errors=[]
	entries = []

	def checkBalance(nline, entry):
		balance = sum(movement.amount for movement in entry.movements)
		if balance == 0: return
		raise Exception("Error a la linia {line}: "
			"El moviment no esta compensat, el balanç és {amount}"
			.format(line=nline, amount=balance))

	with open(filename) as movementsFile:
		currentEntry = None
		for nline, line in enumerate(movementsFile, 1):
			try:
				line = line.rstrip() # ignore empty content on the right
				# empty lines close any pending entry
				if not line:
					if not currentEntry: continue
					# Close movement
					entries.append(currentEntry)
					currentEntry = None
					checkBalance(nline, entries[-1])
					continue

				# Comments removed
				line = line.split('#')[0]

				# empty lines after comments removed ignored
				line = line.rstrip()
				if not line: continue

				# tab separated
				cells = line.split('\t')
				if cells[0].strip():
					if currentEntry:
						raise Exception("Error a la linia {line}: "
							"Has de finalitzar el seient anterior amb una linia buida"
							.format(line=nline))

					currentEntry = parseEntryHeader(nline,cells)
				else:
					if not currentEntry:
						raise Exception("Error a la linia {line}: "
							"S'esperava una capcelera de seient amb la data "
							"i la descripció com a primera i segona columna"
							.format(line=nline))

					currentEntry.movements.append(
						parseMovement(nline, cells[1:]))
			except Exception as e:
				errors.append(str(e))

		if currentEntry:
			# Close movement
			entries.append(currentEntry)
			currentEntry = None
			checkBalance(nline, entries[-1])

	if errors:
		raise Exception('\n'.join(errors))
	return ns(book=entries)


def balance(book):
	balance = ns()
	for entry in book.book:
		for movement in entry.movements:
			amount = balance.get(movement.account, Decimal(0))
			amount += movement.amount
			balance[movement.account] = amount
	return balance

def accountReferences(book, account):
	"""
	Returns a list of tuples
	"""
	return [
		ns([
			('reference', movement.reference),
			('amount', movement.amount),
		])
		for entry in book.book
		for movement in entry.movements
		if 'reference' in movement
		and movement.account == account
		]

if __name__ == '__main__':
	from consolemsg import fail
	import sys
	movementsFileName = sys.argv[1]
	try:
		movements = parseMovements(movementsFileName)
	except Exception as e:
		fail("No s'han pogut llegir els moviments:\n{}".format(e))

	print(movements.dump())
	print(balance(movements).dump())





