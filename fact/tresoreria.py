from yamlns import namespace as ns
from yamlns import dateutils
from consolemsg import fail, error
import decimal
from fact import movements
from pathlib import Path
import os

warnOnMissingYamlInvoice = False
invoiceNumberPadding = 4
payingDaysInterval = 15


zero = decimal.Decimal('0.00')

def _failNotInTresoreria(e) :
	fail("No ha estat trobat el fitxer '{}'.\n"
		"Assegureu-vos de que esteu executant la comanda "
		"desde el directori 'tresoreria' del repositori 'privat'."
		.format(e.filename))

def currentCompany():
	try:
		return ns.load('company.yaml')
	except FileNotFoundError as e:
		_failNotInTresoreria(e)

def spfloat(number) :
	"""
	Turns a (optionally spanish locale) number into a Decimal.
	>>> spfloat('1')
	Decimal('1.00')
	>>> spfloat('1.2')
	Decimal('1.20')
	>>> spfloat('1,2') # spanish decimal comma
	Decimal('1.20')
	>>> spfloat('1.20001')
	Decimal('1.20')
	>>> spfloat('1.2049')
	Decimal('1.20')
	>>> spfloat('1.2050') # rounding TODO: Review
	Decimal('1.21')
	>>> spfloat('1.2051') # rounding TODO: Review
	Decimal('1.21')
	>>> spfloat(decimal.Decimal('1.2051'))
	Decimal('1.21')
	"""
	number = str(number)
	number = number.replace(',','.')
	number = decimal.Decimal(number)
	number = number.quantize(decimal.Decimal('1.00'),rounding=decimal.ROUND_HALF_UP)
	return number


def appData(filename) :
	import os
	return os.path.join(
		os.path.dirname(
			os.path.abspath(__file__)), "data", filename)

def aportacionsSocis() :
	try:
		return ns.load("aportacions.yaml")
	except FileNotFoundError as e:
		_failNotInTresoreria(e)

def cobraments():
	try:
		return ns.load("cobraments.yaml")
	except FileNotFoundError as e:
		_failNotInTresoreria(e)

def extracte_banc(account):
	import csv
	extracteFile='banc-{}-extracte.csv'.format(account)
	try: 
		with open(extracteFile) as csvfile:
			reader = csv.DictReader(csvfile, delimiter='\t')
			extracte = [
				ns((k,v.strip()) for k,v in row.items())
				for row in reader ] # if row['fecha_de_operacion'][0]!='#']
	except FileNotFoundError as e:
		_failNotInTresoreria(e)

	for moviment in extracte:
		try:
			moviment.fecha_de_operacion = dateutils.Date(moviment.fecha_de_operacion)
			moviment.fecha_valor = dateutils.Date(moviment.fecha_valor)
			moviment.cantidad = spfloat(moviment.cantidad)
			moviment.saldo = spfloat(moviment.saldo)
			moviment.compensado = spfloat('0.00')
		except:
			error("Moviment de extracte '{}' te dades incorrectes:\n{}".format(
				extracteFile,
				moviment.dump(),
			))
			raise
	return ns((moviment.referencia, moviment) for moviment in extracte)


def cashBalance():
	entries = movements.parseMovements('caixa.csv')
	return movements.balance(entries)

def bankReferencesOnCashMovements(account):
	entries = movements.parseMovements('caixa.csv')
	return movements.accountReferences(entries, account)

def invoiceById(id, kind='factura') :
	import glob
	invoiceFile =glob.glob('factures/{}-{}*.yaml'.format(kind, id))
	if not invoiceFile:
		return None
	return ns.load(invoiceFile[0])

def invoiceId(invoice) :
	return "{}-{:0{}}".format(invoice.year, invoice.number, invoiceNumberPadding)

def nextInvoiceNumber(year):
	currentCompany() # ensure in a tresoreria
	invoice_directory = Path("factures")
	year_invoices = invoice_directory.glob(
		f"factura-{year}-*.yaml"
	)
	previus_number = max((
		str(invoice).split('-')[2]
		for invoice in year_invoices
	), default=0)
	next_number = int(previus_number)+1
	return f"{next_number:0{invoiceNumberPadding}d}"

def invoiceTotalBeforeVAT(concepts) :
	total = decimal.Decimal('0.00')
	for concept in concepts :
		if not '\t' in concept : continue
		total += decimal.Decimal(concept.split('\t')[1])
	return total

def invoiceTotal(invoice) :
	return (invoiceTotalBeforeVAT(invoice.concepts)*(1+invoice.vatRate)
		).quantize(decimal.Decimal('0.01'),rounding=decimal.ROUND_HALF_EVEN)

def cleanPhone(phone):
	"""
	>>> cleanPhone('')
	>>> cleanPhone('0123456789')
	'0123456789'
	>>> cleanPhone('0123 45 6789')
	'0123456789'
	>>> cleanPhone('0123-45-6789')
	'0123456789'
	>>> cleanPhone('b0123-45-6789')
	>>> cleanPhone('12')
	>>> cleanPhone('0123456789 (comment)')
	'0123456789'
	>>> cleanPhone('012 345 6789 (comment)')
	'0123456789'
	>>> cleanPhone(123456789)
	'123456789'
	"""
	import re
	phone = str(phone)
	regular = re.compile(r'[0-9 -]{3,}([^)])')
	match = regular.match(phone)
	if not match: return None
	phone = phone.split('(')[0]
	phone = ''.join(phone.split())
	phone = ''.join(phone.split('-'))
	return phone

def cleanEmail(email):
	"""
	>>> cleanEmail('a@a.net')
	'a@a.net'
	>>> cleanEmail('junk')
	>>> cleanEmail('a@a.net (a comment)')
	'a@a.net'
	>>> cleanEmail('a@a.net (a comment)')
	'a@a.net'
	>>> cleanEmail('junk (a@a.net)')
	>>> cleanEmail('bu  a@a.net ')
	>>> cleanEmail('  a@a.net ')
	'a@a.net'
	>>> cleanEmail('<a@a.net>')
	'a@a.net'
	"""
	email = email.split('(')[0].strip()
	if len(email.split())>1 :
		return None
	if not '@' in email:
		return None
	return email.replace('<','').replace('>','')

def isNifValid(nif) :
	import stdnum.es
	return stdnum.es.nif.is_valid(nif)

import unittest

class Nif_Test(unittest.TestCase) :
	def test_validNIF_whenValid(self) :
		self.assertEqual(True, isNifValid("12345678Z"))

	def test_validNIF_whenFormatted(self) :
		self.assertEqual(True, isNifValid("12 345-678-Z"))

	def test_validNIF_withInvalidCheckLetter(self) :
		self.assertEqual(False, isNifValid("12345678X"))

class Quotes_Test(unittest.TestCase) :

	def test_invoiceTotalBeforeVAT_withNoConcept(self) :
		self.assertEqual(invoiceTotalBeforeVAT([
			]), 0.0)

	def test_invoiceTotalBeforeVAT_withSingleConcept(self) :
		self.assertEqual(invoiceTotalBeforeVAT([
			"concept 1 \t 10.0 ",
			]), 10.0)

	def test_invoiceTotalBeforeVAT_withManyConcepts(self) :
		self.assertEqual(invoiceTotalBeforeVAT([
			"concept 1 \t 10.0 ",
			"concept 1 \t 30.0 ",
			]), 40.0)

	def test_invoiceTotalBeforeVAT_withTitle(self) :
		self.assertEqual(invoiceTotalBeforeVAT([
			"title",
			"concept 1 \t 10.0 ",
			]), 10.0)

def issuedInvoices_computePaidAndPending(cobraments):
	for invoice in cobraments:
		invoice.pagat = issuedInvoice_vencimentsPagats(invoice)
		invoice.pendent = issuedInvoice_vencimentsPendents(invoice)
		invoice.vencut = issuedInvoice_vencimentsVencuts(invoice)

def issuedInvoice_vencimentsPendents(issuedInvoice):
	if 'venciments' not in issuedInvoice:
		return issuedInvoice.valor

	return sum(
		v.valor
		for v in issuedInvoice.venciments
		if not v.get('pagat', None)
		)

def issuedInvoice_vencimentsPagats(issuedInvoice):
	if 'venciments' not in issuedInvoice:
		return decimal.Decimal('0.00')

	return sum(
		v.valor
		for v in issuedInvoice.venciments
		if v.get('pagat', None)
		)

def issuedInvoice_vencimentsVencuts(issuedInvoice):
	import datetime
	unpaymentDate = dateutils.Date(
		datetime.date.today() - datetime.timedelta(days=7))

	if 'venciments' not in issuedInvoice:
		if issuedInvoice.data < unpaymentDate:
			return issuedInvoice.valor
		return decimal.Decimal('0.00')

	return sum(
		v.valor
		for v in issuedInvoice.venciments
		if not v.get('pagat', None)
		and v.data < unpaymentDate
		)



def issuedInvoices_checkInconsistencies(cobraments, error, warn):
	"""
	Checks for inconsistencies on issued invoice registry, including:

	- Unregistered final invoices
	- All registers match an existing invoice
	- Invoice amount matches the registered invoice amount
	- Amounts to be paid at due dates sum the invoice amount
	"""
	import glob
	yamlInvoices = [
		invoice[:-5]
		for invoice in glob.glob('factures/factura-*.yaml') ]
	invoicesFromCobraments = [
		"factures/factura-"+invoice.factura+"-"+invoice.concepte
		for invoice in cobraments ]

	for invoice in invoicesFromCobraments :
		if invoice in yamlInvoices: continue
		error("Factura a cobraments.yaml no es correspon amb cap factura: %s"%(invoice))
	for yaml in sorted(yamlInvoices):
		if yaml in invoicesFromCobraments: continue
		warn("Factura no registrada a cobraments.yaml: %s.yaml"%(yaml))

	for factura in cobraments:
		if 'valor' not in factura:
			warn("Factura %s: No te valor [%s]"%(
				factura.factura, factura.concepte))
			continue
		valor = factura.valor
		pendent = factura.get('pendent', 0)
		venciments = issuedInvoice_vencimentsPendents(factura)

		if venciments != pendent :
			warn("Factura %s: No coincideix el pendent (%s) amb la suma dels venciments (%s) [%s]"%(
				factura.factura, pendent, venciments, factura.concepte))
		if pendent + factura.pagat != valor :
			warn("Factura %s: No coincideix el pendent (%s) i el pagat (%s) amb el total de la factura (%s) [%s]"%(
				factura.factura, pendent, factura.pagat, valor, factura.concepte))

		facturaYamlFile = "factures/factura-{}-{}.yaml".format(
			factura.factura,
			factura.concepte,
			)
		try:
			invoice = ns.load(facturaYamlFile)
			invoiceTotalAndVat = invoiceTotal(invoice)
			if valor != invoiceTotalAndVat :
				warn(
					"Factura %s: El total %s no es correspon amb "
					"l'import de la factura impresa (%s) [%s]"%(
					factura.factura, valor, invoiceTotalAndVat, factura.concepte))
		except FileNotFoundError:
			if warnOnMissingYamlInvoice :
				warn("Factura %s: No te yaml '%s'"%(factura.factura, facturaYamlFile))


def load_tests(loader, tests, ignore):
	import doctest
	tests.addTests(doctest.DocTestSuite())
	return tests

