"""
Invoice: Manages an invoice
"""


from yamlns import namespace as ns
from yamlns import dateutils
from yamlns.dateutils import Period
import datetime
from fact import tresoreria
from iban import CodigoCuentaCorriente, iban
from pathlib import Path

class InvalidContract(Exception):pass

def defaultIssueDueDates(issueDate=None, dueDate=None):
	"""
	Returns default issue/due dates if provided ones
	are none or invalid dates (issue date in the past,
	or due date earlier than issue date)
	"""
	today = dateutils.Date.today()
	if not issueDate or issueDate<today:
		issueDate = today
	if not dueDate or dueDate < issueDate:
		dueDate = defaultDueDate(issueDate)
	return issueDate, dueDate

def trimesterPayDay(year, trimester) :
	"""The normal issue date of the maintenance fee invoice."""
	return dateutils.date((year, 1+3*(trimester-1), 1))

def defaultDueDate(issueDate) :
	"""The usual default due date given an issue date.
	As many days later as payingDaysInterval
	"""
	timeToWait = datetime.timedelta(days=tresoreria.payingDaysInterval)
	dueDate = dateutils.date(issueDate) + timeToWait
	return dateutils.date(dueDate)

def dummyInvoice():
	invoice = ns()
	invoice.year = 2014
	invoice.number = 0
	invoice.subject = "Manteniment 3T-2014 sjd-percebe13"
	invoice.issueDate = dateutils.date('2014-07-10')
	invoice.dueDate   = dateutils.date('2014-07-12')
	invoice.vatRate = 0.21

	invoice.client = ns()
	invoice.client.expedient = "sjd-percebe13-at1a"
	invoice.client.name = "Perico Palotes"
	invoice.client.nif = "12345678F"
	invoice.client.address = "Rue del Percebe, 13"
	invoice.client.city = "Sant Joan Despí"
	invoice.client.postalcode = '08970'
	invoice.client.state = 'Barcelona'
	invoice.client.iban = "ES7714001234661234567890"
	invoice.client.bic = "ICROESMMXXX (INSTITUTO DE CREDITO OFICIAL)"

	invoice.provider = None

	invoice.concepts = [
		'Manteniment trimestral Guifibaix',
		"Període 10 de juliol a 9 d'octubre\t 12337.50",
		"Descompte 10 de juliol a 5 d'agost\t -13.86",
		'Manteniment trimestral Guifibaix (atrassat)',
		"Període 10 d'abril a 9 de juliol\t 37.50",
	]
	invoice.draft = True
	return invoice

def availableInvoiceTemplates():
	"""Returns the available invoice templates.
	pre: working dir is company dir
	"""
	return [
		str(path.stem)
		for path in (Path('templates')/'factures').glob('*.yaml')
	]


def invoiceErrors(invoice) :
	"Check invoice fields for validity and return a list of found errors."
	import stdnum
	import stdnum.es
	import stdnum.iban
	errors = []

	if not stdnum.es.nif.is_valid(invoice.provider.nif) :
		errors.append("Invalid provider.nif '{provider.nif}'".format(**invoice))

	if not stdnum.es.nif.is_valid(invoice.client.nif) :
		errors.append("Invalid client.nif '{client.nif}'".format(**invoice))

	if 'iban' in invoice.client :
		if not stdnum.iban.is_valid(invoice.client.iban) :
			errors.append("Invalid client.iban '{client.iban}'".format(**invoice))
	elif 'ccc' in invoice.client :
		if not CodigoCuentaCorriente.is_valid(invoice.client.ccc) :
			errors.append("Invalid client.ccc '{client.ccc}'".format(**invoice))
	else :
		errors.append("Neither client.iban nor client.ccc could be found")

	return errors

def invoicePeriod(year, trimester, invoicingInfo) :
	period = Period.trimester(year, trimester)
	return period.notUntil(invoicingInfo.inici)

def periodPrice(year, trimester, contract, monthlyQuote) :
	trimesterDays = Period.trimester(year, trimester).days
	facturedDays = invoicePeriod(year, trimester, contract).days
	return monthlyQuote * 3 * facturedDays / trimesterDays

def invoiceConcepts(year, trimester, contract) :
	fullTrimester = Period.trimester(year, trimester)
	puntsDeServei = contract.get('puntsDeServei', 0)
	afadrinats = contract.get('afadrinats', 0)
	aportadors = contract.get('aportadors', 0)

	monthlyFee = 12.50
	contributorDiscount = monthlyFee
	sponsoredDiscount = 5.00

	if puntsDeServei < aportadors + afadrinats :
		raise InvalidContract("Discounts outnumber access points in the contract")

	servicePointTrimesterFee = (
		3 * monthlyFee * puntsDeServei
		if puntsDeServei else
		3 * contract.quotaMensualSenseIva
		)
	actualPeriod = invoicePeriod(year, trimester, contract)
	effectiveMonthlyFee = (
		puntsDeServei * monthlyFee
		- afadrinats*sponsoredDiscount
		- aportadors*contributorDiscount
		if puntsDeServei else
		contract.quotaMensualSenseIva
		)

	# Transitionally check that both methods match
	if 'quotaMensualSenseIva' in contract :
		if contract.quotaMensualSenseIva != effectiveMonthlyFee :
			raise InvalidContract(
				"Transition missmatch: Different prices from attributes. "
				"Annotated: {:.2f}€ Computed: {:.2f}€"
				.format(contract.quotaMensualSenseIva, effectiveMonthlyFee)
				)

	conceptes = [
		"Període {} a {}{} \t {:.2f}"
			.format(
				dateutils.catalanDate(actualPeriod.first),
				dateutils.catalanDate(actualPeriod.last),
				" (x{})".format(puntsDeServei) if puntsDeServei>1 else '',
				servicePointTrimesterFee * actualPeriod.days / fullTrimester.days,
				)
		]
	if aportadors :
		conceptes += [
			"Descompte 'Aportador'{} \t {:.2f}"
				.format(
					" (x{})".format(aportadors) if aportadors>1 else '',
					-3*contributorDiscount*aportadors * actualPeriod.days / fullTrimester.days,
				)
			]
	if afadrinats :
		conceptes += [
			"Descompte 'Connectivitat'{} \t {:.2f}"
				.format(
					" (x{})".format(afadrinats) if afadrinats>1 else '',
					-3*sponsoredDiscount*afadrinats * actualPeriod.days / fullTrimester.days,
				)
			]
	return conceptes + [
		"{} {} a {} \t {:.2f}"
			.format(
				motiu,
				dateutils.catalanDate(discount.first),
				dateutils.catalanDate(discount.last),
				- 3 * effectiveMonthlyFee * (discount.days) / fullTrimester.days,
			)
		for motiu, discount in [(
			originalDiscount.get('motiu',"Descompte"),
			actualPeriod.intersect(Period(
				originalDiscount.inici,
				originalDiscount.final)))
			for originalDiscount in contract.periodesExempts
			]
		if not discount.empty()
		]

def discountedPeriodsInTrimester(year, trimester, discountePeriods) :
	trimester = Period.trimester(year, trimester)
	return [
		p for p in (
			Period(*discounted).intersect(trimester)
			for discounted in discountedPeriods
			)
		if not p.empty()
		]

def installInvoiceSubject(expedient) :
	return "Instal·lació GuifiBaix - {}".format(expedient)

def invoiceSubject(year, trimester, expedient) :
	return 'Manteniment GuifiBaix {}{} Trimestre {} - {}'.format(
		trimester,
		['er','on','er','rt'][trimester-1],
		year,
		expedient,
		)

def jointName(name) :
	# TODO: Make it complex when required
	for old, new in zip(
		'ÑñÇçàèìòùáéíóúâêîôûäëïöüÁÉÍÒÙÁÉÍÓÚÂÊÎÔÛÄËÏÖÜ',
		'NnCcaeiouaeiouaeiouaeiouAEIOUAEIOUAEIOUAEIOU') :
		name = name.replace(old, new)
	import re
	name = re.sub(r'[^a-zA-Z0-9]+', ' ', name)
	return ''.join(name.title().split())

def invoiceFileName(year, trimester, number, payer, install=False) :
	subject = 'instal' if install else 'quota{}T'.format(trimester)
	return 'factura-{}-{:0{}}-{}-{}-{}.yaml'.format(
		year,
		number,
		tresoreria.invoiceNumberPadding,
		payer.expedient,
		jointName(payer.name),
		subject,
		)

def filenameFromInvoice(invoice, tag, kind=None):
	payer = invoice.get('payer') or invoice.get('client')
	return '{}-{}-{:0{}}-{}-{}-{}.yaml'.format(
		kind or 'factura',
		invoice.year,
		invoice.number,
		tresoreria.invoiceNumberPadding,
		payer.expedient,
		jointName(payer.name),
		tag,
		)

def generateInvoice(year, trimester, number,
		issuer, payer, contract,
		issueDate=None,
		dueDate=None,
		install=False,
		draft=None,
		):
	if issueDate is None :
		issueDate = trimesterPayDay(year, trimester)
	if dueDate is None :
		dueDate = defaultDueDate(issueDate)

	factura = ns()
	factura.vatRate = 0.21
	factura.number = number
	factura.year = year
	factura.subject = invoiceSubject(year,trimester,payer.expedient)
	if install:
		factura.subject = installInvoiceSubject(payer.expedient)
	factura.issueDate = issueDate
	factura.dueDate = dueDate

	factura.client = payer
	factura.provider = issuer
	actualPeriod = invoicePeriod(year, trimester, contract)
	factura.maintenance = ns(
		trimester = trimester,
		year = year,
		firstDate = dateutils.Date(actualPeriod.first),
		lastDate = dateutils.Date(actualPeriod.last),
	)
	factura.concepts = [
		'Manteniment trimestral Guifibaix {expedient}'.format(**payer)
		]+invoiceConcepts(year,trimester,contract)
	if install :
		basicInstall = ns.load(tresoreria.appData('concepts-basicInstall.yaml'))
		factura.concepts[:0] = [
			'Instal·lació Guifibaix {expedient}'.format(**payer),
			] + basicInstall
	if draft is not None:
		factura.draft = draft
	return factura



