#!/usr/bin/env python3
"""
Este script sirve para generar facturas trimestrales de mantenimiento,
dado un yaml con la informacion de facturacion otro con la información del contrato,
y algunos datos.

- Para ver su uso desde linea de comandos usar la opcion --help.
- Para usarlo los puntos de entrada de mas alto nivel son generateInvoice y invoiceFileName
"""

import os
import sys
from yamlns import namespace as ns
from fact.sequence import sequence
from yamlns import dateutils
from fact import tresoreria
from fact.invoice import generateInvoice, invoiceFileName, invoiceErrors
from fact.invoiceprint import printInvoice

"""
TODO
- Take default install lines from a template file
- Check issue <= due
- Check year valid
- Check dates valid
"""

def parseCommandLine() :
	"""
	Parses the command line options of the module when used as script.
	"""
	import argparse

	parser = argparse.ArgumentParser(
		description=
			"Genera una factura trimestral de manteniment.\n\n"
			,
		epilog =
			"La factura es genera en format YAML amb un nom de fitxer normalitzat.\n"
			"Amb l'opcio --pdf es genera, a més, el PDF. "
			"També es pot generar el PDF a posteriori passant el YAML a l'script `gb_factura`.\n"
			"Les dates d'emisió i venciment s'especifiquen amb format ISO 8601, YYYY-MM-DD, per exemple 2004-12-31.\n"
			"Els números de factura es poden especificar separats per comes, i intervals amb el '-', "
			"per exemple 2,5,10-12 genera les factures 2, 5, 10, 11 i 12.\n"
			"S'espera que del fitxer del contracte contingui 'contractemanteniment' "
			"i que existeixi un altre yaml canviant 'contractemanteniment' per 'dadesfacturacio' "
			"amb les dades de facturació.\n"
	)
	parser.add_argument(
		'--test',
		action='store_true',
		help="run unittests",
		)

	parser.add_argument(
		'trimester',
		metavar="TRIMESTRE",
		type=int,
		help="número de trimestre (1 a 4)",
		)
	parser.add_argument(
		'year',
		type=int,
		metavar="ANY",
		help="any del període de facturació (ie 2014)",
		)
	parser.add_argument(
		'number',
		type=sequence,
		metavar="NUMEROS_FACTURA",
		help="sequencia de números de factura separats per comes",
		)
	parser.add_argument(
		'contract',
		metavar="CONTRACTE.yaml",
		nargs='+',
		help="arxiu yaml amb la informació del contracte de servei",
		)
	parser.add_argument(
		'--show',
		dest='show',
		action='store_true',
		help="mostra només per pantalla, no genera els fitxers",
		)
	parser.add_argument(
		'--pdf',
		action='store_true',
		dest='pdf',
		help="genera també un PDF",
		)
	parser.add_argument(
		'--install',
		dest='install',
		action='store_true',
		help="genera una factura d'instal·lació amb la primera porció de trimestre i els conceptes bàsics",
		)
	parser.add_argument(
		'-d',
		'--output-dir',
		dest='outputDir',
		metavar='OUTPUT_DIR',
		help='genera els fitxer al directori OUTPUT_DIR',
		)
	parser.add_argument(
		'--issue',
		dest='issue',
		type=dateutils.date,
		metavar='ISSUE_DATE',
		help='canvia la data de emisió (YYYY-MM-DD), per defecte el primer dia 1 del trimestre',
		)
	parser.add_argument(
		'--due',
		dest='due',
		type=dateutils.date,
		metavar='DUE_DATE',
		help="canvia la data de venciment (YYYY-MM-DD), per defecte deu dies després de la data d'emisió",
		)

	parser.add_argument(
		'-f,--force',
		dest='force',
		action='store_true',
		help="força la generacio de la factura encara que siguin amb import zero",
		)

	parser.add_argument(
		'--final',
		dest='draft',
		action='store_false',
		default=None,
		help="força la generació d'una factura final (no esborrany)",
		)

	parser.add_argument(
		'--draft',
		dest='draft',
		action='store_true',
		default=None,
		help="força la generació d'una factura esborrany (no final)",
		)

	parser.add_argument(
		'-p','--provider',
		dest='provider',
		metavar="PROVIDER.yaml",
		help="fitxer YAML amb la informació de proveidor que substituirà la del proveidor especificat a la factura o al proveidor per defecte en cas del dump",
		)

	return parser



from consolemsg import step, error, fail, warn, success

def main() :

	argsparser = parseCommandLine()
	args = argsparser.parse_args()

	issuer = ns.load(args.provider) if  args.provider else tresoreria.currentCompany()

	contractInfix = 'contractemanteniment'
	clientInfix = 'dadesfacturacio'

	invoiceNumbers = iter(args.number)
	number = next(invoiceNumbers)

	for contractFile in args.contract :
		step("Processant {}...".format(os.path.basename(contractFile)))
		if contractInfix not in contractFile :
			error("El nom del fitxer del contracte hauria de contenir '{1}' però es diu '{0}'"
				.format(os.path.basename(contractFile), contractInfix),
				)
			continue

		payerFile = contractFile.replace(contractInfix, clientInfix)
		payer = ns.load(payerFile)
		contract = ns.load(contractFile)

		invoice = generateInvoice(
			year = args.year,
			trimester = args.trimester,
			number = number or 0,
			issuer = issuer,
			payer = payer,
			contract = contract,
			dueDate = args.due,
			issueDate = args.issue,
			install = args.install,
			draft = args.draft,
			)

		if args.show :
			invoice.dump(sys.stdout)
			return 0

		errors = invoiceErrors(invoice)
		if errors:
			for e in errors:
				error(e)
			continue
		

		totalBeforeVAT = tresoreria.invoiceTotalBeforeVAT(invoice.concepts)
		if totalBeforeVAT < 0 :
			error(
				"Import negatiu {} per la factura trimestal de manteniment de {}"
				.format(totalBeforeVAT, payer.expedient)
				)
			continue

		if totalBeforeVAT == 0 and not args.force and not args.install:
			warn("Ignorant factura amb import zero per {}, feu servir --force si encara la voleu".format(payer.expedient))
			continue

		outputfile = invoiceFileName(
			year = args.year,
			trimester = args.trimester,
			number = number or 0,
			payer = payer,
			install = args.install,
			)

		if number is None :
			warn("No hi havia suficients numeros de factura, asignant el 0")

		if args.outputDir :
			outputfile = os.path.join(args.outputDir, outputfile)
		success("\tGenerant '{}' import {:.2f}€+IVA...".format(outputfile, totalBeforeVAT))
		invoice.dump(outputfile)

		if args.pdf :
			pdffilename = os.path.splitext(outputfile)[0]+'.pdf'
			success("\tGenerant '{}'...".format(pdffilename))
			printInvoice(invoice, pdffilename, draft=args.draft)

		try:
			number = next(invoiceNumbers)
		except StopIteration:
			number = None

	if number is not None:
		excessNumbers = ", ".join([str(number)] + [str(n) for n in invoiceNumbers])
		warn("Han sobrat aquests numeros de factura: {}".format(excessNumbers))

if __name__ == '__main__' :

	sys.exit(main())



