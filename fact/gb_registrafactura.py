#!/usr/bin/env python3


from consolemsg import *
from fact import tresoreria
from yamlns import dateutils

def parseCommandLine() :
	"""
	Parses the command line options of the module when used as script.
	"""
	import argparse

	parser = argparse.ArgumentParser(
		description=
			"Registra factura al fitxer de control de cobraments (cobraments.yaml).\n\n"
			,
		epilog ="",
	)
	parser.add_argument(
		'--test',
		action='store_true',
		help="run unittests",
		)
	parser.add_argument(
		'--all',
		action='store_true',
		help="no filtra si ja estan registrades",
		)
	parser.add_argument(
		'--warn-existing',
		action='store_true',
		help="avisa si no es registra una factura per que ja està registrada",
		)
	parser.add_argument(
		'--new',
		action='store_true',
		help="no llegeix el cobraments (per començar de zero)",
		)
	parser.add_argument(
		'yamlinvoices',
		metavar="FACTURA.yaml",
		nargs='*',
		help="Fitxers yaml amb la informació de les factures a enviar",
		)
	return parser

import os
import sys
import unittest
import glob
from yamlns import namespace as ns

def main() :

	argsparser = parseCommandLine()
	args = argsparser.parse_args()

	cobraments = [] if args.new else tresoreria.cobraments()

	yamlinvoices = args.yamlinvoices or (
		glob.glob("factures/factura-*.yaml"))

	for invoiceyaml in sorted(yamlinvoices) :
		invoice = ns.load(invoiceyaml)
		try:
			invoiceId = tresoreria.invoiceId(invoice)
		except ValueError:
			warn("Ignorant la factura sense número: "+invoiceyaml)
			continue
		if invoiceyaml[:len('factures/factura-YYYY-NNNN-')] != f'factures/factura-{invoiceId}-':
			error(
				f"El nom del fitxer '{invoiceyaml}' "
				f"no coincideix amb les dades internes: "
				f"year: {invoice.year} number: {invoice.number}"
			)
			continue
		matching = [
			f
			for f in cobraments
			if f.factura == invoiceId
			]
		if matching and not args.all:
			if args.warn_existing:
				warn('Ignorant factura ja registrada: '+invoiceyaml)
			continue
		try:
			int(invoice.number)
		except ValueError:
			warn('Ignorant factura sense número: '+invoiceyaml)
			continue

		step("Registrant Factura '{}'".format(invoiceyaml))
		concepte=invoiceyaml[len('factures/factura-2015-0000-'):-len('.yaml')]
		totalAmount = tresoreria.invoiceTotal(invoice)
		output = (
			"- factura: {}\n"
			"  concepte: {}\n"
			"  valor: {}\n"
			"  pendent: {}\n"
			).format(
				invoiceId,
				concepte,
				totalAmount,
				totalAmount,
			)
			
		import datetime
		try:
			venciment= dateutils.date(invoice.dueDate)
		except:
			warn("No ha estat possible deduir la data de venciment de '{}'".format(invoice.dueDate))
		else:
			output += (
				"  venciments:\n"
				"  - data: {}\n"
				"    valor: {}\n"
				).format(
					venciment,
					totalAmount,
				)
		print(output)
			



def load_tests(loader, tests, ignore):
	import doctest
	tests.addTests(doctest.DocTestSuite())
	return tests

if __name__ == '__main__' :
	if '--test' in sys.argv :
		sys.argv.remove('--test')
		sys.exit(unittest.main())

	sys.exit(main())




