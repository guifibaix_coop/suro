#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""\
Aquest script es baixa extractes de CaixaGuissona en format CSB43
i els converteix en fitxers CSV mes entendibles per humans.

Necessita que hi hagi un fitxer accounts.yaml amb la següent estructura:

caixaguissona:
  at2:
    account: 1234567890
    user: usuari_at2
    password: contrasenya_at2
  gb:
    account: 0987654321
    user: usuari_gb
    password: contrasenya_gb

'caixaguissona' indica l'entitat bancaria (De moment la unica suportada).
'at2' i 'gb' es faran servir com a suffix en els fitxers de sortida.
'account' només son els 10 darrers digits que son els que identifiquen el compte dins de l'entitat.

"""

installInstructions="""\
\033[31;1mAquest script necessita alguns packets:

$ pip3 install csb43 selenium consolemsg yamlns

Si estás instal·lant els paquets Python al sistema prova amb 'sudo' al davant.

Tambe necessitaras matxacar la darrera versio del webdriver que normalment ve amb selenium:

$ wget https://github.com/mozilla/geckodriver/releases/download/v0.17.0/geckodriver-v0.17.0-linux64.tar.gz
$ tar xvfz geckodriver-v0.17.0-linux64.tar.gz
$ mv geckodriver $(which geckodriver)

Si actualitzes el navegador i l'script deixa de funcionar torna a fer upgrade del selenium i del webdriver.

\033[0m
"""

import os
import glob
import datetime
import time
import tempfile
import shutil
import sys

try:
	from selenium import webdriver
	import csb43.csb43
	from selenium.webdriver.common.keys import Keys
	from selenium.common.exceptions import NoSuchElementException
	from yamlns import namespace as ns
	from consolemsg import step, error, warn
except ImportError:
	sys.stderr.write(installInstructions)
	raise


def downloadBankstatement_caixaguissona(account, user, password, output) :
	targetDirectory =  tempfile.mkdtemp()

	profile = webdriver.FirefoxProfile()
	profile.set_preference('browser.download.folderList', 2)
	profile.set_preference('browser.download.manager.showWhenStarting', False)
	profile.set_preference('browser.download.dir', targetDirectory)
	profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'application/x-msdownload')
	profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'text/csv')

	import time
	browser = webdriver.Firefox(profile)
	try:
		step("\tIdentificant-nos a la web")

		browser.get("https://www.caixaguissona.com/")
		time.sleep(2)
		userbox = browser.find_element_by_id("user")
		userbox.send_keys(user)
		passbox = browser.find_element_by_name("pwd")
		passbox.send_keys(password)
		time.sleep(2)
		button = browser.find_element_by_xpath('//*[contains(@class, "form-sign-in")]//input[contains(@class, "btn-identificat")]')
		button.click()

		try:
			button = browser.find_element_by_id('btnContinuar')
		except NoSuchElementException:
			pass
		else:
			warn("Saltant un missatge d'avis en 4 segons")
			time.sleep(4)
			button.click()


		step("\tEntrant a transmissió de fitxers")
		browser.execute_script("loadGetURL('/ca/private/filetransmission','')")

		step("\tEntrant al compte {}".format(account))
		browser.execute_script("loadPostURL('/ca/private/filetransmission',{{'accountNumber':'{}','referencia':'0'}})".format(account))

		step("\tEntrant al formulari de l'informe")

		browser.execute_script("loadPostURL('/ca/private/findCSB43',{{'accountNumber': '{}'}});".format(account))
		time.sleep(2)

		step("\tOmplint formulari per descargar CSB43 entre dues dates")

		inici = browser.find_element_by_id("txtDataInici")
		inici.clear()
		inici.send_keys('01/01/13')
		final = browser.find_element_by_id("txtDataFi")
		final.clear()
		final.send_keys(datetime.date.today().strftime('%d/%m/%y'))
		browser.execute_script('$("#frmFind").submit();')

		step("\tEsperant que es descarregui...")
		while True:
			downloads = glob.glob(targetDirectory+'/*')
			try:
				time.sleep(1)
			except:
				break
			if not downloads: continue
			if 'part' in downloads[0]: continue
			break

		step("\tGrabant-ho a {}".format(output))
		shutil.move(downloads[0], output)
	except Exception as e:
		error("\tException raised")
		import traceback
		traceback.print_exc()
		raise

	finally:
		step("\tSortint del navegador")
		browser.get("https://www.caixaguissona.com/sortir.asp")
		browser.quit()

def csb43_to_csv(inputfile, outputfile, infix):
	fields=[
		"fecha_de_operacion",
		"fecha_valor",
		"tipo_concepto",
		"referencia",
		"cantidad",
		"saldo",
		"concepto",
	]
	with open(inputfile, encoding='latin1') as csb43file:
		csb = csb43.csb43.File(csb43file)
	doc = csb

	def utf8(s):
		from yamlns import text as u
		return u(s)

	output = []
	output.append('\t'.join(fields)) # header
	saldo = 0
	generatedRefs = set()
	for movement in doc.accounts[0].transactions:
		print(dir(movement))
		print(movement.sharedItem)
		isTranfer = utf8(movement.sharedItem) in (
			'02', # Incoming transfer
			'04', # Outcoming transfer
			)
		reference2 = utf8(movement.reference2)
		transactionDate = str(movement.transactionDate)[:10]
		valueDate = str(movement.valueDate)[:10]

		# Inhouse transfers have same reference, append date
		if isTranfer and reference2[:2]=='00':
			datesuffix = ''.join(transactionDate[2:].split('-'))
			base = infix + reference2 + datesuffix
			import itertools
			for sequence in itertools.count():
				reference = base+format(sequence,'02d')
				if reference not in generatedRefs:
					break
			generatedRefs.add(reference)
			reference2 = reference

		saldo+=movement.amount
		output.append('\t'.join([
			transactionDate,
			valueDate,
			'-'.join([
				utf8(movement.sharedItem),
				utf8(movement.ownItem),
				]),
			reference2.rjust(21),
			'{: >10.2f}'.format(movement.amount),
			'{: >10.2f}'.format(saldo),
			utf8(movement.optionalItems[0].item1)+utf8(movement.optionalItems[0].item2),
		]))

	with open(outputfile, 'w') as csvfile:
		csvfile.write('\n'.join(output))


def main():
	try:
		accounts = ns.load("accounts.yaml")
	except Exception:
		error(__doc__)
		raise

	for infix, account in accounts.caixaguissona.items():
		if not account.account or not account.user or not account.password:
			warn("Ignorant compte '{}', dades buides".format(infix))
			continue

		if len(str(account.account)) != 10:
			error("El numero compte per '{}' ha de tenir 10 digits. "
				"Si començés per 0 enrecorda't de posar-hi cometes.")

		step("Descarregant extracte per {} "
			"(S'obrirà un navegador, deixa'l fer)".format(infix))

		csb43file = 'banc-{}-extracte.csb43'.format(infix)
		csvfile = 'banc-{}-extracte.csv'.format(infix)
		downloadBankstatement_caixaguissona(
			account.account,
			account.user,
			account.password,
			csb43file,
			)
		step("\tConvertint {} a {}".format(csb43file, csvfile))
		csb43_to_csv(csb43file, csvfile, infix)

if __name__ == "__main__":
	sys.exit(main())


