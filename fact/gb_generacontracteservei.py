#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Dependencias Ubuntu:
# sudo apt-get install pandoc texlive-lang-spanish pdftk
# sudo pip3 install pypandoc

import sys, os
import argparse, datetime
from yamlns import namespace as ns
from fact.tresoreria import currentCompany

def sourceRelative(path) :
	"""Returns files relative this source"""
	return os.path.join(
		os.path.dirname(
		os.path.relpath(
			__file__, os.curdir)), path)


def parseDate(v) :
	import re
	if not re.match('^20[0-9][0-9]-(0[1-9]|10|11|12)-[0-3][0-9]$', v) :
		raise ValueError
	year, month, day = (int(x) for x in v.split('-'))
	if day<1 or day > 31 : raise ValueError
	return year, month, day

def isodate(value) :
	try : return parseDate(value)
	except ValueError: raise argparse.ArgumentError()

def cleanContactInformation(person):
	person.contact.email = [
		email.split('(')[0].strip()
		for email in person.contact.email
		]
	person.contact.phone = [
		phone.split('(')[0].strip()
		for phone in person.contact.phone
		]

def parseArguments() :
	parser = argparse.ArgumentParser(
		description="Genera el contracte de servei de GuifiBaix")
	parser.add_argument(
		'user',
			help='YAML file containing user personal info',
			metavar='USER.yaml'
		)
	parser.add_argument(
		'provider',
			help='YAML file containing provider info',
			nargs='?',
			metavar='PROVEEDORA.yaml'
		)
	parser.add_argument(
		'--md',
			action='store_true',
			dest='generateMarkdown',
			help='Dumps intermediate markdown content on stderror.',
		)
	parser.add_argument(
		'-o', '--output',
			dest='output',
			help='specifies an output file. '
				'Format is deduced from the  extension. '
				'If not specified, markdown is dumped on stdout',
			metavar='OUTPUT',
		)
	parser.add_argument(
		'-d', '--data',
			dest='data',
			type=isodate,
			help='Contract date in ISO format YYYY-MM-DD',
			default=parseDate(datetime.date.today().isoformat()),
			metavar='ISODATE',
		)
	return parser.parse_args()

def main():
	args = parseArguments()

	proveedora = ns.load(args.provider) if args.provider else currentCompany()

	client = ns.load(args.user)

	filled = generateMarkdown(
		datetime.date(*args.data), client, proveedora )

	if args.generateMarkdown :
		print(filled)
		sys.exit(0)

	outputfile = args.output or (
		"contrato-guifibaix-{}-{}-{}.pdf".format(
			datetime.date(*args.data).isoformat(),
			client.nif,
			''.join(client.name.title().split())
		))
	generateLatex_pandoc(filled, outputfile)

def generateLatex_pandoc(markdown, outputfile=None):
	import subprocess
	outputOption = [
		'-o', outputfile,
		] if outputfile else []

	output = subprocess.check_output(
		[
			'pandoc',
			'--from', 'markdown',
			'--to', 'latex',
			'--template',
				appData('contratoservicio-template.latex'),
		]+outputOption,
		input=markdown.encode('utf-8')
		)
	if outputfile is None :
		return output.decode('utf-8')

def generateLatex_pypandoc(markdown, outputfile):
	import pypandoc
	pypandoc.convert(
		markdown,
		format='markdown',
		to='latex',
		extra_args=[
		'-o', outputfile,
		'--template',
			appData('contratoservicio-template.latex'),
		]
	)

def generateMarkdown(date, user, provider):
	female = ns(
		quotedElCliente = "la \"CLIENTA\"",
		cliente = "CLIENTA",
		elCliente = "la CLIENTA",
		delCliente = "de la CLIENTA",
		ELCLIENTE = "LA CLIENTA",
		ElCliente = "La CLIENTA",
		alCliente = "a la CLIENTA",
		DelCliente = "De la CLIENTA",
		AlCliente = "A la CLIENTA",
		)

	male = ns(
		quotedElCliente = "el \"CLIENTE\"",
		cliente = "CLIENTE",
		elCliente = "el CLIENTE",
		delCliente = "del CLIENTE",
		ELCLIENTE = "EL CLIENTE",
		ElCliente = "El CLIENTE",
		alCliente = "al CLIENTE",
		DelCliente = "Del CLIENTE",
		AlCliente = "Al CLIENTE",
		)

	provider.NAME = provider.name.upper()

	cleanContactInformation(provider)

	meses="enero febrero marzo abril mayo junio julio agosto septiembre octubre noviembre diciembre".split()
	data = ns(
		fecha = ns(
			any = date.year,
			mes = meses[date.month-1],
			dia = date.day,
			),
		lugar = "Sant Joan Despí",
		client = user,
		proveedor = provider,
		genre = female if user.genre.lower() == 'female' else male,
		# TODO
#		imagepath = sourceRelative(''),
		imagepath = '',
	)



	with open(appData('contratoservicio.md'),encoding="utf8") as f :
		template = f.read()
	return template.format(**data)


from fact.tresoreria import appData


import unittest
import b2btest

class ServiceContractGenerator_Test(unittest.TestCase):
	def setUp(self):
		self.maxDiff = None

	def test_generateMarkdown_male(self):
		self.assertB2BEqual(
			generateMarkdown(
				date=datetime.date(2014,2,20),
				user=ns.load(appData('b2b/input-adam.yaml')),
				provider=ns.load(appData('provider-gb.yaml')),
				),
			appData('b2b/output-adam.md'),
			appData('b2b/output-adam-failed.md'),
			)

	def test_generateMarkdown_female(self):
		self.assertB2BEqual(
			generateMarkdown(
				date=datetime.date(2014,2,20),
				user=ns.load(appData('b2b/input-eva.yaml')),
				provider=ns.load(appData('provider-gb.yaml')),
				),
			appData('b2b/output-eva.md'),
			appData('b2b/output-eva-failed.md'),
			)


	def test_generateMarkdown_legal(self):
		self.assertB2BEqual(
			generateMarkdown(
				date=datetime.date(2014,2,20),
				user=ns.load(appData('b2b/input-acme.yaml')),
				provider=ns.load(appData('provider-gb.yaml')),
				),
			appData('b2b/output-acme.md'),
			appData('b2b/output-acme-failed.md'),
			)

	def test_generateLatex_female(self):
		md = generateMarkdown(
			date=datetime.date(2014,2,20),
			user=ns.load(appData('b2b/input-eva.yaml')),
			provider=ns.load(appData('provider-gb.yaml')),
			)
		self.assertB2BEqual(
			generateLatex_pandoc(md),
			appData('b2b/output-eva.tex'),
			appData('b2b/output-eva-failed.tex'),
			)


if __name__ == '__main__':
	import unittest
	import sys
	if '--test' in sys.argv:
		sys.argv.remove('--test')
		sys.exit(unittest.main())

	if '--accept' in sys.argv:
		sys.argv.remove('--accept')
		unittest.TestCase.acceptMode=True
		sys.exit(unittest.main())

	main()

