import unittest
from pathlib import Path
from .testutils import sandbox_dir
from .tresoreria import (
	nextInvoiceNumber,
	invoiceById,
	invoiceId,
)
from .invoice import availableInvoiceTemplates

class Tresury_Test(unittest.TestCase):

	def create_company(self):
		Path('company.yaml').write_text(
			""
		)

	def create_invoices(self, year, numbers, kind='factura'):
		invoice_dir = Path('factures')
		invoice_dir.mkdir(exist_ok=True)
		for number in numbers:
			invoice_file = invoice_dir / f'{kind}-{year}-{number}-expedient.yaml'
			invoice_file.write_text(
				f"year: {year}\n"
				f"number: {int(number.lstrip('0'))}\n"
			)

	def test_nextInvoiceNumber___base(self):
		with sandbox_dir() as sandbox:
			self.create_company()
			self.create_invoices(2020, ['1130'])
			self.assertEqual(nextInvoiceNumber(2020), '1131')

	def test_nextInvoiceNumber___withMany(self):
		with sandbox_dir() as sandbox:
			self.create_company()
			self.create_invoices(2020, ['1130', '2030'])
			self.assertEqual(nextInvoiceNumber(2020), '2031')

	def test_nextInvoiceNumber___otherYearsIgnored(self):
		with sandbox_dir() as sandbox:
			self.create_company()
			self.create_invoices(2020, ['1130', '2030'])
			self.create_invoices(2021, ['3130'])
			self.assertEqual(nextInvoiceNumber(2020), '2031')

	def test_nextInvoiceNumber___with_zero_padding_numbers(self):
		with sandbox_dir() as sandbox:
			self.create_company()
			self.create_invoices(2020, ['0030'])
			self.assertEqual(nextInvoiceNumber(2020), '0031')

	def test_nextInvoiceNumber___year_with_no_invoice(self):
		with sandbox_dir() as sandbox:
			self.create_company()
			self.create_invoices(2020, ['0030'])
			self.assertEqual(nextInvoiceNumber(2021), '0001')

	def test_nextInvoiceNumber___not_in_company_dir(self):
		with sandbox_dir() as sandbox:
			with self.assertRaises(SystemExit) as ctx:
				nextInvoiceNumber(2021)

	from yamlns.testutils import assertNsEqual

	def test_invoice__byId(self):
		with sandbox_dir() as sandbox:
			self.create_company()
			self.create_invoices(2020, ['0030'])
			self.assertNsEqual(
				invoiceById('2020-0030'),
				"year: 2020\n"
				"number: 30\n"
			)

	def test_invoiceById__missing(self):
		with sandbox_dir() as sandbox:
			self.create_company()
			self.create_invoices(2020, ['0010'])
			self.assertIsNone(invoiceById('2020-0030'))

	def test_invoiceById__missing(self):
		with sandbox_dir() as sandbox:
			self.create_company()
			self.create_invoices(2020, ['0030'])
			self.assertIsNone(invoiceById('2020-0030', kind='abonament'))

	def test_invoiceById__kindAbonament(self):
		with sandbox_dir() as sandbox:
			self.create_company()
			self.create_invoices(2020, ['0030'], kind='factura')
			self.create_invoices(2020, ['0030'], kind='abonament')
			self.assertNsEqual(
				invoiceById('2020-0030', kind='abonament'),
				"year: 2020\n"
				"number: 30\n"
			)

	def test_invoiceById__kindAbonament_notFound(self):
		with sandbox_dir() as sandbox:
			self.create_company()
			self.create_invoices(2020, ['0030'], kind='factura')
			self.assertIsNone(
				invoiceById('2020-0030', kind='abonament')
			)

	def test_invoiceId(self):
		with sandbox_dir() as sandbox:
			self.create_company()
			self.create_invoices(2020, ['0030'])
			self.assertEqual(
				invoiceId(invoiceById('2020-0030')), '2020-0030'
			)


	def test_availableInvoiceTemplates_noneAvailable(self):
		with sandbox_dir() as sandbox:
			self.assertEqual(availableInvoiceTemplates(), [
			])


	def test_availableInvoiceTemplates_withMany(self):
		with sandbox_dir() as sandbox:
			template_dir = Path('templates')/'factures'
			template_dir.mkdir(parents=True)
			(template_dir / 'one.yaml').write_text('')
			(template_dir / 'two.yaml').write_text('')
			(template_dir / 'three.txt').write_text('') # This will be ignored
			self.assertEqual(set(availableInvoiceTemplates()), set([
				'one', 'two'
			]))

