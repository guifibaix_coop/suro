Bones {client/a},

Se'ns ha comunicat la devolució del rebut {motiu} de {data} 
referent a la factura {número-factura}, que et tornem a adjunta a aquest correu.

Podeu fer-nos una transferència per import de {import} al compte següent:
  iban: ES55 3140 0001 9500 1161 2500
  bic: BCOEESMM140
  name: AT2, Acció Transversal per a la Transformació Social
  nif: G64922131

O també podeu comunicar-nos per fer un altra emisió de rebut contra el 
vostre compte sense cost afegit.

Per qualsevol dubte, quedem a la vostra disposició.

Una salutació,
Els cooperativistes de Guifibaix.

