#!/usr/bin/env python3

from yamlns import namespace as ns
from yamlns import dateutils
import sys
import consolemsg
from fact import tresoreria
from fact import gb_compres

logs=[]

def error(msg):
	consolemsg.error(msg)
	logs.append("Error: "+msg)

def warn(msg):
	consolemsg.warn(msg)
	logs.append("Avís: "+msg)

def main():
	today=dateutils.Date(dateutils.Date.today())

	allCompanies = 'at2 gb fgb'.split()

	company = tresoreria.currentCompany()
	others = [c for c in allCompanies if c != company.infix ]

	otherInfix = 'gb' if company.infix == 'at2' else 'at2'

	extractes = ns()
	for c in allCompanies:
		extractes[c] = tresoreria.extracte_banc(c)
	extracte = extractes[company.infix]


	cobraments = tresoreria.cobraments()
	tresoreria.issuedInvoices_computePaidAndPending(cobraments)
	tresoreria.issuedInvoices_checkInconsistencies(cobraments,error,warn)


	cashBalance = tresoreria.cashBalance()

	aportacionsSocis = ns(
		(key[len("Capital Social "):], -value)
		for key, value in cashBalance.items()
		if key.startswith("Capital Social ")
		)
	capitalSocial = sum((v for s,v in aportacionsSocis.items()))

	compres = gb_compres.purchases_loadFromOld()
	compresOther = gb_compres.purchases_loadFromOld('-'+otherInfix)
	sumariCompres = gb_compres.purchases_summary(compres, company.short)

	print("\n# Balanç {short} {today}\n".format(today=today, **company))

	print("\n## Capital social\n")

	print("- Total: %.2f"%capitalSocial)
	for soci, aportacio in sorted(aportacionsSocis.items()):
		print("\t- %s: %.2f"%(soci, aportacio))

	print("\n## Vendes\n")

	print ("- Facturat: %.2f"% sum((
		factura.valor
		for factura in cobraments
		)))
	print("\t- Cobrat: %.2f"% sum((
		factura.valor-factura.pendent
		for factura in cobraments
		)))
	print("\t- Pendent: %.2f"% sum((
		factura.pendent
		for factura in cobraments
		)))
	print("\t\t- A vencer: %.2f"% sum((
		factura.pendent-factura.vencut
		for factura in cobraments
		)))
	for factura in cobraments :
		if factura.pendent == factura.vencut: continue
		print('\t\t\t-', factura.concepte, factura.pendent-factura.vencut)

	print("\t\t- Vencut: %.2f"% sum((
		factura.vencut
		for factura in cobraments
		)))
	for factura in cobraments :
		if factura.vencut==0: continue
		print('\t\t\t-', factura.concepte, factura.vencut)

	horesTotalsCobrades = sum(( f.madobra.total for f in cobraments if 'madobra' in f ))
	horesPerParticipant = dict()
	for f in cobraments:
		if 'madobra' not in f: continue
		participants = f.madobra.participants
		part = f.madobra.total/len(participants)
		for participant in participants :
			try :
				horesPerParticipant[participant] += part
			except KeyError:
				horesPerParticipant[participant]  = part

	print("\n## Compres i despeses\n")

	print("- Total despeses: %.2f"%(sumariCompres.totalPayments+horesTotalsCobrades))
	print("\t- Compres a compte corrent: %.2f"%sumariCompres.sumProperlyPaid)
	print("\t- Compres amb caixa: %.2f"%sumariCompres.sumPaidByPartners)
	for pagador, amount in sumariCompres.paidByPartners.items():
		print( '\t\t- %s: %.2f'%(pagador, amount))

	print("\t- Aportat pels socis en material: %.2f"%sumariCompres.sumContributionsInKind)
	for pagador, amount in sumariCompres.contributionsInKind.items():
		print( '\t\t- %s: %.2f'%(pagador, amount))

	print("\t- Diners en hores que es deuen: %.2f"%(horesTotalsCobrades))
	for p,v in sorted(horesPerParticipant.items()) :
		print ("\t\t- %s: %.2f"%(p,v))
		for f in cobraments :
			if 'madobra' not in f : continue
			if p not in sorted(f.madobra.participants) : continue
			print('\t\t\t- %s: %.2f'%(f.concepte, f.madobra.total/len(f.madobra.participants)))

	print("\t- TODO: Impostos, taxes i despeses financeres")

	print("\n### Compres clasificades per\n")

	print("- Com s'ha facturat:")
	for nif, amount in sumariCompres.byVat.items():
		print( '\t- %s: %.2f'%(nif,amount))

	print("- Qui ha fet el pagament:")
	for pagador, amount in sumariCompres.byPayer.items():
		print( '\t- %s: %.2f'%(pagador, amount))

	print("- Per proveidors:")
	for proveidor, amount in sumariCompres.byProvider.items():
		print( '\t- %s: %.2f'%(proveidor, amount))

	'''
	for factura in compres :
		if factura.Nif == '' :
			print(factura.dump())
	'''

	# TODO: check abs(Compensado) no pot ser major que abs(cantidad)
	# TODO: check Compensado >= 0 

	# Compensating bank statements with cash movements
	bankReferences = tresoreria.bankReferencesOnCashMovements(
		'Banco {}'.format(company.short))
	for reference in bankReferences:
		if str(reference.reference) not in extracte:
			error("Referencia '{}' de caixa.csv, no trobada a l'extracte"
				.format(reference.reference))
			continue
		extracte[str(reference.reference)].compensado += reference.amount

	# Compensating bank statements with paid issued invoices
	for factura in cobraments:
		if 'venciments' not in factura: continue
		for v in factura.venciments:
			if 'pagat' not in v: continue
			if str(v.pagat) not in extracte:
				error("Referencia '{}' de cobraments.yaml, no trobada a l'extracte"
					.format(v.pagat))
				continue
			extracte[str(v.pagat)].compensado += v.valor

	# Compensating bank statement with purchases
	for compra in compres:
		payer = compra.payedBy.lower()

		if payer not in ('at2', 'gb', 'fgb'):
			continue

		if payer != company.infix:
			warn("{number} compra pagada per {payedBy} en el registre de {home}, "
				"{date} '{providerName}' quantitat: {totalAmount} {description}"
				.format(home=company.infix, **compra))

		if not compra.bankReference:
			error("{number} compra pagada per {payedBy} sense referencia de quadrament, "
				"{date} '{providerName}' quantitat: {totalAmount} {description}"
				.format(**compra))
			continue

		purchaseBankReferences = compra.bankReference.split(',')
		remainingAmount = compra.totalAmount

		for bankReference in compra.bankReference.split(','):

			if not bankReference in extractes[payer]:
				error("{number} compra pagada per {payedBy} no existeix la referencia {singleBankReference}, "
					"{date} '{providerName}' quantitat: {totalAmount} {description}"
					.format(singleBankReference=bankReference,**compra))
				continue

			if len(purchaseBankReferences)==1:
				extractes[payer][bankReference].compensado -= remainingAmount
				remainingAmount = 0
			else:
				toBeCompensated = min(extractes[payer][bankReference].cantidad, remainingAmount)
				extractes[payer][bankReference].compensado += toBeCompensated
				remainingAmount += toBeCompensated

		if remainingAmount and compra.bankReference:
			error("{number} compra pagada per {payedBy} no compensada completament per {bankReference}, "
				"{date} '{providerName}' quantitat: {totalAmount} {description}, falten {remaining}"
				.format(remaining=remainingAmount, **compra))


	# Compensating bank statement with purchases from the other company
	for compra in compresOther:
		payer = compra.payedBy.lower()

		if payer != company.infix:
			continue

		if not compra.bankReference:
			error("{number} compra pagada per {payedBy} sense referencia de quadrament, "
				"{date} '{providerName}' quantitat: {totalAmount} {description}"
				.format(**compra))
			continue

		if not compra.bankReference in extractes[payer]:
			error("{number} compra pagada per {payedBy} no existeix la referencia {bankReference}, "
				"{date} '{providerName}' quantitat: {totalAmount} {description}"
				.format(**compra))
			continue

		warn("{number} compra pagada per {payedBy} en el registre de {home}, "
			"{date} '{providerName}' quantitat: {totalAmount} {description}"
			.format(home=otherInfix, **compra))

		extractes[payer][compra.bankReference].compensado -= compra.totalAmount


	saldoBancari = sum(moviment.cantidad for moviment in extracte.values())
	totalOperacionsBancAFavor = sum(
		moviment.cantidad
		for moviment in extracte.values()
		if moviment.cantidad >= 0
		)

	operacionsBancAFavorSenseCompensar = sum(
		moviment.cantidad-moviment.compensado
		for moviment in extracte.values()
		if moviment.cantidad >= 0
		)

	totalOperacionsBancEnContra = sum(
		moviment.cantidad
		for moviment in extracte.values()
		if moviment.cantidad < 0
		)

	operacionsBancEnContraSenseCompensar = sum(
		moviment.cantidad-moviment.compensado
		for moviment in extracte.values()
		if moviment.cantidad < 0
		)

	print("\n## Resum de moviments de caixa:\n")

	for k, v in sorted(cashBalance.items()):
		print("- {}: {}".format(k,v))


	print("\n## Resum moviments bancaris:\n")

	print("- Saldo al banc: %.2f"%(saldoBancari))
	print("- Operacions a favor: %.2f"%(totalOperacionsBancAFavor))
	print("- Operacions en contra: %.2f"%(totalOperacionsBancEnContra))
	print("- Operacions a favor sense quadrar: %.2f"%(operacionsBancAFavorSenseCompensar))
	for moviment in extracte.values():
		if moviment.cantidad < 0: continue
		if abs(moviment.cantidad) == abs(moviment.compensado): continue
		print("\t- {} {}: {} - {} ({})".format(moviment.fecha_de_operacion, moviment.referencia, moviment.cantidad, moviment.compensado, moviment.concepto))
	print("- Operacions en contra sense quadrar: %.2f"%(operacionsBancEnContraSenseCompensar))
	for moviment in extracte.values():
		if moviment.cantidad >= 0: continue
		if abs(moviment.cantidad) == abs(moviment.compensado): continue
		print("\t- {} {}: {} - {} ({})".format(moviment.fecha_de_operacion, moviment.referencia, moviment.cantidad, moviment.compensado, moviment.concepto))


	print("\n# Descuadres detectados:\n")

	for entry in logs:
		print ("- " + entry)


