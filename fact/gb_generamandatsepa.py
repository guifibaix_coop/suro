#!/usr/bin/env python3

from consolemsg import step, warn
from yamlns import namespace as ns
from yamlns import dateutils
from fact.tresoreria import appData, currentCompany
from fact.gb_factura import dummyInvoice
from fact.sepa import creditorIdentifier
from fact import iban

texts = ns.load(
	appData('sepa-core-mandate-texts.yaml')
	).sepamandate

def sepaMandateGuissona(client, provider, outputFilename):
	data = ns()
	data.client=client
	data.creditor=provider
	with open(appData('Guifibaix-MandatoSEPA-template.yaml')) as t:
		template = t.read()
	formdata = template.format(**data)

	import pdfform
	pdfform.fill(
		inputpdf=appData('Guifibaix-MandatoSEPA-original.pdf'),
		inputyaml=formdata,
		outputpdf=outputFilename,
		)



def sepaMandate(client, provider, outputFilename):
	"""
		Generates a SEPA Core mandate to be signed by client.
	"""

	from reportlab.lib import colors
	from reportlab.lib.enums import TA_JUSTIFY, TA_RIGHT, TA_CENTER, TA_LEFT
	from reportlab.lib.pagesizes import A4
	from reportlab.platypus import SimpleDocTemplate, Paragraph, Image, Table, Spacer
	from reportlab.platypus.flowables import Flowable
	from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
	from reportlab.lib.units import mm
	from reportlab.pdfbase import pdfmetrics
	from reportlab.pdfbase.ttfonts import TTFont, TTFError
	from reportlab.graphics.widgets.signsandsymbols import YesNo

	styles=getSampleStyleSheet()
	try :
		pdfmetrics.registerFont(TTFont('Ubuntu', 'Ubuntu-R.ttf'))
		pdfmetrics.registerFont(TTFont('Ubuntu-I', 'Ubuntu-RI.ttf'))
		pdfmetrics.registerFont(TTFont('Ubuntu-B', 'Ubuntu-B.ttf'))
		pdfmetrics.registerFont(TTFont('Ubuntu-BI', 'Ubuntu-BI.ttf'))
		pdfmetrics.registerFontFamily('Ubuntu', normal='Ubuntu',italic='Ubuntu-I',bold='Ubuntu-B',boldItalic='Ubuntu-BI')

		ParagraphStyle.defaults['fontName'] = 'Ubuntu'
		styles['Normal'].fontName='Ubuntu'
	except TTFError :
		warn("Using default font instead coorporative one, 'Ubuntu', consider installing it.")

	styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
	styles.add(ParagraphStyle(name='Right', alignment=TA_RIGHT))
	styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))


	def p(text, style='Normal') :
		return Paragraph(text, style=styles[style])

	def color(color, string) :
		return '<font color={}>{}</font>'.format(color, string)

	def title(string):
		return p('<font color=darkred size=+1>{}</font>'.format(string), style='Right')

	def r(texts, content):
		return p(
			'<font size=-1><b>{ca}</b> / <b><i>{es}</i></b> / <i>{en}</i>:</font>&nbsp;{content}'
			.format(content=content,**texts),
			style='Normal',
			)
	def section(texts, size=-3, style='Right'):
		return p(
			'<font color=white size={size}><b>{ca}</b> / <b><i>{es}</i></b> / <i>{en}</i></font>'
			.format(size=size,**texts),
			style=style,
			)
	def m(texts, size=-3, style='Right'):
		return p(
			'<font size={size}><b>{ca}</b> / <b><i>{es}</i></b> / <i>{en}</i></font>'
			.format(size=size,**texts),
			style=style,
			)
	def smallMulti(texts):
		return (
			p('<font size="-3">{ca}</font>'.format(**texts), style='Justify'),
			p('<b><i><font size="-5">{es}</font></i></b>'.format(**texts), style='Justify'),
			p('<i><font size="-5">{en}</font></i>'.format(**texts), style='Justify'),
			)

	class CheckBox(Flowable):
		def __init__(self, paragraph, checked=False):
			self.p = paragraph
			self.checked = checked
		def wrap(self, w, h):
			pw, ph = self.p.wrap(w-12,h)
			return pw+12, ph
		def draw(self):
			self.canv.rect(0,2,9,9)
			if self.checked:
				self.canv.line(0,2,8,10)
				self.canv.line(0,10,8,2)
			self.p.drawOn(self.canv,12,0)


	def checkboxes(option1, option2):
		cols = Cols(
			CheckBox(p(u'<font size=-2><b>{ca}</b> / <b><i>{es}</i></b> / <i>{en}</i></font>'
				.format(**option1),
				style='Normal',
				), True),
			CheckBox(p(u'<font size=-2><b>{ca}</b> / <b><i>{es}</i></b> / <i>{en}</i></font>'
				.format(**option2),
				style='Normal',
				)),
			)
		cols.proportions= 4,3
		return cols


	class Cols(Flowable):
		def __init__(self, *subs):
			self._subs = subs
			self.separation = 5*mm
			self.proportions = [1]*len(subs)

		def wrap(self, w, h):
			nsubs = len(self._subs)
			remaining = w-self.separation*nsubs
			proportionsSum = sum(self.proportions)
			self.sizes = [
				s.wrapOn(self.canv,remaining*p/proportionsSum, h)
				for s,p in zip(self._subs, self.proportions)
			]
			return w, max( h for _,h in self.sizes)

		def draw(self):
			x=0
			for s,(w,h) in zip(self._subs, self.sizes):
				s.drawOn(self.canv,x,0)
				x+=w + self.separation

	rows=31
	cols=2


	doc = SimpleDocTemplate(
		outputFilename,
		pagesize=A4,
		rightMargin=18*mm,
		leftMargin=18*mm,
		topMargin=18*mm,
		bottomMargin=18*mm,
		title="Mandato SEPA CORE",
		author='GuifiBaix',
#		showBoundary=1,
		)
	style=[
		('GRID', (0,0),(-1,-1),1, colors.lightgrey), # Debug aid
		]
	data = [[None for _ in range(cols)] for _ in range(rows)]
	line = 0


	style=[
		('SPAN', (0,line), (1,line)),
		]
	data[line][0]=r(texts.reference, client.mandate.reference)
	line+=1

	style+=[
		('SPAN', (0,line), (1,line), ),
		('BACKGROUND', (0,line), (1,line), colors.lightgrey),
		('BACKGROUND', (0,line), (1,line), colors.darkred),
		('TEXTCOLOR', (0,line), (1,line), colors.white),
	]
	data[line][0]=section(texts.creditorSection, size=-1)
	line+=1

	for label, content in [
		(texts.creditorid, creditorIdentifier(provider.nif, suffix=provider.suffix)),
		(texts.creditorName, provider.name),
		(texts.creditorAddress, provider.address),
		(texts.postalCodeCity,
			' - '.join((
				provider.postalcode,
				provider.city,
				))),
		(texts.stateCountry,
			' - '.join((
				provider.state,
				provider.get('country','Spain'),
				))),
		]:
		style+=[
			( 'LINEBELOW', (1,line), (1,line), 1, 'GREY'),
			( 'LINEBEFORE', (1,line), (1,line), 1, 'GREY'),
			]
		data[line][1]=p(content)
		data[line+1][1]=m(label)
		line+=2

	style+= [
		('SPAN', (0,line), (1,line), ),
		('BACKGROUND', (0,line), (1,line), colors.darkred),
	]
	data[line][0]=section(texts.debtorSection, size=-1)
	line+=1

	for label, content in [
		(texts.debtorName, client.name),
		(texts.debtorAddress, client.address),
		(texts.postalCodeCity,
			', '.join((
				client.postalcode,
				client.city,
				))),
		(texts.stateCountry,
			', '.join((
				client.state,
				client.get('country','Spain'),
				))),
		(texts.bic, client.bic),
		(texts.iban, '&nbsp;'.join(iban.prettyIban(client.iban))),
		]:
		style+=[
			( 'LINEBELOW', (1,line), (1,line), 1, 'GREY'),
			( 'LINEBEFORE', (1,line), (1,line), 1, 'GREY'),
			]
		data[line][1]=p(content)
		data[line+1][1]=m(label)
		line+=2


	data[line][1] = checkboxes(
		texts.recurrentPayment,
		texts.oneOffPayment,
		)
	line+=1

	style+= [
		('SPAN', (0,line), (1,line), ),
	]
	data[line][0] = smallMulti(texts.legalText)
	line+=1

	data[line][1] = p("A {} el {} de {}".format(
			provider.city,
			client.mandate.date.catalanDate,
			client.mandate.date.year,
			))
	line+=1

	style+= [
		('BOX', (1,line), (1,-1), 1, 'BLACK'),
		('SPAN', (1,line), (1,-1)),
	]
	data[line][1] = m(texts.signature)


	t=Table(
		data,
		style=style,
		colWidths=[
			.02*doc.width,
			.98*doc.width,
			],
		)

	def AllPageSetup(canvas, doc):
		canvas.saveState()
		canvas.scale(.5,.5)
		canvas.drawImage(
			appData('logo-guifibaix.png'),
			40*mm,
			canvas._pagesize[1]*2-70*mm)
		canvas.restoreState()
		if not doc.draft: return

		canvas.saveState()

		canvas.setFont("Helvetica", 100)
		canvas.setStrokeGray(0.90)
		canvas.setFillGray(0.90)
		canvas.rotate(45)
		canvas.drawCentredString(170 * mm, 10 * mm, 'ESBORRANY')

		canvas.restoreState()

	doc.draft = False

	content = [
		title('<b>'+texts.title.ca+'</b>'),
		title('<b><i>'+texts.title.es+'</i></b>'),
		title('<i>'+texts.title.en+'</i>'),
		title('&nbsp;'),
		t
		]

	doc.build(content, onFirstPage=AllPageSetup, onLaterPages=AllPageSetup)


def parseArguments():
	import argparse

	parser = argparse.ArgumentParser(
		description="Genera el mandat SEPA donades les dades del client.",
		)
	parser.add_argument(
		'client',
		nargs='?',
		metavar="DADESFACTURACIO.yaml",
		help="fitxer YAML amb la informació del client",
		)
	parser.add_argument(
		'-p',
		'--provider',
		dest='provider',
		metavar="PROVIDER.yaml",
		help="fitxer YAML amb la informació de facturació del proveidor, "
			"si no s'especifica es farà servir el proveidor per defecte",
		)

	parser.add_argument(
		'-g',
		'--guissona',
		action='store_true',
		help="fa servir la plantilla de la Caixa Guissona",
		)
	parser.add_argument(
		'-f',
		'--force',
		action='store_true',
		help="renova el mandat encara que ja hi hagi un al YAML",
		)

	return parser.parse_args()

def main():
	args = parseArguments()

	step("Llegint informació del creditor...")
	provider = ns.load(args.provider) if args.provider else currentCompany()

	if args.client:
		step("Llegint informacio del deutor de {}...".format(args.client))
		client = ns.load(args.client)
	else:
		warn("Fent servir dades de proves pel deutor...")
		client = dummyInvoice().client

	if 'mandate' not in client or args.force:
		mandate = ns()
		mandate.date = dateutils.Date(dateutils.Date.today())
		mandate.reference='GB2016-'+client.nif+'-'+mandate.date.compact
		mandate.sequence = 'RCUR'
		mandate.pending = True
		if args.client:
			warn("Afegint informació del mandat a '{}'"
				.format(args.client))
			with open(args.client, 'a') as f:
				f.write('\n'+ns(mandate=mandate).dump())
			client = ns.load(args.client)
		else:
			client.mandate = mandate

	client.iban = iban.compact(client.iban)

	provider.setdefault('country', 'Spain')
	client.setdefault('country', 'Spain')

	outputFilename = "{}-mandatsepa.pdf".format(
		client.expedient)
	step("Generant {}..".format(outputFilename))

	if args.guissona:
		sepaMandateGuissona(client, provider, outputFilename)
	else:
		sepaMandate(client, provider, outputFilename)


if __name__ == '__main__':
	main()

