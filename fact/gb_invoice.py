#!/usr/bin/env python

import click
import datetime
from yamlns import ns, dateutils
from consolemsg import step, success, fail, error
from pathlib import Path

from fact.invoiceprint import printInvoice
from fact.invoice import (
	dummyInvoice,
	filenameFromInvoice,
	availableInvoiceTemplates,
	defaultIssueDueDates,
)
from fact import tresoreria

def availableInvoiceTemplates():
	return [
		str(path.stem)
		for path in (Path('templates')/'factures').glob('*.yaml')
	]

@click.group()
def cli():
	"""Comandes per a processar factures"""

# Reused options
yaml_invoice_argument = click.argument(
	'yaml_invoice',
	metavar="FACTURA.yaml",
	type=Path,
)
yaml_invoices_argument = click.argument(
	'yaml_invoices',
	metavar="FACTURA.yaml",
	type=Path,
	nargs=-1,
	#multiple=True,
	#required=False,
)
optional_yaml_invoice_argument = click.argument(
	'yaml_invoice',
	metavar="FACTURA.yaml",
	type=Path,
	required=False,
)
pdf_output_argument = click.argument(
	'pdf_output',
	metavar="SORTIDA.pdf",
	required=False,
	type=Path,
)
provider_option = click.option(
	'--provider','-p',
	metavar='PROVIDER.yaml',
	type=Path,
	help="Yaml que substitueix l'emisor de la factura (provider)"
)
client_option = click.option(
	'--client','-c',
	metavar='CLIENT.yaml',
	type=Path,
	help="Yaml que substitueix al receptor de la factura (client)"
)
invoice_number_option = click.option(
	'-n',
	'invoice_number',
	type=int,
	help="Número de factura"
)
subject_option = click.option(
	'--subject','-s',
	metavar='SUBJECT',
	help="Assumpte pel correu d'enviament de la factura",
)
issue_date_option = click.option(
	'--issue',
	'issueDate',
	type=dateutils.date,
	metavar="YYYY-MM-DD",
	help="Força la data d'emisió",
)
due_date_options = click.option(
	'--due',
	'dueDate',
	type=dateutils.date,
	metavar="YYYY-MM-DD",
	help="Força la data de venciment",
)
draft_option = click.option(
	'--draft/--final',
	'draft',
	default=None,
	is_flag=True,
	help="Afegeix/treu una marca d'aigua al pdf, per indicar que es un esborrany"
)
tag_option = click.option(
	'--tag',
	metavar="TAG",
	help="Indica el tag de la factura (darrera part del nom del fitxer)",
)

templates_option = click.option(
	'--template','-t',
	'templates',
	metavar="template",
	type=click.Choice(availableInvoiceTemplates()),
	multiple=True,
	help="Indica el nom d'un template de factura, per afegir tags, subjects o conceptes",
)

debug_layout_option = click.option(
	'--debuglayout',
	is_flag=True,
	default=False,
	help="Mostra la graella de maquetació",
)
warn_existing_option = click.option(
	'--warn-existing',
	is_flag=True,
	default=False,
	help="Avisa quan s'ignori una factura ja registrada",
)
include_existing_option = click.option(
	'--include-existing',
	'include_existing',
	is_flag=True,
	default=False,
	help="No ignoris les factures ja registrades",
)
quiet_option = click.option(
	'--quiet',
	'-q',
	is_flag=True,
	default=False,
	help="Nomes mostra els errors",
)
new_cobraments_option = click.option(
	'--new',
	is_flag=True,
	default=False,
	help="Ignora el cobraments.yaml existent i comença de zero",
)
apply_option = click.option(
	'--apply',
	is_flag=True,
	default=False,
	help="Afegeix les entrades al cobraments.yaml",
)



@cli.command(
	short_help= "Crea un yaml per una factura nova.",
	help=(
		"Crea una nova factura."
	),
)
@optional_yaml_invoice_argument
@client_option
@provider_option
@invoice_number_option
@subject_option
@issue_date_option
@due_date_options
@tag_option
@templates_option
@draft_option
def new(
	yaml_invoice, invoice_number,
	subject, issueDate, dueDate, client, provider,
	tag, templates, draft,
):
	issueDate, dueDate = defaultIssueDueDates(issueDate, dueDate)

	invoice = ns(
		year = issueDate.year,
		number = 0,
		issueDate = issueDate,
		dueDate   = dueDate,
		vatRate = 0.21,
		draft = True if draft is None else draft,
		client = ns.load(client) if client else ns(
			expedient = "sjd-percebe13-at1a",
			name = "Perico Palotes",
			nif = "12345678F",
			address = "Rue del Percebe, 13",
			city = "Sant Joan Despí",
			postalcode = '08970',
			state = 'Barcelona',
			iban = "ES7714001234661234567890",
			bic = "ICROESMMXXX (INSTITUTO DE CREDITO OFICIAL)",
		),
		provider = ns.load(provider) if provider else tresoreria.currentCompany(),
	)
	template_dir = Path('templates')/'factures'
	subjects = []
	concepts = []
	tags = []
	for template in templates:
		template_file = template_dir / (template+".yaml")
		step(f"Carregant la plantilla {template_file}")
		if not template_file.exists():
			fail(f"No s'ha trobat la plantilla de factura '{template_file}'")
		template_content = template_file.read_text().format(**invoice)
		template_object = ns.loads(template_content)
		if 'subject' in template_object:
			subjects.append(template_object.subject)
		if 'concepts' in template_object:
			concepts.extend(template_object.concepts)
		if 'tag' in template_object:
			tags.append(template_object.tag)

	invoice.concepts = concepts or [
		'Manteniment trimestral Guifibaix',
		"Període 10 de juliol a 9 d'octubre\t 12337.50",
		"Descompte 10 de juliol a 5 d'agost\t -13.86",
		'Manteniment trimestral Guifibaix (atrassat)',
		"Període 10 d'abril a 9 de juliol\t 37.50",
	]
	invoice.subject = subject or ', '.join(subjects) or "Manteniment 3T-2014 sjd-percebe13"
	tag = tag if tag else ' '.join(tags).replace(' ', '_') if tags else None
	if not tag:
		fail("No heu especificat tags i els templates no en porten")

	if not yaml_invoice:
		yaml_invoice = filenameFromInvoice(invoice, tag=tag)
	step("Generating {} ...", yaml_invoice)
	invoice.dump(yaml_invoice)


@cli.command(
	'print', # because print is reserved
	short_help = "Converteix una factura yaml en pdf.",
	help=
		"Converteix un yaml de factura (FACTURA.yaml) "
		"en un pdf imprimible (SORTIDA.pdf). "
		"Si no s'indica sortida, es fara servir l'entrada "
		"amb l'extensió pdf."
		"\n\n"
		"Les opcions permeten considerar valors diferents "
		"als que hi ha al yaml per la generació, pero sense "
		"modificar el yaml original."
		,
)
@yaml_invoice_argument
@pdf_output_argument
@client_option
@provider_option
@invoice_number_option
@subject_option
@issue_date_option
@due_date_options
@draft_option
@debug_layout_option
def print_(
	yaml_invoice, pdf_output, invoice_number,
	subject, issueDate, dueDate, client, provider,
	draft, debuglayout,	
):
	invoice = ns.load(yaml_invoice)
	if invoice_number is not None:
		invoice.number = invoice_number
	if subject:
		invoice.subject = subject
	if client:
		invoice.client = ns.load(client)
	if provider:
		invoice.provider = ns.load(provider)
	if issueDate:
		invoice.issueDate = issueDate
	invoice.year = invoice.issueDate.year
	if dueDate:
		invoice.dueDate=dueDate
	if draft is not None:
		invoice.draft = draft

	pdf_output = pdf_output or Path(yaml_invoice).with_suffix('.pdf')
	step(f"Generating '{pdf_output}'...")
	printInvoice(invoice, str(pdf_output), debuglayout=debuglayout )



@cli.command(
	'open', # reseved word, so alias
	short_help="Obre la factura, deixant de ser un draft",
	help=(
		"Obre la factura FACTURA.yaml. Això implica que "
		"obtindra el següent número disponible per l'any, "
		"li treura la marca de esborrany si la té "
		"i la moura al directori de factures obertes."
	),
)
@yaml_invoice_argument
@issue_date_option
@due_date_options
def open_(
	yaml_invoice,
	issueDate,
	dueDate,
):
	step(f"Opening invoice '{yaml_invoice}'")
	step(f"  Llegint fitxer")
	invoice = ns.load(yaml_invoice)

	step(f"  Calculant la data")
	issueDate, dueDate = defaultIssueDueDates(issueDate, dueDate)
	invoice.year = issueDate.year
	invoice.issueDate = issueDate
	invoice.dueDate = dueDate
	success(f"    Issue: {issueDate}, Due: {dueDate}")

	step(f"  Assignant número")
	number = tresoreria.nextInvoiceNumber(invoice.year)
	if invoice.number:
		warn(
			f"La factura ja tenia el número {invoice.number} "
			f"però se li assignarà {number}"
		)
	invoice.number = int(number)
	success("    {}", number)

	step(f"  Treient-li l'estat d'esborrany")
	if not invoice.get('draft'):
		fail("La factura no està en esborrany")
	invoice.draft = False

	tag = yaml_invoice.stem.split("-")[-1] if '-' in yaml_invoice.stem else 'install'

	filename = Path('factures') / filenameFromInvoice(invoice, tag=tag)
	step(f"Generating '{filename}'...")
	invoice.dump(filename)


@cli.command(
	short_help = "Registra factures al cobraments.yaml",
	help=
		"Registra els cobraments pendents relacionats amb les factures indicades. "
		"La comanda genera, per la sortida estàndard, "
		"les entrades que cal afegir al cobraments.yaml. "
		"Si no s'especifiquen es consideren totes les que hi hagi a factures/. "
		"Per defecte, s'ignoren les factures ja registrades. "
		"Aquest registre serveix per generar les remeses dels cobraments pendents, "
		"i revisar el cobraments realitzats. "
		"\n\n"
		"Les opcions permeten considerar valors diferents "
		"als que hi ha al yaml per la generació, pero sense "
		"modificar el yaml original."
		,
)
@yaml_invoices_argument
@warn_existing_option
@include_existing_option
@new_cobraments_option
@apply_option
@quiet_option
def register(yaml_invoices, new, include_existing, warn_existing, quiet, apply):
	cobraments = [] if new else tresoreria.cobraments()
	if not yaml_invoices:
		yaml_invoices = Path('factures').glob('factura-*.yaml')

	processed = dict()
	full_output=[]

	for invoiceyaml in sorted(yaml_invoices) :
		invoice = ns.load(invoiceyaml)
		try:
			invoiceId = tresoreria.invoiceId(invoice)
		except ValueError:
			warn("Ignorant la factura sense número: "+invoiceyaml)
			continue
		prefix_pattern='factures/factura-YYYY-NNNN-'
		if str(invoiceyaml)[:len(prefix_pattern)] != f'factures/factura-{invoiceId}-':
			error(
				f"'{invoiceyaml}': No coincideix el nom amb les dades de dintre; "
				f"year: {invoice.year} number: {invoice.number}"
			)
			continue
		matching = [
			f
			for f in cobraments
			if f.factura == invoiceId
			]
		if matching and not include_existing:
			if warn_existing:
				warn('Ignorant factura ja registrada: '+invoiceyaml)
			continue
		try:
			int(invoice.number)
		except ValueError:
			warn('Ignorant factura sense número: '+invoiceyaml)
			continue

		if invoiceId in processed:
			error(
				f"{invoiceyaml} duplica l'id {invoiceId} "
				f"de {processed[invoiceId]}"
			)
			continue
		processed[invoiceId] = invoiceyaml

		quiet or step("Registrant Factura '{}'".format(invoiceyaml))
		concepte=str(invoiceyaml)[len(prefix_pattern):-len('.yaml')]
		totalAmount = tresoreria.invoiceTotal(invoice)
		output = (
			"- factura: {}\n"
			"  concepte: {}\n"
			"  valor: {}\n"
			"  pendent: {}\n"
			).format(
				invoiceId,
				concepte,
				totalAmount,
				totalAmount,
			)
			
		import datetime
		try:
			venciment= dateutils.date(invoice.dueDate)
		except:
			warn("No ha estat possible deduir la data de venciment de '{}'".format(invoice.dueDate))
			continue
		else:
			output += (
				"  venciments:\n"
				"  - data: {}\n"
				"    valor: {}\n"
				).format(
					venciment,
					totalAmount,
				)
		quiet or print(output)
		full_output.append(output)

	if apply and full_output:
		step("Afegint entrades al cobraments.yaml")
		with Path('cobraments.yaml').open('a') as cobramentsyaml:
			cobramentsyaml.write('\n'.join(['\n']+full_output))

def main():
	cli()


if __name__ == '__main__':
	import sys
	sys.exit(main())




